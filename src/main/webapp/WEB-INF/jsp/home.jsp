<%@ include file = "header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
</head>

<body>
<div class="container">
<h1>Welcome</h1>
Hello <b><span id="name"></span></b>
<br />
Your ID: <span id="id"></span>
<br />
<button class="btn btn-lg btn-primary btn-block" id="logout" name="Logout" value="Logout" type="Button">Logout</button>
</div>
</body>


<script>
function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}
function url_base64_decode(str) {
	var output = str.replace('-', '+').replace('_', '/');
	switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
	}
	var result = window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	return result;
}

$(document).ready(function () {
    var token = decodeURIComponent(url_base64_decode(getCookieValue('oauth2-access-token').split(".")[1]));
    
    var result = JSON.parse(token);
    console.log(result);
    $('#name').text(result.real_name);
    $('#id').text(result.user_name);
    
    $( "#logout" ).click(function() {
    	$( "#logout" ).text("Please wait..");
    	$.ajax({
    	    url: '/revoke-token',
    	    //url: 'http://localhost:8080/revoke-token',
    	    method: "GET",
    	    crossDomain: true,
    	    contentType: "application/x-www-form-urlencoded",
    	    data: '',
    	    cache: false,
    	    beforeSend: function (xhr) {
    	        /* Authorization header */
    	        
    	        //xhr.setRequestHeader("Authorization", 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsib2F1dGgyX2hpanIiXSwidXNlcl9uYW1lIjoiMTQ5OTg1MTEzMzI2NSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJyZWFsX25hbWUiOiJyYW1hZGhhbiBidWxhbiIsImV4cCI6MTUwNDgxNjQ3NCwianRpIjoiZTBmNWIwMDMtZTgyYS00ZTQ5LWJjMGMtNjQ0NzRjY2M5NGZjIiwicGljdHVyZSI6Imh0dHA6Ly9hcGkuaGlqci5jby5pZC9wcm9maWxlL3BpY3R1cmUvMTQ5OTg1MTEzMzI2NSIsImNsaWVudF9pZCI6ImhpanItYXBwIn0.QH0jNsBKTv0dgr4IuVVoXgmq2uJxDxr0voT92gAhC8QMsWOJqWVwhidHyWg_7oH1mIS-4JnQMi3hW3OKp3cnydfBeZGyTNhMNEsrqjm8hRSJgBoeHuXk5dOqawdwhAotyLPm1GfIdQ9XFMX0ev_E-2YfZyHUOJ4cgfEqEDctBgW90KfaKj7K3iFJKGHIrJViKvW_-EXXyePkiPDXu57MCZwIyixac0tCceXpiwjY4iF0koUC2LwsGQcjxHzSoCqfMGAQMK8G-eshpfgiIF8dnifv2KwVNHPMhtvwW_f3JiWKeoDud2ScUFWsHMuAJV7JnPFt3odsY3GJntWZ30lyIQ');
    	        xhr.setRequestHeader("Authorization", 'Bearer '+getCookieValue('oauth2-access-token'));
    	    },
    	    success: function (data) {
		console.log('success');
		//alert('OKE');
		
		Cookies.remove('oauth2-access-token',{domain: '.hijr.co.id', path: '/'});
		Cookies.remove('oauth2-refresh-token',{domain: '.hijr.co.id', path: '/'});;
    	    	location.href='/login'
    	    },
    	    error: function (jqXHR, textStatus, errorThrown) {
    	    	//$( "#logout" ).text("Logout");
    	    	Cookies.remove('oauth2-access-token',{domain: '.hijr.co.id', path: '/'});
		Cookies.remove('oauth2-refresh-token',{domain: '.hijr.co.id', path: '/'});
		alert(errorThrown);
		location.href='/login'
    	    }
    		
    	});	
    });
});
</script>
</html>