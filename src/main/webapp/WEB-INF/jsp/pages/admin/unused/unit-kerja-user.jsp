<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<div class="container">
		<div class="col-md-4">
	
			<div class="panel panel-success">
			  <!-- Default panel contents -->
			  <div class="panel-heading">
			  	<h3 class="panel-title">Unit Kerja Auditor</h3>
			  </div>
			  
			
			  <!-- List group -->
			  <ul class="list-group" id="listUnitKerja">
			    <li class="list-group-item">Loading...</li>		    
			  </ul>
			</div>
		
		</div>
		<div class="col-md-8">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Daftar User</h3>
						<div class="dropdown pull-right">
			            			<div class="btn-toolbar">
							
							<button id="btnAdd" class="btn btn-default pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						        <span class="glyphicon glyphicon-plus"></span>
					        </button>
			        			<ul id="list-add-position" class="dropdown-menu" aria-labelledby="btnAdd">
							    <!-- <li><a href="javascript:addUser(3)">Supervisor</a></li>
							    <li><a href="javascript:addUser(4)">Analis</a></li>
							    <li><a href="javascript:addUser(5)">Tata Usaha</a></li> -->
							  </ul>
							  </div>
			        		</div>
			        <div class="clearfix"></div>
				</div>
				<div class="panel-body">
					
					
					
					<br/>
					
					<div class="table-responsive">
						<table id="tbl" class="table">
						    <thead>
									<tr>
										<th width="20">No</th>
										<th width="150">Nama Lengkap</th>
										<th width="150">Email</th>
										<th width="80">Posisi</th>
										<th width="50">Action</th>
									</tr>
								</thead>
								
								<tbody>
								
								</tbody>
						 </table>
					</div>
						  	
							
					
				</div>
				
							
				
			</div>
		</div>
	</div>
	
	
	
	<!-- Modal Feature Begin -->
	<form id="frmInput" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalPengguna" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Daftar User</h4>
					
				</div>
				
				<div class="modal-body">
					 <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
						
			    		<div class="form-group hidden">
					  <label class="col-md-4">Pilih Posisi/Peran </label>
					      <div class="col-md-8">		      
								<select class="form-control input-sm" onchange="filterUser(this.value);" id="fPosition">
									<option value="" selected="selected">-- Pilih Posisi  --</option>
								</select>
					      	</div>
				    </div>
					
				    <div class="form-group">
					  <label class="col-md-4">Pilih User </label>
					      <div class="col-md-8">		      
								<select class="combobox form-control input-sm" id="fUser">
									
								</select>
					      	</div>
				    </div>
				    
				    
			    
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
	
	<!-- Modal Feature-->
	<div class="modal fade" data-backdrop="static" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
		<div class="modal-dialog">
			
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
					<div class="row">
						<div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
						</div>
					</div>
				</div>
				
				<div class="modal-body">		
					<div  id="progressBar1"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>		
					<div class="form-group">
						<p>Apakah anda yakin akan menghapus data pengguna ini?</p>
					</div>
					
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnRemove" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-danger">
						<span class="glyphicon glyphicon-trash"></span>  
						HAPUS</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	 
	 <script src="${pageContext.request.contextPath}/js/unit-kerja-user-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>