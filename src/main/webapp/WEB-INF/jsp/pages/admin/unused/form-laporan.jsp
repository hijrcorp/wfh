<!DOCTYPE html>
<html>

<head>
    <%@include file="inc/header.jsp"%>
</head>

<body>

    <div class="wrapper">

        <%@include file="inc/sidebar.jsp"%>

            <div id="content" class="pt-3">

                <%@include file="inc/navbar.jsp"%>

                    <h2>Laporan</h2>
                    <p>Laporan Aplikasi</p>
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="mt-2">Form Laporan</h5></div>
                                <div class="col-md-6">
                                    <button id="btnAdd" class="btn btn-info float-right" style="margin-right: 10px;">
                                        <svg class="svg-inline--fa fa-save fa-w-14" aria-hidden="true" data-prefix="fa" data-icon="save" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                            <path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"></path>
                                        </svg>
                                        <!-- <i class="fa fa-plus"></i> -->SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="progressBar" class="row" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            <span class="sr-only">45% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="msg" class="alert" role="alert" hidden="hidden" style="display: none;"></div>
                                </div>
                            </div>

                            <input type="hidden" id="id" value="0">

                            <div class="card mb-3">
                                <div class="card-body">

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>Tanggal Laporan</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control datepicker" placeholder="Tentukan tanggal Laporan">
                                                <div class="input-group-append">
                                                    <button class="btn btn-info" type="button" id="btn-tampilkan">Tampilkan</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            <h5 class="card-title">Laporan</h5>
                            <div class="card mb-3 ">
                                <div class="card-body ">
                                    <div class="row form-group ">
                                        <div class="col-md-6 ">
                                            <label>Status Sehat</label>
                                            <br>
                                            <div class="custom-control custom-radio custom-control-inline ">
                                                <input type="radio" id="customRadioInline1 " name="status_sehat_laporan " class="custom-control-input ">
                                                <label class="custom-control-label " for="customRadioInline1 ">SEHAT</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline ">
                                                <input type="radio" id="customRadioInline2 " name="status_sehat_laporan " class="custom-control-input ">
                                                <label class="custom-control-label " for="customRadioInline2 ">SAKIT</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <label>Kondisi Sakit</label>
                                            <select class="form-control" name="kondisi_sakit_laporan " disabled>
                                                <option value="0 ">-- Pilih Kondisi Sakit --</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="FormSakit" class="row form-group">
                                        <div class="col-md-12 ">
                                            <label>Keterangan Sakit</label>
                                            <input type="text" class="form-control" name="keterangan_sakit_laporan" placeholder="Ketik keterangan sakit" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 class="float-left py-2">Detail Pekerjaan</h5>
                            <button id="btn-tambah" type="button" class="btn btn-dark float-right my-2 "><i class="fas fa-fw fa-plus"></i> Tambah</button>

                            <table id="tbl-data" class="table table-sm table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="5%">Jenis</th>
                                        <th width="25%">Nama Pekerjaan</th>
                                        <th width="25%">Uraian Pekerjaan</th>
                                        <th width="25%">Output Pekerjaan</th>
                                        <th width="15%">Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="data-row " id="row-1543475744602 ">
                                        <td>1</td>
                                        <td class=" ">YES</td>
                                        <td class=" ">Ngoding</td>
                                        <td class=" ">Aplikasi WFH</td>
                                        <td class=" ">CRUD PEKERJAAN</td>
                                        <td class=" ">
                                            <a href="javascript:void(0) " class="btn btn-sm " type="button " onclick="doAction( '1543475744602', 'edit') ">
                                                <svg class="svg-inline--fa fa-pencil-alt fa-w-16 fa-fw " aria-hidden="true " data-prefix="fas " data-icon="pencil-alt " role="img " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 512 512 " data-fa-i2svg=" ">
                                                    <path fill="currentColor " d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z "></path>
                                                </svg>
                                                <!-- <i class="fas fa-fw fa-pencil-alt "></i> --></a>
                                            <a href="javascript:void(0) " class="btn btn-sm " type="button " onclick="doAction( '1543475744602', 'delete') ">
                                                <svg class="svg-inline--fa fa-trash fa-w-14 fa-fw " aria-hidden="true " data-prefix="fas " data-icon="trash " role="img " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 448 512 " data-fa-i2svg=" ">
                                                    <path fill="currentColor " d="M0 84V56c0-13.3 10.7-24 24-24h112l9.4-18.7c4-8.2 12.3-13.3 21.4-13.3h114.3c9.1 0 17.4 5.1 21.5 13.3L312 32h112c13.3 0 24 10.7 24 24v28c0 6.6-5.4 12-12 12H12C5.4 96 0 90.6 0 84zm415.2 56.7L394.8 467c-1.6 25.3-22.6 45-47.9 45H101.1c-25.3 0-46.3-19.7-47.9-45L32.8 140.7c-.4-6.9 5.1-12.7 12-12.7h358.5c6.8 0 12.3 5.8 11.9 12.7z "></path>
                                                </svg>
                                                <!-- <i class="fas fa-fw fa-trash "></i> --></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <script src="${pageContext.request.contextPath}/js/user-script.js"></script>
                    <%@include file="inc/footer.jsp"%>

</body>

</html>