<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<style>
	tbody tr:nth-child(even) {
	  background-color: white;
	}
	
	.table>tbody>tr.active>td, .table>tbody>tr.active>th, .table>tbody>tr>td.active, .table>tbody>tr>th.active, .table>tfoot>tr.active>td, .table>tfoot>tr.active>th, .table>tfoot>tr>td.active, .table>tfoot>tr>th.active, .table>thead>tr.active>td, .table>thead>tr.active>th, .table>thead>tr>td.active, .table>thead>tr>th.active {
	        color: #fff;
	    background-color: #5cb85c;
	    border-color: #4cae4c;
	  font-weight: bold;
	}
	textarea {
	    resize: none;
	}
	  .panel-heading a:after {
	    font-family:'Glyphicons Halflings';
	    content:"\e114";
	    float: right;
	    color: #3c763d;
	    background-color: #dff0d8;
	    border-color: #d6e9c6;
	}
	.panel-heading a.collapsed:after {
	    content:"\e080";
	}
	.selectric-items {
		border: 1px solid #2196f3;
		background : #fff;
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
		font-weight: normal;
		 color: #444;
		
	}
	.selectric-items li {
		
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
		font-weight: normal;
		 color: #444;
		
	}
	.selectric-items li:hover {
		background : #2196f3;
		color : #fff
	}
	.selectric-items li.highlighted {
		background : #2196f3;
		color : #fff
	}
	.selectric .label {
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
		font-size: 14px;
		font-weight: normal;
		margin: 0 0px 0 0px; */
	}
	.selectric {
		background : #fff;
	    border-radius: 4px;
	}
	.label {
		text-align : left;
		background  : #fff;
	};
	.select2-container  {
		margin-top: 453px;
	    position: fixed;
	}
	</style>
	
    <style>
      #tab .nav-pills > li > a {
        border-radius: 4px 4px 0 0 ;
      }
      
      #tab .nav-pills > li > a {
        border-radius: 4px 4px 0 0;
        background-color: #f4f4f4;
        border: 1px solid #d6e9c6;
        border-bottom: 0px;
        margin-bottom: -1px;
      }
      
      #tab .nav-pills>li.active>a, #tab .nav-pills>li.active>a:focus, #tab .nav-pills>li.active>a:hover {
        background-color: #feb372 !important;
      }
      
      #tab .tab-content {
        border: 1px solid #d6e9c6;
        padding : 20px 15px;
      }
    </style>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	<br />
	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">Satker : <span id="label_satker"></span></h3>
		        
		        <div class="clearfix"></div>
			</div>
			<div class="panel-body">
		 	<div class="col-md-12">
              <div id="tab"> 
              
                <ul id="tab-menu" class="nav nav-pills">
                    <!-- <li class="active"><a  href="#tab-1" data-toggle="tab">Temuan (<span id="jumlah-temuan">0</span>)</a></li>
                    <li><a href="#tab-2" data-toggle="tab">Sebab (<span id="jumlah-sebab">0</span>)</a></li>
                    <li><a href="#tab-3" data-toggle="tab">Akibat (<span id="jumlah-akibat">0</span>)</a></li>
                    <li><a href="#tab-4" data-toggle="tab">Rekomendasi (<span id="jumlah-rekomendasi">0</span>)</a></li> -->
                </ul>
              
                <div id="tab-content" class="tab-content">
					<div class="row tab-pane active" id="tab-1">
						<div class="col-md-12">
						<div class="panel panel-success">
							<div class="panel-body bg-success"><p>Kemampuan tim audit dalam memahami dalam mengevaluasi keterkaitan antara kegiatan pada Satuan Kerja/Unit Pelaksana Teknis dengan target kinerja Eselon I-nya(1561895686876625)</p></div>
							<canvas id="myChart">1</canvas>
						</div>
						</div>
					</div>
               	</div>
               	
               </div>
            </div>
		  	</div>
			
		</div>
	</div>
	
	 
	<script src="${pageContext.request.contextPath}/js/admin-view-detil-index-script.js"></script>
	
	<%@include file="inc/footer.jsp"%>

</body>

</html>