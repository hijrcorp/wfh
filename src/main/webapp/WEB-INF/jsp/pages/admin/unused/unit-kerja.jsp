<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	<div class="container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">Daftar Unit Kerja</h3>
	            <div class="btn-toolbar">
					<button id="btnRefresh" data-id="1" class="btn btn-default pull-right">
				        <span class="glyphicon glyphicon-refresh"></span>
			        </button>
					<!-- <button id="btnExport" class="btn btn-default pull-right">
				        <span class="glyphicon glyphicon-file"></span> Download
			        </button> -->
					<button id="btnAdd" class="btn btn-default pull-right">
				        <span class="glyphicon glyphicon-plus"></span> Tambah
			        </button>
	        	</div>
		        <div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="row">
				  <div class="col-md-12">
				    <div class="input-group">
				      <div class="input-group-btn">
				        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="column" data-id="">Kriteria Pencarian</span> <span class="caret"></span></button>
				        <ul class="dropdown-menu">
				          <!-- <li><a href="#" onclick="changeFilter('filter_keyword','Nama Unit Kerja')">Nama Unit Kerja</a></li> -->
				          <li><a href="#" onclick="changeFilter('filter_kode','Kode Unit Kerja')">Kode Unit Kerja</a></li>
				          <li><a href="#" onclick="changeFilter('filter_nama','Nama Unit Kerja')">Nama Unit Kerja</a></li>
				          <li><a href="#" onclick="changeFilter('filter_alamat','Alamat')">Alamat</a></li>
				          <li><a href="#" onclick="changeFilter('filter_telepon','Telepon')">Telepon</a></li>
				          <li role="separator" class="divider"></li>
          				  <li><a href="#" onclick="changeFilter('','')">Reset Pencarian</a></li>
				        </ul>
				      </div>
				      <input id="txtsearch" type="text" class="form-control" aria-label="..." placeholder="Ketik kata kunci yang ingin anda cari">
				      <span class="input-group-btn">
				        <button id="btnSearch" onclick="search()" type="button" class="btn btn-default">
							<span class="glyphicon glyphicon-search"></span> Cari
						</button>
				      </span>
				    </div>
				  </div>
				</div>
				
				<br/>
				
				<div class="table-responsive">
					<table id="tbl" class="table">
					    <thead>
							<tr>
								<th width="20">No</th>
								<th width="130">Kode</th>
								<th width="250">Nama</th>
								<th width="150">Alamat</th>
								<th width="80">Telepon</th>
								<th width="100">Action</th>
							</tr>
						</thead>
						
						<tbody></tbody>
					</table>
				</div>
				<nav>
					<ul id="tblpages" class="pagination"></ul>
				</nav>
			</div>		
		</div>
	</div>	
	
	<!-- Modal Feature Begin -->
	<form id="frmInput" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Form Unit Kerja</h4>
					
				</div>
				
				<div class="modal-body">
					 <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
					
					<p class="text-danger">Input dengan tanda (*) wajib diisi.</p>
					
					<div class="form-group">
				      <div class="col-md-12">
				      	<label>Kode Unit Kerja *</label>
			    			<input autocomplete="off" type="text" class="form-control" name="kode" placeholder="Ketik Kode Unit Kerja">
				      </div>
				      
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-12">
				      	<label>Nama Unit Kerja *</label>
			    			<input autocomplete="off" type="text" class="form-control" name="nama" placeholder="Ketik Nama Unit Kerja">
				      </div>
				      
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-12">
				      	<label>Alamat</label>
			    			<input autocomplete="off" type="text" class="form-control" name="alamat" placeholder="Ketik Alamat">
				      </div>
				      
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-12">
				      	<label>Telepon</label>
			    			<input autocomplete="off" type="text" class="form-control" name="telepon" placeholder="Ketik Telepon">
				      </div>
				      
				    </div>
				 
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnReset" type="button" class="btn btn-default" aria-hidden="true">RESET</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
	 
	 <script src="${pageContext.request.contextPath}/js/unit-kerja-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>