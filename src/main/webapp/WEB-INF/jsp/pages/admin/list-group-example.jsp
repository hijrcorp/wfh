<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <%@include file="inc/sidebar.jsp"%>

        <!-- Page Content  -->
        <div id="content" class="pt-3">

            <%@include file="inc/navbar.jsp"%>

            <h2>Title</h2>
            <p>Sub title.</p>
			
			<div class="list-group">
			  <a href="#" class="list-group-item list-group-item-action active">
			    Cras justo odio
			  </a>
			  <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
			  <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
			  <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
			  <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1" aria-disabled="true">Vestibulum at eros</a>
			</div>
			
			<div class="line"></div>
			
			<h2>Title</h2>
            <p>Sub title.</p>
			
			<div class="card">
			  <div class="card-header">
			    Featured
			  </div>
			  <div class="card-body">
			    <h5 class="card-title">Special title treatment</h5>
			    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
			    <div class="list-group">
				  <a href="#" class="list-group-item list-group-item-action active">
				    Cras justo odio
				  </a>
				  <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
				  <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
				  <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
				  <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1" aria-disabled="true">Vestibulum at eros</a>
				</div>
			  </div>
			</div>
        </div>
   </div>

   <%@include file="inc/footer.jsp"%>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
</body>

</html>