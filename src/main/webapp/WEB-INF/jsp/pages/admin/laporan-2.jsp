<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

  <div class="wrapper">
  
    <%@include file="inc/sidebar.jsp"%>
    
      <div id="content" class="pt-3">
      
      <%@include file="inc/navbar.jsp"%>
      
      <h2>Laporan Kesehatan Periodik</h2>
      <!-- <p>Daftar Perkembangan Kinerja Pegawai</p> -->
      <div class="line"></div>
      
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6"><h5 class="mt-2">Daftar Perkembangan Kesehatan Pegawai</h5></div>
            <div class="col-md-6">
              <button id="btnDownload" class="btn btn-info float-right" style="margin-right: 10px;"><i class="fa fa-download"></i> DOWNLOAD</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form id="form-filter">
          	<div class="card mb-3">
          		<div class="card-body">
		          	<div class="row mb-3">
					      	<div class="col-md-2"><label>Unit Kerja</label></div>
									<div class="col-md-7">
				            <select class="form-control" name="id_unit_kerja">
			                <option value="">-- Semua Unit Kerja --</option>
				            </select>
				    			</div>
		            </div>
		          	<div class="row mb-3">
					      	<div class="col-md-2"><label>Periode</label></div>
									<div class="col-md-3">
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="periode_awal" autocomplete="off" readonly style="background: white;">
				              <span class="input-group-prepend">
				                <button type="button" type="button" class="btn btn-info"><i class="fa fa-calendar"></i></button>
				              </span>
				            </div>
			            </div>
									<div class="col-md-1"><label>Sampai</label></div>
									<div class="col-md-3">
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="periode_akhir" autocomplete="off" readonly style="background: white;">
				              <span class="input-group-prepend">
				                <button type="button" type="button" class="btn btn-info"><i class="fa fa-calendar"></i></button>
				              </span>
				            </div>
									</div>
		            </div>
		          	<div class="row">
					      	<div class="col-md-2"></div>
									<div class="col-md-5">
										<button id="btnSearch" type="button" onclick="search()" class="btn btn-info"><i class="fa fa-search"></i> Tampil</button>
										<button id="btnReset" type="button" onclick="refresh()" class="btn btn-info"><i class="fa fa-sync"></i> Reset</button>
										<!-- <button id="btnDownload" class="btn btn-info" style="margin-right: 10px;"><i class="fa fa-download"></i> DOWNLOAD</button> -->
									</div>
		            </div>
	            </div>
            </div>
          </form>
          <div class="table-responsive p-0" style="overflow-y: scroll;max-height: 500px;height: max-content;">
          <table id="tbl" class="table table-bordered text-center mt-15">
            <thead class="thead-light">
              <tr>
                <th width="20"  rowspan="3">No.</th>
                <!-- <th width="150" rowspan="3">Unit Kerja</th> -->
                <th width="150" rowspan="3">Tanggal</th>
                <th width="100" colspan="4">Pegawai</th>
                <th width="100" colspan="2">Sehat</th>
                <th width="50"  colspan="12">Sakit</th>
              </tr>
              <tr>
                <th rowspan="2">Jumlah</th>
                <th colspan="2">Melapor</th>
                <th rowspan="2">%</th>
                <th rowspan="2">Jumlah</th>
                <th rowspan="2">%</th>
                <th rowspan="2">Jumlah</th>
                <th rowspan="2">%</th>
                <th colspan="2">Demam</th>
                <th colspan="2">Batuk</th>
                <th colspan="2">Pilek</th>
                <th colspan="2">Tenggorokan</th>
                <th colspan="2">Sesak Nafas</th>
              </tr>
              <tr>
              	<th>Ya</th>
              	<th>Tidak</th>
                <th>Jml</th>
                <th>%</th>
                <th>Jml</th>
                <th>%</th>
                <th>Jml</th>
                <th>%</th>
                <th>Jml</th>
                <th>%</th>
                <th>Jml</th>
                <th>%</th>
              </tr>
            </thead>
            <tbody></tbody>
            <tfoot></tfoot>
          </table>
          </div>
          <nav><ul id="tblpages" class="pagination"></ul></nav>
        </div>
      </div>
  	
	
	<script src="${pageContext.request.contextPath}/js/laporan-2-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>