<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <%@include file="inc/sidebar.jsp"%>

        <!-- Page Content  -->
        <div id="content" class="pt-3">

            <%@include file="inc/navbar.jsp"%>

            <h2>Dashboard Monitoring Pelaporan</h2>
      			<p>Monitoring Penyampaian Laporan Kinerja dan Kesehatan Harian</p>
            <!-- <div class="line"></div> -->
       
       
       <div class="card mb-3">
			    <div class="card-header bg-white">
		        <div class="row justify-content-between">
	            <div class="col-md-7">
                <h5 class="mt-2">Monitoring Pelaporan</h5>
                
             	</div>
	            <div class="col-md-5">
	            	<div class="row mt-2">
	                <div class="col-md-4"><h5 class="mt-1">Tanggal:</h5></div>
	                <div class="col-md-8">
		                <div class="input-group">
											<input type="text" class="form-control datepicker" name="tanggal" autocomplete="off" readonly style="background: white">
				              <span class="input-group-prepend">
				                <!-- <button type="button" type="button" class="btn btn-info"><i class="fa fa-calendar"></i></button> -->
				                <button id="btnDownload" class="btn btn-info" style="margin-right: 10px;"><i class="fa fa-download"></i> DOWNLOAD</button>
				              </span>
				            </div>
				             
			            </div>
		            </div>
	            </div>
		        </div>
			    </div>
			    <div class="card-body">
			        <!-- <h5 class="card-title">Special title treatment</h5>
						 <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
			        <div class="table-responsive p-0" style="overflow-y: scroll;max-height: 500px;height: max-content;">
			            <table id="tbl" class="table table-bordered table-sm table-hover text-center">
			                <thead class="thead-light">
			                    <tr>
			                        <th rowspan="2">No</th>
			                        <th rowspan="2" width="600">Unit Kerja</th>
			                        <th rowspan="2" width="200">Jumlah Pegawai</th>
			                        <th colspan="3" width="400">Kinerja</th>
			                        <th colspan="3" width="400">Kesehatan</th>
			                    </tr>
			                    <tr>
			                    		<th>Melapor</th>
			                    		<th>Tidak</th>
			                    		<th>%</th>
			                    		<th>Melapor</th>
			                    		<th>Tidak</th>
			                    		<th>%</th>
			                    </tr>
			                </thead>
			                <tbody>
			                </tbody>
			                <tfoot>
			                </tfoot>
			            </table>
			        </div>
			        
			        <div class="row mt-3 mb-5 justify-content-center">
			        	<div class="col-md-1"></div>
			        	<div class="col-md-4">
			        		<canvas id="chart-kinerja" width="550" height="550"></canvas>
			        	</div>
			        	<div class="col-md-2"></div>
			        	<div class="col-md-4">
			        		<canvas id="chart-kesehatan" width="550" height="550"></canvas>
			        	</div>
			        	<div class="col-md-1"></div>
			        </div>
			        
			    </div>
			</div>

   </div>

	<%@include file="inc/footer.jsp"%>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/dashboard-1-script.js"></script>

</body>

</html>