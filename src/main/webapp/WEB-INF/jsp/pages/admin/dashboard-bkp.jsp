<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <%@include file="inc/sidebar.jsp"%>

        <!-- Page Content  -->
        <div id="content" class="pt-3">

            <%@include file="inc/navbar.jsp"%>

            <h2>Dashboard</h2>
            <div class="line"></div>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->
            <div class="card mb-3">
			    <div class="card-header bg-white">
			        <div class="row justify-content-between">
			            <div class="col-md-6">
			                <h5 class="mt-2">Monitoring Ticket</h5></div>
			            <div class="col-md-auto ">
			                <select class="custom-select custom-select-sm text-right">
			                    <option value="week" selected="selected">Last 7 days</option>
			                    <option value="month">Last month</option>
			                    <option value="year">Last year</option>
			                </select>
			            </div>
			        </div>
			    </div>
			    <div class="card-body">
			        <!-- <h5 class="card-title">Special title treatment</h5>
						 <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
			        <div class="table-responsive p-0" style="overflow-y: scroll;max-height: 500px;height: max-content;">
			            <table id="tbl" class="table table-sm table-hover text-center">
			                <thead class="thead-light">
			                    <tr>
			                        <th scope="col"></th>
			                        <th scope="col" class="text-left">Ticket</th>
			                        <th scope="col">SUBMIT</th>
			                        <th scope="col">REVIU</th>
			
			                        <th scope="col">PROSES</th>
			                        <th scope="col">APPROVAL</th>
			                        <th scope="col">SELESAI</th>
			                        <th scope="col"></th>
			
			                    </tr>
			                </thead>
			                <tbody>
			                    <tr class="data-row small" id="row-1576462216492507">
			                        <td></td>
			                        <td class="text-left">25</td>
			                        <td>10</td>
			                        <td>5</td>
			                        <td>4</td>
			                        <td><span class="text-success">3</span></td>
			                        <td>3</td>
			                        <td class="text-center"> </td>
			                    </tr>
			                </tbody>
			            </table>
			        </div>
			        
			        <div class="row justify-content-center">
			        	<div class="col-md-6">
			        		<canvas id="chartTicket" width="550" height="550"></canvas>
			        	</div>
			        </div>
			        
			    </div>
			</div>
			
			<div class="card">
			    <div class="card-header bg-white">
			        <div class="row justify-content-between">
			            <div class="col-md-6">
			                <h5 class="mt-2">Monitoring Tim</h5></div>
			            <div class="col-md-auto ">
			                <select class="custom-select custom-select-sm text-right">
			                    <option value="week" selected="selected">Last 7 days</option>
			                    <option value="month">Last month</option>
			                    <option value="year">Last year</option>
			                </select>
			            </div>
			        </div>
			    </div>
			    <div class="card-body">
			        <!-- <h5 class="card-title">Special title treatment</h5>
						    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
			        <div class="table-responsive p-0" style="overflow-y: scroll;max-height: 500px;height: max-content;">
			            <table id="tbl" class="table table-sm table-hover text-center">
			                <thead class="thead-light">
			                    <tr>
			                        <th scope="col"></th>
			                        <th scope="col" class="text-left">TIM</th>
			                        <th scope="col">Penanya</th>
			                        <th scope="col">SUBMIT</th>
			
			                        <th scope="col">REVIU</th>
			                        <th scope="col">PROSES</th>
			                        <th scope="col">APPROVAL</th>
			                        <th scope="col">SELESAI</th>
			                        <th scope="col"></th>
			
			                    </tr>
			                </thead>
			                <tbody>
			                    <tr class="data-row" id="row-1576164912634163">
			                        <td></td>
			                        <td class="text-left">Tim Wilayah I</td>
			                        <td>10</td>
			                        <td>2</td>
			                        <td>2</td>
			                        <td>2</td>
			                        <td>2</td>
			                        <td>2</td>
			                    </tr>
			                    <tr class="data-row" id="row-1576164966074239">
			                        <td></td>
			                        <td class="text-left">Tim Wilayah II</td>
			                        <td>15</td>
			                        <td>4</td>
			                        <td>2</td>
			                        <td>2</td>
			                        <td>2</td>
			
			                        <td>0</td>
			                    </tr>
			                    <tr class="data-row" id="row-1576165020251671">
			                        <td></td>
			                        <td class="text-left">Tim Wilayah III</td>
			                        <td>20</td>
			                        <td>5</td>
			                        <td>3</td>
			                        <td>1</td>
			                        <td>4</td>
			                        <td>8</td>
			                    </tr>
			                    <tr class="data-row" id="row-1576177156784605">
			                        <td></td>
			                        <td class="text-left">Tim Wilayah IV</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                    </tr>
			                    <tr class="data-row" id="row-1576387535322518">
			                        <td></td>
			                        <td class="text-left">Tim Analis LHP</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                    </tr>
			                    <tr class="data-row" id="row-1576387677629354">
			                        <td></td>
			                        <td class="text-left">Tim Tindak Lanjut</td>
			                        <td>x</td>
			
			                        <td>x</td>
			                        <td>x</td>
			
			                        <td>x</td>
			                        <td>x</td>
			                        <td>x</td>
			                    </tr>
			                </tbody>
			            </table>
			        </div>
			        
			        
			        <div class="row justify-content-center">
			        	<div class="col-md-7">
			        		<canvas id="chartTim" width="550" height="550"></canvas>
			        	</div>
			        </div>
			    </div>
			</div>

            <div class="line"></div>

            <!-- <h2>Lorem Ipsum Dolor</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <div class="line"></div>

            <h2>Lorem Ipsum Dolor</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <div class="line"></div>

            <h3>Lorem Ipsum Dolor</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->
        </div>
   </div>

   <%@include file="inc/footer.jsp"%>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
            renderChart('ticket');
            renderChart('tim');
        });
        
        function renderChart(type){
        	/*data = {
        		    datasets: [{
        		        data: [10, 20, 30]
        		    }],

        		    // These labels appear in the legend and in the tooltips when hovering different arcs
        		    labels: [
        		        'Red',
        		        'Yellow',
        		        'Blue'
        		    ]
        		};
        	
        	var myDoughnutChart = new Chart(ctx, {
        	    type: 'doughnut',
        	    data: data,
        	    options: options
        	});*/
        	if(type=="ticket"){
	        	var ctx = document.getElementById('chartTicket');
	        	var myChart = new Chart(ctx, {
	        	    type: 'doughnut',
	        	    data: {
	        	        labels: ['SUBMIT', 'REVIU', 'PROSES', 'APPROVAL', 'SELESAI'],
	        	        datasets: [{
	        	            label: '# of Votes',
	        	            data: [10, 5, 4, 3, 3],
	        	            backgroundColor: [
	        	                'rgba(54, 162, 235, 0.2)',
	        	                'rgba(255, 159, 64, 0.2)',
	        	                'rgba(255, 206, 86, 0.2)',
	        	                'rgba(153, 102, 255, 0.2)',
	        	                'rgba(75, 192, 192, 0.2)',
	        	            ],
	        	            borderColor: [
	        	                'rgba(54, 162, 235, 1)',
	        	                'rgba(255, 159, 64, 1)',
	        	                'rgba(255, 206, 86, 1)',
	        	                'rgba(153, 102, 255, 1)',
	        	                'rgba(75, 192, 192, 1)',
	        	            ],
	        	            borderWidth: 1
	        	        }]
	        	    },
	        	    options: {
	        	        scales: {
	        	            yAxes: [{
	        	                ticks: {
	        	                    beginAtZero: true
	        	                }
	        	            }]
	        	        }
	        	    }
	        	});
        	}
        	
        	if(type=="tim"){
	        	var ctx = document.getElementById('chartTim');
	        	var myChart = new Chart(ctx, {
	        	    type: 'doughnut',
	        	    data: {
	        	        labels: ['SUBMIT', 'REVIU', 'PROSES', 'APPROVAL', 'SELESAI'],
	        	        datasets: [{
	        	            label: '# of Votes',
	        	            data: [10, 5, 4, 3, 3],
	        	            backgroundColor: [
	        	                'rgba(54, 162, 235, 0.2)',
	        	                'rgba(255, 159, 64, 0.2)',
	        	                'rgba(255, 206, 86, 0.2)',
	        	                'rgba(153, 102, 255, 0.2)',
	        	                'rgba(75, 192, 192, 0.2)',
	        	            ],
	        	            borderColor: [
	        	                'rgba(54, 162, 235, 1)',
	        	                'rgba(255, 159, 64, 1)',
	        	                'rgba(255, 206, 86, 1)',
	        	                'rgba(153, 102, 255, 1)',
	        	                'rgba(75, 192, 192, 1)',
	        	            ],
	        	            borderWidth: 1
	        	        }]
	        	    },
	        	    options: {
	        	        scales: {
	        	            yAxes: [{
	        	                ticks: {
	        	                    beginAtZero: true
	        	                }
	        	            }]
	        	        }
	        	    }
	        	});
        	}
        }
    </script>
</body>

</html>