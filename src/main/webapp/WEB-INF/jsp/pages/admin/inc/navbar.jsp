<%@page import="java.util.List"%>

<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <div class="container-fluid">

        <h1 class="my-0 mr-md-auto font-weight-normal navbar-brand text-muted"><img src="${pageContext.request.contextPath}/images/logo-menlhk.png" width="38px" /> Laporan Kegiatan Harian Pegawai</h1>
        
				

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
				
        <% if(((List)request.getAttribute("roleList")).contains("ROLE_USER")) { %>
            <ul class="nav navbar-nav ml-auto">
            		<% if(request.getAttribute("position").equals("Kepala")) { %>
            			<li class="nav-item dropdown pr-4"><a href="${pageContext.request.contextPath}/page/" class="btn btn-info pull-right">Form Input</a></li>
            		<% } %>
                <li class="nav-item dropdown pr-4">
				    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				      <%= request.getAttribute("realName") %>
				    </a>
				    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
				      <a class="dropdown-item" href="#" onclick="showChangePassword()">Ganti Password</a>
				      <div class="dropdown-divider"></div>
				      <a class="dropdown-item" href="${pageContext.request.contextPath}/login-logout">Keluar</a>
				    </div>
				</li>
            </ul>
        <% } %>    
        </div>
    </div>
</nav>