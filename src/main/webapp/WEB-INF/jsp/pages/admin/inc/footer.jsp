    
  <!-- Modal Feature Begin -->
  <form id="frmVerifyThenDownload" class="form-horizontal">
    <div class="modal fade" data-backdrop="static" id="modalVerifyThenDownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Warning</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="col-md-12">
                <div id="verifyThenDownloadMsg"></div>
                <div>Silahkan lengkapi <a onclick="showUpdateProfile()" style="cursor: pointer;">disini</a>.</div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">CLOSE</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  
  <!-- Modal Feature Begin -->
  <form id="frmUpdateProfile" class="form-horizontal">
    <div class="modal fade" data-backdrop="static" id="modalUpdateProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Update Profile</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div id="updateProfileProgress" class="row">
              <div class="col-xs-12">
                <div class="progress">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">45% Complete</span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-md-12">
                <div id="updateProfileMsg" class="alert alert-success" role="alert" hidden="hidden"></div>
              </div>
            </div>
            
            <input autocomplete="off" type="hidden" name="id" >
		    
		    <div class="row form-group">
		        <div class="col-md-6">
		            <label>NIP Pegawai </label>
		            <input autocomplete="off" type="text" class="form-control" id="me_nip_pegawai" placeholder="" readonly="">
		        </div>
		        <div class="col-md-6">
		            <label>Nama Lengkap </label>
		            <input autocomplete="off" type="text" class="form-control" id="me_nama_lengkap" name="lastName" placeholder="Ketik nama lengkap anda">
		        </div>
		    </div>
		    <input autocomplete="off" type="hidden" id="me_birth_date" name="birthDate" placeholder="Tentukan Tgl.Lahir" value="">
		    <div class="row form-group">
		        <div class="col-md">
		            <label>Tanggal Lahir</label>
		            <select class="form-control" id="me_hari">
		                <option value="">Pilih Hari</option>
		                <option value="01">01</option>
		                <option value="02">02</option>
		                <option value="03">03</option>
		                <option value="04">04</option>
		                <option value="05">05</option>
		                <option value="06">06</option>
		                <option value="07">07</option>
		                <option value="08">08</option>
		                <option value="09">09</option>
		                <option value="10">10</option>
		                <option value="11">11</option>
		                <option value="12">12</option>
		                <option value="13">13</option>
		                <option value="14">14</option>
		                <option value="15">15</option>
		                <option value="16">16</option>
		                <option value="17">17</option>
		                <option value="18">18</option>
		                <option value="19">19</option>
		                <option value="20">20</option>
		                <option value="21">21</option>
		                <option value="22">22</option>
		                <option value="23">23</option>
		                <option value="24">24</option>
		                <option value="25">25</option>
		                <option value="26">26</option>
		                <option value="27">27</option>
		                <option value="28">28</option>
		                <option value="29">29</option>
		                <option value="30">30</option>
		                <option value="31">31</option>
		            </select>
		        </div>
		        <div class="col-md">
		            <label>&nbsp;</label>
		            <select class="form-control" id="me_bulan">
		                <option value="">Pilih Bulan</option>
		                <option value="01">January</option>
		                <option value="02">February</option>
		                <option value="03">March</option>
		                <option value="04">April</option>
		                <option value="05">May</option>
		                <option value="06">June</option>
		                <option value="07">July</option>
		                <option value="08">August</option>
		                <option value="09">September</option>
		                <option value="10">October</option>
		                <option value="11">November</option>
		                <option value="12">December</option>
		            </select>
		        </div>
		        <div class="col-md">
		            <label>&nbsp;</label>
		            <select class="form-control" id="me_tahun">
		                <option value="">Pilih Tahun</option>
		                <option value="2010">2010</option>
		                <option value="2009">2009</option>
		                <option value="2008">2008</option>
		                <option value="2007">2007</option>
		                <option value="2006">2006</option>
		                <option value="2005">2005</option>
		                <option value="2004">2004</option>
		                <option value="2003">2003</option>
		                <option value="2002">2002</option>
		                <option value="2001">2001</option>
		                <option value="2000">2000</option>
		                <option value="1999">1999</option>
		                <option value="1998">1998</option>
		                <option value="1997">1997</option>
		                <option value="1996">1996</option>
		                <option value="1995">1995</option>
		                <option value="1994">1994</option>
		                <option value="1993">1993</option>
		                <option value="1992">1992</option>
		                <option value="1991">1991</option>
		                <option value="1990">1990</option>
		                <option value="1989">1989</option>
		                <option value="1988">1988</option>
		                <option value="1987">1987</option>
		                <option value="1986">1986</option>
		                <option value="1985">1985</option>
		                <option value="1984">1984</option>
		                <option value="1983">1983</option>
		                <option value="1982">1982</option>
		                <option value="1981">1981</option>
		                <option value="1980">1980</option>
		                <option value="1979">1979</option>
		                <option value="1978">1978</option>
		                <option value="1977">1977</option>
		                <option value="1976">1976</option>
		                <option value="1975">1975</option>
		                <option value="1974">1974</option>
		                <option value="1973">1973</option>
		                <option value="1972">1972</option>
		                <option value="1971">1971</option>
		                <option value="1970">1970</option>
		                <option value="1969">1969</option>
		                <option value="1968">1968</option>
		                <option value="1967">1967</option>
		                <option value="1966">1966</option>
		                <option value="1965">1965</option>
		                <option value="1964">1964</option>
		                <option value="1963">1963</option>
		                <option value="1962">1962</option>
		                <option value="1961">1961</option>
		                <option value="1960">1960</option>
		                <option value="1959">1959</option>
		                <option value="1958">1958</option>
		                <option value="1957">1957</option>
		                <option value="1956">1956</option>
		                <option value="1955">1955</option>
		                <option value="1954">1954</option>
		                <option value="1953">1953</option>
		                <option value="1952">1952</option>
		                <option value="1951">1951</option>
		                <option value="1950">1950</option>
		            </select>
		        </div>
		    </div>
		    <div class="row form-group">
		        <div class="col-md-6">
		            <label>Email</label>
		            <input autocomplete="off" type="text" class="form-control" id="me_email" name="email" placeholder="Ketik Alamat email anda">
		        </div>
		        <div class="col-md-6">
		            <label>No.HP</label>
		            <input autocomplete="off" type="text" class="form-control" id="me_mobile_phone" name="mobilePhone" placeholder="Ketik No.HP anda">
		        </div>
		    </div>
		    <div class="row form-group satker-row">
		        <div class="col-md-6">
		            <label class="">Unit Kerja</label>
		            <select class="form-control" id="me_unit_kerja" name="unitKerja">
		                <option value="">Pilih Unit Kerja</option>
		            </select>
		        </div>
		        <div class="col-md-6">
		            <label class="">Jabatan</label>
		            <select class="form-control" id="me_jabatan" name="jabatan">
		                <option value="">Pilih Jabatan</option>
		            </select>
		        </div>
		    </div>
		    <div class="row form-group">
		        <div class="col-md-12">
		            <label>Alamat</label>
		            <input autocomplete="off" type="text" class="form-control" id="me_alamat" name="alamat" placeholder="Ketik Alamat anda">
		        </div>
		    </div>
		    <div class="row form-group">
		        <div class="col-md-12">
		            <label>Riwayat Penyakit</label>
		            <textarea class="form-control shadow-none" id="me_riwayat_penyakit" name="riwayatPenyakit" rows="4" placeholder="Jelaskan riwayat penyakit anda"></textarea>
		        </div>
		    </div>
    
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">BATAL</button>
            <button id="btnUpdateProfile" type="submit" class="btn btn-primary">
              <span class="glyphicon glyphicon-floppy-disk"></span> SIMPAN
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>
  
    <!-- Modal Feature Begin -->
	<form id="frmChangePassword" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<h4 class="modal-title">Ganti Password Anda</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					</div>
					<div class="modal-body">
						<div  id="changePasswordProgress" class="row">
					      <div class="col-xs-12">
							<div class="progress">
							  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only">45% Complete</span>
							  </div>
							</div>
					      </div>
					    </div>
						<div class="row form-group">
							<div class="col-md-12">
								<div id="changePasswordMsg" class="alert alert-success" role="alert" hidden="hidden"></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
					      	<label>Current Password</label>
				    			<input type="password" class="form-control" name="oldPassword" id="changePasswordOld" placeholder="Type your current password">
					      </div>					     
						</div>		
						<div class="row form-group">
							<div class="col-md-6">
					      	<label>Kata Kunci</label>
				    			<input type="password" class="form-control" name="newPassword" id="changePasswordNew" placeholder="New password">
					      </div>
					      
					      <div class="col-md-6">
					      	<label>Ulangi Kata Kunci</label>
				    			<input type="password" class="form-control" name="newPassword2" id="changePasswordNewRetype" placeholder="Retype new password">
					      </div>
						</div>
						
						
					</div>
					<div class="modal-footer">
							<button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnChangePassword" type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-floppy-disk"></span> 
						SIMPAN</button>
						</div>
				</div>
			</div>
		</div>
	</form>
	
	
	<!-- Modal Feature-->
	<div class="modal fade" data-backdrop="static" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
		<div class="modal-dialog">
			
			<div class="modal-content">
				<!-- <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
					<div class="row">
						<div class="col-md-12"><div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div></div>
					</div>
				</div> -->
				
			      <div class="modal-header">
			        <h5 class="modal-title">Konfirmasi Hapus</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
				
				<div class="modal-body">		
					<div id="progressBar1" class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
				    
					<div class="form-group">
						<div>Apakah anda yakin akan menghapus record data ini?</div>
					</div>
					
					<div class="modal-footer">
						<button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btn-modal-confirm-yes" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-danger">
							<span class="glyphicon glyphicon-trash"></span>HAPUS
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal Feature-->
	<div class="modal fade" data-backdrop="static" id="modal-confirm-notif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title">Konfirmasi</h5>
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">		
					<div id="modal-confirm-notif-msg" class="card-title">Apakah anda yakin akan mengirim <strong>email notifikasi</strong> pada pegawai ini?</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btn-modal-confirm-notif-yes" type="button" class="btn btn-primary">
						<i class="fas fa-paper-plane"></i> KIRIM
					</button>
				</div>
			</div>
		</div>
	</div>
	
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
	<div id="render-modal"></div>

	<footer class="footer fixed-bottom d-none">
      <div class="container">
        <p class="text-muted text-center">Copyright � 2017. Kementerian Lingkungan Hidup dan Kehutanan</p>
      </div>
    </footer>
    
	<!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- Moment JS, untuk function get date -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	
	<!-- Javascript -->
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.md5.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-datetimepicker.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-combobox.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.selectric.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/mprogress.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/apexcharts.min.js"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
	
	<!-- Tiny Mce -->
	<script src="${pageContext.request.contextPath}/js/libs/tinymce.min.js" referrerpolicy="origin"></script>
	
	
	<script src="${pageContext.request.contextPath}/js/common-script.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<script>
		function showAlertMessage(msg, timeout = 30000){
			$('#modal-alert-msg').html(msg);
			$('#modal-alert').modal('show');
			if(timeout >0 ){
					setTimeout(function(){ $('#modal-alert').modal('hide'); }, timeout);
			}
		}
	</script>
	