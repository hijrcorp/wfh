<%@page import="java.util.List"%>
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>SIMAWAS</h3>
    </div>
    <ul class="list-unstyled components">
        <li><a href="${pageContext.request.contextPath}/page/admin/dashboard-1">Monitoring Pelaporan</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/dashboard-2">Monitoring Hasil Pelaporan</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/laporan-1">Laporan Kinerja Periodik</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/laporan-2">Laporan Kesehatan Periodik</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/laporan-3">Laporan Kesehatan Pegawai</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/laporan-4">Laporan Kinerja Pegawai</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/detil-kesehatan">Lap. Detil Kesehatan Pegawai</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/detil-kinerja">Lap. Detil Kinerja Pegawai</a></li>
        <%-- <li><a href="${pageContext.request.contextPath}/page/admin/laporan-4">Daftar Pelaporan Kinerja</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/laporan-3">Daftar Pelaporan Kesehatan</a></li> --%>
        <% if(((List)request.getAttribute("roleList")).contains("ROLE_ADMIN")){ %>
        <%-- <li><a href="${pageContext.request.contextPath}/page/admin/informasi?sumber=internal">Informasi</a></li> --%>
        <% } %>
        <% if(((List)request.getAttribute("roleList")).contains("ROLE_SUPERVISOR") || ((List)request.getAttribute("roleList")).contains("ROLE_OPR")){ %>
        <%-- <li><a href="${pageContext.request.contextPath}/page/admin/informasi?sumber=user">Jawaban</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/ticket">Ticket</a></li> --%>
        <% } %>
        <% if(((List)request.getAttribute("roleList")).contains("ROLE_ADMIN")){ %>
        <%-- <li><a href="${pageContext.request.contextPath}/page/admin/tim">Tim</a></li>
        <li><a href="${pageContext.request.contextPath}/page/admin/kodefikasi">Kategori</a></li> --%>
        <li><a href="${pageContext.request.contextPath}/page/admin/user">Daftar Pengguna</a></li>
        <% } %>
        <% if(((List)request.getAttribute("roleList")).contains("ROLE_SUPERVISOR")){ %>
        <%-- <li><a href="${pageContext.request.contextPath}/page/admin/laporan">Laporan</a></li> --%>
        <% } %>
        
    </ul>

    <%-- <ul class="list-unstyled CTAs">
        <li>
            <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/page/welcome2" class="article">Back to website</a>
        </li>
    </ul> --%>
</nav>