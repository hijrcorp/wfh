
	<div class="form-group">
      <label class="col-md-2">Kode Sesi  </label>
      <div class="col-md-4">
			<input name="filter_kode" class="form-control input-sm" type="text" placeholder="Ketik Kode Sesi">
      </div>
      <label class="col-md-2">Periode </label>
      <div class="col-md-4">
      	<input name="filter_periode_awal" type="text" class="datepicker form-control-lha-sm" placeholder="Awal">
      	<label class="control-label input-sm">s.d</label>
      	<input name="filter_periode_akhir" type="text" class="datepicker form-control-lha-sm" placeholder="Akhir">
      </div>
    </div>
    
	<div class="form-group">
      <label class="col-md-2">Tahun Sesi </label>
      <div class="col-md-4">
			<select name="filter_tahun" class="form-control input-sm">
				<option value="">-- Semua Tahun --</option>
			</select>
      </div>
      <label class="col-md-2">Unit Kerja </label>
      <div class="col-md-4">		      
			<select name="filter_unit_kerja" class="combobox form-control input-sm">
				<option value="" selected="selected">-- Semua Unit Kerja --</option>
			</select>
    	</div>
    </div>
	
	<div class="form-group">
      <label class="col-md-2">Kegiatan  </label>
      <div class="col-md-4">
      		<select name="filter_kegiatan" class="form-control input-sm">
				<option value="">-- Semua Kegiatan --</option>
			</select>
      </div>
      <label class="col-md-2">Sumber Sesi  </label>
      <div class="col-md-4">
			<select name="filter_sumber" class="form-control input-sm">
				<option value="">-- Semua Sumber --</option>
				<option value="LAPGAS">LAPGAS</option>
				<option value="INTERNAL">INTERNAL</option>
			</select>
      </div>
	</div>
	