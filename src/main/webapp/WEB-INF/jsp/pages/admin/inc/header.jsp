<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Laporan Kegiatan Harian Pegawai</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- CSS -->
<script>var ctx = "${pageContext.request.contextPath}"</script>
<script>var tokenCookieName = "${cookieName}"</script>
<script>
var position = "${position}";

var disabled='';
if((position == 'Supervisior_View')){
	disabled='disabled';
}
</script>

<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<!-- Our Custom CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sidebar-cool.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
<!-- DateRangePicker CSS & JS -->
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 -->
<!-- Font Awesome JS -->
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

<!-- Other Plugin -->
<link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-combobox.css">
<%-- <link href="${pageContext.request.contextPath}/css/sticky-footer-navbar.css" rel="stylesheet"> --%>
<link href="${pageContext.request.contextPath}/css/select2.min.css" rel="stylesheet">
<%-- <link href="${pageContext.request.contextPath}/css/common-style.css" rel="stylesheet"> --%>
<%-- <link href="${pageContext.request.contextPath}/css/mod-navbar.css" rel="stylesheet"> --%>
<link href="${pageContext.request.contextPath}/css/selectric.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/mprogress.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/apexcharts.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond./js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicon and touch icons -->
<link href="${pageContext.request.contextPath}/images/logo.png" rel="shortcut icon" type="image/x-icon" />

<style>
	<!-- CUSTOME CSS HERE -->
</style>
<link href="https://unpkg.com/nprogress@0.2.0/nprogress.css" rel="stylesheet">