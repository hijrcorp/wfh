<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

  <div class="wrapper">
  
    <%@include file="inc/sidebar.jsp"%>
    
      <div id="content" class="pt-3">
      
      <%@include file="inc/navbar.jsp"%>
      
      <h2>Laporan Kesehatan Pegawai</h2>
      <!-- <p>Daftar Perkembangan Kinerja Pegawai</p> -->
      <div class="line"></div>
      
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6"><h5 class="mt-2">Daftar Laporan Monitoring Kesehatan Pegawai</h5></div>
            <div class="col-md-6">
              <button id="btnDownload" class="btn btn-info float-right" style="margin-right: 10px;"><i class="fa fa-download"></i> DOWNLOAD</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form id="form-filter">
          	<div class="card mb-3">
          		<div class="card-body">
		          	<div class="row mb-3">
					      	<div class="col-md-2"><label>Status Laporan</label></div>
									<div class="col-md-7">
					      		<div class="custom-control custom-radio custom-control-inline">
										  <input type="radio" id="radio1" name="status_pelaporan" value="ALL" class="custom-control-input" checked>
										  <label class="custom-control-label" for="radio1">Semua</label>
										</div>
					      		<div class="custom-control custom-radio custom-control-inline">
										  <input type="radio" id="radio2" name="status_pelaporan" value="Melapor" class="custom-control-input">
										  <label class="custom-control-label" for="radio2">Melapor</label>
										</div>
					      		<div class="custom-control custom-radio custom-control-inline">
										  <input type="radio" id="radio3" name="status_pelaporan" value="Tidak" class="custom-control-input">
										  <label class="custom-control-label" for="radio3">Tidak Melapor</label>
										</div>
				    			</div>
		            </div>
		          	<div class="row mb-3">
					      	<div class="col-md-2"><label>Tanggal</label></div>
									<div class="col-md-3">
										<div class="input-group">
											<input type="text" class="form-control datepicker" name="tanggal_laporan" autocomplete="off" readonly style="background: white;">
				              <span class="input-group-prepend">
				                <button type="button" type="button" class="btn btn-info"><i class="fa fa-calendar"></i></button>
				              </span>
				            </div>
			            </div>
		            </div>
		          	<div class="row">
					      	<div class="col-md-2"></div>
									<div class="col-md-5">
										<button id="btnSearch" type="button" onclick="search()" class="btn btn-info"><i class="fa fa-search"></i> Tampil</button>
										<button id="btnRefresh" type="button" onclick="refresh()" class="btn btn-info"><i class="fa fa-sync"></i> Reset</button>
									</div>
		            </div>
	            </div>
            </div>
          </form>
          <table id="tbl" class="table table-bordered text-center mt-15">
            <thead class="thead-light">
              <tr>
                <th width="20"  rowspan="2">No.</th>
                <th width="150" rowspan="2">Unit Kerja</th>
                <th width="100" rowspan="2" colspan="2">NIP</th>
                <th width="300" rowspan="2">Nama</th>
                <th width="50"  rowspan="2">Status</th>
                <th width="50"  rowspan="2">Sehat</th>
                <th width="50"  colspan="5">Sakit</th>
                <th width="50"  rowspan="2">Aksi</th>
              </tr>
              <tr>
                <th>DM</th>
                <th>BT</th>
                <th>PL</th>
                <th>ST</th>
                <th>SN</th>
              </tr>
            </thead>
            <tbody></tbody>
            <tfoot></tfoot>
          </table>
          <nav><ul id="tblpages" class="pagination"></ul></nav>
        </div>
      </div>
  	
	
	<script src="${pageContext.request.contextPath}/js/laporan-3-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>