<!DOCTYPE html>
<html>

<head>
   <%@include file="inc/header.jsp"%>
</head>

<body>

  <div class="wrapper">
  
    <%@include file="inc/sidebar.jsp"%>
    
      <div id="content" class="pt-3">
      
      <%@include file="inc/navbar.jsp"%>
      
      <h2>Pengguna</h2>
      <p>Pengguna Aplikasi</p>
      
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6"><h5 class="mt-2">Daftar Pengguna</h5></div>
            <div class="col-md-6">
              <button id="btnAdd" class="btn btn-info float-right" style="margin-right: 10px;"><i class="fa fa-plus"></i> TAMBAH</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form id="form-filter">
            <div class="input-group" style="margin-bottom: 15px">
              <input id="txtsearch" name="filter_keyword" type="text" class="form-control" aria-label="..." placeholder="Ketik kata kunci yang ingin anda cari" autocomplete="off">
              <span class="input-group-prepend"">
                <button type="button" onclick="search()" type="button" class="btn btn-info">
                  <i class="fa fa-search"></i> Cari
                </button>
              </span>
            </div>
          </form>
          <table id="tbl" class="table">
            <thead class="thead-light">
              <tr>
                <th width="20">#</th>
                <th width="150">Profil</th>
                <th width="200">Organisasi</th>
                <th width="150">Kontak</th>
                <th width="20">Role</th>
                <th width="80">Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          <nav><ul id="tblpages" class="pagination"></ul></nav>
        </div>
      </div>
  	
	
	<!-- Modal Feature Begin -->
	<form id="entry-form" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
			
				<div class="modal-header">
                    <h4 class="modal-title">Form Pengguna</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
				
				<div class="modal-body">
					 <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
					
					<p class="text-danger small">Input dengan tanda (*) wajib diisi.</p>
					
					<input type="hidden" id="id" value="0" />
			    	
				    <div class="row form-group">
				    	 	<div class="col-md-6">
					      	<label>NIP Pegawai *</label>
				    			<input autocomplete="off" type="text" class="form-control" name="username" placeholder="Ketik NIP Pegawai">
					      </div>
				      <div class="col-md-6">
				      	<label>Nama Lengkap *</label>
			    			<input autocomplete="off" type="text" class="form-control" name="last_name" placeholder="Ketik nama lengkap">
				      </div>
				    </div>
				    
				     <div class="row form-group">
					  	<div class="col-md-6">
					    <label>Jenis Kelamin</label>
					        <select class="form-control" id="gender" name="gender">
					            <option value="0">-- Pilih Jenis Kelamin --</option>
					        	<option value="M">LAKI-LAKI</option>
					        	<option value="F">PEREMPUAN</option>
					        </select>
					  	</div>
					    <div class="col-md-6">
				      	<label>Tipe *</label>
			    			<select class="form-control" id="position" name="position">
			    				<option value="0">-- Pilih Tipe Pengguna --</option>
							</select>
				      </div>
					</div>
				    
				    
					<div id="passwordForm" class="row form-group">
				      <div class="col-md-6">
				      	<label>Kata Kunci *</label>
			    			<input type="password" class="form-control" name="password" id="password" placeholder="Ketik kata kunci">
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Ulangi Kata Kunci *</label>
			    			<input type="password" class="form-control" name="repassword" id="repassword" placeholder="Ketik lagi kata kunci yang sama">
				      </div>
				    </div>
				    
				    <div class="row form-group">
				      <div class="col-md-6">
				      	<label>Email</label>
			    			<input autocomplete="off" type="text" class="form-control" name="email" id="email" placeholder="Ketik Alamat email">
				      </div>
				      <div class="col-md-6">
				      	<label>No.HP</label>
			    			<input autocomplete="off" type="text" class="form-control" name="mobile_phone" id="mobile_phone" placeholder="Ketik No.HP">
				      </div>
				    </div>
			    
				    <div class="row form-group satker-row">
				        <div class="col-md-6">
				            <label class="">Unit Kerja</label>
				            <select class="form-control" name="unit_kerja">
				                <option value="">-- Pilih Unit Kerja --</option>
				            </select>
				        </div>
				        <div class="col-md-6">
				            <label class="">Jabatan</label>
				            <select class="form-control" name="jabatan">
				                <option value="">-- Pilih Jabatan --</option>
				            </select>
				        </div>
				    </div>
				    
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnReset" type="button" class="btn btn-default" aria-hidden="true">RESET</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
  
    <div class="modal fade" id="modal-import-pengguna" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog modal-lg modal-notify modal-success  shadowmod" role="document" style="max-width: 600px;">
        <!--Content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Import Pengguna</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="white-text">&times;</span>
            </button>
          </div>
          <div class="modal-body white">
            <div id="msgImport" class="msg alert alert-danger" role="alert" style="display:none"></div>
              <form class="" id="frm-import-pengguna">
                <div class="row">
                  <div class="col-md-12">
                    <div class="md-form">
                      <div class="md-form">
                        <label for="" class="">NIP *</label>
                        <input type="text" name="nip" class="form-control" placeholder="Ketik NIP Pegawai" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row p-1">
                  <div class="col-md-12">
                    <div class="md-form">
                      <div class="md-form">
                      <label for="" class="">Tipe *</label>
                      <select onchange="onChangeType(this.value);" class="form-control input-sm" name="position" id="position-import">
                        <option value="0">-- Pilih Tipe Pengguna --</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer justify-content-right">
            <button class="btn btn-sm btn-danger shadowmod " type="button" data-dismiss="modal"> <i class="fa fa-remove mr-1"> </i>  TUTUP</button>
            <button onclick="importPengguna()" class="btn btn-sm btn-primary defaultcolor shadowmod " type="button"> <i class="fa fa-save mr-1"> </i> SIMPAN</button>
          </div>
        </div>
      </div>
    </div>
	
	
	<!-- Modal Feature Begin -->
	
	<form id="frmInputPass" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<h4 class="modal-title">Ubah Kata Kunci</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					</div>
					<div class="modal-body">
						<div  id="progressBar2"  class="row">
					      <div class="col-xs-12">
							<div class="progress">
							  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only">45% Complete</span>
							  </div>
							</div>
					      </div>
					    </div>
						<div class="row">
							<div class="col-md-12">
								<div id="msg1" class="alert alert-success" role="alert" hidden="hidden"></div>
							</div>
						</div>
										
						<div class="row">
							<div class="col-md-6">
					      	<label>Kata Kunci</label>
				    			<input type="password" class="form-control" name="password" id="password1" placeholder="Ketik kata kunci">
					      </div>
					      
					      <div class="col-md-6">
					      	<label>Ulangi Kata Kunci</label>
				    			<input type="password" class="form-control" name="password2nd" id="repassword1" placeholder="Ketik lagi kata kunci yang sama">
					      </div>
						</div>
						
						
					</div>
					<div class="modal-footer">
							<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnUpdate" type="button" onclick="doUpdate($(this).data('id'));" data-id="0" class="btn btn-primary">
						<span class="fa fa-save"></span> 
						SIMPAN</button>
						</div>
				</div>
			</div>
		</div>
		</form>
	 
	 <script src="${pageContext.request.contextPath}/js/user-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>