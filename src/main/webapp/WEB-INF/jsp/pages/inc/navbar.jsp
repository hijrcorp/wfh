<nav class="navbar navbar-expand-lg navbar-light bg-light sb-navbar  bg-white border-bottom shadow-sm fixed-top">
	    <div class="container-fluid">
	        <h1 class="my-0 mr-md-auto font-weight-normal navbar-brand text-muted"><img src="${pageContext.request.contextPath}/images/logo-menlhk.png" width="38px" /> <%= request.getAttribute("realName") %></h1>

        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="navbar-collapse collapse text-center" id="navbarSupportedContent" style="">
            <ul class="navbar-nav ml-auto pt-3 pt-lg-0">
                <li class="nav-item">
                    <a class="btn btn-sm bg-main-color text-white ml-md-3" href="${pageContext.request.contextPath}/page/">Update Profil</a>
                </li>
                <c:if test="${adminPage != null && adminPage == 'yes'}">
                <li class="nav-item">
                    <a class="btn btn-sm btn-outline-success ml-md-3" href="${pageContext.request.contextPath}/page/admin/">Laporan</a>
                </li>
                </c:if>
            </ul>

            <ul class="navbar-nav pb-3 pb-lg-0">
            <c:if test="${updateProfil}">
                <li class="nav-item">
                    <a class="btn btn-sm btn-outline-primary  ml-md-3" href="${pageContext.request.contextPath}/page/laporan"><i class="fas fa-file-alt"></i> Kesehatan</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-sm btn-outline-primary  ml-md-3" href="${pageContext.request.contextPath}/page/laporan-detil-pekerjaan"><i class="fas fa-file-alt"></i> Pekerjaan</a>
                </li>
                </c:if>
                <li class="nav-item">
                    <a class="btn btn-sm btn-outline-danger  ml-md-3" href="${pageContext.request.contextPath}/login-logout"><i class="fas fa-sign-out-alt"></i> Keluar</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="btn btn-sm bg-main-color text-white ml-md-3" href="https://app.rente.ai/signup">SIGN IN</a>
                </li> -->
            </ul>

        </div>
    </div>
</nav>