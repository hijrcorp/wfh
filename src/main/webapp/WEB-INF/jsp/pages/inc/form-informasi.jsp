	<div class="form-group">
      	<label class="col-md-2 input-sm">Nomor LHP : </label>
      	<label class="col-md-4 input-sm text-info" id="nomorLhp"></label>
      	<label class="col-md-2 input-sm" for="tanggalLhp">Tanggal LHP : </label>
      	<label class="col-md-4 input-sm text-info" id="tanggalLhp"></label>
    </div>
    
	<div class="form-group">
      	<label class="col-md-2 input-sm" for="tanggalLhp">Tanggal Terima : </label>
      	<label class="col-md-4 input-sm text-info" id="tanggalTerimaLhp"></label>
		<label class="col-md-2 input-sm">PKPT Tahun : </label>
      	<label class="col-md-4 input-sm text-info" id="pkpt"></label>
    </div>
	
	<div class="form-group">
      	<label class="col-md-2 input-sm" for="tanggalLhp">Satker/Auditi : </label>
      	<label class="col-md-4 input-sm text-info" id="unitAuditi"></label>
		<label class="col-md-2 input-sm">Nama Kepala : </label>
      	<label class="col-md-4 input-sm text-info" id="auditiKepala"></label>
	</div>
	
	<div class="form-group">
      	<label class="col-md-2 input-sm" for="tanggalLhp">NIP : </label>
      	<label class="col-md-4 input-sm text-info" id="auditiNip"></label>
		<label class="col-md-2 input-sm">Jenis Audit : </label>
      	<label class="col-md-4 input-sm text-info" id="jenisAudit"></label>
	</div>
	
	<div class="form-group">
      	<label class="col-md-2 input-sm" for="tanggalLhp">Instansi Pemeriksa : </label>
      	<label class="col-md-4 input-sm text-info" id="unitKerja"></label>
		<label class="col-md-2 input-sm">File Berkas : </label>
      	<label class="col-md-4 input-sm text-info" id="fileBerkas"></label>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 input-sm">Eselon1 : </label>
      	<label class="col-md-4 input-sm text-info" id="eselon1"></label>
      	<label class="col-md-2 input-sm" for="tanggalLhp">Nomor Surat Tugas : </label>
      	<label class="col-md-4 input-sm text-info" id="nomorSurat"></label>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 input-sm">Tanggal : </label>
      	<label class="col-md-4 input-sm text-info" id="tanggalSurat"></label>
      	<label class="col-md-2 input-sm" for="tanggalLhp">Lama Audit : </label>
      	<label class="col-md-4 input-sm text-info" id="lamaAudit"></label>
	</div>
	
	<div class="form-group">
      	<label class="col-md-2 input-sm" for="tanggalLhp">Periode Audit : </label>
     		<div class="col-md-4">
     			<div class="row">
    			<label class="input-sm text-info" id="periodeAwal"></label><!-- &nbsp;&nbsp;&nbsp;&nbsp; -->
		    	<label class="input-sm text-info">s.d</label>
    			<label class="input-sm text-info" id="periodeAkhir"></label>
   			</div>
	    </div>
		<label class="col-md-2 input-sm">Tanggal Pelaksanaan : </label>
		<div class="col-md-4">
     			<div class="row">
    			<label class="input-sm text-info" id="tanggalAwal"></label><!-- &nbsp;&nbsp;&nbsp;&nbsp; -->
		    	<label class="input-sm text-info">s.d</label>
    			<label class="input-sm text-info" id="tanggalAkhir"></label>
   			</div>
	    </div>
	</div>