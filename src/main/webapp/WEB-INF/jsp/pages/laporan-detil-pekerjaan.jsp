<!doctype html>
<html lang="en">
  <head>
    <%@include file="inc/header.jsp"%>
    
  	<!-- Select2 -->
  	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	
  	<style>
  		.bg-main-color {
  			background-color: #7c70f4;
  			color: white;
  		}
  		.search-box .search-input {
		    width: 20rem;
		    padding-left: 1.95rem;
		}
		.search-box {
		    position: relative;
		}

        .card-sm .card-body {
            padding: 0.75rem 1.5rem;
        }
        .bg-gradient {
        	background: linear-gradient(135deg, #6f42c1 0%, #4582EC 100%);   
  			color: white;
        }
  	</style>
  </head>
  <body>
	<header>
	  <%@include file="inc/navbar.jsp"%>
	</header>
	
	<main role="main">
	<div class="container mt-5" style="padding-top: 2.5rem;padding-bottom: 0.5rem;">
	    <!-- Three columns of text below the carousel -->
		
		<div class="row justify-content-center">
		    <div class="col-12 col-md-10">
		        
                    <div class="card">
                        <div class="card-header bg-transparent">
                            <div class="row">
                                <div class="col-md-6"><h5 class="mt-2">Pekerjaan</h5></div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="progressBar" class="row" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            <span class="sr-only">45% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="msg" class="alert" role="alert" hidden="hidden" style="display: none;"></div>
                                </div>
                            </div>
							<form id="frmInput">

                            <div class="card mb-3">
                                <div class="card-body">

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>Tanggal <img id="loadtampil" style="display:none" width="20" src="${pageContext.request.contextPath}/images/loading-spinner.gif"></label>
                                            <div class="input-group mb-3">
                                                <input type="text" name="tanggal_laporan" class="form-control datepicker" placeholder="Tentukan tanggal Laporan">
                                                <div class="input-group-append">
                                                    <button class="btn btn-info" type="button" id="btn-tampilkan">Tampilkan</button>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-6">
										 <label>&nbsp;</label>
										  <!-- <small id="msg-nip" class="form-text text-muted" style="display:none">*Ketik NIP, dan tekan tombol Enter</small> -->
										  <div id="msg-nip" class="valid-feedback" style="display: block;"></div>
										</div>
                                    </div>

                                </div>

                            </div>
                            </form>
                            <!-- <h5 class="float-left py-2">Detail Pekerjaan</h5> -->
                            <button id="btn-tambah" type="button" class="btn btn-dark shadow-none float-right my-2 "><i class="fas fa-fw fa-plus"></i> Tambah</button>

                            <table id="tbl-data" class="table table-sm table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="5%">Jenis</th>
                                        <th width="25%">Nama Pekerjaan</th>
                                        <th width="25%">Uraian Pekerjaan</th>
                                        <th width="25%">Output Pekerjaan</th>
                                        <th class="text-center" width="15%">Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr class="data-row " id="row-1543475744602 ">
                                        <td>1</td>
                                        <td class=" ">TUSI</td>
                                        <td class=" ">Ngoding</td>
                                        <td class=" ">Aplikasi WFH</td>
                                        <td class=" ">CRUD PEKERJAAN</td>
                                        <td class=" ">
                                            <a href="javascript:void(0) " class="btn btn-sm " type="button " onclick="doAction( '1543475744602', 'edit') ">
                                                <svg class="svg-inline--fa fa-pencil-alt fa-w-16 fa-fw " aria-hidden="true " data-prefix="fas " data-icon="pencil-alt " role="img " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 512 512 " data-fa-i2svg=" ">
                                                    <path fill="currentColor " d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z "></path>
                                                </svg>
                                                <i class="fas fa-fw fa-pencil-alt "></i></a>
                                            <a href="javascript:void(0) " class="btn btn-sm " type="button " onclick="doAction( '1543475744602', 'delete') ">
                                                <svg class="svg-inline--fa fa-trash fa-w-14 fa-fw " aria-hidden="true " data-prefix="fas " data-icon="trash " role="img " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 448 512 " data-fa-i2svg=" ">
                                                    <path fill="currentColor " d="M0 84V56c0-13.3 10.7-24 24-24h112l9.4-18.7c4-8.2 12.3-13.3 21.4-13.3h114.3c9.1 0 17.4 5.1 21.5 13.3L312 32h112c13.3 0 24 10.7 24 24v28c0 6.6-5.4 12-12 12H12C5.4 96 0 90.6 0 84zm415.2 56.7L394.8 467c-1.6 25.3-22.6 45-47.9 45H101.1c-25.3 0-46.3-19.7-47.9-45L32.8 140.7c-.4-6.9 5.1-12.7 12-12.7h358.5c6.8 0 12.3 5.8 11.9 12.7z "></path>
                                                </svg>
                                                <i class="fas fa-fw fa-trash "></i></a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
		    </div>
		</div>
		
        
		<div class="row justify-content-center mt-3">
		    <div class="col-12 col-md-10">
		
		        <nav aria-label="breadcrumb " class="mt-3">
		            <ol class="breadcrumb bg-light">
	            	 	<li class="breadcrumb-item mr-auto small d-none">
		                    <span>*No.Ticket anda akan dikirim ke email anda.</span>
		                </li>
		                
		                <li class="breadcrumb-item ml-auto">
		                    <button id="btn-create" type="button" onclick="save();" class="btn btn-primary d-none"><i class="fa fa-save"></i> SIMPAN</button>
		                </li>
		
		            </ol>
		
		            <form class="form-inline my-2 my-lg-0 d-none">
		                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		            </form>
		        </nav>
		    </div>
		</div>
	   
	    <!-- START THE FEATURETTES -->
	
	    <hr class="featurette-divider">
	
	    <!-- /END THE FEATURETTES -->
	
	
	<!-- Modal Feature Begin -->
	<form id="entry-form" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog ">
				<div class="modal-content">
			
				<div class="modal-header">
                    <h4 class="modal-title">Form Detil Pekerjaan</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
				
				<div class="modal-body">
					 <div  id="progressBar" class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
					
					<p class="text-danger small">Input dengan tanda (*) wajib diisi.</p>
		
				     <div class="row form-group">
					  	<div class="col-md-12">
					    <label>Jenis TUSI</label>
					        <select class="form-control" name="status_tusi">
					            <option value="">-- Pilih Jenis TUSI --</option>
					        	<option value="YES">TUSI</option>
					        	<option value="NO">Tambahan</option>
					        </select>
					  	</div>
					</div>
				    
				     <div class="row form-group">
				    	<div class="col-md-12">
					      	<label>Nama Pekerjaan *</label>
				    			<input autocomplete="off" type="text" class="form-control" name="nama" placeholder="Jelaskan Nama Pekerjaan anda">
					      </div>
				    </div>
				    
				    <div class="row form-group">
				      <div class="col-md-12">
				      	<label>Uraian Pekerjaan</label>
		    			<textarea class="form-control shadow-none" name="uraian"  rows="5" placeholder="Jelaskan Uraian Pekerjaan anda"></textarea>
				      </div>
				    </div>
				    
				    <div class="row form-group">
				      <div class="col-md-12">
				      	<label>Output Pekerjaan</label>
		    			<textarea class="form-control shadow-none" name="output"  rows="5" placeholder="Jelaskan Output Pekerjaan anda"></textarea>
				      </div>
				    </div>
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnReset" type="button" class="btn btn-default" aria-hidden="true">RESET</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
	</div>
	<%@include file="inc/footer.jsp"%>
	</main>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <!-- Select2 -->
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  	
	<!-- start -->
	<!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- Moment JS, untuk function get date -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	
	<!-- Javascript -->
	<%-- <script src="${pageContext.request.contextPath}/js/libs/jquery-1.11.2.min.js"></script> --%>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>	
	<%-- <script src="${pageContext.request.contextPath}/js/libs/bootstrap.min.js"></script> --%>
	
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.md5.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-datetimepicker.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-combobox.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.selectric.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/mprogress.min.js"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
	
	<!-- Tiny Mce -->
	<script src="${pageContext.request.contextPath}/js/libs/tinymce.min.js" referrerpolicy="origin"></script>
	
	
	<script src="${pageContext.request.contextPath}/js/common-new-general-script.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<!-- end -->
	<script src="${pageContext.request.contextPath}/js/user/laporan-detil-script.js"></script>

  </body>
</html>