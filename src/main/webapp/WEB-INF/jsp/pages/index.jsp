<!doctype html>
<html lang="en">
  <head>
    <%@include file="inc/header.jsp"%>
    
  	<!-- Select2 -->
  	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	
  	<style>
  		.bg-main-color {
  			background-color: #7c70f4;
  			color: white;
  		}
  		.search-box .search-input {
		    width: 20rem;
		    padding-left: 1.95rem;
		}
		.search-box {
		    position: relative;
		}

        .card-sm .card-body {
            padding: 0.75rem 1.5rem;
        }
        .bg-gradient {
        	background: linear-gradient(135deg, #6f42c1 0%, #4582EC 100%);   
  			color: white;
        }
  	</style>
  </head>
  <body>
	<header>
	  <%@include file="inc/navbar.jsp"%>
	</header>
	
	<main role="main">
	<div class="container mt-5" style="padding-top: 2.5rem;padding-bottom: 0.5rem;">
	    <nav aria-label="breadcrumb">
		    <ol class="breadcrumb bg-light">
		    <c:choose>
		    <c:when test="${updateProfil}">
		    		 <li class="breadcrumb-item"><h5 class="card-title text-muted my-1"><strong>Terimakasih </strong> Anda telah melengkapi form ini.</h5></li>
		    </c:when>
		    <c:otherwise>
		    		 <li class="breadcrumb-item"><h5 class="card-title text-muted my-1">Anda wajib melengkapi form ini sebelum dapat menggunakan aplikasi. </h5></li>
		    </c:otherwise>
		    </c:choose>
		       
		    </ol>
		
		    <form class="form-inline my-2 my-lg-0 d-none">
		        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		    </form>
		</nav>
	</div>
	<div class="container">
	    <!-- Three columns of text below the carousel -->
		
		<form id="frmInput">
		<div class="row justify-content-center">
		    <div class="col-12 col-md-10">
		        <div class="card border-0s">
		            <div class="card-header bg-transparent">
		                <h5 class="card-title text-muted my-1">Update Profile</h5>
		            </div>
		            <div class="card-body card-body-sm card-body-md">
					    <input autocomplete="off" type="hidden" id="me_id"  name="id">
					    <div class="row form-group">
					        <div class="col-md-6">
					            <label>NIP Pegawai </label>
					            <input autocomplete="off" type="text" class="form-control" id="me_nip_pegawai" placeholder="Ketik nama depan anda" readonly="">
					        </div>
					        <div class="col-md-6">
					            <label>Nama Lengkap </label>
					            <input autocomplete="off" type="text" class="form-control" id="me_nama_lengkap" name="lastName" placeholder="Ketik nama belakang anda">
					        </div>
					    </div>
					    <input autocomplete="off" type="hidden" id="me_birth_date"  name="birthDate" placeholder="Tentukan Tgl.Lahir">
					    <div class="row form-group">
					        <div class="col-md">
					            <label>Tanggal Lahir</label>
					            <select class="form-control" id="me_hari">
					                <option value="">Pilih Hari</option>
					            </select>
					        </div>
					        <div class="col-md">
					            <label>&nbsp;</label>
					            <select class="form-control" id="me_bulan">
					                <option value="">Pilih Bulan</option>
					            </select>
					        </div>
					        <div class="col-md">
					            <label>&nbsp;</label>
					            <select class="form-control" id="me_tahun">
					                <option value="">Pilih Tahun</option>
					            </select>
					        </div>
					    </div>
					    <div class="row form-group">
					        <div class="col-md-6">
					            <label>Email</label>
					            <input autocomplete="off" type="text" class="form-control" id="me_email" name="email" placeholder="Ketik Alamat email anda">
					        </div>
					        <div class="col-md-6">
					            <label>No.HP</label>
					            <input autocomplete="off" type="text" class="form-control" id="me_mobile_phone" name="mobilePhone" placeholder="Ketik No.HP anda">
					        </div>
					    </div>
					    <div class="row form-group satker-row">
					        <div class="col-md-6">
					            <label class="">Unit Kerja</label>
					            <select class="form-control" id="me_unit_kerja" name="unitKerja">
					                <option value="">-- Pilih Unit Kerja --</option>
					            </select>
					        </div>
					        <div class="col-md-6">
					            <label class="">Jabatan</label>
					            <select class="form-control" id="me_jabatan" name="jabatan">
					                <option value="">-- Pilih Jabatan --</option>
					            </select>
					        </div>
					    </div>
					    <div class="row form-group">
					        <div class="col-md-6">
					            <label>Alamat Rumah</label>
					            <textarea class="form-control shadow-none" id="me_alamat" name="alamat"  rows="5"placeholder="Ketik Alamat anda"></textarea>
					        </div>
					        <div class="col-md-6">
					            <label>Riwayat Penyakit (Opsional)</label>
					            <textarea class="form-control shadow-none" id="me_riwayat_penyakit" name="riwayatPenyakit" rows="5" placeholder="Jelaskan riwayat penyakit yang pernah anda alami.."></textarea>
					        </div>
					    </div>
					
					    <button id="btn-submit" type="submit" class="btn btn-primary d-none">Submit</button>
		            </div>
		        </div>
		    </div>
		</div>
		
		<div class="row justify-content-center mt-3">
		    <div class="col-12 col-md-10">
		        <div class="card border-0s">
		            <div class="card-header bg-transparent">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="card-title text-muted my-1">Form Password</h5></div>
                            <div class="col-md-6">
                                <div class="custom-control custom-switch text-right">
								  <input type="checkbox" class="custom-control-input" id="customSwitch1">
								  <label class="custom-control-label" for="customSwitch1">Edit Password</label>
								</div>
                            </div>
                        </div>
		                <!-- <h5 class="card-title text-muted my-1">Update Password</h5> -->
		            </div>
		            <div class="card-body card-body-sm card-body-md">
					    <div class="row form-group">
					        <div class="col-md-6">
					            <label>Password Lama</label>
					            <input autocomplete="off" type="password" class="form-control" id="me_old_password" name="oldPassword" placeholder="Ketik Password lama anda" disabled>
					        </div>
					    </div>
					    <div class="row form-group">
					        <div class="col-md-6">
					            <label class="">Password Baru</label>
					            <input type="password" class="form-control" id="me_new_password" name="newPassword" placeholder="Ketik Password baru anda" disabled>
					        </div>
					        <div class="col-md-6">
					            <label class="">Ketik Ulang Password Baru</label>
					            <input type="password" class="form-control" id="me_renew_password" name="newPassword2" placeholder="Ketik Ulang Password baru anda" disabled>
					        </div>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>
		</form>
		
		<div class="row justify-content-center mt-3">
		    <div class="col-12 col-md-10">
		
		        <nav aria-label="breadcrumb " class="mt-3">
		            <ol class="breadcrumb bg-light">
	            	 	<li class="breadcrumb-item mr-auto small d-none">
		                    <span>*No.Ticket anda akan dikirim ke email anda.</span>
		                </li>
		                
		                <li class="breadcrumb-item ml-auto">
		                    <button id="btn-create" type="button" onclick="save();" class="btn btn-primary btn-sm">SUBMIT</button>
		                </li>
		
		            </ol>
		
		            <form class="form-inline my-2 my-lg-0 d-none">
		                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		            </form>
		        </nav>
		    </div>
		</div>
	   
	    <!-- START THE FEATURETTES -->
	
	    <hr class="featurette-divider">
	
	    <!-- /END THE FEATURETTES -->
	
	</div>
	<%@include file="inc/footer.jsp"%>
	</main>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <!-- Select2 -->
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  	
	<!-- start -->
	<!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- Moment JS, untuk function get date -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	
	<!-- Javascript -->
	<%-- <script src="${pageContext.request.contextPath}/js/libs/jquery-1.11.2.min.js"></script> --%>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>	
	<%-- <script src="${pageContext.request.contextPath}/js/libs/bootstrap.min.js"></script> --%>
	
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.md5.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-datetimepicker.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-combobox.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.selectric.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/mprogress.min.js"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
	
	<!-- Tiny Mce -->
	<script src="${pageContext.request.contextPath}/js/libs/tinymce.min.js" referrerpolicy="origin"></script>
	
	
	<script src="${pageContext.request.contextPath}/js/common-new-general-script.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<!-- end -->
	<%-- <script src="${pageContext.request.contextPath}/js/create-ticket-welcome-script.js"></script> --%>
	<script>
	function init(){
		showUpdateProfile();
		$('#customSwitch1').on('change.bootstrapSwitch', function(e) {
			if(e.target.checked){
		    	$("[type=password]").removeAttr('disabled');
			}else{
				$("[type=password]").attr('disabled',true);
			}
		});
	}
	
	function save(){
		var obj = new FormData(document.querySelector('#frmInput'));
		ajaxPOST(ctx + "/user/updateProfile",obj,'succSave','errSave');
	}
	function succSave(response){
		Swal.fire({
			  type: 'success',
			  title: "Data Berhasil Disimpan.",
			  showConfirmButton: true,
			  allowOutsideClick: false
			}).then(function(result) { 
				  // result.value will containt the input value
				window.location.reload();
			});
		
	}
	function errSave(response){
		if(response.responseJSON.code==400){
			alert(response.responseJSON.message);
		}else{
		 	alert("something wrongess");
		}
	}
	/*function doUpdateProfile(){
		var obj = new FormData(document.querySelector('#frmUpdateProfile'));
		ajaxPOST(ctx + "/user/updateProfile",obj,'succDoUpdateProfile','errDoUpdateProfile');
	}
	function succDoUpdateProfile(response){
		console.log(response);
		$('#modalUpdateProfile').modal('hide');
		$('#frmUpdateProfile')[0].reset();

		$("#me_unit_kerja").html("<option value=''>Pilih Unit Kerja</option>");
		$("#me_jabatan").html("<option value=''>Pilih Jabatan</option>");
	}
	function errDoUpdateProfile(response){
		 $("#updateProfileProgress").hide();
	     addAlert('updateProfileMsg', "alert-danger", response.message);
	}*/
	
	setInterval(function(){ 
		var tahun = $('#me_tahun').val();
		var bulan = $('#me_bulan').val();
		var hari = $('#me_hari').val();
		if(tahun!="" && bulan!="" && hari!=""){
			$('#me_birth_date').val(hari+"-"+bulan+"-"+tahun);
		}else{
			//$('#btn-create-ticket').attr('disabled',false);
		}
	}, 1000);
	
	function showUpdateProfile(mode='check'){
		$.ajax({
		    url: ctx+'/ref/unit-kerja/list',
		    method: "GET",
		    success: function(response) {
		    	if(response.code == 200){
		    		var responseUnitKerja=response;
		    		$.ajax({
		    		    url: ctx+'/ref/jabatan/list',
		    		    method: "GET",
		    		    success: function(response) {
		    		    	if(response.code == 200){
		    		    		var responseJabatan=response;
		    		    		$.each(responseJabatan.data, function(key, value) { $("#me_jabatan").append(new Option(value.namaJabatan,value.idJabatan)); });
		    		    		$.each(responseUnitKerja.data, function(key, value) { $("#me_unit_kerja").append(new Option(value.nama_unit_kerja,value.id_unit_kerja)); });
		    		    		for(var i=0; i < 31;i++){$('#me_hari').append(new Option((i+1).convpart(), (i+1).convpart()));}
		    		    		var month=moment.months();
		    		    		for(var i=0; i < 12;i++){$('#me_bulan').append(new Option(month[i], (i+1).convpart())); }
		    		    		var d = new Date();now = d.getFullYear();tahun = (now-10) - 70;
		    		    		for (var i = (now-10); i >= tahun; i--) {$("#me_tahun").append(new Option(i, i));}
		    		    	    
		    		    		$.getJSON(urlLog+"me", function(response) {
		    		    	        if(response.code == 200) {
		    		    	            var data = response.data;
		    		    	            console.log('me: ', data);
		    		    	            $('#me_id').val(data.id);
		    		    	            $('#me_nama_lengkap').val(data.lastName);
		    		    	            $('#me_nip_pegawai').val(data.username);
		    		    	            $('#me_email').val(data.email);
		    		    	            $('#me_mobile_phone').val(data.mobilePhone);
		    		    	            $('#me_gender').val(data.gender);
		    		    	            $('#me_unit_kerja').val(data.unitKerjaId);
		    		    	            $('#me_jabatan').val(data.idJabatanUserDetil);
		    		    	            $('#me_alamat').val(data.alamatRumahUserDetil);
		    		    	            $('#me_riwayat_penyakit').val(data.riwayatPenyakitUserDetil);
		    		    	            $('#me_birth_date').val(data.birthDate);
		    		    	            var newBirthDate=$('#me_birth_date').val().split("-");
		    		    	            $('#me_hari').val(newBirthDate[2]);
		    		    	            $('#me_bulan').val(newBirthDate[1]);
		    		    	            $('#me_tahun').val(newBirthDate[0]);
		    		    	        }
		    		    	    });
		    		    	}
		    		    }
		    		});	
		    	}
		    }
		});	
	}
	</script>
  </body>
</html>