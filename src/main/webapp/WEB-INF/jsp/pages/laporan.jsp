<!doctype html>
<html lang="en">
  <head>
    <%@include file="inc/header.jsp"%>
    
  	<!-- Select2 -->
  	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	
  	<style>
  		.bg-main-color {
  			background-color: #7c70f4;
  			color: white;
  		}
  		.search-box .search-input {
		    width: 20rem;
		    padding-left: 1.95rem;
		}
		.search-box {
		    position: relative;
		}

        .card-sm .card-body {
            padding: 0.75rem 1.5rem;
        }
        .bg-gradient {
        	background: linear-gradient(135deg, #6f42c1 0%, #4582EC 100%);   
  			color: white;
        }
  	</style>
  </head>
  <body>
	<header>
	  <%@include file="inc/navbar.jsp"%>
	</header>
	
	<main role="main">
	<div class="container mt-5" style="padding-top: 2.5rem;padding-bottom: 0.5rem;">
	    <!-- Three columns of text below the carousel -->
		
		<div class="row justify-content-center">
		    <div class="col-12 col-md-10">
		        
                    <div class="card">
                        <div class="card-header bg-transparent">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="mt-2">Kesehatan</h5></div>
                                <div class="col-md-6">
                                    <button id="btn-save" onclick="save();" class="btn btn-primary float-right" style="margin-right: 10px;"><i class="fa fa-save"></i> SIMPAN</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="progressBar" class="row" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            <span class="sr-only">45% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="msg" class="alert" role="alert" hidden="hidden" style="display: none;"></div>
                                </div>
                            </div>
							<form id="frmInput">

                            <div class="card mb-3">
                                <div class="card-body">

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>Tanggal </label>
                                            <div class="input-group mb-3">
                                                <input type="text" name="tanggal_laporan" class="form-control datepicker" placeholder="Tentukan tanggal Laporan">
                                                <div class="input-group-append">
                                                    <button class="btn btn-info" type="button" id="btn-tampilkan">Tampilkan</button>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-6">
										 <label>&nbsp;</label>
										  <!-- <small id="msg-nip" class="form-text text-muted" style="display:none">*Ketik NIP, dan tekan tombol Enter</small> -->
										  <div id="msg-nip" class="valid-feedback" style="display: block;"></div>
										</div>
                                    </div>

                                </div>

                            </div>

                            <!-- <h5 class="card-title">Kesehatan</h5> -->
                            <div class="card mb-3 ">
                                <div class="card-body ">
                                    <div class="row form-group ">
                                        <div class="col-md-4 ">
                                            <label>Status Sehat</label>
                                            <br>
                                            <div class="custom-control custom-radio custom-control-inline ">
                                                <input type="radio" id="customRadioInline1" name="status_sehat_laporan" class="custom-control-input" value="YES">
                                                <label class="custom-control-label" for="customRadioInline1">SEHAT</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline ">
                                                <input type="radio" id="customRadioInline2" name="status_sehat_laporan" class="custom-control-input" value="NO">
                                                <label class="custom-control-label" for="customRadioInline2">SAKIT</label>
                                            </div>
                                        </div>
                                        <div id="kondisi_sakit_checkbox" class="col-md-8 d-none">
                                            <label>Kondisi Sakit</label>
                                            <br>
                                            <label class="checkbox-inline"><input type="checkbox" value="YES" name="kondisi_sakit_demam_laporan"> DEMAM </label>
											<label class="checkbox-inline"><input type="checkbox" value="YES" name="kondisi_sakit_batuk_laporan"> BATUK </label>
											<label class="checkbox-inline"><input type="checkbox" value="YES" name="kondisi_sakit_pilek_laporan"> PILEK </label>
											<label class="checkbox-inline"><input type="checkbox" value="YES" name="kondisi_sakit_tenggorokan_laporan"> TENGGOROKAN </label>
											<label class="checkbox-inline"><input type="checkbox" value="YES" name="kondisi_sakit_sesak_laporan"> SESAK NAFAS </label>
                                    </div>
                                    </div>

                                    <div id="FormSakit" class="row form-group">
                                        <div class="col-md-8 ">
                                            <label>Tindakan yang sudah dilakukan</label>
                                            <input type="text" class="form-control" name="keterangan_sakit_laporan" disabled>
                                        </div>
                                        <div class="col-md-4 ">
                                            <label>Lama Gejala</label>
                                            <div class="input-group mb-3">
											  <input type="number" class="form-control" name="lama_gejala_sakit_laporan" aria-describedby="basic-addon2">
											  <div class="input-group-append">
											    <span class="input-group-text" id="basic-addon2">Hari</span>
											  </div>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
		    </div>
		</div>
		
        
		<div class="row justify-content-center mt-3 d-none">
		    <div class="col-12 col-md-10">
		
		        <nav aria-label="breadcrumb " class="mt-3">
		            <ol class="breadcrumb bg-light">
	            	 	<li class="breadcrumb-item mr-auto small d-none">
		                    <span>*No.Ticket anda akan dikirim ke email anda.</span>
		                </li>
		                
		                <li class="breadcrumb-item ml-auto">
		                    <button id="btn-create" type="button" onclick="save();" class="btn btn-primary "><i class="fa fa-save"></i> SIMPAN</button>
		                </li>
		
		            </ol>
		
		            <form class="form-inline my-2 my-lg-0 d-none">
		                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		            </form>
		        </nav>
		    </div>
		</div>
	   
	    <!-- START THE FEATURETTES -->
	
	    <hr class="featurette-divider">
	
	    <!-- /END THE FEATURETTES -->
	
	</div>
	<%@include file="inc/footer.jsp"%>
	</main>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <!-- Select2 -->
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  	
	<!-- start -->
	<!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Sweetalert2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<!-- Moment JS, untuk function get date -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	
	<!-- Javascript -->
	<%-- <script src="${pageContext.request.contextPath}/js/libs/jquery-1.11.2.min.js"></script> --%>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>	
	<%-- <script src="${pageContext.request.contextPath}/js/libs/bootstrap.min.js"></script> --%>
	
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.backstretch.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.cookie.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.md5.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-datetimepicker.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/bootstrap-combobox.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/select2.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/jquery.selectric.js"></script>
	<script src="${pageContext.request.contextPath}/js/libs/mprogress.min.js"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/base64.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/js.cookie.js"></script>
	
	<!-- Tiny Mce -->
	<script src="${pageContext.request.contextPath}/js/libs/tinymce.min.js" referrerpolicy="origin"></script>
	
	
	<script src="${pageContext.request.contextPath}/js/common-new-general-script.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<!-- end -->
	<script src="${pageContext.request.contextPath}/js/user/laporan-script.js"></script>
  </body>
</html>