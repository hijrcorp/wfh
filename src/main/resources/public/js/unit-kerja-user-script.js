var url = ctx + "/user/unit/";
var urlUnitKerja = ctx + "/unit_kerja/";
var urlPosition = ctx + "/user/position/";
var listUser = {};

function init() {
	
	display(0);
	getUnitKerjaList();
	getUserList();
	getPositionList();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
}

function getPositionList(){
	$.getJSON(urlPosition, function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("#fPosition").append(new Option(value.name_position, value.position_id));
				$('#list-add-position').append('<li><a href="javascript:addUser('+value.position_id+')">'+value.name_position+'</a></li>');
			});
		}else{
			alert("Connection error");
		}
	});
}

function getUserList(){
	
	$.getJSON(url+'all', function(response) {
		if (response.status == '200') {
			listUser = response.data;
		}else{
			alert("Connection error");
		}
	});
}

function getUnitKerjaList(){
	var str = '';
	$.getJSON(urlUnitKerja+"list", function(response) {
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				str += '<li style="cursor: pointer;" data-id="'+value.id_unit_kerja+'" id="unitKerja'+value.id_unit_kerja+'" class="list-group-item" onclick="selectUnitKerja(\''+value.id_unit_kerja+'\');">'+value.nama_unit_kerja+'</li>';
			});
		}else{
			str = '<li class="list-group-item">Connection error..</li>'
		}
		
		$('#listUnitKerja').html(str);
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}

function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}



function add(){
	if($('.list-group-item.active').data('id') !== undefined){
		/*
		reset();
		$("#progressBar").hide();
		$("#passwordForm").show();
		$("#modalPengguna").modal('show');
		*/
	}else{
		alert("Anda belum memilih unit kerja auditor..");
	}
}

function addUser(pos){
	reset();
	$("#progressBar").hide();
	filterUser(pos);
	$("#modalPengguna").modal('show');
}

function filterUser(selPosition){
	
	$("#fUser").text('');
	$("#fUser").append(new Option('-- Pilih User --', ''));
	$.each(listUser, function(key, user) {
		//console.log(value);
		var found = false;
		upper = user;
		$('.anal').each(function(idx, el) {
			if(upper.id == el.value){
				found = true;
			}
		});
		if(found == false){
			if(selPosition == user.positionId){
				$("#fUser").append(new Option(user.firstName + ' ' +user.lastName, user.id));
			}
			
		}
		
	});
}

function validate(){
	var msg = '';
	
	if($('#password').val() != $('#repassword').val()){
		msg += 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	// if save existing record (EDIT/UPDATE)
	if ($('#id').val() == '0'){
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}else{
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}	
	return msg;
}

function save(){
		if($('.list-group-item.active').data('id') !== undefined){
			
			if($("#fUser").val() != '') {
				$.ajax({
			        url : url+$('.list-group-item.active').data('id')+"/save",
			        type : "POST",
			        traditional : true,
			        data : "userId="+$("#fUser").val(),
			        success : function (response) {
			        		$("#modalPengguna").modal('hide');
				        	display($('.list-group-item.active').data('id'));
			        },
			        error : function (response) {
			        		alert("Connection error");
			        },
			    });
			}
		
		}
}

function reset(){
	cleanMessage('msg');
	
	$("#fUser").val('');
	$("#fPosition").val('');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	//console.log(id);
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function doRemove(id){
	//console.log(id);
	if($('.list-group-item.active').data('id') !== undefined){
		
		$.ajax({
	        url : url+$('.list-group-item.active').data('id')+"/remove",
	        type : "POST",
	        traditional : true,
	        data : "userId="+id,
	        success : function (response) {
	        		$("#modalconfirm").modal('hide');
		        	display($('.list-group-item.active').data('id'));
	        },
	        error : function (response) {
	        		alert("Connection error");
	        },
	    });
	
	}
	
}

function edit(id){
	reset();
	$("#progressBar").show();
	$("#passwordForm").hide();
	$("#modalPengguna").modal('show');
	
	$.getJSON(url+id+"?token="+token, function(response, status) {
		$("#progressBar").hide();
    	if (response.code == '200') {
    		var o = response.data;
    		$('#id').val(o.id);
    		$('#first_name').val(o.firstName);
    		$('#last_name').val(o.lastName);
    		$('#username').val(o.username);
    		$('#position').val(o.position.id);
    		$('#description').val(o.description);
    		
    		
		}else{
			alert("Connection error");
		}
	});
	

	
}

function editPassword(id){
	$("#progressBar2").hide();
	$("#btnUpdate").data("id",id);
	$("#modalPassword").modal('show');
}

function doUpdate(id){
	var msg = '';
	
	if($('#password1').val() == '' || $('#repassword1').val() == ''){
		msg = 'Kata kunci tidak boleh kosong';
	}
	
	if($('#password1').val() != $('#repassword1').val()){
		msg = 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	if (msg == '') {
		$("#progressBar2").show();
		
		var obj;
		
		$.getJSON(url+id+"?token="+token, function(response, status) {
			
	    	if (response.code == '200') {
	    		var user = response.data;
	    		
	    		user.expired = new Date(user.expired);
	    		user.currentPassword = $('#password1').val();
	    		obj = {"user" :  user };
	    		
	    		$.ajax({
	    	        url : url+"save?token="+token,
	    	        type : "POST",
	    	        traditional : true,
	    	        contentType : "application/json",
	    	        dataType : "json",
	    	        data : JSON.stringify(obj),
	    	        success : function (response) {
	    	        	if (response.code == '200') {
	    	        		$("#progressBar2").hide();
	    	        		$("#modalPassword").modal('hide');
	    	        		reset();
	    				}else{
	    					addAlert('msg1', "alert-danger", msg + 'Gagal Simpan. Error tidak diketahui');
	    					
	    				}
	    	        },
	    	        error : function (response) {
	    	        	alert("Connection error");
	    	        },
	    	    });
			}else{
				alert("Connection error");
			}
		});		
	}else{
		addAlert('msg1', "alert-danger", msg);
	}
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(unitKerjaId){

	
	
	var tbody = $('#tbl').find($('tbody'));

	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	$.ajax({
        url : url+unitKerjaId,
        type : "GET",
        traditional : true,
        data : "",
        success : function (response) {
        	console.log(response);
        	if (response.status == '200') {
        		var row = '';
        		tbody.text('');
        		var i = 1;
        		
        		$.each(response.data, function(key, value) {
        			
        			row += '<tr>';
        			row += '<td scope="row">'+i+'<input type="hidden" class="anal" value="'+value.id+'"></td>';
        			
        			row += '<td>'+value.firstName+' '+value.lastName+'</td>';        			
        			row += '<td>'+value.email+'</td>';
        			row += '<td>'+value.positionName+'</td>';
    				
    				row += '<td><button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
    				row += '</td>';
        			
        			row += '</tr>';
        			
        			i++;
        		});
        		if(row == ''){
        			tbody.text('');
        			if(unitKerjaId > 0) {
        				tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
        			}else{
        				tbody.append('<tr><td colspan="10" align="center">-- Anda belum memilih unit kerja auditor --</td></tr>');
        			}
        		}else{
        			tbody.html(row);
        		}
        		//createPagination('tblpages','display', response.pagination);
			}else{
				alert("Connection error");
			}
        },
        error : function (response) {
        	alert("Connection error");
        },
    });
}


function selectUnitKerja(id){
	$('.list-group-item').removeClass('active');
	$('#unitKerja'+id).addClass('active');
	display(id);
}

function removeAnalis(id){
	
}
