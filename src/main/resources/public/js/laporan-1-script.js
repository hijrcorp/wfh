var url = ctx + "/laporan/";
var selected_id=0;
function init() {
	
	display(1);
	getUnitKerja();
	
    $("#btnDownload").click(function(e){ download(); });
	
	$('[name=tipe_unit_kerja]').change(function(){
		if($(this).val() == 'SATUAN') {
			$('[name=id_unit_kerja]').removeAttr('disabled');
		} else {
			$('[name=id_unit_kerja]').attr('disabled', 'disabled');
		}
	});
}

function search(){
	display(1);
}

function getUnitKerja(){
	$.getJSON(ctx+'/ref/unit-kerja/list', function(response) {
//		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=id_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}

function download(){
    location = url+'download?id=1&id_unit_kerja='+$('[name=id_unit_kerja]').val()+'&periode_awal='+getDateForInputCostum($('[name=periode_awal]').val())+'&periode_akhir='+getDateForInputCostum($('[name=periode_akhir]').val());
}

function refresh(){
	$("[name=id_unit_kerja]").val('');
	$("[name=periode_awal]").val('');
	$("[name=periode_akhir]").val('');
	var page = $('#btnRefresh').data("id");
	display(page);
}

function display(page = 1, limit = 50){

    var tbody = $('#tbl').find($('tbody'));
    var tfoot = $('#tbl').find($('tfoot'));
    $('#btnRefresh').data("id",page);
    
    if(page == 1) tbody.text('');
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

    ajaxGET(url+'?page='+page+'&limit='+limit+'&id=1&id_unit_kerja='+$('[name=id_unit_kerja]').val()+'&periode_awal='+getDateForInputCostum($('[name=periode_awal]').val())+'&periode_akhir='+getDateForInputCostum($('[name=periode_akhir]').val()), function(response){
        console.log(response);
        if (response.code == 200) {
            var rowBody = '';
            var rowFoot = '';
            var rowNext = '';
            var i = 1;
            console.log(response.data);
            $('[id=table-loading], [id=more-button-row]').remove();
            $.each(response.data, function(key, value) {
            	rowBody += "<tr scope='row'>";
	            	rowBody += '<td>'+(((page-1)*limit)+i)+'</td>';
	            	rowBody += '<td nowrap>'+getDateForList(value.tanggalLaporan)+'</td>';
	            	rowBody += '<td id="kolom-4">'+value.jumlahPegawai+'</td>';
	            	rowBody += '<td id="kolom-5">'+value.jumlahMelaporPekerjaan+'</td>';
	            	rowBody += '<td id="kolom-6">'+value.jumlahTidakMelaporPekerjaan+'</td>';
	            	rowBody += '<td id="kolom-7">'+value.jumlahPersenMelaporPekerjaan+'%</td>';
	            	rowBody += '<td id="kolom-8">'+value.jumlahTusiLaporan+'</td>';
	            	rowBody += '<td id="kolom-9">'+value.jumlahPersenTusiLaporan+'%</td>';
	            	rowBody += '<td id="kolom-10">'+value.jumlahTambahanLaporan+'</td>';
	            	rowBody += '<td id="kolom-11">'+value.jumlahPersenTambahanLaporan+'%</td>';
            	rowBody += '</tr>';
                i++;
            });
            if(response.next_more){
                rowNext += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
                rowNext += '    <button type="button" class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
                rowNext += '</div></td></tr>';
            }
            if(rowBody == ''){
                tbody.text('');
                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
                tfoot.text('');
            }else{
                if(page == 1) tbody.html(rowBody);
                else tbody.html(tbody.html()+rowBody);
                var kolom4 = 0; $('[id=kolom-4]').each(function(key, value){ kolom4 += parseFloat($(this).text()) });
                var kolom5 = 0; $('[id=kolom-5]').each(function(key, value){ kolom5 += parseFloat($(this).text()) });
                var kolom6 = 0; $('[id=kolom-6]').each(function(key, value){ kolom6 += parseFloat($(this).text()) });
                var kolom7 = 0; $('[id=kolom-7]').each(function(key, value){ kolom7 += parseFloat($(this).text()) });
                var kolom8 = 0; $('[id=kolom-8]').each(function(key, value){ kolom8 += parseFloat($(this).text()) });
                var kolom9 = 0; $('[id=kolom-9]').each(function(key, value){ kolom9 += parseFloat($(this).text()) });
                var kolom10 = 0; $('[id=kolom-10]').each(function(key, value){ kolom10 += parseFloat($(this).text()) });
                var kolom11 = 0; $('[id=kolom-11]').each(function(key, value){ kolom11 += parseFloat($(this).text()) });
                var count = tbody.find('tr').length;
                rowFoot += "<tr scope='row'>";
	                rowFoot += '<td nowrap class="text-left" colspan="2">'+($('[name=id_unit_kerja]').val()==''?'Total':'Jumlah')+' Rata-Rata</td>';
	                rowFoot += '<td>'+(kolom4/count).toFixed(0)+'</td>';
	                rowFoot += '<td>'+(kolom5/count).toFixed(0)+'</td>';
	                rowFoot += '<td>'+(kolom6/count).toFixed(0)+'</td>';
	                rowFoot += '<td>'+(kolom7/count).toFixed(2)+'%</td>';
	                rowFoot += '<td>'+(kolom8/count).toFixed(0)+'</td>';
	                rowFoot += '<td>'+(kolom9/count).toFixed(2)+'%</td>';
	                rowFoot += '<td>'+(kolom10/count).toFixed(0)+'</td>';
	                rowFoot += '<td>'+(kolom11/count).toFixed(2)+'%</td>';
	                rowFoot += '</tr>';
	            tfoot.html(rowFoot+rowNext);
            }
        }else{
            alert("Connection error!!");
        }
        }
    );
}
