var url = ctx_lapgas + "/surat-tugas/";
var url_unit_auditi = ctx_lapgas + "/ref/unit-auditi/";
var url_user_responden = ctx_lapgas + "/surat-tugas-user-responden/";
var url_responden = ctx + "/responden/";
var url_sesi = ctx + "/sesi/";
var url_dashboard = ctx + "/dashboard/";

var selected_status='';

var param = "";
var selected_id='';
function init() {
	selected_status = ($.urlParam('status') == null ? '' : $.urlParam('status'));
	getDashboard();
	if(selected_status != "") getSuratTugasList();
	renderChart();
}

function to_persen(nilai){
	return((nilai)*100)+'%';
}

function renderChart(){
	var ctx = document.getElementById('myChart').getContext('2d');
	var obj_chart = [];
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = tahun; i <= now; i++) {
		$("[name=tahun]").append(new Option(i));
		obj_chart.push({"tahun" : i, "responden" : 0});
	}
	
	$.getJSON(url_dashboard+'list_tahun', function(response_dashboard) {
		console.log("response_dashboard", response_dashboard);
		console.log(obj_chart);
		if (response_dashboard.code == 200) {
			var arr_tahun=[];
			var arr_tahun_data=[];
			var arr_tahun_data_persen=[];
			var count_responden=0;
			$.each(obj_chart, function(key_chart, val_chart){
				$.each(response_dashboard.data, function(key, value_dashboard){
					if(value_dashboard.tahun_sesi == val_chart.tahun){
						obj_chart[key_chart].responden=value_dashboard.jumlah_responden
						count_responden+=parseInt(value_dashboard.jumlah_responden);
					}
				});
				arr_tahun_data.push(obj_chart[key_chart].responden);
				arr_tahun_data_persen.push(to_persen(obj_chart[key_chart].responden));
				arr_tahun.push(val_chart.tahun);
			});

			console.log("arr_tahun_data ",arr_tahun_data);
			console.log("arr_tahun_data_persen ",arr_tahun_data_persen);
			console.log("arr_tahun ",arr_tahun);
			console.log("count_responden ", count_responden);
			$('#total_responden').text(count_responden);
			
//			var ctx = document.getElementById("myPieChart");
//			var myPieChart = new Chart(ctx, {
//			  type: 'pie',
//			  data: {
//			    labels: ["Laki laki:<?php echo $jumlah_pria; ?>","Perempuan:<?php echo $jumlah_perempuan; ?>"],
//			    datasets: [{
//			    label: '',
//			      data: [<?php echo $tampilkan['jumlah_pria']; ?>, <?php echo $tampilkan['jumlah_perempuan']; ?>],
//			      backgroundColor: ['#007bff', '#dc3545'],
//			    }],
//			  },
//			});
			
			var chart = new Chart(ctx, {
			    // The type of chart we want to create
			    type: 'bar',
			    // The data for our dataset
			    data: {
			        labels: arr_tahun,//['2017', '2018', '2019', 'Ya'],
			        datasets: [{
			            label: 'Responden',
			            backgroundColor: 'rgba(75, 203, 115, 1)',
			            borderColor: 'rgb(255, 99, 132)',
			            data: arr_tahun_data//[3, 27, 84, 1]
			        }]
			    },
			    // Configuration options go here
			    options: {
			        legend: {
			            display: false,
			            labels: {
			                fontColor: 'rgb(255, 99, 132)'
			            }
			        }
			    }
			});
		}
	});
}
function getListTahun() {
	var d = new Date();
	var now = d.getFullYear();
	var tahun = now - 5;
	for (var i = now; i >= tahun; i--) {
		$("[name=tahun]").append(new Option(i));
	}
}
function getSuratTugasList(){
	var tbody=$("#tbl-data-responden");
	var row="";
	$.getJSON(url+'list', function(response_surat_tugas) {
		console.log("surat_tugas", response_surat_tugas);
		if (response_surat_tugas.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list?filter_status='+selected_status, function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							$.each(response_surat_tugas.data, function(key, value_surat_tugas) {
								$.each(response_user_responden.data, function(key, value_user_responden) {
									var id_responden="";
									var status_responden="";
									$.each(response_responden.data, function(key, value_responden) {
										if(value_surat_tugas.id_surat_tugas==value_responden.surat_tugas_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
											id_responden= "data-responden="+value_responden.id_responden;
											status_responden=value_responden.status_responden;
										}
									});
/*//									if(status_responden=="SELESAI"){
//										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
//											row += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" \'> '+value_surat_tugas.nomor_surat_tugas+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
//									}else{
////										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
////											row += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
//									}
*/									
									if(status_responden=="SELESAI"){
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											row += '<tr>';
										row += '<td>'+(key+1)+'</td>';
										row += '<td>'+value_user_responden.user_id+'</td>';
										row += '<td>'+value_surat_tugas.nomor_surat_tugas+'</td>';
										row += '<td><a href="#">detil</a></td>';
											row += '</tr>';	
									}else if(status_responden=="PROSES"){
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											row += '<tr>';
										row += '<td>'+(key+1)+'</td>';
										row += '<td>'+value_user_responden.user_id+'</td>';
										row += '<td>'+value_surat_tugas.nomor_surat_tugas+'</td>';
										row += '<td><a href="#">detil</a></td>';
											row += '</tr>';	
									}
								});
							});
							tbody.append(row);
						}
					});
					
				}
			});
		}
	});
}
function getDashboard(){
	var tbody=$("#tbl-data");
	var row="";
	//setLoad('wraper-surat-tugas');
	$.getJSON(url_unit_auditi+'list', function(response_unit_auditi) {
		console.log("response_unit_auditi", response_unit_auditi);
		if (response_unit_auditi.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list', function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							var new_arr=[];
							$.each(response_unit_auditi.data, function(key, value_unit_auditi) {
								var new_obj={"id" : null, "unit_auditi" : null, "responden" : 0, "tidak_respon" : 0, "sedang_proses" : 0, "selesai" : 0};
								
								new_obj.id=value_unit_auditi.id_unit_auditi;
								new_obj.unit_auditi=value_unit_auditi.nama_unit_auditi;

								var count_responden=0;
								var count_responden_tidak_respon=0;
								var count_responden_sedang_respon=0;
								var count_responden_selesai_respon=0;
								$.each(response_user_responden.data, function(key, value_user_responden) {
									if(value_unit_auditi.id_unit_auditi == value_user_responden.surat_tugas_tim.id_unit_auditi){
									
										var id_responden="";
										var status_responden="";
										$.each(response_responden.data, function(key, value_responden) {
											if(value_user_responden.user_id == value_responden.user_id_responden){
												count_responden_tidak_respon++;
												new_obj.tidak_respon=count_responden_tidak_respon;
												if(value_responden.status_responden=="PROSES") {
													count_responden_sedang_respon++;
													new_obj.sedang_proses=count_responden_sedang_respon;
												}
												if(value_responden.status_responden=="SELESAI") {
													count_responden_selesai_respon++;
													new_obj.selesai=count_responden_selesai_respon;
												}
											}
											//if(status.equals("SELESAI")){
												//out.print("<li onclick='setVal(this);' data-id="+su.getIdSuratTugas()+" class='list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"'>"+su.getNomorSuratTugas()+"<span class='badge badge-success badge-pill'>"+status+"</span></li>");
											//}else{
											//myvar += '<li onclick=\'setVal(this);\' data-id='+value.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"\'>'+value.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>"+status+"</span></li>';
											//}
											
											/*if(value_surat_tugas.id_surat_tugas==value_responden.surat_tugas_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
												id_responden= "data-responden="+value_responden.id_responden;
												status_responden=value_responden.status_responden;
											}*/
											
											
										});
										
									/*	if(status_responden=="SELESAI"){
											if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
												myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" disabled\'>'+value_surat_tugas.nomor_surat_tugas+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
										}else{
											if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
												myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
										};*/
										

										count_responden++;
										new_obj.responden=count_responden;
									}
								});
								new_arr.push(new_obj);
							});

							var no=1;
							$.each(new_arr, function(key){
								if(this.responden!=0){
									row +='<tr>';
										row +='<td width="1" class="text-center">'+no+'</td>';
										row +='<td><a href="'+ctx+'/admin/view-detil-index?status=&unit_auditi='+this.id+'">'+this.unit_auditi+'</a></td>';
										row +='<td class="text-center">'+this.responden+'</td>';
										row +='<td class="text-center">'+(this.responden-this.tidak_respon)+'</td>';
										row +='<td class="text-center"><a href="'+ctx+'/admin/index?status=PROSES">'+this.sedang_proses+'</a></td>';
										row +='<td class="text-center"><a href="'+ctx+'/admin/index?status=SELESAI">'+this.selesai+'</a></td>';
									
									row +='</tr>';
									no++;
								}
							});
							console.log(new_arr);
							tbody.append(row);
							
						}
					});
					
				}
			});
		}
	});
}

function save(){
	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	if($(".list-group .list-group-item.active").data('responden') != undefined) obj.append('id', $(".list-group .list-group-item.active").data('responden'));
	
	if($(".list-group .list-group-item.active").length > 0)
	obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
    console.log(obj);
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
	window.location.href=ctx+"/page/survey?id="+response.data.id_responden+"&kegiatan_pengawasan_id="+$('[name=jenis_audit_id]').val();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", "kesalahan dari server");
	if(response.responseJSON.code=="409")
	alert(response.responseJSON.message)
	else alert("Surat tugas belum dipilih.");
}

//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

