var url = ctx + "/pekerjaan/";
var selected_id=0;
var list_data = [];
function init() {
	
	display(1);
	getUnitKerja();
	
    $("#btnDownload").click(function(e){ download(); });
	
	$('[name=tipe_unit_kerja]').change(function(){
		if($(this).val() == 'SATUAN') {
			$('[name=id_unit_kerja]').removeAttr('disabled');
		} else {
			$('[name=id_unit_kerja]').attr('disabled', 'disabled');
		}
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function search(){
	display(1);
}

function getUnitKerja(){
	$.getJSON(ctx+'/ref/unit-kerja/list', function(response) {
//		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=id_unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}

function download(){
	location = ctx+'/export/detil-kinerja?id=1&nip_pegawai='+$('[name=nip_pegawai]').val()+'&id_unit_kerja='+$('[name=id_unit_kerja]').val()+'&periode_awal='+getDateForInputCostum($('[name=periode_awal]').val())+'&periode_akhir='+getDateForInputCostum($('[name=periode_akhir]').val())+"&nama_unit_kerja="+$('[name=id_unit_kerja] option:selected').text();
}

function refresh(){
	$("[name=id_unit_kerja]").val('');
	$("[name=periode_awal]").val('');
	$("[name=periode_akhir]").val('');
	$('#form-filter').trigger('reset');
	var page = $('#btnRefresh').data("id");
	display(page);
}

function display(page = 1, limit = 50){
	console.log(page);
	    var tbody = $('#tbl').find($('tbody'));
	    $('#btnRefresh').data("id",page);
	    
	    if(page == 1) { tbody.text(''); list_data = [] }
	    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

	    ajaxGET(url+'list/detil?page='+page+'&limit='+limit+'&nip_pegawai='+$('[name=nip_pegawai]').val()+'&id_unit_kerja='+$('[name=id_unit_kerja]').val()+'&periode_awal='+getDateForInputCostum($('[name=periode_awal]').val())+'&periode_akhir='+getDateForInputCostum($('[name=periode_akhir]').val()), function(response){
	    	console.log(response);
	        if (response.code == 200) {
	            var rowBody = '';
	            var rowFoot = '';
	            var rowNext = '';
	            var i = 1;
	            console.log(response.data);
	            list_data = list_data.concat(response.data);
	            $('[id=table-loading], [id=more-button-row]').remove();
	            $.each(groupByArray(list_data, 'nip_pegawai'), function(keyGroup, valueGroup) {
	            	$.each(valueGroup.values, function(key, value) {
		                rowBody += "<tr scope='row'>";
		            	if(key==0) {
		                	rowBody += '<td>'+(keyGroup+1)+'</td>';
		                	rowBody += '<td class="text-left">'+value.nip_pegawai+'</td>';
		                    rowBody += '<td class="text-left">'+value.nama_pegawai+'</td>';
		                    rowBody += '<td class="text-left">'+value.nama_unit_kerja+'</td>';
		                    rowBody += '<td class="text-left">'+value.nama_jabatan+'</td>';
	                	} else { rowBody += '<td></td><td></td><td></td><td></td><td></td>'; }
		                    
		                    rowBody += '<td class="text-left">'+getDateForList(value.tanggal, '-')+'</td>';
		                    rowBody += '<td class="text-left">'+(getValue(value.status_tusi)=='YES'?'TUSI':'TAMBAHAN')+'</td>';
		                    rowBody += '<td class="text-right">'+getValue(value.nama)+'</td>';
		                    rowBody += '<td class="text-left">'+getValue(value.uraian)+'</td>';
		                    rowBody += '<td class="text-left">'+getValue(value.output)+'</td>';
		                    //
		                    rowBody += '<td class="text-center">';
		                    rowBody += '<a href="javascript:void(0)" class="btn btn-danger btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash "></i></a>';
		                	rowBody += '</td>';
		                    //
		                rowBody += '</tr>';
		                i++; 
	                });
	            });
	            if(response.next_more){
	            	rowNext += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
	            	rowNext += '    <button type="button" class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
	                rowNext += '</div></td></tr>';
	            }
	            if(rowBody == ''){
	                tbody.text('');
	                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
	            }else{
	            	
	            	tbody.html(rowBody+rowNext);
	            }
	        }else{
	            alert("Connection error!!");
	        }
	        }
	    );
	}
var selected_action="";
var selected_id="";
function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		ajaxGET(url+id+'?'+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		ajaxPOST(url+id+'/'+selected_action+'?',{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$("#passwordForm").hide();
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'editPassword'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	/*
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	*/
	//$.LoadingOverlay("hide");
	//stopLoading('btn-save', 'Simpan');
	//stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	//showAlertMessage(response.message, 1500);
}
