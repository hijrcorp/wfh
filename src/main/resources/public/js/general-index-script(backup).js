var url = ctx_lapgas + "/surat-tugas/";
var url_user_responden = ctx_lapgas + "/surat-tugas-user-responden/";
var url_responden = ctx + "/responden/";

var param = "";
var selected_id='';
function init() {
//	display(1);
	//renderDisplay(1, '', 0);
	//getListTahun();
	getSuratTugasList();
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#form-surat-tugas').submit(function(e){
		save();
		e.preventDefault();
	});
	
}

function setVal(elem){
	var ids=$(elem).data('id');
//	$(elem).toggle('active');
	$(".list-group .list-group-item").removeClass("active");
	$(elem).addClass("active");
}

function getSuratTugasList(){
	var body=$("#wraper-surat-tugas");
	var myvar="";
	//setLoad('wraper-surat-tugas');
	$.getJSON(url+'list', function(response_surat_tugas) {
		console.log("surat_tugas", response_surat_tugas);
		if (response_surat_tugas.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list', function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							$.each(response_surat_tugas.data, function(key, value_surat_tugas) {
								$.each(response_user_responden.data, function(key, value_user_responden) {
									var id_responden="";
									var status_responden="";
									$.each(response_responden.data, function(key, value_responden) {
										$('[name=user_id]').val(value_user_responden.user_id);
										$('[name=jenis_audit_id]').val(value_surat_tugas.id_kegiatan_pengawasan);

										//if(status.equals("SELESAI")){
											//out.print("<li onclick='setVal(this);' data-id="+su.getIdSuratTugas()+" class='list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"'>"+su.getNomorSuratTugas()+"<span class='badge badge-success badge-pill'>"+status+"</span></li>");
										//}else{
										//myvar += '<li onclick=\'setVal(this);\' data-id='+value.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"\'>'+value.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>"+status+"</span></li>';
										//}
										if(value_surat_tugas.id_surat_tugas==value_responden.surat_tugas_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
											id_responden= "data-responden="+value_responden.id_responden;
											status_responden=value_responden.status_responden;
										}
										
										
									});
									if(status_responden=="SELESAI"){
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" disabled\'>'+value_surat_tugas.nomor_surat_tugas+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
									}else{
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
									}
									
								});
							});
							body.append(myvar);
						}
					});
					
				}
			});
		}
	});
}

function save(){
	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	if($(".list-group .list-group-item.active").data('responden') != undefined) obj.append('id', $(".list-group .list-group-item.active").data('responden'));
	
	if($(".list-group .list-group-item.active").length > 0)
	obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
    console.log(obj);
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
	window.location.href=ctx+"/page/survey?id="+response.data.id_responden+"&kegiatan_pengawasan_id="+$('[name=jenis_audit_id]').val();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", "kesalahan dari server");
	if(response.responseJSON.code=="409")
	alert(response.responseJSON.message)
	else alert("Surat tugas belum dipilih.");
}

//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

