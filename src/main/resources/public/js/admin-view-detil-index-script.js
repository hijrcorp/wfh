var url = ctx_lapgas + "/surat-tugas/";
var url_unit_auditi = ctx_lapgas + "/ref/unit-auditi/";
var url_user_responden = ctx_lapgas + "/surat-tugas-user-responden/";
var url_responden = ctx + "/responden/";

var selected_status='';
var selected_satker='';

var param = "";
var selected_id='';
function init() {
	selected_status = ($.urlParam('status') == null ? '' : $.urlParam('status'));
	selected_satker = ($.urlParam('unit_auditi') == null ? '' : $.urlParam('unit_auditi'));
	//getDashboard();
	//if(selected_status != "") 
		//getSuratTugasList();
	if(selected_satker != "") $('#label_satker').text(selected_satker)
	else $('#label_satker').html("<strong>Semua Satker</strong>");
	renderDisplay(1, '');
	
	//
//	var ctx = document.getElementById('myChart').getContext('2d');
//	var chart = new Chart(ctx, {
//	    // The type of chart we want to create
//	    type: 'horizontalBar',
//
//	    // The data for our dataset
//	    data: {
//	        labels: ['Sangat Baik', 'Baik', 'Cukup', 'Kurang', 'Sangat Kurang'],
//	        datasets: [{
//	            label: 'Jawaban',
//	            backgroundColor: 'rgb(255, 99, 132)',
//	            borderColor: 'rgb(255, 99, 132)',
//	            data: [10, 5, 2, 20, 30]
//	        }]
//	    },
//
//	    // Configuration options go here
//	    options: {}
//	});
}

function getSuratTugasList(){
	var tbody=$("#tbl-data-responden");
	var row="";
	$.getJSON(url+'list', function(response_surat_tugas) {
		console.log("surat_tugas", response_surat_tugas);
		if (response_surat_tugas.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list?filter_status='+selected_status, function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							$.each(response_surat_tugas.data, function(key, value_surat_tugas) {
								$.each(response_user_responden.data, function(key, value_user_responden) {
									var id_responden="";
									var status_responden="";
									$.each(response_responden.data, function(key, value_responden) {
										if(value_surat_tugas.id_surat_tugas==value_responden.surat_tugas_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
											id_responden= "data-responden="+value_responden.id_responden;
											status_responden=value_responden.status_responden;
										}
									});
/*//									if(status_responden=="SELESAI"){
//										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
//											row += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" \'> '+value_surat_tugas.nomor_surat_tugas+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
//									}else{
////										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
////											row += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
//									}
*/									
									if(status_responden=="SELESAI"){
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											row += '<tr>';
										row += '<td>'+(key+1)+'</td>';
										row += '<td>'+value_user_responden.user_id+'</td>';
										row += '<td>'+value_surat_tugas.nomor_surat_tugas+'</td>';
										row += '<td><a href="#">detil</a></td>';
											row += '</tr>';	
									}else if(status_responden=="PROSES"){
										if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
											row += '<tr>';
										row += '<td>'+(key+1)+'</td>';
										row += '<td>'+value_user_responden.user_id+'</td>';
										row += '<td>'+value_surat_tugas.nomor_surat_tugas+'</td>';
										row += '<td><a href="#">detil</a></td>';
											row += '</tr>';	
									}
								});
							});
							tbody.append(row);
						}
					});
					
				}
			});
		}
	});
}
function getDashboard(){
	var tbody=$("#tbl-data");
	var row="";
	//setLoad('wraper-surat-tugas');
	$.getJSON(url_unit_auditi+'list', function(response_unit_auditi) {
		console.log("response_unit_auditi", response_unit_auditi);
		if (response_unit_auditi.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					$.getJSON(url_responden+'list', function(response_responden) {
						console.log("responden", response_responden);
						if (response_responden.code == 200) {
							var new_arr=[];
							$.each(response_unit_auditi.data, function(key, value_unit_auditi) {
								var new_obj={"id" : null, "unit_auditi" : null, "responden" : 0, "tidak_respon" : 0, "sedang_proses" : 0, "selesai" : 0};
								
								new_obj.id=value_unit_auditi.id_unit_auditi;
								new_obj.unit_auditi=value_unit_auditi.nama_unit_auditi;

								var count_responden=0;
								var count_responden_tidak_respon=0;
								var count_responden_sedang_respon=0;
								var count_responden_selesai_respon=0;
								$.each(response_user_responden.data, function(key, value_user_responden) {
									if(value_unit_auditi.id_unit_auditi == value_user_responden.surat_tugas_tim.id_unit_auditi){
									
										var id_responden="";
										var status_responden="";
										$.each(response_responden.data, function(key, value_responden) {
											if(value_user_responden.user_id == value_responden.user_id_responden){
												count_responden_tidak_respon++;
												new_obj.tidak_respon=count_responden_tidak_respon;
												if(value_responden.status_responden=="PROSES") {
													count_responden_sedang_respon++;
													new_obj.sedang_proses=count_responden_sedang_respon;
												}
												if(value_responden.status_responden=="SELESAI") {
													count_responden_selesai_respon++;
													new_obj.selesai=count_responden_selesai_respon;
												}
											}
											//if(status.equals("SELESAI")){
												//out.print("<li onclick='setVal(this);' data-id="+su.getIdSuratTugas()+" class='list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"'>"+su.getNomorSuratTugas()+"<span class='badge badge-success badge-pill'>"+status+"</span></li>");
											//}else{
											//myvar += '<li onclick=\'setVal(this);\' data-id='+value.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center"+disab+"\'>'+value.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>"+status+"</span></li>';
											//}
											
											/*if(value_surat_tugas.id_surat_tugas==value_responden.surat_tugas_id_responden && value_user_responden.user_id==value_responden.user_id_responden) {
												id_responden= "data-responden="+value_responden.id_responden;
												status_responden=value_responden.status_responden;
											}*/
											
											
										});
										
									/*	if(status_responden=="SELESAI"){
											if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
												myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center" disabled\'>'+value_surat_tugas.nomor_surat_tugas+'<span class="badge badge-success badge-pill">'+status_responden+'</span></li>';	
										}else{
											if(value_surat_tugas.id_surat_tugas==value_user_responden.surat_tugas_id)
												myvar += '<li '+id_responden+' onclick=\'setVal(this);\' data-id='+value_surat_tugas.id_surat_tugas+' class=\'list-group-item list-group-item-action  d-flex justify-content-between align-items-center\'>'+value_surat_tugas.nomor_surat_tugas+'<span class=\'badge badge-warning badge-pill\'>'+status_responden+'</span></li>';
										};*/
										

										count_responden++;
										new_obj.responden=count_responden;
									}
								});
								new_arr.push(new_obj);
							});

							var no=1;
							$.each(new_arr, function(key){
								if(this.responden!=0){
									row +='<tr>';
										row +='<td width="1" class="text-center">'+no+'</td>';
										row +='<td><a href="'+ctx+'/admin/index?status=">'+this.unit_auditi+'</a></td>';
										row +='<td class="text-center">'+this.responden+'</td>';
										row +='<td class="text-center">'+(this.responden-this.tidak_respon)+'</td>';
										row +='<td class="text-center"><a href="'+ctx+'/admin/index?status=PROSES">'+this.sedang_proses+'</a></td>';
										row +='<td class="text-center"><a href="'+ctx+'/admin/index?status=SELESAI">'+this.selesai+'</a></td>';
									
									row +='</tr>';
									no++;
								}
							});
							console.log(new_arr);
							tbody.append(row);
							
						}
					});
					
				}
			});
		}
	});
}

function save(){
	var obj = new FormData(document.querySelector('#form-surat-tugas'));
	if($(".list-group .list-group-item.active").data('responden') != undefined) obj.append('id', $(".list-group .list-group-item.active").data('responden'));
	
	if($(".list-group .list-group-item.active").length > 0)
	obj.append('surat_tugas_id', $(".list-group .list-group-item.active").data('id'));
    console.log(obj);
    ajaxPOST(url_responden + 'save',obj,'onSaveSuccess','onSaveError');
}

function onSaveSuccess(response){
//	$("#progressBar").hide();
//	$("#modalPengguna").modal('hide');
//	refresh();
//	reset();
	window.location.href=ctx+"/page/survey?id="+response.data.id_responden+"&kegiatan_pengawasan_id="+$('[name=jenis_audit_id]').val();
}
function failSav(response){
	addAlert('msg', "alert-danger", response.message);
}
function onSaveError(response){
	console.log(response);
	addAlert('msg', "alert-danger", "kesalahan dari server");
	if(response.responseJSON.code=="409")
	alert(response.responseJSON.message)
	else alert("Surat tugas belum dipilih.");
}

//batas


function setLoad(id){
	$('#'+id).html('<div class="loading-row list-group-item text-center"><img width="20" src="'+ctx+'/img/loading-spinner.gif"/> Mohon Tunggu..</div>');
}

function renderDisplay(page, params = '', timeout=0){
	$.getJSON(ctx_lha + "/rest/unitAuditi/"+selected_satker, function(response_unit_audti) {
		if(response_unit_audti.status=="OK"){
			console.log(response_unit_audti);
			 $('#label_satker').text(response_unit_audti.data.namaUnitAuditi);
		}else{
			window.history.back();
		}
	});
	var tbody = $("#table-card-body");
//	setLoad(tbody);
//	params+="header_responden="+selected_id+"&kegiatan_pengawasan_id="+kegiatan_pengawasan_id+"&filter_status=1";
	params+="filter_status=1&filter_unit_auditi="+selected_satker;
//	limit=1;
//	if($.urlParam('confirm')=="true") 
	limit=100000;
//	if($.urlParam('confirm')=="finish") 
//	page=selected_page;
	
	//setTimeout(() => ajaxGET(ctx + "/pernyataan-penetapan/" + 'list-aspek-kegiatan?page='+page+'&limit='+limit+'&'+params,'onGetListSuccess','onGetListError'), timeout);
	//

	$.getJSON(url+'list', function(response_surat_tugas) {
		console.log("surat_tugas", response_surat_tugas);
		if (response_surat_tugas.code == 200) {
			$.getJSON(url_user_responden+'list', function(response_user_responden) {
				console.log("user_responden", response_user_responden);
				if (response_user_responden.code == 200) {
					//set title

					$.each(response_user_responden.data, function(key, value_user_responden){
						if(value_user_responden.surat_tugas_tim.id_unit_auditi==selected_satker){
							console.log("runing..", value_user_responden.surat_tugas_tim.id_unit_auditi==selected_satker);
						}
					});
					//
					$.getJSON(ctx + "/pernyataan-penetapan/" + 'list-aspek-kegiatan?page='+page+'&limit='+limit+'&'+params, function(response) {
						if(response.code == 200){
							console.log("pernyataan_penetapan", response);
							//
							var wraper_menu_tab=$('#tab-menu');
							var wraper_content_tab=$('#tab-content');
							var li_menu="";
							var li_menu2="";
							var count_total_pernyataan=0;
							$.each(response.data.main,function(key,value_main){
								console.log(value_main);
								
								li_menu2 += '<div class="tab-pane '+(key==0?"active":"")+'" id="tab-'+key+'">';

								count_total_pernyataan=0;
								$.each(response.data.penetapan_pernyataan,function(key,value_penetapan_pernyataan){
									console.log(value_penetapan_pernyataan);
									if(value_main.id_aspek==value_penetapan_pernyataan.aspek_id_pernyataan){
										li_menu2 += '<div class="row">'+
										'						<div class="col-md-12">'+
										'						<div class="panel panel-success">'+
										'							<div class="panel-body bg-success"><p>'+value_penetapan_pernyataan.isi_pernyataan+'('+value_penetapan_pernyataan.aspek_id_pernyataan+')'+'</p></div>'+
										'							<ul class="list-group">';
										$.each(value_penetapan_pernyataan.listPernyataanJawaban,function(key,value_pernyataan_jawaban){
											if(value_pernyataan_jawaban.header_pernyataan_id_pernyataan_jawaban==value_penetapan_pernyataan.id_pernyataan){
												var count_jawaban=0;
												$.each(response.data.list_responden_jawaban,function(key,value_responden_jawaban){
														$.each(response_user_responden.data, function(key, value_user_responden){
															if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_pernyataan_jawaban){
																if(value_user_responden.surat_tugas_tim.id_unit_auditi==selected_satker && value_user_responden.user_id==value_responden_jawaban.user_id_responden){
																	count_jawaban++;
																}
															}	
														});
												});
												li_menu2 += '<li class="list-group-item">'+value_pernyataan_jawaban.isi_pernyataan_jawaban+'<span class="badge">'+count_jawaban+'</span></li>';
											}
										});
										li_menu2 += ' 				</ul>'+
										'						</div>'+
										'						</div>'+
										'					</div>';
										count_total_pernyataan++;
									}
								});
								li_menu+='<li class='+(key==0?"active":"")+'><a href="#tab-'+key+'" data-toggle="tab">'+value_main.nama_aspek+' (<span id="jumlah-pernyataan">'+count_total_pernyataan+'</span>)</a></li>';

								li_menu2 +='</div>';
							});
							wraper_menu_tab.html(li_menu);
							wraper_content_tab.html(li_menu2);
							//
						}
					});
				}
			});
		}
	});
	//
}

function onGetListSuccess(response){
	console.log(response);
	var wraper_menu_tab=$('#tab-menu');
	var wraper_content_tab=$('#tab-content');
	//render aspek kegiatan
	var li_menu="";
	var li_menu2="";
	var count_total_pernyataan=0;
	$.each(response.data.main,function(key,value_main){
		console.log(value_main);
		
		li_menu2 += '<div class="tab-pane '+(key==0?"active":"")+'" id="tab-'+key+'">';

		count_total_pernyataan=0;
		$.each(response.data.penetapan_pernyataan,function(key,value_penetapan_pernyataan){
			console.log(value_penetapan_pernyataan);
			if(value_main.id_aspek==value_penetapan_pernyataan.aspek_id_pernyataan){
				li_menu2 += '<div class="row">'+
				'						<div class="col-md-12">'+
				'						<div class="panel panel-success">'+
				'							<div class="panel-body bg-success"><p>'+value_penetapan_pernyataan.isi_pernyataan+'('+value_penetapan_pernyataan.aspek_id_pernyataan+')'+'</p></div>'+
				'							<ul class="list-group">';
				$.each(value_penetapan_pernyataan.listPernyataanJawaban,function(key,value_pernyataan_jawaban){
					if(value_pernyataan_jawaban.header_pernyataan_id_pernyataan_jawaban==value_penetapan_pernyataan.id_pernyataan){
						var count_jawaban=0;
						$.each(response.data.list_responden_jawaban,function(key,value_responden_jawaban){
							if(value_responden_jawaban.jawaban_id_responden_jawaban==value_pernyataan_jawaban.id_pernyataan_jawaban){
								count_jawaban++;
							}
						});
						li_menu2 += '<li class="list-group-item">'+value_pernyataan_jawaban.isi_pernyataan_jawaban+'<span class="badge">'+count_jawaban+'</span></li>';
					}
				});
				li_menu2 += ' 				</ul>'+
				'						</div>'+
				'						</div>'+
				'					</div>';
				count_total_pernyataan++;
			}
		});
		li_menu+='<li class='+(key==0?"active":"")+'><a href="#tab-'+key+'" data-toggle="tab">'+value_main.nama_aspek+' (<span id="jumlah-pernyataan">'+count_total_pernyataan+'</span>)</a></li>';

		li_menu2 +='</div>';
	});
	wraper_menu_tab.html(li_menu);
	wraper_content_tab.html(li_menu2);

	
	
		

	//end here
//	var tbody = $("#table-card-body");
////	var tfooter = $("#table-card-footer");
//	var tfooter = $("#head-pagination");
//	$('#refresh').removeClass('disabled');
//	var row = "";
//	var num = $('.data-row').length+1;
//	
//	row += render_row_card(response, num);
//	
//	if(response.next_more==false && (response.next_page_number-response.count)==2){
//		showConfirm('Mohon <strong>review</strong> kembali jawaban anda, sebelum lanjut.');
//		//window.location.href=ctx+"/page/survey?id="+selected_id+"&kegiatan_pengawasan_id="+kegiatan_pengawasan_id+"&confirm=true&page="+(response.next_page_number-1);
//		row='Mohon tunggu...';
//		$('#modal-action').find('.btn-secondary').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=true&page='+(response.next_page_number-1)+'"');
//		$('#modal-action').find('.btn-success').attr('onclick','window.location.href="'+ctx+'/page/survey?id='+selected_id+'&kegiatan_pengawasan_id='+kegiatan_pengawasan_id+'&confirm=finish&page='+(response.next_page_number)+'"');
//	}
//	if(response.next_more==false && (response.next_page_number-response.count)==3){
//		$('#pernyataan-title').html("<strong>Saran  </strong>");
//
//		row = '<div class="card-header border mb-3 ">	    	<div class="row">			    <div class="col col-md-12" id="pernyataan-title"><strong>Saran  </strong></div>	</div>		</div>';
//			
//
//		row+='<textarea name="isi" class="form-control mb-3" placeholder="Beritahu kami saran dan masukan anda terhadap survey yang dilakukan, agar survey ini dapat menjadi lebih baik." rows="6"></textarea>';
//		$('#info-answers').addClass('d-none');
//		$('#container-btn-end').find('button').attr('onclick', 'saveSaran()');
//		$('#container-btn-end').find('button').text('Selesai');
//	}
//	row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
//
//	myvar = '';
//	if(response.next_page_number > 2 || response.next_page_number > response.count)
//	myvar += '<button onclick="renderDisplay('+(response.next_page_number-2)+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-left"></i></button>';
//	
//	if(response.next_more==false && (response.next_page_number-response.count)==2){
//		myvar += '<button onclick="saveSaran()" type="button" class="btn m-0 p-0"><i class="fas fa-check-circle text-success"></i></button>';
//	}else{
//		myvar += '<button onclick="renderDisplay('+response.next_page_number+')" type="button" class="btn m-0 p-0"><i class="fas fa-chevron-circle-right"></i></button>';
//	}
//	console.log(myvar);
//	tfooter.html(myvar);
	
	//$('#btn-lanjutkan').attr('onclick', 'saveSaran()');
}