var url = ctx + "/laporan/";
var url_pekerjaan = ctx + "/pekerjaan/";
var selected_id=0;
var list_data = [];
function init() {
	
	$("[name=tanggal_laporan]").val(moment().format('DD-MM-YYYY'));
	display(1);
	
	$("#btnDownload").click(function(e){ download(); });
	
	$('[name=tipe_unit_kerja]').change(function(){
		if($(this).val() == 'SATUAN') {
			$('[name=id_unit_kerja]').removeAttr('disabled');
		} else {
			$('[name=id_unit_kerja]').attr('disabled', 'disabled');
		}
	});

	$('#btn-modal-confirm-notif-yes').click(function(e){
		if(selected_action == 'sendmail' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function search(){
	display(1);
}

function refresh(){
	$("[name=status_pelaporan][value=ALL]").prop("checked", true);
	$("[name=tanggal_laporan]").val(moment().format('DD-MM-YYYY'));
	var page = $('#btnRefresh').data("id");
	display(page);
}

function download(){
	location=url+'download?id=4&status_pelaporan='+$('[name=status_pelaporan]:checked').val()+'&tanggal_laporan='+getDateForInputCostum($('[name=tanggal_laporan]').val())
}

function detail(id) {
	
}

function display(page = 1, limit = 50){

    var tbody = $('#tbl').find($('tbody'));
    var tfoot = $('#tbl').find($('tfoot'));
    $('#btnRefresh').data("id",page);
    
    if(page == 1) { tbody.text(''); list_data = [] }
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

    ajaxGET(url+'?page='+page+'&limit='+limit+'&id=4&status_pelaporan='+$('[name=status_pelaporan]:checked').val()+'&tanggal_laporan='+getDateForInputCostum($('[name=tanggal_laporan]').val()), function(response){
        console.log(response);
        if (response.code == 200) {
            var rowBody = '';
            var rowFoot = '';
            var rowNext = '';
            var i = 1;
            console.log(response.data);
            list_data = list_data.concat(response.data);
            $('[id=table-loading], [id=more-button-row]').remove();
            $.each(groupByArray(list_data, 'idUnitKerja'), function(keyGroup, valueGroup) {
            	$.each(valueGroup.values, function(key, value) {
	                rowBody += "<tr scope='row'>";
	                	if(key==0) {
		                	rowBody += '<td>'+(keyGroup+1)+'</td>';
		                    rowBody += '<td class="text-left">'+value.namaUnitKerja+'</td>';
	                	} else { rowBody += '<td></td><td></td>'; }
	                    rowBody += '<td nowrap width="50">'+(key+1)+'</td>';
	                    rowBody += '<td width="50" class="text-left">'+value.nipPegawai+'</td>';
	                    rowBody += '<td class="text-left">'+value.namaPegawai+'</td>';
	                    rowBody += '<td>'+getStatusPelaporan(value.statusPelaporan)+'</td>';
	                    rowBody += '<td>'+getCheck(value.statusTusiLaporan)+'</td>';
	                    rowBody += '<td>'+getCheck(value.statusTambahanLaporan)+'</td>';
	                    rowBody += '<td class="text-center" nowrap>';
	                    if(value.statusPelaporan=='Melapor') rowBody += '<a target="_blank" href="detil-kinerja?tanggal='+$('input[name=tanggal_laporan]').val()+'&nip='+value.nipPegawai+'" class="btn btn-info btn-sm" type="button" title="Detail Laporan Kinerja Pegawai"><i class="fas fa-fw fa-search"></i></a> ';
	                    if(value.statusPelaporan=='Tidak') rowBody += '<a href="javascript:void(0)" class="btn btn-success btn-sm" type="button" onclick="doAction(\''+value.nipPegawai+'\',\'sendmail\')" title="Send Email Notifikasi to this Pegawai"><i class="fas fa-envelope"></i></a>';
	                    rowBody += '</td>';

	                rowBody += '</tr>';
	                i++; 
                });
            });
            if(response.next_more){
            	rowNext += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
            	rowNext += '    <button type="button" class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
                rowNext += '</div></td></tr>';
            }
            if(rowBody == ''){
                tbody.text('');
                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
            }else{
            	tbody.html(rowBody+rowNext);
            }
        }else{
            alert("Connection error!!");
        }
        }
    );
}
var selected_action="";
var selected_id="";
function doAction(id, action, confirm = 0){
	selected_action = action;
	var param = "id=4&tanggal_laporan="+$('[name=tanggal_laporan]').val()+"&nip_pegawai="+id;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		ajaxGET(ctx+"/user/check-username?username="+id+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		var obj= new FormData();
		obj.append('tanggal_laporan', $('[name=tanggal_laporan]').val());
		obj.append('nip_pegawai', id);
		ajaxPOST(url+"send",obj,'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.username;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$("#passwordForm").hide();
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'sendmail'){
		$('#modal-confirm-notif-msg').html("Apakah anda yakin akan mengirim <strong>email notifikasi</strong> pada pegawai : <strong>"+value.lastName+"</strong>?");
		$('#modal-confirm-notif').modal('show');
	}
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	if(selected_action!="sendmail") display();
	/*
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	*/
	//$.LoadingOverlay("hide");
	//stopLoading('btn-save', 'Simpan');
	//stopLoading('btn-modal-confirm-yes', 'Yes');

	$('#modal-confirm').modal('hide');
	$('#modal-confirm-notif').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	showAlertMessage(response.message);
}