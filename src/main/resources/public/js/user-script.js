var url = ctx + "/user/";
var selected_id=0;
function init() {
	
	display(1);
	getPositionList();
	getUnitKerja();
	getJabatan();
	
    $("#btnAdd").click(function(e){ add(); });
    $("#btnImport").click(function(e){ addImportPengguna(); });
	$("#btnReset").click(function(e){ clearForm(); });
	$("#btnRefresh").click(function(e){ refresh(); });
	$('#entry-form').submit(function(e){ save(); e.preventDefault(); });
	$('#txtsearch').keypress(function(e){ if(e.keyCode == 13) { search(); e.preventDefault(); } });

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function search(){
	display(1);
}

function getPositionList(){
	$.getJSON(url+'position/'+"?token="+token, function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=position]").append(new Option(value.name_position, value.position_id));
			});
		}
	});
}
function getUnitKerja(){
	$.getJSON(ctx+'/ref/unit-kerja/list', function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}
function getJabatan(){
	$.getJSON(ctx+'/ref/jabatan/list', function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=jabatan]").append(new Option(value.namaJabatan, value.idJabatan));
			});
		}
	});
}

function add(){
	clearForm();
    $("#progressBar").hide();
    $("#passwordForm").show();
    $("#modal-form").modal('show');
}

function addImportPengguna(){
    $('#frm-import-pengguna')[0].reset();
    $('#msgImport').hide();
    $('#modal-import-pengguna').modal('show');
}

function importPengguna(){
    var obj = new FormData(document.querySelector('#frm-import-pengguna'));
    //obj+="&position=6";
    ajaxPOST(ctx + '/user/insert-pegawai',obj,'succSavImport','failSavImport','errSavImport');
}
function succSavImport(response){
    if(response.status == '200') {
        refresh();
        $('#modal-import-pengguna').modal('hide');
        $('#frm-import-pengguna')[0].reset();
        $('#msgImport').show();
        $('#msgImport').html(response.message);
        $('#msgImport').addClass('alert-success');
        $('#msgImport').removeClass('alert-danger');
    } else {
        failSavImport(response);
    }
}
function failSavImport(response){
    $('#msgImport').show();
    $('#msgImport').addClass('alert-danger');
    $('#msgImport').removeClass('alert-success');
    $('#msgImport').html(response.message);
}
function errSavImport(response){
    alert("kesalahan dari server");
}

function validate(){
	var msg = '';
	
	if($('#password').val() != $('#repassword').val()){
		msg += 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	// if save existing record (EDIT/UPDATE)
	if ($('#id').val() == '0'){
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}else{
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}	
	return msg;
}
function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(url + 'save',obj,'onModalActionSuccess','onSaveError');
}
function succSav(response){
	refresh();
	$('#modalPengguna').modal('hide');
	$('#frmInput')[0].reset();
}
function onSaveError(response){
	console.log(response.responseJSON.message);
	addAlert('msg', "alert-danger", response.responseJSON.message);
}


function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

//
function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		ajaxGET(url+id+'?'+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		ajaxPOST(url+id+'/'+selected_action+'?',{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$("#passwordForm").hide();
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'editPassword'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}
//
function fillFormValue(value){
	clearForm();
	$('[name=first_name]').val(value.firstName);
	$('[name=last_name]').val(value.lastName);
	$('[name=username]').val(value.username);
	$('[name=position]').val(value.positionId);
	$('[name=email]').val(value.email);
	$('[name=gender]').val(value.gender);
	$('[name=birth_date]').val(moment(value.birthDate, "DD-MM-YYYY")._i);
	$('[name=mobile_phone]').val(value.mobilePhone);
	$('[name=unit_kerja]').val(value.unitKerjaId);
	$('[name=jabatan]').val(value.idJabatan);
	$('[name=alamat]').val(value.alamatRumahUserDetil);
	$('[name=riwayat_penyakit]').val(value.riwayatPenyakitUserDetil);

	$("#progressBar").hide();
}
//
function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	/*
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	*/
	//$.LoadingOverlay("hide");
	//stopLoading('btn-save', 'Simpan');
	//stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	//showAlertMessage(response.message, 1500);
}
//
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
    row += '<td scope="row">'+(num)+'</td>';
    row += "<td>";
	row += "<strong>Nama : </strong>"+(value.name != null && value.name != "" ? value.name : "(Tidak Ada)")+" ("+(value.gender=='M'?'L':'P')+")</br>";
	row += "<strong>NIP : </strong>"+(value.username)+"</br>";
	//row += "<strong>Tgl.Lahir : </strong>"+(value.birthDate != null && value.birthDate != "" ? value.birthDate : "(Tidak Ada)")+"";
    row += "</td>";
    row += "<td>";
 	row += "<strong>Jabatan : </strong>"+(value.namaJabatan != null && value.namaJabatan != "" ? value.namaJabatan : "(Tidak Ada)")+"</br>";
	row += "<strong>Unit Kerja : </strong>"+(value.unitKerjaName != null && value.unitKerjaName != "" ? value.unitKerjaName : "(Tidak Ada)")+"</br>";
    row += "</td>";
//    row += "<td>"+(value.unitKerjaName != null && value.unitKerjaName != "" ? value.unitKerjaName : "(Tidak Ditentukan)")+"</td>";
    row += "<td>";
	row += "<strong>Email : </strong>"+(value.email != null && value.email != "" ? value.email : "(Tidak Ada)")+"</br>";
	row += "<strong>HP : </strong>"+(value.mobilePhone != null && value.mobilePhone != "" ? value.mobilePhone : "(Tidak Ada)")+"</br>";
    row += "</td>";
    row += "<td>"+value.positionName;
    row += "</td>";
//    row += '<td nowrap>';
//    row += '<button onclick="edit(' + value.id + ')" class="btn btn-info btn-sm"><span class="fa fa-pencil-alt"></span></button> ';
//    row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-primary btn-sm"><span class="fa fa-lock"></button> ';
//    row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-danger btn-sm"><span class="fa fa-trash"></button>';
//    row += '</td>';
	row += '<td class="text-center" nowrap>';
	row += '<a href="javascript:void(0)" class="btn btn-info btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-primary btn-sm"><span class="fa fa-lock"></button> ';
	row += '<a href="javascript:void(0)" class="btn btn-danger btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash "></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	return row;
}
//

function editPassword(id){
	$("#progressBar2").hide();
	$("#btnUpdate").data("id",id);
	$("#modalPassword").modal('show');
}

function doUpdate(id){
	var obj = new FormData(document.querySelector('#frmInputPass'));
	$("#progressBar2").show();
	ajaxPOST(url+id+'/updatePass',obj,'succUpdate','onUpdateError');
}

function succUpdate(response){
	$('#modalPassword').modal('hide');
	$('#frmInputPass')[0].reset();
}
function onUpdateError(response){
	console.log(response.responseJSON.message);
	addAlert('msg1', "alert-danger", response.responseJSON.message);
}
//batas


function onChangeType(pos){
    //alert(obj.value);
    if(pos == 6){// Operator
        $('.satker-row').show();
    }else{
        $('.satker-row').hide();
    }
    
    if(pos == 3 || pos == 4){// Analis / Supervisor
        $('.unit-kerja-row').show();
    }else{
        $('.unit-kerja-row').hide();
    }
    
}

function display(page = 1, limit = 50){
    
    var tbody = $('#tbl').find($('tbody'));
    $('#btnRefresh').data("id",page);
    
    if(page == 1) tbody.text('');
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

    var param = $('#form-filter').serialize();
    param += '&position-exclude=100';

    ajaxGET(url+'list-load-more?page='+page+'&limit='+limit+'&'+param, function(response){
        console.log(response);
        if (response.code == 200) {
            var row = '';
            //tbody.text('');
            var i = 1;
            //console.log(response.data);
            $('[id=table-loading], [id=more-button-row]').remove();
            var num = $('.data-row').length+1;
            $.each(response.data, function(key, value) {
            	row+=renderRow(value, num);
            	num++;
            });
            if(response.next_more){
                row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
                row += '    <button type="button" class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
                row += '</div></td></tr>';
            }
            if(row == ''){
                tbody.text('');
                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
            }else{
                if(page == 1) tbody.html(row);
                else tbody.html(tbody.html()+row);
            }
        }else{
            alert("Connection error!!");
        }
        }
    );
}
