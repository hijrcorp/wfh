//var tokenCookieName = "newlha.token";
var domainRoot = window.location.hostname.replace(window.location.hostname.substr(0,window.location.hostname.indexOf(".")),'');

var token;
var loginUser;
var urlLog = ctx + "/rest/user/";
function logout() {
	
	/*
	var obj = {};

	console.log("LOGOUT: "+token);
	
	$.ajax({
	    url: ctx + '/revoke-token',
	    method: "GET",
	    crossDomain: true,
	    contentType: "application/x-www-form-urlencoded",
	    data: '',
	    cache: false,
	    success: function (data) {
	    		clearTokenAndRedirect();
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    		clearTokenAndRedirect();
	    }
		
	});	
	*/
	clearTokenAndRedirect();
}

function clearTokenAndRedirect(){
	if(window.location.hostname != 'localhost') {
		Cookies.remove(tokenCookieName,{domain: domainRoot, path: '/'});
		//Cookies.remove('oauth2-refresh-token',{domain: '.hijr.co.id', path: '/'});;	
	}else{
		Cookies.remove(tokenCookieName,{ path: '/'});
		//Cookies.remove('oauth2-refresh-token',{path: '/'});;
	}
	location.href=ctx + '/login-logout'
}

function formatSqlDate(strdate){
	var arrdate = strdate.split('/');
	return arrdate[2] + "-" + arrdate[1] + "-" + arrdate[0];
}



jQuery(document).ready(function() {
	token = $.cookie(tokenCookieName);
	//token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbjo6U1VSVkVZIiwicG9zaXRpb25fbmFtZSI6IkFkbWluIiwicmVhbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJvcmdhbml6YXRpb25fbmFtZSI6IlN1cnZleSIsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfVFUiLCJST0xFX0FOQUxJUyIsIlJPTEVfTUFOQUdFUiIsIlJPTEVfQURNSU4iLCJST0xFX09QUiIsIlJPTEVfU1VQRVJWSVNPUiJdLCJjbGllbnRfaWQiOiJzdXJ2ZXktd2ViIiwiYXVkIjpbImxoa19zdXJ2ZXkiLCJsaGtfbGFwZ2FzIiwibGhrX2Jhc2UiLCJsaGtfbGhhIl0sInVzZXJfaWQiOiIxNDk5ODUxMTMzMjY1IiwidXNlcl9waWN0dXJlIjoiaHR0cHM6Ly9pdGplbi5oaWpyLmNvLmlkL2FwcC9waWN0dXJlL3VzZXIvMTQ5OTg1MTEzMzI2NSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJvcmdhbml6YXRpb25faWQiOiIxNTA4NjI4OTIxNDMxIiwiZXhwIjoxNTcyMzQxNjQ2LCJqdGkiOiJiZTI5MjkxYS0xNmVkLTRlNjMtODhhMi0wM2IyNTg1ZmVlZDkiLCJwb3NpdGlvbl9pZCI6IjEifQ.5gcsxQ-LJTJ84Y_WI7R5vFzozgSekgW7QtcddXXHMx8";
//	$.ajaxSetup({
//		headers : {
//			'Authorization' : 'Bearer '+ token
//		},
//		"error":function() { console.log("internal server error");  }
//	});
	/*
	 * Format Input Type Date
	 */
    $(function() {
		$( ".datepicker" ).datepicker({
	      showOtherMonths: true,
	      selectOtherMonths: true,
	      dateFormat : "dd/mm/yy"
	   //dateFormat : "dd-mm-yy"
		});
		
	});
    
    /*
     * Format Input Type Currency
     */
    $('.currency').number( true,0);
    $('.dollar').number( true,2);
    $('.select').select2({
    		width: 'resolve'
    })
    
    
    $('#frmChangePassword').submit(function(e){
        doChangePassword();
        e.preventDefault();
    });
    
    $('#frmUpdateProfile').submit(function(e){
        doUpdateProfile();
        e.preventDefault();
    });
    
    //loginUser = JSON.parse($.cookie("user"));
    init();
    	
});
$(document).ajaxStart(function() {
	NProgress.inc();
});
$(document).ajaxComplete(function() {
	NProgress.done();
});

function showChangePassword(){
	resetChangePassword();
	$("#changePasswordProgress").hide();
	$('#modalChangePassword').modal('show');
	
	$('#modalChangePassword').on('shown.bs.modal', function() {
		$('#changePasswordOld').focus();
    });
}

function doChangePassword(){
	var obj = new FormData(document.querySelector('#frmChangePassword'));
	ajaxPOST(ctx + '/user/changePassword',obj,'changePasswordUpdate','changePasswordFailed');
}
function changePasswordUpdate(response){
	
	$('#modalChangePassword').modal('hide');
	$('#frmChangePassword')[0].reset();
}

function changePasswordFailed(response){
	console.log(response);
	
	addAlert('changePasswordMsg', "alert-danger", response.responseJSON.message);
//	alert(response.message);
}

function resetChangePassword(){
	$('#changePasswordNew').val('');
	$('#changePasswordNewRetype').val('');
	$('#changePasswordOld').val('');
}

function execChangePassword(id){
	var msg = '';
	
	if($('#changePasswordNew').val() == '' || $('#changePasswordNewRetype').val() == ''){
		msg = 'Kata kunci tidak boleh kosong';
	}
	
	if($('#changePasswordNew').val() != $('#changePasswordNewRetype').val()){
		msg = 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	if (msg == '') {
		$("#changePasswordProgress").show();
		
		var obj;
		
		$.getJSON("./rest/user/"+id+"?token="+token, function(response, status) {
			
	    	if (response.status == 'OK') {
	    		var user = response.data;
	    		
	    		user.expired = new Date(user.expired);
	    		user.currentPassword = $('#changePasswordNew').val();
	    		obj = {"user" :  user };
	    		
	    		
	    		$.ajax({
	    	        url : "./rest/user/save?token="+token,
	    	        type : "POST",
	    	        traditional : true,
	    	        contentType : "application/json",
	    	        dataType : "json",
	    	        data : JSON.stringify(obj),
	    	        success : function (response) {
	    	        	if (response.status == 'OK') {
	    	        		$("#changePasswordProgress").hide();
	    	        		$("#modalChangePassword").modal('hide');
	    	        		resetChangePassword();
	    				}else{
	    					addAlert('changePasswordMsg', "alert-danger", msg + 'Gagal Simpan. Error tidak diketahui');
	    					
	    				}
	    	        },
	    	        error : function (response) {
	    	        	alert("Connection error");
	    	        },
	    	    });
			}else{
				alert("Connection error");
			}
		});
		
		
	}else{
		addAlert('changePasswordMsg', "alert-danger", msg);
	}
}

function showUpdateProfile(){

    resetUpdateProfile();
    $.getJSON(urlLog+"me?token="+token, function(response) {
        if(response.status == 'OK') {
            var data = response.data;
            console.log('me: ', data);
            $('#me_first_name').val(data.firstName);
            $('#me_last_name').val(data.lastName);
            $('#me_username').val(data.username);
            $('#me_email').val(data.description);
            $('#me_position_name').val(data.position.name);
            
            $("#updateProfileProgress").hide();
            $('#modalUpdateProfile').modal('show');
        }
    });
    
    $('#modalUpdateProfile').on('shown.bs.modal', function() {
        $('#updateProfileOld').focus();
    });
}

function doUpdateProfile(){
    
    var msg = '';
    
    if($('#updateProfileNew').val() == '' || $('#updateProfileNewRetype').val() == ''){
        msg = 'Kata kunci tidak boleh kosong';
    }
    
    if($('#updateProfileNew').val() != $('#updateProfileNewRetype').val()){
        msg = 'Ketikan ulang kata kunci tidak cocok <br />';
    }
    
    if (msg == '') {
        $("#updateProfileProgress").show();
        
        $.ajax({
            url : ctx + "/rest/user/updateProfile",
            type : "POST",
            traditional : true,
            contentType : "application/x-www-form-urlencoded",
            //dataType : "json",
            data : "firstName="+$("#me_first_name").val()+"&lastName="+$('#me_last_name').val()+"&username="+$('#me_username').val()+"&email="+$('#me_email').val(),
            success : function (response) {
                console.log(response);
                if (response.status == 'OK') {
                    $("#updateProfileProgress").hide();
                    $("#modalUpdateProfile").modal('hide');
                }else{
                    $("#updateProfileProgress").hide();
                    addAlert('updateProfileMsg', "alert-danger", response.message);
                }
            },
            error : function (response) {
                alert("Connection error");
            },
        });
    
    }else{
        addAlert('updateProfileMsg', "alert-danger", msg);
    }
}

function resetUpdateProfile(){
    $('#me_first_name').val('');
    $('#me_last_name').val('');
    $('#me_username').val('');
    $('#me_email').val('');
    $('#me_position_name').val('');
}

function execUpdateProfile(id){
    var msg = '';
    
    if($('#updateProfileNew').val() == '' || $('#updateProfileNewRetype').val() == ''){
        msg = 'Kata kunci tidak boleh kosong';
    }
    
    if($('#updateProfileNew').val() != $('#updateProfileNewRetype').val()){
        msg = 'Ketikan ulang kata kunci tidak cocok <br />';
    }
    
    if (msg == '') {
        $("#updateProfileProgress").show();
        
        var obj;
        
        $.getJSON("./rest/user/"+id+"?token="+token, function(response, status) {
            
            if (response.status == 'OK') {
                var user = response.data;
                
                user.expired = new Date(user.expired);
                user.currentPassword = $('#updateProfileNew').val();
                obj = {"user" :  user };
                
                
                $.ajax({
                    url : "./rest/user/save?token="+token,
                    type : "POST",
                    traditional : true,
                    contentType : "application/json",
                    dataType : "json",
                    data : JSON.stringify(obj),
                    success : function (response) {
                        if (response.status == 'OK') {
                            $("#updateProfileProgress").hide();
                            $("#modalUpdateProfile").modal('hide');
                            resetUpdateProfile();
                        }else{
                            addAlert('updateProfileMsg', "alert-danger", msg + 'Gagal Simpan. Error tidak diketahui');
                            
                        }
                    },
                    error : function (response) {
                        alert("Connection error");
                    },
                });
            }else{
                alert("Connection error");
            }
        });
        
        
    }else{
        addAlert('updateProfileMsg', "alert-danger", msg);
    }
}

function cleanMessage(id){
	$('#'+id).hide();
	$('#'+id).removeClass("alert-success");
	$('#'+id).removeClass("alert-danger");
	$('#'+id).text('');
}

function addAlert(id, type, message) {
	
	cleanMessage(id);
	
	$('#'+id).addClass(type);
	var premsg = '';
	if (type == 'alert-success') {
		premsg = '<strong>Success!</strong><br />';

	}
	if (type == 'alert-danger') {
		premsg = '<strong>Error!</strong><br />';
	}
	$('#'+id).append(premsg + message);
	$('#'+id).show();
}


/**
 * Create HTML links of available pages according to data list querying from database
 * @param id Element ID as container of pagination links
 * @param func Function to retrieve and display data
 * @param pagination Pagination object retrieve from ResponseWrapper
 */
function createPagination(id, func, pagination) {
	console.log(pagination);
	var html = '';
	$('#'+id).text('');

	if (pagination.rowcount == 0) {
		html += '<li class="disabled"><span aria-hidden="true">&laquo;</span> </li>';
		html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
	} else {
		if (pagination.activepage == 1) {
			html += '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'('+(pagination.activepage - 1)+')" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
		}
		var i;
		for ( i = 1; i <= pagination.pagecount; i++) {

			if (pagination.activepage == i) {
				html += '<li class="active"><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			} else {
				html += '<li><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			}
		}
		if (pagination.activepage == pagination.pagecount) {
			html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'(' +(pagination.activepage + 1) + ')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
		}
	}

	$('#'+id).append(html);
}

function createPagination1(id, func, response) {
	var html = "";
	if (response.limit == 0) {
		html += '<li class="disabled"><span aria-hidden="true">&laquo;</span> </li>';
		html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
	} else {
		if (response.activePage == 1) {
			html += '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'('+(response.activePage - 1)+')" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
		}
		var i;
		for ( i = 1; i <= response.pageCount; i++) {

			if (response.activePage == i) {
				html += '<li class="active"><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			} else {
				html += '<li><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			}
		}
		if (response.activePage == response.pageCount) {
			html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'(' +(response.activePage + 1) + ')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
		}
	}
	$('#'+id).html(html);
}
function ajaxPOST(url,obj,fnsuccess, fnfailed, fnerror){
	$.ajax({
	    url : url,
	    method: "POST",
	    crossDomain: true,
	    contentType: "application/x-www-form-urlencoded",
	    data : obj,
	    cache: false,
	    success : function (response) {
	    		console.log(response);
		    	if (response.status == '200') {
		    		var fn = window[fnsuccess];
		    		if(typeof fn === 'function') {
		    		    fn(response);
		    		}
			}else{
  				var fn = window[fnfailed];
	  	    		if(typeof fn === 'function') {
	  	    		    fn(response);
	  	    		}
			}
	    },
	    error : function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}
function ajaxGET(url, fnsuccess, fnerror, element){
	$.ajax({
	    url: url,
	    method: "GET",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response,element);
	    		}
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn();
	    		}
	    }
		
	});	
}

function filterClause(column,operator,value){
	var clause = {
			"column" : column,
			"operator" : operator,
			"value" : value
		};
	return clause;
}

function ajaxPOST1(url,obj,fnsuccess, fnfailed, fnerror){
	$.ajax({
	    url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
	    success : function (response) {
	    	if (response.status == 'OK') {
	    		var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
  			}else{
  				var fn = window[fnfailed];
  	    		if(typeof fn === 'function') {
  	    		    fn(response);
  	    		}
  			}
	    },
	    error : function (response) {
			var fn = window[fnerror];
    		if(typeof fn === 'function') {
    		    fn(response);
    		}
	    },
	});
}

function ajaxPOST_NEW(url,obj,fnsuccess, fnfailed, fnerror){
	$.ajax({
	    url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : obj,
	    success : function (response) {
	    	if (response.status == 'OK') {
	    		var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
  			}else{
  				var fn = window[fnfailed];
  	    		if(typeof fn === 'function') {
  	    		    fn(response);
  	    		}
  			}
	    },
	    error : function (response) {
			var fn = window[fnerror];
    		if(typeof fn === 'function') {
    		    fn(response);
    		}
	    },
	});
}
function ajaxGET_new(url, fnsuccess, fnerror, element){
	$.ajax({
	    url: url,
	    method: "GET",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response,element);
	    		}
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn();
	    		}
	    }
		
	});	
}

function ajaxPOST(url,obj,fnsuccess, fnerror){
	$.ajax({
	    url : url,
	    method: "POST",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error : function (response) {
	    		$('#error_msg').show();
	    		console.log(response);
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}

function getURLParam(param) {
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
	}
	return getUrlParameter(param);
}
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}
function errReq(){
	alert('Connection '+404);
}
function getRow(response){
	var activePage = response.activePage != undefined ? response.activePage : response.pagination.activepage != undefined ? response.pagination.activepage : null;
	var limit = response.limit != undefined ? response.limit : response.pagination.rowcount != undefined ? response.pagination.rowcount : null;
	return 1 + (activePage * limit)-limit;
}

function matikanFungsiIni(name){
	console.log(name);
	$.each(name,function(){
		console.log(this.substring(0));
		console.log(this);
		$(this.substring(0)).attr('disabled','disabled');
	});
}
function fnfailed(){
	alert('Connection Failed');
}

function onGetRenderSucc(response,obj){
	$.each(response.data, function(key, value) {
		$('[name='+obj.element+']').append(new Option(response.data[key][obj.name], response.data[key][obj.id]));
	});
	$('#'+obj.element).select2({
		dropdownParent: $('#modalTemuan')
	});
}

function addslashes(string) {
    return string.
    replace(/\\/g, '\\\\').
    replace(/\u0008/g, '\\b').
    replace(/\t/g, '\\t').
    replace(/\n/g, '\\n').
    replace(/\f/g, '\\f').
    replace(/\r/g, '\\r').
    replace(/'/g, '\\\'').
    replace(/"/g, '\\"');
}

function getDateForList(datetime) {
    let result = moment(datetime).format('DD-MM-YYYY').toUpperCase();
    return (result == 'INVALID DATE' ? '' : result);
}

function getDateForInput(datetime) {
    return moment(datetime).format('DD-MM-YYYY');
}
function verifyThenDownload(url) {
    console.log('original url: ', url);
    if(url.indexOf('?') > 0) url = url + '&';
    else url = url + '?';
    url = url.replace('export', 'export-to-mail');
    console.log('-- verify then download --');
    console.log('url before: ', url);
    console.log('url: ', url+"token="+token);
    $.getJSON(url+"token="+token, function(response) {
        if (response.status == 'OK') {
            console.log('verify download OK');
            console.log(response.data);
            var path_download = ctx + '/rest/export-to-mail/download?filename='+addslashes(response.data);
            console.log('path_download: ', path_download);
            //window.open(path_download, '_blank');
            location = path_download;
        }else{
            $('#verifyThenDownloadMsg').html(response.message);
            $('#modalVerifyThenDownload').modal('show');
            //alert(response.message);
        }
    });
}