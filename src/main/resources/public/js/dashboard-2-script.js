var url = ctx + "/dashboard/";
function init() {
	
	$('[name=tanggal]').val(moment().format('DD/MM/YYYY'));
	$('[name=tanggal]').change(function(){ display(); });
	display();

	
    $("#btnDownload").click(function(e){ download(); });
	
}
function download(){
    location = url+'download?id=2&tanggal='+getDateForInputCostum($('[name=tanggal]').val());
}

function display() {
	const tbody = $('#tbl').find($('tbody'));
    const tfoot = $('#tbl').find($('tfoot'));

    tfoot.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');
    
    const url = ctx + '/dashboard/?id=2&tanggal='+getDateForInputCostum($('[name=tanggal]').val());
    
    console.log('url: ', url);
    
    ajaxGET(url, function(response){
//    	response = getResponse();
        if (response.code == 200) {
            var row = '';
            var i = 1;
            var list = [];
            $('[id=table-loading], [id=more-button-row]').remove();
            tbody.text('');
            console.log('response.data: ', response.data);
            $.each(response.data, function(key, value) {
                row += '<tr class="data-row small">';
                    row += '<td>'+(key+1)+'</td>';
                    row += '<td class="text-left">'+value.namaUnitKerja+'</td>';
                    row += '<td>'+value.jumlahPegawai+'</td>';
                    row += '<td>'+value.jumlahMelaporPekerjaan+'</td>';
                    row += '<td>'+value.jumlahPersenMelaporPekerjaan+'%</td>';
                    row += '<td>'+value.jumlahTusiLaporan+'</td>';
                    row += '<td>'+value.jumlahTambahanLaporan+'</td>';
                    row += '<td>'+value.jumlahMelaporKesehatan+'</td>';
                    row += '<td>'+value.jumlahPersenMelaporKesehatan+'%</td>';
                    row += '<td>'+value.jumlahSehat+'</td>';
                    row += '<td>'+getValue(value.jumlahPersenSehat, 0)+'%</td>';
                    row += '<td>'+value.jumlahSakit+'</td>';
                    row += '<td>'+getValue(value.jumlahPersenSakit, 0)+'%</td>';
                    row += '<td>'+value.jumlahDemam+'</td>';
                    row += '<td>'+value.jumlahBatuk+'</td>';
                    row += '<td>'+value.jumlahPilek+'</td>';
                    row += '<td>'+value.jumlahTenggorokan+'</td>';
                    row += '<td>'+value.jumlahSesak+'</td>';
                row += '</tr>';
                i++;
            });
            if(row == ''){
                tbody.text('');
                tfoot.text('');
                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
            }else{
                tbody.html(row);
                row = '';
                row += '<tr class="data-row small">';
	                row += '<td></td>';
	                row += '<td class="text-left">Jumlah</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPegawai,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahMelaporPekerjaan,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPersenMelaporPekerjaan,0)/response.data.length).toFixed(2)+'%</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahTusiLaporan,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahTambahanLaporan,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahMelaporKesehatan,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPersenMelaporKesehatan,0)/response.data.length).toFixed(2)+'%</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahSehat,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPersenSehat,0)/response.data.length).toFixed(2)+'%</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahSakit,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPersenSakit,0)/response.data.length).toFixed(2)+'%</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahDemam,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahBatuk,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahPilek,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahTenggorokan,0))+'</td>';
	                row += '<td>'+(response.data.reduce((count,value)=>count+value.jumlahSesak,0))+'</td>';
	            row += '</tr>';
                tfoot.html(row);
                
            }

            renderChart('kinerja', 'Kinerja Pegawai', ['Tusi', 'Tambahan'], [(response.data.reduce((count,value)=>count+value.jumlahTusiLaporan,0)), (response.data.reduce((count,value)=>count+value.jumlahTambahanLaporan,0))]);
            renderChart('kesehatan', 'Kondisi Kesehatan', ['Sehat', 'Sakit'], [(response.data.reduce((count,value)=>count+value.jumlahSehat,0)), (response.data.reduce((count,value)=>count+value.jumlahSakit,0))]);
            renderChart('sebaran', 'Sebaran Kondisi Sakit', ['Demam', 'Batuk', 'Pilek', 'Tenggorokan', 'Sesak Nafas'], [
            	(response.data.reduce((count,value)=>count+value.jumlahDemam,0)), 
            	(response.data.reduce((count,value)=>count+value.jumlahBatuk,0)), 
            	(response.data.reduce((count,value)=>count+value.jumlahPilek,0)), 
            	(response.data.reduce((count,value)=>count+value.jumlahTenggorokan,0)), 
            	(response.data.reduce((count,value)=>count+value.jumlahSesak,0))
        	]);
        }else{
            alert("Connection error!!");
        }   
    });
}