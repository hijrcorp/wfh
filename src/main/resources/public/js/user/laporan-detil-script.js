var selected_id="";
var selected_id_header="";
var selected_action="";
var appendIds = [];
var url = ctx+"/pekerjaan/";

function init(){
    $('[name=tanggal_laporan]').val(moment().format('DD-MM-YYYY'));
    tampilkan();
    
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
    
    $('[name=tanggal_laporan]').change(function(){ 
    	selected_id_header = "change";
	});
	$('#btn-tambah').click(function(){
		if(selected_id_header=="change"){
			$('#msg-nip').html("*Button Tampilkan belum diklik.");
			$("#msg-nip").attr('class', 'invalid-feedback');
			return true;
		}
		var param="?"+$('#frmInput').serialize();
		$.getJSON(url+"check-me"+param, function(response) {
	        if(response.code == 200) {
	        	console.log(response);
	        	selected_action="add";
	    		$('#modal-form').modal('show');
	    		clearForm();
            	$('#msg-nip').html("");
				$("#msg-nip").attr('class', 'valid-feedback');
	        }else{
	        	$('#msg-nip').html(response.message);
				$("#msg-nip").attr('class', 'invalid-feedback');
	        }
	    });
	});
	$('#btn-tampilkan').click(function(){
		tampilkan();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function tampilkan(){
	$("#loadtampil").css("display", "inline-block");
	var param="?"+$('#frmInput').serialize();
	$.getJSON(ctx+"/kesehatan/check-laporan"+param, function(response) {
		$("#loadtampil").css("display", "none");
        if(response.code == 200) {
            var data = response.data;
            console.log('me: ', data);
            if(data.length==0) {
            	//$('#msg-nip').html("Anda belum melaporkan status kesehatan anda.");
            	$('#msg-nip').html("*Laporan kesehatan belum diinput.");
				$("#msg-nip").attr('class', 'invalid-feedback');
				selected_id_header="";
				$('#tbl-data').find($('tbody')).text('');
            }
            else {
            	$('#msg-nip').html(""); //27-mar-2020 10:45
				$("#msg-nip").attr('class', 'valid-feedback');
				selected_id_header=data[0].id;
            	renderDisplay(data);
            }
        }else{
        	alert(response.message);
        }
    });
}

function renderDisplay(data, page = 1, limit = 50){//
    var tbody = $('#tbl-data').find($('tbody'));
    if(page == 1) tbody.text('');
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

     var row = '';
     var num = $('.data-row').length+1;
	 $.each(data[0].detilPekerjaan, function(key, value) {
          row+=renderRow(value, data[0].allowed, num);
          num++;
      });
     if(row == ''){
         tbody.text('');
         tbody.append('<tr id="no-data-row"><td colspan="20" align="center">Data tidak tersedia</td></tr>');
     }else{
         if(page == 1) tbody.html(row);
         else tbody.html(tbody.html()+row);
     }
}

function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
	obj.append('laporan_id', selected_id_header);
	ajaxPOST(ctx + "/pekerjaan/save",obj,'onModalActionSuccess','errSave');
}
function errSave(response){
	if(response.responseJSON.code==400){
		alert(response.responseJSON.message);
	}else{
	 	alert("something wrongess");
	}
}


function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		ajaxGET(url+id+'?'+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		ajaxPOST(url+id+'/'+selected_action+'?',{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=status_tusi]').val(value.status_tusi);
	$('[name=nama]').val(value.nama);
	$('[name=uraian]').val(value.uraian);
	$('[name=output]').val(value.output);
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	/*display();*/
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	//$.LoadingOverlay("hide");
	//stopLoading('btn-save', 'Simpan');
	//stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, true, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, true, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function renderRow(value, allowed, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
    row += '<td scope="row">'+(num)+'</td>';
    row += "<td>"+(value.status_tusi=="NO"?"TAMBAHAN":"TUSI")+"</td>";
    row += "<td>"+value.nama+"</td>";
    row += "<td>"+value.uraian+"</td>";
    row += "<td>"+value.output+"</td>";
    
    console.log(getDateForList(value.tanggal) + " / " + $('[name=tanggal_laporan]').val());
	
	    row += '<td class="text-center">';
	    //if(getDateForList(value.tanggal)==$('[name=tanggal_laporan]').val()){
		if(allowed){
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	    }else{
	    	row += '<small>No Action</small>';
	    }
	    row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	return row;
}