var selected_id="";
var selected_id_header="";
var selected_action="";
var appendIds = [];
var url = ctx+"/kesehatan/";

var flag = false;

function resetform(){
	selected_id = '';
	$('#kondisi_sakit_checkbox').addClass('d-none');
	$('#frmInput').find('input, select, button').prop('disabled',false);
	
	$('[name=status_sehat_laporan][value=YES]').prop('checked', false);
	$('[name=status_sehat_laporan][value=NO]').prop('checked', false);
	$('[name=keterangan_sakit_laporan]').prop('disabled',true);
	$('[name=lama_gejala_sakit_laporan]').prop('disabled',true);
	
	$('[name=kondisi_sakit_demam_laporan]').prop( "checked", false );
	$('[name=kondisi_sakit_batuk_laporan]').prop( "checked", false );
	$('[name=kondisi_sakit_pilek_laporan]').prop( "checked", false );
	$('[name=kondisi_sakit_tenggorokan_laporan]').prop( "checked", false );
	$('[name=kondisi_sakit_sesak_laporan]').prop( "checked", false );
	
	$('[name=keterangan_sakit_laporan]').val('');
	$('[name=lama_gejala_sakit_laporan]').val('');
	
	$('#btn-save').attr('onclick','save()');
	$('#btn-save').html('<i class="fas fa-save"></i> SIMPAN');
}

function init(){
    $('[id=btn-save]').prop('disabled',true);
    $('[name=tanggal_laporan]').val(moment().format('DD-MM-YYYY'));
    $('[name=tanggal_laporan]').datepicker({
	      showOtherMonths: true,
	      selectOtherMonths: true,
	      dateFormat : "dd-mm-yy",
    	  maxDate: 0 
	});
    
    $('[name=tanggal_laporan]').change(function(){ 
    	flag = false;
    	$('[id=btn-save]').prop('disabled',true);
    	$('[id=btn-save]').prop('title','Klik tombol "Tampilkan" terlebih dahulu!');
	});
	setLaporan();
	
	$('[name=status_sehat_laporan]').on('change',function(){
		console.log(this.value);
		if(this.value=="NO"){
			
			$('#kondisi_sakit_checkbox').removeClass('d-none');
			$('[name=keterangan_sakit_laporan]').prop('disabled',false);
			$('[name=lama_gejala_sakit_laporan]').prop('disabled',false);
			
			$('[name=kondisi_sakit_demam_laporan]').prop( "checked", false );
			$('[name=kondisi_sakit_batuk_laporan]').prop( "checked", false );
			$('[name=kondisi_sakit_pilek_laporan]').prop( "checked", false );
			$('[name=kondisi_sakit_tenggorokan_laporan]').prop( "checked", false );
			$('[name=kondisi_sakit_sesak_laporan]').prop( "checked", false );
		}else{
			$('#kondisi_sakit_checkbox').addClass('d-none');
			$('[name=keterangan_sakit_laporan]').prop('disabled',true);
			$('[name=lama_gejala_sakit_laporan]').prop('disabled',true);
			
			
			$('[name=keterangan_sakit_laporan]').val('');
			$('[name=lama_gejala_sakit_laporan]').val('');
		}
	});
	
	$('#btn-tampilkan').click(function(){
		setLaporan();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
	});
}

function displayData(data){
	$("[name=status_sehat_laporan][value='"+data.status_sehat+"']").prop('checked', true);
	if(data.status_sehat == 'NO'){
		$('#kondisi_sakit_checkbox').removeClass('d-none');
	}else{
		$('#kondisi_sakit_checkbox').addClass('d-none');
	}
	// kosongin dulu
	$('[name=kondisi_sakit_demam_laporan]').prop('checked', false);
	$('[name=kondisi_sakit_batuk_laporan]').prop('checked', false);
	$('[name=kondisi_sakit_pilek_laporan]').prop('checked', false);
	$('[name=kondisi_sakit_tenggorokan_laporan]').prop('checked', false);
	$('[name=kondisi_sakit_sesak_laporan]').prop('checked', false);
	
	$('[name=kondisi_sakit_demam_laporan][value='+data.kondisi_sakit_demam+']').prop('checked', true);
	$('[name=kondisi_sakit_batuk_laporan][value='+data.kondisi_sakit_batuk+']').prop('checked', true);
	$('[name=kondisi_sakit_pilek_laporan][value='+data.kondisi_sakit_pilek+']').prop('checked', true);
	$('[name=kondisi_sakit_tenggorokan_laporan][value='+data.kondisi_sakit_tenggorokan+']').prop('checked', true);
	$('[name=kondisi_sakit_sesak_laporan][value='+data.kondisi_sakit_sesak+']').prop('checked', true);
	$('[name=keterangan_sakit_laporan]').val(data.keterangan_sakit);
	$('[name=lama_gejala_sakit_laporan]').val(data.lama_gejala_sakit);
    checkStatus(data);
}

function setLaporan(){
	selected_id="";
	var param="?"+$('#frmInput').serialize();
	$.getJSON(ctx+"/kesehatan/check-laporan"+param, function(response) {
        if(response.code == 200) {
        	flag = true;
        	$('[id=btn-save]').prop('disabled',false);
        	$('[id=btn-save]').prop('title','');
            var data = response.data;
            console.log('me: ', data);
            if(data.length==0) {
            	//$('#msg-nip').html("Anda belum melaporkan status kesehatan anda.");
            	$('#msg-nip').html("*Laporan belum diinput.");
				$("#msg-nip").attr('class', 'invalid-feedback');
				resetform();
            }else{
            		$('#msg-nip').html("");
            		selected_id=data[0].id;
            		displayData(response.data[0]);
            }
//            else {
//            	$('#msg-nip').html("*Diupdate "); //27-mar-2020 10:45
//				$("#msg-nip").attr('class', 'valid-feedback');
//				//selected_id_header=data[0].id;
//            	//renderDisplay(data);
//            }
            
            
        }else{
        	alert(response.message);
        }
    });
}
function checkStatus(data){
	if(data!=null){
    	$('#btn-save').html('<i class="fas fa-pencil-alt"></i> EDIT');
    	$('#frmInput').find('input, select, button').prop('disabled',true);
    	$('#btn-save').attr('onclick','edit()');
    }else{
    	$('#frmInput').find('input, select, button').prop('disabled',false);
    	$('#btn-save').attr('onclick','save()');
    	$('#btn-save').html('<i class="fas fa-save"></i> SIMPAN');
    }
	$('input[name=tanggal_laporan]').prop('disabled',false);
	$('#btn-tampilkan').prop('disabled',false);
}
function edit(){
	if(flag == false) { alert('Klik tombol "Tampilkan" terlebih dahulu!'); return false; }
	$('#frmInput').find('input, select, button').prop('disabled',false);
	$('#btn-save').attr('onclick','save()');
	$('#btn-save').html('<i class="fas fa-save"></i> SIMPAN');
	//$("[name=status_sehat_laporan][value='"+$("[name=status_sehat_laporan]").val()+"']").trigger('change');
	if($("[name=status_sehat_laporan][value=NO]").is(':checked')){
		$('#kondisi_sakit_checkbox').removeClass('d-none');
		
	}else{
		$('[name=keterangan_sakit_laporan]').prop('disabled',true);
		$('[name=lama_gejala_sakit_laporan]').prop('disabled',true);
	}
}
function save(){
//	if(flag == false) { alert('Klik tombol "Tampilkan" terlebih dahulu!'); return false; }
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
	ajaxPOST(ctx + "/kesehatan/save",obj,'succSave','errSave');
}
function succSave(response){
//	flag = false;
	checkStatus(response.data);
	Swal.fire({
	  type: 'success',
	  title: "Data Berhasil Disimpan.",
	  showConfirmButton: true,
	  allowOutsideClick: false
	});
	$('#msg-nip').html("");
}

function errSave(response){
	if(response.responseJSON.code==400){
		Swal.fire({
		  type: 'warning',
		  title: response.responseJSON.message,
		  showConfirmButton: true,
		  allowOutsideClick: false
		});
	}else{
	 	alert("something wrongess");
	}
}

/*function renderDisplay(data, page = 1, limit = 50){//
    var tbody = $('#tbl-data').find($('tbody'));
    if(page == 1) tbody.text('');
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

     var row = '';
     var i = 1;
    	 $.each(data[0].detilPekerjaan, function(key, value) {
              row+=renderRow(value, key);
          });
     if(row == ''){
         tbody.text('');
         tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
     }else{
         if(page == 1) tbody.html(row);
         else tbody.html(tbody.html()+row);
     }
}

function save(){
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
	ajaxPOST(ctx + "/pekerjaan/save",obj,'onModalActionSuccess','errSave');
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) {
		ajaxGET(url+id+'?'+'&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	}else{
		ajaxPOST(url+id+'/'+selected_action+'?',{},'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	//$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function fillFormValue(value){
	clearForm();
	$('[name=id]').val(value.id);
	$('[name=status_tusi]').val(value.status_tusi);
	$('[name=nama]').val(value.nama);
	$('[name=uraian]').val(value.uraian);
	$('[name=output]').val(value.output);
}

function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	//$.LoadingOverlay("hide");
	//stopLoading('btn-save', 'Simpan');
	//stopLoading('btn-modal-confirm-yes', 'Yes');
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
		num=num+1;
	}
    row += '<td scope="row">'+(num)+'</td>';
    row += "<td>"+(value.status_tusi=="NO"?"TAMBAHAN":"TUSI")+"</td>";
    row += "<td>"+value.nama+"</td>";
    row += "<td>"+value.uraian+"</td>";
    row += "<td>"+value.output+"</td>";

	row += '<td class="text-center">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash text-danger"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	return row;
}*/