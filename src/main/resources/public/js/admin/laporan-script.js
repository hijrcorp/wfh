var url = ctx + "/user/";
var selected_id=0;
function init() {
	
	display(1);
	getPositionList();
	getUnitKerja();
	getJabatan();
	
    $("#btnAdd").click(function(e){ add(); });
    $("#btnImport").click(function(e){ addImportPengguna(); });
	$("#btnReset").click(function(e){ reset(); });
	$("#btnRefresh").click(function(e){ refresh(); });
	$('#frmInput').submit(function(e){ save(); e.preventDefault(); });
	$('#txtsearch').keypress(function(e){ if(e.keyCode == 13) { search(); e.preventDefault(); } });
}

function search(){
	display(1);
}

function getPositionList(){
	$.getJSON(url+'position/'+"?token="+token, function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=position]").append(new Option(value.name_position, value.position_id));
			});
		}
	});
}
function getUnitKerja(){
	$.getJSON(ctx+'/ref/unit-kerja/list', function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=unit_kerja]").append(new Option(value.nama_unit_kerja, value.id_unit_kerja));
			});
		}
	});
}
function getJabatan(){
	$.getJSON(ctx+'/ref/jabatan/list', function(response) {
		console.log(response);
		if (response.code == '200') {
			$.each(response.data, function(key, value) {
				$("[name=jabatan]").append(new Option(value.namaJabatan, value.idJabatan));
			});
		}
	});
}

function add(){
    window.location.href="form-laporan";
	/*reset();
    $("#progressBar").hide();
    $("#passwordForm").show();
    $("#modalPengguna").modal('show');*/
}

function addImportPengguna(){
    $('#frm-import-pengguna')[0].reset();
    $('#msgImport').hide();
    $('#modal-import-pengguna').modal('show');
}

function importPengguna(){
    var obj = new FormData(document.querySelector('#frm-import-pengguna'));
    //obj+="&position=6";
    ajaxPOST(ctx + '/user/insert-pegawai',obj,'succSavImport','failSavImport','errSavImport');
}
function succSavImport(response){
    if(response.status == '200') {
        refresh();
        $('#modal-import-pengguna').modal('hide');
        $('#frm-import-pengguna')[0].reset();
        $('#msgImport').show();
        $('#msgImport').html(response.message);
        $('#msgImport').addClass('alert-success');
        $('#msgImport').removeClass('alert-danger');
    } else {
        failSavImport(response);
    }
}
function failSavImport(response){
    $('#msgImport').show();
    $('#msgImport').addClass('alert-danger');
    $('#msgImport').removeClass('alert-success');
    $('#msgImport').html(response.message);
}
function errSavImport(response){
    alert("kesalahan dari server");
}

function validate(){
	var msg = '';
	
	if($('#password').val() != $('#repassword').val()){
		msg += 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	// if save existing record (EDIT/UPDATE)
	if ($('#id').val() == '0'){
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}else{
		if($('#position').val() == '0' || $('#fullname').val() == '' || $('#username').val() == ''){
			msg += 'Input masih belum lengkap';
		}
	}	
	return msg;
}
function save(){
	var obj = new FormData(document.querySelector('#frmInput'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(url + 'save',obj,'succSav','onSaveError');
}
function succSav(response){
	refresh();
	$('#modalPengguna').modal('hide');
	$('#frmInput')[0].reset();
}
function onSaveError(response){
	console.log(response.responseJSON.message);
	addAlert('msg', "alert-danger", response.responseJSON.message);
}

//function save(){
//	var msg = validate();
//	
//	if (msg == '') {
//		cleanMessage('msg');
//		$("#progressBar").show();
//		
//		var obj = {
//				"user" : {
//					"id" : $('#id').val(),
//					"firstName" : $('#first_name').val(),
//					"lastName" : $('#last_name').val(),
//					"currentPassword" : $('#password').val(),
//					"username" : $('#username').val(),
//					"position" : {
//						"id": $('#position').val()
//					},
//					"description" : $('#description').val()
//				}
//		}
//
//		console.log(JSON.stringify(obj));
//		$.ajax({
//	        url : url+"save?token="+token,
//	        type : "POST",
//	        traditional : true,
//	        contentType : "application/json",
//	        dataType : "json",
//	        data : JSON.stringify(obj),
//	        success : function (response) {
//	        	$("#progressBar").hide();
//	        	if (response.status == 'OK') {
//	        		
//	        		$("#modalPengguna").modal('hide');
//	        		refresh();
//	        		reset();
//				}else{
//					var msg = 'Gagal Simpan. ';
//					//if(response.message.indexOf('DUPLICATE') >= 0) {
//					//	var errmsg = response.message.split(':');
//					//	addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
//					//}else{
//						addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
//					//}
//					
//				}
//	        },
//	        error : function (response) {
//	        	$("#progressBar").hide();
//	        	alert("Connection error");
//	        },
//	    });
//	}else{
//		addAlert('msg', "alert-danger", msg);
//	}
//	
//}

function reset(){
	cleanMessage('msg');
    selected_id = '';
    $('#frmInput').trigger('reset');
	
	$('#id').val('0');
	$('#fullname').val('');
	$('#username').val('');
	$('#first_name').val('');
	$('#last_name').val('');
	$('#password').val('');
	$('#repassword').val('');
	$('#password1').val('');
	$('#repassword1').val('');
	$('#position').val('0');
	$('#description').val('');
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modal-confirm").modal('show');
}

function doRemove(id){
	$("#progressBar1").show();
	ajaxPOST(url+id+'/delete',null,'succRem','failRem','errRem');
}
function succRem(response){
	console.log(response);
	refresh();
	$("#modal-confirm").modal('hide');
}

function edit(id){
	reset();
	$("#progressBar").show();
	$("#passwordForm").hide();
	$("#modalPengguna").modal('show');
	console.log(url+id);
	ajaxGET(url+id,'onSuccessById','onFailedById','onError');
	selected_id = id;
}
function onSuccessById(response) {
	var o = response.data;
	console.log(response);
	//$('[name=first_name]').val(o.firstName);
	$('[name=last_name]').val(o.lastName);
	$('[name=username]').val(o.username);
	$('[name=position]').val(o.positionId);
	$('[name=email]').val(o.email);
	$('[name=gender]').val(o.gender);
	$('[name=birth_date]').val(o.birthDate);
	$('[name=mobile_phone]').val(o.mobilePhone);

	$("#progressBar").hide();
}

function editPassword(id){
	$("#progressBar2").hide();
	$("#btnUpdate").data("id",id);
	$("#modalPassword").modal('show');
}

function doUpdate(id){
	var obj = new FormData(document.querySelector('#frmInputPass'));
	$("#progressBar2").show();
	ajaxPOST(url+id+'/updatePass',obj,'succUpdate','onUpdateError');
}
//function doUpdate(){
//	var obj = $('#frm-password').serialize();
//	ajaxPOST(ctx + '/user/'+paramId+'/updatePass',obj,'succUpdate','failSav','errSav');
//}
function succUpdate(response){
	$('#modalPassword').modal('hide');
	$('#frmInputPass')[0].reset();
}
function onUpdateError(response){
	console.log(response.responseJSON.message);
	addAlert('msg1', "alert-danger", response.responseJSON.message);
}
//batas
function displayOld(page = 1){
	var param = "";
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		param = "&"+$('#column').data('id')+"="+$('#txtsearch').val()
	}

	param = "?page="+page+param;
	
	var tbody = $('#tbl').find($('tbody'));
	$('#btnRefresh').data("id",page);
	ajaxGET(url + 'list'+param+"&limit=",'succDisp','failDisp');
}
function succDisp(response){
	var tbody = $("#tbl").find('tbody');
	tbody.text("");
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	var row = "";
	console.log(response.data);
	$.each(response.data,function(key,value){
		row += "<tr scope='row'>";
			row += "<th class=text-left>"+(key+1)+"</th>";
			row += "<td>"+(this.name != "null null" ? this.name : "(Tidak Ada)")+"</td>";
			row += "<td>"+this.username+"</td>";
			row += "<td>"+this.positionName+"</td>";
			row += "<td>"+(this.unitKerjaName != null && this.unitKerjaName != "" ? this.unitKerjaName : "(Tidak Ditentukan)")+"</td>";
			row += "<td>"+(this.email != null && this.email != "" ? this.email : "(Tidak Ada)")+"</td>";
			//row += "<td>"+renderBtnAct(this.id,this.dokumen)+"</td>";
		
			row += '<td>';
			row += '<button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
			row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button> ';
			row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';
			row += '</td>';
			
		row += "</tr>";
	});
	row == "" ? tbody.html('<tr><td colspan="10" align="center">Data Tidak Ada.</td></tr>') : tbody.html(row);
	createPagination1('tblpages','display', response);
}

function failDisp(response){
	console.log(response);
}
function renderBtnAct(id,dokumen){
	return '<button onclick="edit('+id+')" class="btn btn-yellow btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-pencil"></i></button>'+
		   '<button onclick="editPassword('+id+')" class="btn btn-info btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-lock"></i></button>'+
		   '<button onclick="remove('+id+')" class="btn btn-danger btn-xs shadowmod waves-effect waves-light" style="margin: 1px;"><i class="fa fa-trash"></i></button>';
}







function onChangeType(pos){
    //alert(obj.value);
    if(pos == 6){// Operator
        $('.satker-row').show();
    }else{
        $('.satker-row').hide();
    }
    
    if(pos == 3 || pos == 4){// Analis / Supervisor
        $('.unit-kerja-row').show();
    }else{
        $('.unit-kerja-row').hide();
    }
    
}

function display(page = 1, limit = 50){
    
    var tbody = $('#tbl').find($('tbody'));
    $('#btnRefresh').data("id",page);
    
    if(page == 1) tbody.text('');
    tbody.append('<tr id="table-loading"><td colspan="20" align="center">Loading data....</td></tr>');

    var param = $('#form-filter').serialize();
    param += '&position-exclude=100';

    ajaxGET(url+'list-load-more?page='+page+'&limit='+limit+'&'+param, function(response){
        console.log(response);
        if (response.code == 200) {
            var row = '';
            //tbody.text('');
            var i = 1;
            //console.log(response.data);
            $('[id=table-loading], [id=more-button-row]').remove();
            $.each(response.data, function(key, value) {
                row += "<tr scope='row'>";
                    row += '<td scope="row">'+(((page-1)*limit)+i)+'</td>';
                    row += "<td>";
                	row += "<strong>Nama : </strong>"+(this.name != null && this.name != "" ? this.name : "(Tidak Ada)")+" ("+(this.gender=='M'?'L':'P')+")</br>";
                	row += "<strong>NIP : </strong>"+(this.username)+"</br>";
                	row += "<strong>Tgl.Lahir : </strong>"+(this.birthDate != null && this.birthDate != "" ? this.birthDate : "(Tidak Ada)")+"";
                    row += "</td>";
                    row += "<td>";
                 	row += "<strong>Jabatan : </strong>"+(this.namaJabatan != null && this.namaJabatan != "" ? this.namaJabatan : "(Tidak Ada)")+"</br>";
                	row += "<strong>Unit Kerja : </strong>"+(this.unitKerjaName != null && this.unitKerjaName != "" ? this.unitKerjaName : "(Tidak Ada)")+"</br>";
                    row += "</td>";
//                    row += "<td>"+(this.unitKerjaName != null && this.unitKerjaName != "" ? this.unitKerjaName : "(Tidak Ditentukan)")+"</td>";
                    row += "<td>";
                	row += "<strong>Email : </strong>"+(this.email != null && this.email != "" ? this.email : "(Tidak Ada)")+"</br>";
                	row += "<strong>HP : </strong>"+(this.mobilePhone != null && this.mobilePhone != "" ? this.mobilePhone : "(Tidak Ada)")+"</br>";
                    row += "</td>";
                    row += "<td>"+this.positionName;
                    row += "</td>";
                    row += '<td nowrap>';
                    row += '<button onclick="edit(' + value.id + ')" class="btn btn-info btn-sm"><span class="fa fa-pencil-alt"></span></button> ';
                    row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-primary btn-sm"><span class="fa fa-lock"></button> ';
                    row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-danger btn-sm"><span class="fa fa-trash"></button>';
                    row += '</td>';
                row += '</tr>';
                i++;
            });
            if(response.next_more){
                row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
                row += '    <button type="button" class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
                row += '</div></td></tr>';
            }
            if(row == ''){
                tbody.text('');
                tbody.append('<tr><td colspan="20" align="center">Data tidak tersedia</td></tr>');
            }else{
                if(page == 1) tbody.html(row);
                else tbody.html(tbody.html()+row);
            }
        }else{
            alert("Connection error!!");
        }
        }
    );
}
