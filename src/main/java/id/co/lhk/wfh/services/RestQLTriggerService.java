package id.co.lhk.wfh.services;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class RestQLTriggerService {
	
	public void onAfterInsert(String entity, Map<String, Object> data) {
		System.out.println("onAfterInsert: >> call another service method: " + data.get("id"));
		
	}
	
	public void onAfterUpdate(String entity, Map<String, Object> data, Map<String, Object> prev) {
		System.out.println("onAfterUpdate: >> call another service method");
		System.out.println(prev.get("alamat_rumah") + " >> DIFF << " + data.get("alamat_rumah"));
	}
	
	public void onAfterDelete(String entity, Map<String, Object> data) {
		System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
	}
	
}
