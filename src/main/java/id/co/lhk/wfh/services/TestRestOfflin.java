package id.co.lhk.wfh.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TestRestOfflin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			URL url = new URL("http://localhost:7079/lapgas/surat-tugas-tim/list");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbjo6SVRKRU4iLCJwb3NpdGlvbl9uYW1lIjoiQWRtaW4iLCJyZWFsX25hbWUiOiJTdXBlciBBZG1pbiIsIm9yZ2FuaXphdGlvbl9uYW1lIjoiSW5zcGVrdG9yYXQgSmVuZGVyYWwiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiLCJST0xFX1RVIiwiUk9MRV9BTkFMSVMiLCJST0xFX01BTkFHRVIiLCJST0xFX0FETUlOIiwiUk9MRV9PUFIiLCJST0xFX1NVUEVSVklTT1IiXSwiY2xpZW50X2lkIjoibGFwZ2FzLXdlYiIsImF1ZCI6WyJsaGtfbGFwZ2FzIiwibGhrX2Jhc2UiLCJsaGtfbGhhIiwibGhrX3RsIl0sInVzZXJfaWQiOiIxNDk5ODUxMTMzMjY1IiwidXNlcl9waWN0dXJlIjoiaHR0cHM6Ly9pdGplbi5oaWpyLmNvLmlkL2FwcC9waWN0dXJlL3VzZXIvMTQ5OTg1MTEzMzI2NSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJvcmdhbml6YXRpb25faWQiOiIxNTA4NjI5MTM0ODI3IiwiZXhwIjoxNTY1MDc2NjA5LCJqdGkiOiI2YWM3NGI3Yi01N2RmLTRhNzUtOTkwNC03ZjQ1NjNiNGYxMGYiLCJwb3NpdGlvbl9pZCI6IjEifQ.Ls2BB9Ms3TMf8WKm1CY3CcLfRQLGGavCyo7OgHTMYEk");
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException  e) {
			e.printStackTrace();
		}
	}

}
