package id.co.lhk.wfh.services;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import id.co.lhk.wfh.OrganizationReference;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.mapper.UserMapper;

@Service
public class NotificationService implements UserDetailsService {
	
	@Value("${app.organization.id}")
	private String organizationId;
	
	@Value("${app.organization.code}")
	private String organizationCode;

    
    @Value("${app.state}")
    private String appState;
    
    @Value("${app.folder}")
    private String appFolder;
    
    @Value("${app.folder.dev}")
    private String appFolderDev;
    
	@Value("${app.url.ntfc}")
    private String appUrlNtfc;
	
	@Value("${email.setting}")
    private String emailSetting;

	private SimpleDateFormat sda = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
  
    static String capitailizeFirstWord(String str) {
    	//str = str.toLowerCase();
        StringBuffer s = new StringBuffer();
        char ch = ' '; 
        for (int i = 0; i < str.length(); i++) { 
            if (ch == ' ' && str.charAt(i) != ' ') 
                s.append(Character.toUpperCase(str.charAt(i))); 
            else
                s.append(str.charAt(i)); 
            ch = str.charAt(i); 
        }
        return s.toString().trim(); 
    } 

    public freemarker.template.Configuration getConfiguration() throws IOException{
    	freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_27);

        // Where do we load the templates from:
		String templateFolder = appFolderDev + "mail/";
		if(appState.equals("production")) {
			templateFolder = appFolder + "mail/";
		}
        cfg.setTemplateLoader(new FileTemplateLoader(new File(templateFolder)));

        // Some other recommended settings:
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return cfg;
    }
    
    public void sendMailReport(String email, String filename, String password, File file) throws Exception {
    	sendMailReport(email, null, filename, password, file);
    }
        
    public void sendMailReport(String email, String name, String filename, String password, File file) throws Exception {
    	
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_laporan_alhp.html");
        StringWriter stringWriter = new StringWriter();
        Map<String, Object> input = new HashMap<String, Object>();
        input.put("filename", name);
        input.put("password", password);
        input.put("datetime", datetime);
		template.process(input, stringWriter);
        
		String reportFileName = filename.replaceAll(".pdf", "").replaceAll(".xlsx", "").replaceAll(".xls", "");
		String emails = "EMAIL_GROUP_TO:ubaycreative@gmail.com";//+email;
        String subject = "Laporan LHA - " + (name!=null? (name.equals("datetime")?"(" + datetime + ")":name) : reportFileName);
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] password: " + password);
        System.out.println("[sendMailReport] datetime: " + datetime);
        //System.out.println("[sendMailReport] message: " + message);
        System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        map.add("email_attachments", new FileSystemResource(file));
        
        System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        callSendEmailService(map);
		
    }
   
    public void sendMailPemberitahuan(String emails, String tanggalLaporan) throws Exception {
    	
    	Calendar now = Calendar.getInstance();
    	String datetime =now.get(Calendar.HOUR_OF_DAY) + ".00";
		
        Template template = getConfiguration().getTemplate("template-pemberitahuan-kinerja-kesehatan.html");
        StringWriter stringWriter = new StringWriter();
        Map<String, Object> input = new HashMap<String, Object>();
        input.put("tanggalLaporan", tanggalLaporan);
        input.put("datetime", datetime);
		template.process(input, stringWriter);
        
		//String reportFileName = filename.replaceAll(".pdf", "").replaceAll(".xlsx", "").replaceAll(".xls", "");
		//String emails = "EMAIL_GROUP_TO:kayumiman@gmail.com";//+email;
		//String emails = "EMAIL_GROUP_TO:"+email;
        //String subject = "Laporan LHA - " + (name!=null? (name.equals("datetime")?"(" + datetime + ")":name) : reportFileName);
		String subject="Notifikasi Laporan Pegawai";
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        System.out.println("[sendMailReport] message: " + message);
        //System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        //map.add("email_attachments", new FileSystemResource(file));
        
        //System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        callSendEmailService(map);
    }
    
    public void callSendEmailService(LinkedMultiValueMap<String, Object> payload) {
/*
        // harus ada auth header
        //ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        //@RequestHeader("Authorization") String authHeader
        String authHeader = "";
        String tokenValue = authHeader.replace("Bearer", "").trim();
        //BearerAuthRestTemplate restTemplate = new BearerAuthRestTemplate(tokenValue);
        RestTemplate restTemplate = new RestTemplate();
        String urlNotificationSave = appUrlNtfc + "/notification/save";
        System.out.println("url: " + urlNotificationSave);
        //restTemplate.post

        // set headers
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(payload, headers);
        
        ResponseEntity<String> loginResponse = restTemplate.postForEntity(urlNotificationSave, request, String.class);
        //ResponseEntity<String> result = restTemplate.exchange(urlNotificationSave, HttpMethod.POST, request, String.class);
        
        System.out.println("hasil api: " + loginResponse);
*/
	}
	
    public static void main(String[] args) {
    	NotificationService ns = new	NotificationService();	
   
    	LinkedMultiValueMap<String, Object> payload = new LinkedMultiValueMap<String, Object>();
    	payload.add("notification_app", "HIJRTEST");
    	payload.add("notification_title", "berita penting");
    	payload.add("notification_body_text", "halo.., cuma testing");
    	payload.add("notification_settings", "ITJEN");
    	payload.add("notification_user_added", "101010");
    //payload.add("email_attachments", new FileSystemResource(file));
    	payload.add("email_recipients", "EMAIL_SINGLE_TO:gustaroska@yahoo.com,EMAIL_SINGLE_TO:refit.gustaroska@gmail.com,EMAIL_SINGLE_TO:gustaroska@outlook.com");
    //	payload.add("email_recipients", "EMAIL_SINGLE_TO:gustaroska@yahoo.com");
        ns.callSendEmailService(payload);
	
}
	
	

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    		String[] userOrg =  s.split("::");
    		String username = userOrg[0];
    		String orgcode = OrganizationReference.HEDES.toString();
    		if(userOrg.length > 1) {
    			orgcode = userOrg[1];
    		}
    		String clause = "(" + OauthUserOrganization.USERNAME + "='"+username+"' or " + OauthUserOrganization.EMAIL + "='"+userOrg[0]+"' )";
    		clause += " and " + OauthUserOrganization.ORGANIZATION_CODE + "='"+orgcode+"'";
    		List<OauthUserOrganization> lst = userMapper.findListWithOrganization(clause);
    		 
        if (lst.size() > 0 ) {
        		OauthUserOrganization user = lst.get(0);
        	
        	Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        		List<String> roles = userMapper.findRolesByUserOrganization(user.getId(), user.getOrganizationId());
            for (String role : roles) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role));
            }
        		user.setAuthorities(grantedAuthorities);
            
//            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getId(),
//                    user.getPassword(), user.isEnabled(),
//                    user.isAccountNonExpired(), user.isCredentialsNonExpired(),
//                    user.isAccountNonLocked(), grantedAuthorities);
//
//            return userDetails;
            
            return user;
        } else {
            throw new UsernameNotFoundException(String.format("User with login ID [%s] not found", s));
        }
    }

   
}
