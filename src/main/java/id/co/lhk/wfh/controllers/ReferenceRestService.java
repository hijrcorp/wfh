package id.co.lhk.wfh.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.Message;
import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.mapper.ReferenceMapper;


@RestController
@RequestMapping("/ref")
public class ReferenceRestService {

	@Autowired
	private ReferenceMapper refMapper;

//	@RequestMapping(value = "/kelompokUPT", method = RequestMethod.GET,headers="Accept=application/json")
//	public Message getListCategory() throws Exception{
//		Message resp = new Message(Message.SUCCESS_CODE);
//		resp.setData(refMapper.getListKelompokUPT());
//		return resp;
//	}
//	@RequestMapping(value = "/indikator-kinerja", method = RequestMethod.GET,headers="Accept=application/json")
//	public Message getListIndikatorKinerja(
//    		@RequestParam("indikator_kinerja_id") Optional<String> id,
//    		@RequestParam("jenis") Optional<String> jenis
//    		) throws Exception{
//		Message resp = new Message(Message.SUCCESS_CODE);
//		QueryParameter param = new QueryParameter();
//		if(jenis.isPresent()) param.setClause(param.getClause() + " AND "+ IndikatorKinerja.JENIS + " LIKE '%"+jenis.get()+"%'");
//		List<IndikatorKinerja> data = indikatorKinerjaMapper.getList(param);
//		resp.setData(data);
//		return resp;
//	}
//	@RequestMapping(value = "/program", method = RequestMethod.GET,headers="Accept=application/json")
//	public Message getListProgram() throws Exception{
//		Message resp = new Message(Message.SUCCESS_CODE);
//		resp.setData(refMapper.getListProgram());
//		return resp;
//	}
//	@RequestMapping(value = "/tugasFungsi/", method = RequestMethod.GET,headers="Accept=application/json")
//	public Message getListTugasFungsi() throws Exception{
//		Message resp = new Message(Message.SUCCESS_CODE);
//		resp.setData(refMapper.getListTugasFungsi());
//		return resp;
//	}
//	@RequestMapping(value = "/unitAuditiForTugasFungsi/{id}", method = RequestMethod.GET,headers="Accept=application/json")
//	public Message getUnitAuditiForTugasFungsi(@PathVariable String id) throws Exception{
//		Message resp = new Message(Message.SUCCESS_CODE);
//		resp.setData(refMapper.findBySatker(id));
//		return resp;
//	}
	@RequestMapping(value = "/unit-kerja/list", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList getListUnitKerja() throws Exception{
		ResponseWrapperList resp = new ResponseWrapperList();
		resp.setData(refMapper.getListUnitKerja());
		return resp;
	}

	@RequestMapping(value = "/jabatan/list", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList getListJabatan() throws Exception{
		ResponseWrapperList resp = new ResponseWrapperList();
		resp.setData(refMapper.getListJabatan());
		return resp;
	}
	
}
