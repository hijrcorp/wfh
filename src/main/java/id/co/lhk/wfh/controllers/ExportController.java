package id.co.lhk.wfh.controllers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.core.StyleExcel;
import id.co.lhk.wfh.domain.Dashboard1;
import id.co.lhk.wfh.domain.Dashboard2;
import id.co.lhk.wfh.domain.DetilKesehatan;
import id.co.lhk.wfh.domain.DetilPekerjaan;
import id.co.lhk.wfh.domain.Kesehatan;
import id.co.lhk.wfh.domain.Laporan1;
import id.co.lhk.wfh.domain.Laporan2;
import id.co.lhk.wfh.domain.Laporan3;
import id.co.lhk.wfh.domain.Laporan4;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.DashboardMapper;
import id.co.lhk.wfh.mapper.KesehatanMapper;
import id.co.lhk.wfh.mapper.LaporanMapper;
import id.co.lhk.wfh.mapper.PekerjaanMapper;


@RestController
@RequestMapping("/export")
public class ExportController extends BaseController {

	@Autowired
	private KesehatanMapper kesehatanMapper;

	@Autowired
	private PekerjaanMapper pekerjaanMapper;
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	
	@RequestMapping(value = "/detil-kesehatan", method = RequestMethod.GET)
	public void downloadDetilKesehatan(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Integer id, 
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("nama_unit_kerja") Optional<String> nama_unit_kerja, 
			@RequestParam("nip_pegawai") Optional<String> nip_pegawai, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
			@RequestParam("status_kesehatan") Optional<String> status_kesehatan, 
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page) throws Exception {
    	
		if(!passCookieAuthorization(request)) throw new Exception("User not authorized");
		
		String nipPegawai = "Semua";
		String statusKesehatan = "Semua";
		String namaUnitKerja = "Semua";
		String namaPeriode = "- s/d -";

		QueryParameter param = new QueryParameter();
		if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) {
			namaUnitKerja = nama_unit_kerja.get();
			param.setClause(param.getClause()+" AND "+DetilKesehatan.ID_UNIT_KERJA+" ='"+id_unit_kerja.get()+"'");
		}
		if(nip_pegawai.isPresent() && !nip_pegawai.get().equals("")) {
			nipPegawai = nip_pegawai.get();
			param.setClause(param.getClause()+" AND "+DetilKesehatan.NIP_PEGAWAI+" ='"+nip_pegawai.get()+"'");
		}
		if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
			namaPeriode = changeToDDMMYYYY(periode_awal.get()) + " s/d " + changeToDDMMYYYY(periode_akhir.get());
			param.setClause(param.getClause()+" AND "+DetilKesehatan.TANGGAL+" BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'");
		}
		else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) {
			namaPeriode = changeToDDMMYYYY(periode_awal.get()) + " s/d -";
			param.setClause(param.getClause()+" AND "+DetilKesehatan.TANGGAL+" >='"+periode_awal.get()+"'");
		}
		else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
			namaPeriode = "- s/d " + changeToDDMMYYYY(periode_akhir.get());
			param.setClause(param.getClause()+" AND "+DetilKesehatan.TANGGAL+" <='"+periode_akhir.get()+"'");
		}
		if(status_kesehatan.isPresent() && !status_kesehatan.get().equals("ALL")) {
			statusKesehatan = status_kesehatan.get();
			param.setClause(param.getClause()+" AND "+DetilKesehatan.STATUS_SEHAT+" ='"+status_kesehatan.get()+"'");
		}
		
		//param.setOrder(DetilKesehatan.NIP_PEGAWAI);
		param.setOrder(DetilKesehatan.ID_UNIT_KERJA + "," + DetilKesehatan.NAMA_PEGAWAI + ", " + DetilKesehatan.TANGGAL);
		if(limit.isPresent()) { 
			param.setLimit(limit.get()); 
		}else { 
			param.setLimit(999999999); 
		}
    	
		List<DetilKesehatan> data = kesehatanMapper.getListDetil(param);
		
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapHeader = new HashMap<>();
		mapHeader.put("header", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true);
		Map<String, Boolean> mapBodyCenter = new HashMap<>();
		mapBodyCenter.put("wrap", true);
		mapBodyCenter.put("center", true);
		Map<String, Boolean> mapBodyCenterYes = new HashMap<>();
		mapBodyCenterYes.put("wrap", true);
		mapBodyCenterYes.put("center", true);
		mapBodyCenterYes.put("green", true);
		Map<String, Boolean> mapBodyCenterNo = new HashMap<>();
		mapBodyCenterNo.put("wrap", true);
		mapBodyCenterNo.put("center", true);
		mapBodyCenterNo.put("red", true);
		Map<String, Boolean> mapBodyCenterMaybe = new HashMap<>();
		mapBodyCenterMaybe.put("wrap", true);
		mapBodyCenterMaybe.put("center", true);
		mapBodyCenterMaybe.put("orange", true);
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		Map<String, Boolean> mapFilter = new HashMap<>();
		mapFilter.put("filter", true);
		mapFilter.put("no-border", true);
		
		
		int panjangKolom = 0;
		String filename = "";
		String titlename = "";
		String sheetname = null;
		List<String> listHeader = new ArrayList<String>();
		List<String> listHeader2 = new ArrayList<String>();
		List<String> listHeader3 = new ArrayList<String>();
		if(id == 1) { 
			panjangKolom = 9; 
			filename = "Laporan-Detil-Kesehatan";
			titlename = "LAPORAN DETIL KEADAAN KESEHATAN PEGAWAI";
			sheetname = "Laporan Detil Kesehatan";
			String[] headerArr = {"No.:3:1","NIP:3:1","NAMA PEGAWAI:3:1","UNIT KERJA:3:1","JABATAN:3:1","NOMOR HP/EMAIL:3:1","ALAMAT:3:1","RIWAYAT PENYAKIT BERAT:3:1","KONDISI KESEHATAN:1:7","LAMA GEJALA (HARI):3:1","TINDAKAN YANG SUDAH DILAKUKAN:3:1"};
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			listHeader2.add("TANGGAL:2:1:8");
			listHeader2.add("SEHAT:2:1:9");
			listHeader2.add("SAKIT:1:5:10");
			listHeader3.add("DEMAM:1:1:10");
			listHeader3.add("BATUK:1:1:11");
			listHeader3.add("PILEK:1:1:12");
			listHeader3.add("SAKIT TENGGOROKAN:1:1:13");
			listHeader3.add("SESAK NAFAS:1:1:14");
		}
		else throw new Exception("Format Laporan "+id+" tidak tersedia!");
		
		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename="+filename+".xlsx"); //name file
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetname==null?filename:sheetname);
		StyleExcel style = new StyleExcel();

		XSSFRow aRow;
	    
		int idxRow = 0;
		int idx = 0;
		int iB = 0,iS = 0;
		idxRow++;
		XSSFRow title = sheet.createRow(idxRow);
		title.createCell(0).setCellValue(titlename);
		title.getCell(0).setCellStyle(style.styleBody(workbook, mapTitle));
		CellRangeAddress range = new CellRangeAddress(idxRow, idxRow, 0, panjangKolom);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Unit Kerja: " + namaUnitKerja);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("NIP Pegawai : " + nipPegawai);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Periode : " + namaPeriode);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);

		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Status Kesehatan: " + statusKesehatan);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		idxRow++;
		idxRow++;
		
		XSSFRow header = sheet.createRow(idxRow++);
		int maxBaris = 1;
		for(String namaHeader : listHeader) {
			if(namaHeader.split(":").length > 1) {
				String nama = namaHeader.split(":")[0]; 
				int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
				int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
				try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
				maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
				header.createCell(idx).setCellValue(nama);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
				//if(idx==15 || idx==16) header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
				if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
					range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
					RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
					sheet.addMergedRegion(range);
				}
				idx += jumlahKolom;
			}
			else {
				header.createCell(idx).setCellValue(namaHeader);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				idx++;
			}
		}
		if(listHeader2.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader2) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		if(listHeader3.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader3) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
					//if(idx==15 || idx==16) header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
	    //List<Object> uniqData = data.stream().map(o -> o.getNipPegawai()).distinct().collect(Collectors.toList());
	    //System.out.println(uniqData);
	    
	    List<String> ht = new ArrayList<>();
		for (DetilKesehatan o : data) {
			String name = o.getNipPegawai();
			ht.indexOf(name);
			int aa = ht.indexOf(name);
			if(aa == -1) ht.add(name);
			else  ht.add("");
		}
		int i = 1;
		int index=0;
		for(DetilKesehatan o : data) {
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			if(!ht.get(index).isEmpty()) {
				aRow.createCell(iB++).setCellValue(i);
				aRow.createCell(iB++).setCellValue(o.getNipPegawai());
				aRow.createCell(iB++).setCellValue(o.getNamaPegawai());
				aRow.createCell(iB++).setCellValue(o.getNamaUnitKerja());
				aRow.createCell(iB++).setCellValue(o.getNamaJabatan());
				aRow.createCell(iB++).setCellValue(o.getHandphone()+" / "+o.getEmail());
				aRow.createCell(iB++).setCellValue(o.getAlamatRumah());
				aRow.createCell(iB++).setCellValue(o.getRiwayatPenyakit());
			}else {
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				i = i-1;
			}
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapTopCenter));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			
			aRow.createCell(iB++).setCellValue(Utils.formatIndonesianReportDate(o.getTanggal()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

			aRow.createCell(iB++).setCellValue(o.getStatusSehat());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

			aRow.createCell(iB++).setCellValue(o.getKondisiSakitDemam());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getKondisiSakitBatuk());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getKondisiSakitPilek());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getKondisiSakitTenggorokan());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getKondisiSakitSesak());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getLamaGejalaSakit()!=null?o.getLamaGejalaSakit():0);
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getKeteranganSakit());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));

			index++;
			i++;
		}
		
		sheet.setColumnWidth(0, (5  * 256) + 200); // ID
		sheet.setColumnWidth(1, (25 * 256) + 200); // Tanggal Laporan
		sheet.setColumnWidth(2, (25 * 256) + 200); // Jumlah
		sheet.setColumnWidth(3, (15 * 256) + 200); // Ya
		sheet.setColumnWidth(4, (15 * 256) + 200); // Tidak
		sheet.setColumnWidth(5, (25 * 256) + 200); // %
		sheet.setColumnWidth(6, (25 * 256) + 200); // %
		sheet.setColumnWidth(7, (25 * 256) + 200); // %
		sheet.setColumnWidth(8, (15 * 256) + 200); // Jumlah
		sheet.setColumnWidth(9, (15 * 256) + 200); // %
		sheet.setColumnWidth(16, (15 * 256) + 200); // %
		
		workbook.write(response.getOutputStream());
	}
	
	@RequestMapping(value = "/detil-kinerja", method = RequestMethod.GET)
	public void downloadDetilKinerja(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Integer id, 
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("nama_unit_kerja") Optional<String> nama_unit_kerja, 
			@RequestParam("nip_pegawai") Optional<String> nip_pegawai, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
    		@RequestParam("limit") Optional<Integer> limit) throws Exception {
    	
		if(!passCookieAuthorization(request)) throw new Exception("User not authorized");
		
		String nipPegawai = "Semua";
		String namaUnitKerja = "Semua";
		String namaPeriode = "- s/d -";

		QueryParameter param = new QueryParameter();
		if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) {
			namaUnitKerja = nama_unit_kerja.get();
			param.setClause(param.getClause()+" AND "+DetilPekerjaan.ID_UNIT_KERJA+" ='"+id_unit_kerja.get()+"'");
		}
		if(nip_pegawai.isPresent() && !nip_pegawai.get().equals("")) {
			nipPegawai = nip_pegawai.get();
			param.setClause(param.getClause()+" AND "+DetilPekerjaan.NIP_PEGAWAI+" ='"+nip_pegawai.get()+"'");
		}
		if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
			namaPeriode = changeToDDMMYYYY(periode_awal.get()) + " s/d " + changeToDDMMYYYY(periode_akhir.get());
			param.setClause(param.getClause()+" AND "+DetilPekerjaan.TANGGAL+" BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'");
		}
		else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) {
			namaPeriode = changeToDDMMYYYY(periode_awal.get()) + " s/d -";
			param.setClause(param.getClause()+" AND "+DetilPekerjaan.TANGGAL+" >='"+periode_awal.get()+"'");
		}
		else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
			namaPeriode = "- s/d " + changeToDDMMYYYY(periode_akhir.get());
			param.setClause(param.getClause()+" AND "+DetilPekerjaan.TANGGAL+" <='"+periode_akhir.get()+"'");
		}
		
		//param.setOrder(DetilPekerjaan.NIP_PEGAWAI);
		param.setOrder(DetilPekerjaan.ID_UNIT_KERJA + "," + DetilPekerjaan.NAMA_PEGAWAI + ", " + DetilPekerjaan.TANGGAL);
		if(limit.isPresent()) { 
			param.setLimit(limit.get()); 
		}else { 
			param.setLimit(999999999); 
		}
		List<DetilPekerjaan> data = pekerjaanMapper.getListDetil(param);
		
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapHeader = new HashMap<>();
		mapHeader.put("header", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true);
		Map<String, Boolean> mapBodyCenter = new HashMap<>();
		mapBodyCenter.put("wrap", true);
		mapBodyCenter.put("center", true);
		Map<String, Boolean> mapBodyCenterYes = new HashMap<>();
		mapBodyCenterYes.put("wrap", true);
		mapBodyCenterYes.put("center", true);
		mapBodyCenterYes.put("green", true);
		Map<String, Boolean> mapBodyCenterNo = new HashMap<>();
		mapBodyCenterNo.put("wrap", true);
		mapBodyCenterNo.put("center", true);
		mapBodyCenterNo.put("red", true);
		Map<String, Boolean> mapBodyCenterMaybe = new HashMap<>();
		mapBodyCenterMaybe.put("wrap", true);
		mapBodyCenterMaybe.put("center", true);
		mapBodyCenterMaybe.put("orange", true);
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		Map<String, Boolean> mapFilter = new HashMap<>();
		mapFilter.put("filter", true);
		mapFilter.put("no-border", true);
		
		
		int panjangKolom = 0;
		String filename = "";
		String titlename = "";
		String sheetname = null;
		List<String> listHeader = new ArrayList<String>();
		List<String> listHeader2 = new ArrayList<String>();
		List<String> listHeader3 = new ArrayList<String>();
		if(id == 1) { 
			panjangKolom = 9; 
			filename = "Laporan-Detil-Kinerja-Pegawai";
			titlename = "LAPORAN DETIL KINERJA PEGAWAI";
			sheetname = "Laporan Detil Kinerja";
			String[] headerArr = {"No.:1:1","NIP:1:1","NAMA PEGAWAI:1:1","UNIT KERJA:1:1","JABATAN:1:1","TANGGAL:1:1","WAKTU:1:1","JENIS PEKERJAAN:1:1","NAMA KEGIATAN:1:1","URAIAN KEGIATAN:1:1","OUTPUT KEGIATAN:1:1"};
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			//listHeader2.add("TANGGAL:2:1:8");
		}
		else throw new Exception("Format Laporan "+id+" tidak tersedia!");
		
		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename="+filename+".xlsx"); //name file
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetname==null?filename:sheetname);
		StyleExcel style = new StyleExcel();

		XSSFRow aRow;
	    
		int idxRow = 0;
		int idx = 0;
		int iB = 0,iS = 0;
		idxRow++;
		XSSFRow title = sheet.createRow(idxRow);
		title.createCell(0).setCellValue(titlename);
		title.getCell(0).setCellStyle(style.styleBody(workbook, mapTitle));
		CellRangeAddress range = new CellRangeAddress(idxRow, idxRow, 0, panjangKolom);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Unit Kerja: " + namaUnitKerja);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("NIP Pegawai : " + nipPegawai);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Periode : " + namaPeriode);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		
		idxRow++;
		idxRow++;
		
		XSSFRow header = sheet.createRow(idxRow++);
		int maxBaris = 1;
		for(String namaHeader : listHeader) {
			if(namaHeader.split(":").length > 1) {
				String nama = namaHeader.split(":")[0]; 
				int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
				int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
				try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
				maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
				header.createCell(idx).setCellValue(nama);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
				//if(idx==15 || idx==16) header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
				if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
					range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
					RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
					sheet.addMergedRegion(range);
				}
				idx += jumlahKolom;
			}
			else {
				header.createCell(idx).setCellValue(namaHeader);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				idx++;
			}
		}
		if(listHeader2.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader2) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		if(listHeader3.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader3) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
					//if(idx==15 || idx==16) header.getCell(idx).setCellStyle(style.styleHeader(workbook, "wrap"));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		
	    List<String> ht = new ArrayList<>();
		for (DetilPekerjaan o : data) {
			String name = o.getNipPegawai();
			ht.indexOf(name);
			int aa = ht.indexOf(name);
			if(aa == -1) ht.add(name);
			else  ht.add("");
		}
		int i = 1;
		int index=0;
		for(DetilPekerjaan o : data) {
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			if(!ht.get(index).isEmpty()) {
				aRow.createCell(iB++).setCellValue(i);
				aRow.createCell(iB++).setCellValue(o.getNipPegawai());
				aRow.createCell(iB++).setCellValue(o.getNamaPegawai());
				aRow.createCell(iB++).setCellValue(o.getNamaUnitKerja());
				aRow.createCell(iB++).setCellValue(o.getNamaJabatan());
			}else {
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				aRow.createCell(iB++).setCellValue("");
				i = i-1;
			}
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapTopCenter));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));

			aRow.createCell(iB++).setCellValue(Utils.formatIndonesianReportDate(o.getTanggal()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue("-");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

			aRow.createCell(iB++).setCellValue((o.getStatusTusi().equals("YES")?"TUSI":"TAMBAHAN"));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(o.getNama());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(o.getUraian());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(o.getOutput());
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));

			index++;
			i++;
		}
		
		sheet.setColumnWidth(0, (5  * 256) + 200); // ID
		sheet.setColumnWidth(1, (25 * 256) + 200); // Tanggal Laporan
		sheet.setColumnWidth(2, (25 * 256) + 200); // Jumlah
		sheet.setColumnWidth(3, (15 * 256) + 200); // Ya
		sheet.setColumnWidth(4, (15 * 256) + 200); // Tidak
		sheet.setColumnWidth(5, (25 * 256) + 200); // %
		sheet.setColumnWidth(6, (25 * 256) + 200); // %
		sheet.setColumnWidth(7, (25 * 256) + 200); // %
		sheet.setColumnWidth(8, (15 * 256) + 200); // Jumlah
		sheet.setColumnWidth(9, (15 * 256) + 200); // %
		
		workbook.write(response.getOutputStream());
	}
    
    private String changeToDDMMYYYY(String date) {
    	try {
	    	String arr[] = date.split("-");
	    	return arr[2] + "/" + arr[1] + "/" + arr[0];
    	} catch(Exception e) {
    		return date;
    	}
    }
}
