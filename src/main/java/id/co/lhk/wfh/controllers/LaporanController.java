package id.co.lhk.wfh.controllers;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.OrganizationReference;
import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.core.StyleExcel;
import id.co.lhk.wfh.domain.Laporan1;
import id.co.lhk.wfh.domain.Laporan2;
import id.co.lhk.wfh.domain.Laporan3;
import id.co.lhk.wfh.domain.Laporan4;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.LaporanMapper;
import id.co.lhk.wfh.mapper.UserMapper;
import id.co.lhk.wfh.services.NotificationService;


@RestController
@RequestMapping("/laporan")
@EnableScheduling
public class LaporanController extends BaseController {
	
	@Value("${jobs.enabled}")
	 private boolean isEnabled;

	@Autowired
	private LaporanMapper laporanMapper;
	
	@Autowired
	private UserMapper userMapper;

	@Autowired
	NotificationService notificationService;
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");

	private final String YES = "YES";
	private final String MELAPOR = "MELAPOR";
	
	private Map<String, Boolean> mapBodyCenter = new HashMap<>();
	private Map<String, Boolean> mapBodyCenterYes = new HashMap<>();
	private Map<String, Boolean> mapBodyCenterNo = new HashMap<>();
	
	@RequestMapping(value = "/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList get(
    		HttpServletRequest request,
			@RequestParam("id") Integer id, 
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
			@RequestParam("status_pelaporan") Optional<String> status_pelaporan, 
			@RequestParam("tanggal_laporan") Optional<String> tanggal_laporan,
			@RequestParam("nip_pegawai") Optional<String> nip_pegawai,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page) throws Exception{
		String tanggalClause = "";
		String idUnitAuditiClause = "";
		String statusPelaporan = "";
		Integer limitClause = 0, offsetClause = 0;
		if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) idUnitAuditiClause = " AND unit_kerja_id = '" + id_unit_kerja.get() + "'";
		if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) tanggalClause = " AND tanggal_laporan BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'";
		else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) tanggalClause = " AND tanggal_laporan >= '" + periode_awal.get() + "'";
		else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) tanggalClause = " AND tanggal_laporan <= '" + periode_akhir.get() + "'";
		if(status_pelaporan.isPresent() && !status_pelaporan.get().equals("ALL")) statusPelaporan = " AND status_pelaporan = '" + status_pelaporan.get() + "'";
		if((tanggal_laporan.isPresent() && !tanggal_laporan.get().equals(""))) tanggalClause = " AND (tanggal_laporan = '" + tanggal_laporan.get() + "' or tanggal_laporan is null)";
		if((nip_pegawai.isPresent() && !nip_pegawai.get().equals(""))) tanggalClause += " AND usernames = '" + nip_pegawai.get() + "'";

		QueryParameter param = new QueryParameter();		
		if(limit.isPresent()) { param.setLimit(limit.get()); if(limit.get() > 2000000000) param.setLimit(2000000000); }
		else { param.setLimit(2000000000); }
    	
    	int pPage = 1;
    	if(page.isPresent()) { pPage = page.get(); int offset = (pPage-1)*param.getLimit(); param.setOffset(offset); }
    	
    	limitClause = param.getLimit();
    	offsetClause = param.getOffset();
    	
    	ResponseWrapperList resp = new ResponseWrapperList();
		if(id==1) { 
			List<Laporan1> data =  laporanMapper.getListLaporan1(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
			resp.setCount(laporanMapper.getCountLaporan1(tanggalClause, idUnitAuditiClause)); 
			resp.setData(data); 
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		else if(id==2) { 
			List<Laporan2> data =  laporanMapper.getListLaporan2(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
			resp.setCount(laporanMapper.getCountLaporan2(tanggalClause, idUnitAuditiClause));  
			resp.setData(data); 
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		else if(id==3) { 
			List<Laporan3> data =  laporanMapper.getListLaporan3(tanggalClause, statusPelaporan, limitClause, offsetClause);
			resp.setCount(laporanMapper.getCountLaporan3(tanggalClause, statusPelaporan));  
			resp.setData(data); 
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		else if(id==4) { 
			List<Laporan4> data =  laporanMapper.getListLaporan4(tanggalClause, statusPelaporan, limitClause, offsetClause);
			resp.setCount(laporanMapper.getCountLaporan4(tanggalClause, statusPelaporan));   
			resp.setData(data); 
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){ qryString += "&limit="+limit.get(); }
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
		return resp;
	}
	
    @RequestMapping(value="/send", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> sendNotif(
			@RequestParam("tanggal_laporan") Optional<String> tanggalLaporan,
			@RequestParam("nip_pegawai") Optional<String> nip
    		) throws Exception {
    	
    	OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
    	OauthUserOrganization userAuthentication = (OauthUserOrganization)oAuth2Authentication.getUserAuthentication().getPrincipal();

    		ResponseWrapper resp = new ResponseWrapper();  
    		String clause = OauthUserOrganization.USERNAME + "='"+nip.get()+"'";
    		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
    		List<OauthUserOrganizationUnit> list = userMapper.findListWithOrganizationUnitNew(clause);
    		notificationService.sendMailPemberitahuan("EMAIL_SINGLE_TO:"+list.get(0).getEmail(), tanggalLaporan.get());
    		resp.setMessage("Email Notifikasi Telah Berhasil Dikirim.");
    		resp.setData(list);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    @Scheduled(cron = "0 0 9 * * *") 
    public void autoSendReminder9() throws Exception {
    		if(isEnabled) {
    			sendReminder();
    		}
    }
    
    @Scheduled(cron = "0 0 18 * * *") 
    public void autoSendReminder18() throws Exception {
    		if(isEnabled) {
    			sendReminder();
    		}
    }
    
    private void sendReminder()  throws Exception {
    		String tanggalClause = " AND tanggal_laporan = '"+new SimpleDateFormat("yyyy-MM-dd").format(new Date())+"'";
		String statusPelaporan = " AND status_pelaporan = 'Tidak'";
		List<Laporan3> data =  laporanMapper.getListLaporan3(tanggalClause, statusPelaporan, 200000000, 0);
		String emails = "";
		for(Laporan3 lap: data) {
			emails += ",EMAIL_SINGLE_TO:"+lap.getEmailPegawai();
		}
		//System.out.println(emails.substring(1));
		notificationService.sendMailPemberitahuan(emails.substring(1), new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
    }
    
    @RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Integer id, 
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
			@RequestParam("status_pelaporan") Optional<String> status_pelaporan, 
			@RequestParam("tanggal_laporan") Optional<String> tanggal_laporan,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page) throws Exception {
    	
		if(!passCookieAuthorization(request)) throw new Exception("User not authorized");
		
		String tanggalClause = "";
		String idUnitAuditiClause = "";
		String statusPelaporan = "";
		String namaFilter1 = "";
		String namaFilter2 = "";
		final Integer limitClause = 2147483647, offsetClause = 0;
		if(id == 1 || id == 2) {
			namaFilter1 = "Unit Kerja: Semua";
			namaFilter2 = "Periode: - s/d -";
			if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) {
				namaFilter1 = laporanMapper.getUnitKerja(id_unit_kerja.get()).getNamaUnitKerja();
				idUnitAuditiClause = " AND unit_kerja_id = '" + id_unit_kerja.get() + "'";
			}
			if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
				namaFilter2 = "Periode: " + changeToDDMMYYYY(periode_awal.get()) + " s/d " + changeToDDMMYYYY(periode_akhir.get());
				tanggalClause = " AND tanggal_laporan BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'";
			}
			else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) {
				namaFilter2 = "Periode: " + changeToDDMMYYYY(periode_awal.get()) + " s/d -";
				tanggalClause = " AND tanggal_laporan >= '" + periode_awal.get() + "'";
			}
			else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) {
				namaFilter2 = "Periode: " + "- s/d " + changeToDDMMYYYY(periode_akhir.get());
				tanggalClause = " AND tanggal_laporan <= '" + periode_akhir.get() + "'";
			}
		}
		if(id == 3 || id == 4) {
			namaFilter1 = "Status Laporan: Semua";
			namaFilter2 = "Tanggal: -";
			if(status_pelaporan.isPresent() && !status_pelaporan.get().equals("ALL")) {
				namaFilter1 = "Status Laporan: " + changeToDDMMYYYY(status_pelaporan.get());
				statusPelaporan = " AND status_pelaporan = '" + status_pelaporan.get() + "'";
			}
			if((tanggal_laporan.isPresent() && !tanggal_laporan.get().equals(""))) {
				namaFilter2 = "Tanggal: " + changeToDDMMYYYY(tanggal_laporan.get());
				tanggalClause = " AND (tanggal_laporan = '" + tanggal_laporan.get() + "' or tanggal_laporan is null)";
			}
		}
		
		
//		List<Laporan1> listLaporan1 = laporanMapper.getListLaporan1(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
//		List<Laporan2> listLaporan2 = laporanMapper.getListLaporan2(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
//		List<Laporan3> listLaporan3 = laporanMapper.getListLaporan3(tanggalClause, statusPelaporan, limitClause, offsetClause);
//		List<Laporan4> listLaporan4 = laporanMapper.getListLaporan4(tanggalClause, statusPelaporan, limitClause, offsetClause);
		

		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapHeader = new HashMap<>();
		mapHeader.put("header", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true);
		mapBodyCenter = new HashMap<>();
		mapBodyCenter.put("wrap", true);
		mapBodyCenter.put("center", true);
		mapBodyCenterYes = new HashMap<>();
		mapBodyCenterYes.put("wrap", true);
		mapBodyCenterYes.put("center", true);
		mapBodyCenterYes.put("green", true);
		mapBodyCenterNo = new HashMap<>();
		mapBodyCenterNo.put("wrap", true);
		mapBodyCenterNo.put("center", true);
		mapBodyCenterNo.put("red", true);
		Map<String, Boolean> mapBodyCenterMaybe = new HashMap<>();
		mapBodyCenterMaybe.put("wrap", true);
		mapBodyCenterMaybe.put("center", true);
		mapBodyCenterMaybe.put("orange", true);
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		Map<String, Boolean> mapFilter = new HashMap<>();
		mapFilter.put("filter", true);
		mapFilter.put("no-border", true);
		
		
		int panjangKolom = 0;
		String filename = "";
		String titlename = "";
		String sheetname = null;
		List<String> listHeader1 = new ArrayList<String>();
		List<String> listHeader2 = new ArrayList<String>();
		List<String> listHeader3 = new ArrayList<String>();
		if(id == 1) {
			panjangKolom = 9; 
			filename = "DAFTAR-PERKEMANGAN-KINERJA-PEGAWAI";
			titlename = "DAFTAR PERKEMANGAN KINERJA PEGAWAI";
			sheetname = "Laporan Kinerja Periodik";
			listHeader1 = Arrays.asList("No.:3:1", "Tanggal:3:1", "Pegawai:1:4", "Tusi:1:2", "Tambahan:1:2");
			listHeader2 = Arrays.asList("Jumlah:2:1:2", "Melapor:1:2", "%:2:1", "Jumlah:2:1", "%:2:1", "Jumlah:2:1", "%:2:1");
			listHeader3 = Arrays.asList("Ya:1:1:3", "Tidak:1:1");
		}
		else if(id == 2) {
			panjangKolom = 19;
			filename = "DAFTAR-PERKEMANGAN-KESEHATAN-PEGAWAI";
			titlename = "DAFTAR PERKEMANGAN KESEHATAN PEGAWAI";
			sheetname = "Laporan Kesehatan Periodik";
			listHeader1 = Arrays.asList("No.:3:1", "Tanggal:3:1", "Pegawai:1:4", "Sehat:1:2", "Sakit:1:12");
			listHeader2 = Arrays.asList("Jumlah:2:1:2", "Melapor:1:2", "%:2:1", "Jumlah:2:1", "%:2:1", "Jumlah:2:1", "%:2:1", "Demam:1:2", "Batuk:1:2", "Pilek:1:2", "Tenggorokan:1:2", "Sesak Nafas:1:2");
			listHeader3 = Arrays.asList("Ya:1:1:3", "Tidak:1:1", "Jml:1:1:10", "%:1:1", "Jml:1:1", "%:1:1", "Jml:1:1", "%:1:1", "Jml:1:1", "%:1:1", "Jml:1:1", "%:1:1");
		}
		else if(id == 3) { 
			panjangKolom = 11;
			filename = "DAFTAR-MONITORING-LAPORAN-KESEHATAN-PEGAWAI";
			titlename = "DAFTAR MONITORING LAPORAN KESEHATAN PEGAWAI";
			sheetname = "Laporan Kesehatan Pegawai";
			listHeader1 = Arrays.asList("No.:2:1", "Unit Kerja:2:1", "NIP:2:2", "Nama:2:1", "Status:2:1", "Sakit:2:1", "Sakit:1:5");
			listHeader2 = Arrays.asList("BM:1:1:6", "BT:1:1", "PL:1:1", "ST:1:1", "SN:1:1");
		}
		else if(id == 4) { 
			panjangKolom = 9; 
			filename = "DAFTAR-MONITORING-LAPORAN-KINERJA-PEGAWAI";
			titlename = "DAFTAR MONITORING LAPORAN KINERJA PEGAWAI";
			sheetname = "Laporan Kinerja Pegawai"; 
			listHeader1 = Arrays.asList("No.:2:1", "Unit Kerja:2:1", "NIP:2:2", "Nama:2:1", "Status:2:1", "Tusi:2:1", "Tambahan:2:1");
		}
		else throw new Exception("Format Laporan "+id+" tidak tersedia!");
		
		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename="+filename+".xlsx"); //name file
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetname==null?filename:sheetname);
		StyleExcel style = new StyleExcel();

		XSSFRow aRow;
	    
		int idxRow = 0;
		int idx = 0;
		int iB = 0,iS = 0;
		idxRow++;
		XSSFRow title = sheet.createRow(idxRow);
		title.createCell(0).setCellValue(titlename);
		title.getCell(0).setCellStyle(style.styleBody(workbook, mapTitle));
		CellRangeAddress range = new CellRangeAddress(idxRow, idxRow, 0, panjangKolom);
		sheet.addMergedRegion(range);
//		idxRow++;
//		idxRow++;
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue(namaFilter1);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue(namaFilter2);
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		idxRow++;
		idxRow++;
		
		XSSFRow header = sheet.createRow(idxRow++);
		int maxBaris = 1;
		for(String namaHeader : listHeader1) {
			if(namaHeader.split(":").length > 1) {
				String nama = namaHeader.split(":")[0]; 
				int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
				int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
				try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
				maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
				header.createCell(idx).setCellValue(nama);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
					range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
					RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
					sheet.addMergedRegion(range);
				}
				idx += jumlahKolom;
			}
			else {
				header.createCell(idx).setCellValue(namaHeader);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				idx++;
			}
		}
		if(listHeader2.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader2) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		if(listHeader3.isEmpty() && (id == 1 || id == 2)) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else if(listHeader3.isEmpty() && (id == 3 || id == 4)) { /*idxRow++;*/ } 
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader3) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		int i = 1;
		if(id == 1) {
			List<Laporan1> listLaporan1 = laporanMapper.getListLaporan1(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
			for(Laporan1 o : listLaporan1) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapTopCenter));
				aRow.createCell(iB++).setCellValue(Utils.formatIndonesianReportDate(o.getTanggalLaporan()));
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPegawai());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporPekerjaan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTidakMelaporPekerjaan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporPekerjaan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTusiLaporan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenTusiLaporan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTambahanLaporan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenTambahanLaporan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			}
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			aRow.createCell(iB++).setCellValue("");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue((idUnitAuditiClause.equals("")?"Total":"Jumlah") + " Rata-Rata");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan1.stream().map(x -> x.getJumlahPegawai()).reduce(0, Integer::sum) / listLaporan1.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan1.stream().map(x -> x.getJumlahMelaporPekerjaan()).reduce(0, Integer::sum) / listLaporan1.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan1.stream().map(x -> x.getJumlahTidakMelaporPekerjaan()).reduce(0, Integer::sum) / listLaporan1.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan1.stream().map(x -> x.getJumlahPersenMelaporPekerjaan()).reduce(0.0, Double::sum) / listLaporan1.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan1.stream().map(x -> x.getJumlahTusiLaporan()).reduce(0, Integer::sum) / listLaporan1.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan1.stream().map(x -> x.getJumlahPersenTusiLaporan()).reduce(0.0, Double::sum) / listLaporan1.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan1.stream().map(x -> x.getJumlahTambahanLaporan()).reduce(0, Integer::sum) / listLaporan1.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan1.stream().map(x -> x.getJumlahPersenTambahanLaporan()).reduce(0.0, Double::sum) / listLaporan1.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
		}
		else if(id == 2) {
			List<Laporan2> listLaporan2 = laporanMapper.getListLaporan2(tanggalClause, idUnitAuditiClause, limitClause, offsetClause);
			for(Laporan2 o : listLaporan2) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapTopCenter));
				aRow.createCell(iB++).setCellValue(Utils.formatIndonesianReportDate(o.getTanggalLaporan()));
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPegawai());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporKesehatan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTidakMelaporKesehatan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporKesehatan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahSehat());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenSehat()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahSakit());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenSakit()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahDemam());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenDemam()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahBatuk());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenBatuk()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPilek());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenPilek()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTenggorokan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenTenggorokan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahSesak());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenSesak()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			}
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			aRow.createCell(iB++).setCellValue("");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue((idUnitAuditiClause.equals("")?"Total":"Jumlah") + " Rata-Rata");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahPegawai()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahMelaporKesehatan()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahTidakMelaporKesehatan()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenMelaporKesehatan()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahSehat()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenSehat()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahSakit()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenSakit()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahDemam()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenDemam()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahBatuk()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenBatuk()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahPilek()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenPilek()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahTenggorokan()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenTenggorokan()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listLaporan2.stream().map(x -> x.getJumlahSesak()).reduce(0, Integer::sum) / listLaporan2.size()));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listLaporan2.stream().map(x -> x.getJumlahPersenSesak()).reduce(0.0, Double::sum) / listLaporan2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
		}
		else if(id == 3) {
			List<Laporan3> listLaporan3 = laporanMapper.getListLaporan3(tanggalClause, statusPelaporan, limitClause, offsetClause);
			Map<Integer, List<Laporan3>> mapLaporan3 = listLaporan3.stream().collect(Collectors.groupingBy(Laporan3::getIdUnitKerja));
			for (Map.Entry<Integer, List<Laporan3>> entry : mapLaporan3.entrySet()) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(entry.getValue().get(0).getNamaUnitKerja());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
				int i2 = 1;
				for(Laporan3 o : entry.getValue()) {
					if(i2 != 1) {
						iB = 0; iS = 0;
						aRow = sheet.createRow(idxRow++);
						aRow.createCell(iB++).setCellValue("");
						aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
						aRow.createCell(iB++).setCellValue("");
						aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					}
					aRow.createCell(iB++).setCellValue(i2++); 
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
					aRow.createCell(iB++).setCellValue(o.getNipPegawai());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					aRow.createCell(iB++).setCellValue(o.getNamaPegawai());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					aRow.createCell(iB++).setCellValue(o.getStatusPelaporan());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getStatusPelaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getStatusSehatLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getStatusSehatLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getKondisiSakitDemamLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getKondisiSakitDemamLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getKondisiSakitBatukLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getKondisiSakitBatukLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getKondisiSakitPilekLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getKondisiSakitPilekLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getKondisiSakitTenggorokanLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getKondisiSakitTenggorokanLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getKondisiSakitSesakLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getKondisiSakitSesakLaporan())));
				}
			};
		}
		else if(id == 4) {
			List<Laporan4> listLaporan4 = laporanMapper.getListLaporan4(tanggalClause, statusPelaporan, limitClause, offsetClause);
			Map<Integer, List<Laporan4>> mapLaporan4 = listLaporan4.stream().collect(Collectors.groupingBy(Laporan4::getIdUnitKerja));
			for (Map.Entry<Integer, List<Laporan4>> entry : mapLaporan4.entrySet()) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(entry.getValue().get(0).getNamaUnitKerja());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
				int i2 = 1;
				for(Laporan4 o : entry.getValue()) {
					if(i2 != 1) {
						iB = 0; iS = 0;
						aRow = sheet.createRow(idxRow++);
						aRow.createCell(iB++).setCellValue("");
						aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
						aRow.createCell(iB++).setCellValue("");
						aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					}
					aRow.createCell(iB++).setCellValue(i2++); 
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
					aRow.createCell(iB++).setCellValue(o.getNipPegawai());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					aRow.createCell(iB++).setCellValue(o.getNamaPegawai());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
					aRow.createCell(iB++).setCellValue(o.getStatusPelaporan());
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getStatusPelaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getStatusTusiLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getStatusTusiLaporan())));
					aRow.createCell(iB++).setCellValue(getCheckValue(o.getStatusTambahanLaporan()));
					aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, getCheckStyle(o.getStatusTambahanLaporan())));
				}
			};
		}
		
		if(id == 1 || id == 2) {
			sheet.setColumnWidth(0, (5  * 256) + 200); // No
			sheet.setColumnWidth(1, (20 * 256) + 200); // Tanggal Laporan
		}
		else if(id == 3 || id == 4) {
			sheet.setColumnWidth(0, (5  * 256) + 200); // No
			sheet.setColumnWidth(1, (26 * 256) + 200); // Unit Kerja
			sheet.setColumnWidth(2, (3  * 256) + 200); // No-2
			sheet.setColumnWidth(3, (21 * 256) + 200); // NIP
			sheet.setColumnWidth(4, (36 * 256) + 200); // Nama
			if(id == 4) {
				sheet.setColumnWidth(5, (15 * 256) + 200); // Status
				sheet.setColumnWidth(6, (15 * 256) + 200); // Tusi
				sheet.setColumnWidth(7, (15 * 256) + 200); // Tambahan
			}
		}
		
		workbook.write(response.getOutputStream());
	}
    
    private String getCheckValue(String status) {
    	return status != null ? (status.equals(YES) ? "✓" : "✗") : "-";
    }
    
    private Map<String, Boolean> getCheckStyle(String status) {
    	return status != null ? (status.equals(YES) || status.equalsIgnoreCase(MELAPOR) ? mapBodyCenterYes : mapBodyCenterNo) : mapBodyCenter;
    }
    
    private String changeToDDMMYYYY(String date) {
    	try {
	    	String arr[] = date.split("-");
	    	return arr[2] + "/" + arr[1] + "/" + arr[0];
    	} catch(Exception e) {
    		return date;
    	}
    }
	
}

//class Laporan3Comparator implements Comparator<List<Laporan3>> {
//	@Override
//	public int compare(List<Laporan3> listLaporan1, List<Laporan3> listLaporan2) {
//		return listLaporan1.get(0).getRownum().compareTo(listLaporan1.get(0).getRownum());
//	}
//}