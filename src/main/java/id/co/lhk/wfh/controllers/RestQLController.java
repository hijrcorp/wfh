package id.co.lhk.wfh.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.mapper.SqlMapper;
import id.co.lhk.wfh.ref.AccountLoginInfo;
import id.co.lhk.wfh.services.RestQLTriggerService;


@Controller
@CrossOrigin
@RequestMapping(value = {"/restql/","/restql/me"})
public class RestQLController extends BaseController {
	
	private String prefix_entity = "tbl_";
	
	@Autowired
	private SqlMapper sqlMapper;
	
	@Autowired
	private RestQLTriggerService triggerService;
	
	private Map<String, Object> data;
	
	@RequestMapping(value="/{entity}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@PathVariable String entity,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter") Optional<String> filter,
    		@RequestParam("select") Optional<String> select,
    		@RequestParam("join") Optional<String> join,
    		@RequestParam("order") Optional<String> order,
    		HttpServletRequest request
    		)throws Exception {
		
		ResponseWrapperList resp = new ResponseWrapperList();
		
		List<String> lstFld = new ArrayList<String>();
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='HIDDEN'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString()) || rsCheck.get("role_permission").toString().equals("*")){
					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
//					System.out.println(rsCheck.get("entity_fields_permission").toString());
					for(String f: flds) {
						lstFld.add(f);
					}
					
				}
			}
		}
		
		StringBuilder sql = construct(entity, limit, page, filter, select, join);
		
		if(!request.isUserInRole("SUPER_ADMIN")) {
			sql.append(" AND id_source_").append(entity);
			sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
		}
		
		if(request.getRequestURI().contains("/me/")) {
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
		}
		
		
		if(order.isPresent()) {
			sql.append(" ORDER BY ");
			StringBuilder orderstr = new StringBuilder();
			
			String[] ordercol = order.get().split(",");
			for(String c: ordercol) {
				String[] ca =  c.split("\\.");
				
				if(ca.length > 1) {
					orderstr.append(",").append(ca[0].toLowerCase()).append("_").append(entity).append(" ").append(ca[1].toUpperCase());
				}else {
					orderstr.append(",").append(c.toLowerCase()).append("_").append(entity).append(" ASC");	
				}
				
			}
			sql.append(orderstr.substring(1));
		}
		
		int pLimit = 50;
		int pOffset = 0;
		
		if(limit.isPresent()) {
			if(limit.get() > 100) {
				pLimit = 100;
			}else {
				pLimit = limit.get();
			}
		}
		
		int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		pOffset = (pPage-1)*pLimit;
	    	}
	    	
	    	StringBuilder qry = new StringBuilder();
	    	qry.append(sql).append(" LIMIT ").append(pLimit);
	    	qry.append(" OFFSET ").append(pOffset);
	    	
//	    	String qry = sql + " LIMIT " + pLimit;
//	    	qry += " OFFSET " + pOffset;

	    	
		resp.setNextPageNumber(pPage+1);
		
		String qryString =	request.getQueryString()==null?"":request.getQueryString();
	    	if(!qryString.contains("page=")) {
	    		qryString += "&page=2";
	    	}else {
	    		qryString = qryString.replace("page="+pPage, "page="+resp.getNextPageNumber());	    		
	    	}
		resp.setNextPage(request.getRequestURL().append("?").append(qryString).toString());
		
		
		//System.out.println(sql);
		
		List<Map<String, Object>> listrs = sqlMapper.select(qry.toString());
		
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		
		
		for(Map<String, Object> rs: listrs) {
			String[] joinent = {};
			
			if(join.isPresent()) {
				joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
			}
			data.add(remap(rs, entity, joinent, lstFld));
		}
		
		resp.setData(data);
		resp.setCount(sqlMapper.count(sql.toString()));
		
		resp.setNextMore(data.size()+((pPage-1)*pLimit) < resp.getCount());
		
		if(!resp.isNextMore()) {
			resp.setNextPage("#");
			resp.setNextPageNumber(-1);
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{entity}/{_id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getOne(
			@PathVariable String entity,
			@PathVariable Optional<String> _id,
			@RequestParam("select") Optional<String> select,
    			@RequestParam("join") Optional<String> join,
			@RequestParam("fetch") Optional<String> fetch,
			HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		List<String> lstFld = new ArrayList<String>();
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='HIDDEN'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString()) || rsCheck.get("role_permission").toString().equals("*")){
					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
//					System.out.println(rsCheck.get("entity_fields_permission").toString());
					for(String f: flds) {
						lstFld.add(f);
					}
					
				}
			}
		}
		
		resp.setData(selectOne(entity, _id, select, join, fetch, lstFld, request));
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRES_NEW)
	private HashMap<String, Object> selectOne(
			String entity,
			Optional<String> _id,
			Optional<String> select,
    			Optional<String> join,
			Optional<String> fetch,
			List<String> lstFld,
			HttpServletRequest request
			) throws Exception {
		
		//System.out.println(join);
		StringBuilder sql = construct(entity, Optional.empty(), Optional.empty(), Optional.empty(), select, join);
		
		if(request.getRequestURI().contains("/me/")) {
			
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
		}else {
			if(_id.isPresent()) {
				sql.append(" AND id_").append(entity).append("='").append(_id.get()).append("'");
			}else {
				sql.append(" AND id_").append(entity).append("=''");
			}
		}
		
		if(!request.isUserInRole("SUPER_ADMIN")) {
			sql.append(" AND id_source_").append(entity);
			sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
		}
		
		String id = "";
		
		List<Map<String, Object>> lst = sqlMapper.select(sql.toString());
		
		if(!lst.isEmpty()) {
		
			Map<String, Object> rs = lst.get(0);
			
			id = rs.get("id_"+entity)==null?"":rs.get("id_"+entity).toString();
			
			String[] joinent = {};
			
			if(join.isPresent() && !join.get().equals("")) {
				joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
			}
			HashMap<String, Object> data = remap(rs, entity, joinent, lstFld);
			
			
			
			if(fetch.isPresent() && !fetch.get().equals("")) {
				String[] ent = fetch.get().split(",(?=[^\\)]*(\\(|$))");
				for(String e: ent) {
//					String[] fe =  e.split("\\.");
					String[] fe =  e.split("\\.(?=[^\\)]*(\\(|$))");
					boolean isOrder = false;
					
					Optional<String> fetchSelect = Optional.empty();
					String limit = "100";
					
					StringBuilder orderstr = new StringBuilder();
					
					if(fe.length > 1) {
						for(int i=1; i < fe.length; i++) {
							if(fe[i].indexOf("select") >= 0) {
								fetchSelect = Optional.of(fe[i].substring(7, fe[i].length()-1));		
							}
							
							if(fe[i].indexOf("limit") >= 0) {
								limit = fe[i].substring(6, fe[i].length()-1);		
							}
							
							if(fe[i].indexOf("order") >= 0) {
								isOrder = true;
								System.out.println(fe[i]);
								String[] ordercol = fe[i].substring(6, fe[i].length()-1).split(",");
								for(String c: ordercol) {
									String[] ca =  c.split("\\.");
									
									if(ca.length > 1) {
										orderstr.append(",").append(ca[0].toLowerCase()).append("_").append(fe[0]).append(" ").append(ca[1].toUpperCase());
									}else {
										orderstr.append(",").append(c.toLowerCase()).append("_").append(fe[0]).append(" ASC");	
									}
									
								}
								
							}
						}
					}
				
					StringBuilder sqlFetch = construct(fe[0], Optional.empty(), Optional.empty(), Optional.empty(), fetchSelect, Optional.empty());
					sqlFetch.append(" AND id_").append(entity).append("_").append(fe[0]).append(" = '").append(id).append("'");
					if(isOrder) sqlFetch.append(" ORDER BY ").append(orderstr.substring(1));
					sqlFetch.append(" LIMIT ").append(limit);
					
					
//					sqlFetch += " AND id_"+entity+"_"+fe[0]+" = '"+id+"'";
//					sqlFetch += " LIMIT " + limit;
					
//					System.out.println(sqlFetch);
					List<Map<String, Object>> listrs = sqlMapper.select(sqlFetch.toString());
					
					List<Map<String, Object>> newlistrs = new ArrayList<Map<String, Object>>();
					
					
					for(Map<String, Object> rsFetch: listrs) {
						
						newlistrs.add(remap(rsFetch, fe[0], new String[]{}, lstFld));
					}
					
					
					data.put(fe[0], newlistrs);
				}
					
			}
			
			return data;
			
		}
		
		return new HashMap<>();
	}
	
    @RequestMapping(value="/{entity}/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@PathVariable String entity,
    		@RequestBody RestQLController controller,
    		@RequestParam("append") Optional<String> append,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		//List<String> lstFld = new ArrayList<String>();
    		List<String> lstEnt = new ArrayList<String>();
    		List<String> lstDel = new ArrayList<String>();
    		
    		Map<String, List<String>> mapFld = new HashMap<String, List<String>>();
    		
    		boolean isAdmin = true;
    		
    		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
    			
    			isAdmin = false;
    			
    			//String sqlCheck = "SELECT entity_permission, fields_permission, role_permission FROM hijr_permission WHERE mode_permission='WRITE'";
    			String sqlCheck = "SELECT entity_permission, join_entity(entity_permission, fields_permission) entity_fields_permission, role_permission FROM hijr_permission WHERE mode_permission='WRITE'";
    			
    			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
    			
    			for(Map<String, Object> rsCheck: lstRsCheck) {
    				if(request.isUserInRole(rsCheck.get("role_permission").toString())){
    					/*
    					String[] flds = rsCheck.get("fields_permission").toString().split(",");
    					lstFld.addAll(Arrays.asList(flds));
    					lstEnt.add(rsCheck.get("entity_permission").toString());
    					*/
    					String[] flds = rsCheck.get("entity_fields_permission").toString().split(",");
    					for(String f: flds) {
    						if(f.indexOf("=") >= 0) {
    							String[] fs = f.split("=");
    							
    							if(mapFld.containsKey(fs[0])) {
    								mapFld.get(fs[0]).add(fs[1]);
    							}else {
    								List<String> lv = new ArrayList<String>();
    								lv.add(fs[1]);
    								mapFld.put(fs[0], lv);
    							}
    							
    						}else {
    							mapFld.put(f, new ArrayList<String>());
    						}
    					}
    					lstEnt.add(rsCheck.get("entity_permission").toString());
    				}
    			}
    			
    			if(lstEnt.size() <= 0) {
	    			resp.setCode(HttpStatus.UNAUTHORIZED.value());
	    			resp.setMessage("Anda bukan admin, jangan coba-coba...!");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    			
    			if(append.isPresent() && append.get().equals("false")) {
    				String sqlDel = "SELECT entity_permission, role_permission FROM hijr_permission WHERE mode_permission='DELETE'";
    				
    				List<Map<String, Object>> lstRsDel = sqlMapper.select(sqlDel);
    				
    				for(Map<String, Object> rsDel: lstRsDel) {
    					if(request.isUserInRole(rsDel.get("role_permission").toString())){
    						lstDel.add(rsDel.get("entity_permission").toString());
    					}
    				}
    			}
    		}
    		
    		for (Map.Entry<String,List<String>> entry : mapFld.entrySet()) {
    			System.out.println(entry.getKey() + ": " + entry.getValue().size());
    		}
    		
    		StringBuilder sql = new StringBuilder();
    		
    		sql.append("INSERT INTO ").append(prefix_entity).append(entity);
    		
    		List<StringBuilder> listsql = new ArrayList<StringBuilder>();
    		
    		Map<String, Object> rs = controller.getData();
    		
    		StringBuilder fields = new StringBuilder();
    		StringBuilder values = new StringBuilder();
    		StringBuilder combines = new StringBuilder();
    		StringBuilder joins = new StringBuilder();
    		StringBuilder select = new StringBuilder("id");
    		
    		String id = "";
    		int countFlds = 0;
    		
    		for (Map.Entry<String,Object> entry : rs.entrySet()) {
    			// entry sebagai join object
    			if(entry.getValue() instanceof Map) {
    				
    				joins.append(",").append(entry.getKey()).append(".select(id");
    				
    				Map<String, Object> subrs = (Map<String, Object>)entry.getValue();
    				for (Map.Entry<String,Object> sub : subrs.entrySet()) {
    					if(sub.getKey().equals("id")) {
    						
    						if(mapFld.containsKey(entity+"."+entry.getKey())) {
    							
    							if(mapFld.get(entity+"."+entry.getKey()).size() > 0 && mapFld.get(entity+"."+entry.getKey()).contains(sub.getValue())) {
    								countFlds++;
	    	    					
		    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
		    	        				values.append(",'").append(sub.getValue()).append("'");
		    	        				
		    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
		        					combines.append("='").append(sub.getValue()).append("'");
    							}else {
    								countFlds++;
	    	    					
		    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
		    	        				values.append(",'").append(sub.getValue()).append("'");
		    	        				
		    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
		        					combines.append("='").append(sub.getValue()).append("'");
    							}
    	    					
	    	    					
	    	    				}else if(mapFld.containsKey(entity+".*") || isAdmin) {
	    	    					countFlds++;
	    	    					
	    	        				fields.append(",id_").append(entry.getKey()).append("_").append(entity);
	    	        				values.append(",'").append(sub.getValue()).append("'");
	    	        				
	    	        				combines.append(",id_").append(entry.getKey()).append("_").append(entity);
	        					combines.append("='").append(sub.getValue()).append("'");
	    	    				}
    						
    					}else {
    						joins.append(",").append(sub.getKey());
    					}
    				}
    				
    				joins.append(")");
    				
    			}else {
    				if(entry.getValue() instanceof List) {
    					// GO TO SECTION: LOOP LIST ENTRY
    				}else {
    					
    					select.append(",").append(entry.getKey());
    					
    					if(entry.getKey().equals("id")) {
    	    					id = entry.getValue().toString();
    	            			
    	        				fields.append(",").append(entry.getKey()).append("_").append(entity);
    	            			values.append(",'").append(entry.getValue()).append("'");
    	            			
    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
    	    					combines.append("='").append(entry.getValue()).append("'");
        				}else {
        					if(mapFld.containsKey(entity+"."+entry.getKey())) {
        					
        						if(mapFld.get(entity+"."+entry.getKey()).size() > 0) {
        							
        							if(mapFld.get(entity+"."+entry.getKey()).contains(entry.getValue())) {
        								countFlds++;
            	    					
        								fields.append(",").append(entry.getKey()).append("_").append(entity);
        		    	            			values.append(",'").append(entry.getValue()).append("'");
        		    	            			
        		    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
        		    	    					combines.append("='").append(entry.getValue()).append("'");	
        							}
								
							}else {
								countFlds++;
	    	    					
								fields.append(",").append(entry.getKey()).append("_").append(entity);
		    	            			values.append(",'").append(entry.getValue()).append("'");
		    	            			
		    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
		    	    					combines.append("='").append(entry.getValue()).append("'");
							}
		    					
	    	    					
	    	    				}else if(mapFld.containsKey(entity+".*") || isAdmin) {
	    	    					countFlds++;
	    	    					
	    	    					fields.append(",").append(entry.getKey()).append("_").append(entity);
	    	            			values.append(",'").append(entry.getValue()).append("'");
	    	            			
	    	            			combines.append(",").append(entry.getKey()).append("_").append(entity);
	    	    					combines.append("='").append(entry.getValue()).append("'");
	    	    				}
        				}
    					
    				}
    				
    			}
    			
    		}
    		
    		sql.append(" (");
    		sql.append(fields.toString().substring(1));
    		sql.append(",id_source_").append(entity);
    		sql.append(",id_account_added_").append(entity);
    		sql.append(",timestamp_added_").append(entity);
    		if(request.getRequestURI().contains("/me/")) {
    			sql.append(",id_account_").append(entity);
    		}
    		sql.append(")");
    		sql.append(" VALUES (");
    		sql.append(values.toString().substring(1));
    		sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("'");
    		sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
    		sql.append(",NOW()");
    		if(request.getRequestURI().contains("/me/")) {
    			sql.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");	
    		}
    		sql.append(")");
    		
		sql.append(" ON DUPLICATE KEY UPDATE ");
		sql.append(combines.toString().substring(1));
		sql.append(",id_account_modified_").append(entity).append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
	    	sql.append(",timestamp_modified_").append(entity).append("=NOW()");
    		
	    	
	    	HashMap<String, Object> prev = selectOne(entity, Optional.of(id), Optional.of("id, "+ select.toString()), joins.length()>1?Optional.of(joins.toString().substring(1)):Optional.empty(), Optional.empty(), new ArrayList<>(), request);
	    	//HashMap<String, Object> prev = new HashMap<>();
    		//System.out.println(sql.toString());
    		
	    	// selain kolom id_xxxxxxxxx
	    	if(countFlds > 0) sqlMapper.execute(sql.toString());
    		
	    	boolean updateFlag = true;    		
    		if(id.equals("")) {
    			Map<String,Object> obj = sqlMapper.selectOne("SELECT LAST_INSERT_ID()");
    			id = obj.values().iterator().next().toString();
    			
    			updateFlag = false;
    		}
    		
    		// SECTION: LOOP FOR LIST ENTRY
    		for (Map.Entry<String,Object> entry : rs.entrySet()) {
    			if(entry.getValue() instanceof List) {
    				
    				if(append.isPresent() && append.get().equals("false")) {
	    				StringBuilder del = new StringBuilder("DELETE FROM  ");
	    				del.append(prefix_entity).append(entry.getKey());
	    				del.append(" WHERE id_").append(entity).append("_").append(entry.getKey());
	    				del.append("='").append(id).append("'");
	    				
	    				if(lstDel.contains(entry.getKey()) || isAdmin) {
	    					listsql.add(del);	
	    				}
	    				
	    			}
    				
    				
    				for(Map<String, Object> map: (List<Map<String, Object>>)entry.getValue()) {
    					
    					StringBuilder ins = new StringBuilder("INSERT INTO  ");
        				ins.append(prefix_entity).append(entry.getKey());
    					
    					StringBuilder subfields = new StringBuilder();
    		    			StringBuilder subvalues = new StringBuilder();
    		    			
    		    			subfields.append(",id_").append(entity).append("_").append(entry.getKey());
    		    			subvalues.append(",'").append(id).append("'");
    					
        				for (Map.Entry<String,Object> sub : map.entrySet()) {
        					if(!sub.getKey().equals("id")) {
//        					if((lstFld.contains(sub.getKey()) && lstEnt.contains(entry.getKey())) || isAdmin) {
        						if(mapFld.containsKey(entry.getKey()+"."+sub.getKey())) {	
        							
        							if(mapFld.get(entry.getKey()+"."+sub.getKey()).size() > 0 ) {
        								if(mapFld.get(entry.getKey()+"."+sub.getKey()).contains(sub.getValue())) {
	        								subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
		        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        								}
        							}else {
        								subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
	        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        							}
	        						
        						}else if(mapFld.containsKey(entry.getKey()+".*") || isAdmin) {
        							subfields.append(",").append(sub.getKey()).append("_").append(entry.getKey());
        	        					subvalues.append(",'").append(sub.getValue()).append("'");
        						}
        					}
        				}
        				
//        				ins.append(" (").append(subfields.toString().substring(1)).append(")");
//        				ins.append(" VALUES (").append(subvalues.toString().substring(1)).append(")");
        				
        				ins.append(" (");
	        	    		ins.append(subfields.toString().substring(1));
	        	    		ins.append(",id_source_").append(entry.getKey());
	        	    		ins.append(",id_account_added_").append(entry.getKey());
	        	    		ins.append(",timestamp_added_").append(entry.getKey());
	        	    		ins.append(")");
	        	    		ins.append(" VALUES (");
	        	    		ins.append(subvalues.toString().substring(1));
	        	    		ins.append(",'").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("'");
	        	    		ins.append(",'").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
	        	    		ins.append(",NOW()");
	        	    		ins.append(")");
	        	    		
	        	    		if(lstEnt.contains(entry.getKey()) || isAdmin) {
	        	    			listsql.add(ins);
	        	    		}
    				}
    				
    			}
    			
    		}
    		
    		String strsql = "";
    		for (StringBuilder sb: listsql) {
    			strsql += sb.append(";\n").toString();
    		}
    		
    		//System.out.println(strsql);
    		if(!id.equals("0") && !strsql.equals("")) sqlMapper.execute(strsql);
    		
    		HashMap<String, Object> data = selectOne(entity, Optional.of(id), Optional.of(select.toString()), joins.length()>1?Optional.of(joins.toString().substring(1)):Optional.empty(), Optional.empty(), new ArrayList<>(), request);
    		
    		if(updateFlag) {
			resp.setMessage("Perubahan data telah berhasil disimpan");
			
			triggerService.onAfterUpdate(entity, data, prev);
    		}else {
    			resp.setMessage("Penambahan data ke dalam sistem telah berhasil");
    			
    			triggerService.onAfterInsert(entity, data);
    		}
    		
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{entity}/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> delete(
    			@PathVariable String entity,
			@PathVariable String id,
			@RequestParam("cascade") Optional<String> cascade,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		
		String[] casent = new String[] {};
		if(cascade.isPresent()) {
			casent = cascade.get().split(",");
		}
		
		List<String> lstEnt = new ArrayList<String>();
		
		boolean isAdmin = true;
		
		if(!request.isUserInRole("ADMIN") && !request.isUserInRole("SUPER_ADMIN")) {
			
			isAdmin = false;
			
			String sqlCheck = "SELECT entity_permission, role_permission FROM hijr_permission WHERE mode_permission='DELETE'";
			
			List<Map<String, Object>> lstRsCheck = sqlMapper.select(sqlCheck);
			
			for(Map<String, Object> rsCheck: lstRsCheck) {
				if(request.isUserInRole(rsCheck.get("role_permission").toString())){
					lstEnt.add(rsCheck.get("entity_permission").toString());
				}
			}
			
			if(lstEnt.size() <= 0) {
	    			resp.setCode(HttpStatus.UNAUTHORIZED.value());
	    			resp.setMessage("Anda bukan admin, jangan coba-coba...!");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}
		
		StringBuilder sql = new StringBuilder("DELETE FROM  ");
		sql.append(prefix_entity).append(entity);
		sql.append(" WHERE id_").append(entity);
		sql.append("='").append(id).append("'");
		
		
		if(!request.isUserInRole("SUPER_ADMIN")) {
			sql.append(" AND id_source_").append(entity);
			sql.append(" IN ('").append(extractAccountLogin(request, AccountLoginInfo.SOURCE)).append("', '*')");
		}
		
		if(request.getRequestURI().contains("/me/")) {
			sql.append(" AND id_account_").append(entity);
			sql.append("='").append(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).append("'");
		}
		
		List<StringBuilder> listsql = new ArrayList<StringBuilder>();
		
		for(String e: casent) {
			
			StringBuilder del = new StringBuilder("DELETE FROM  ");
			del.append(prefix_entity).append(e);
			del.append(" WHERE id_").append(entity).append("_").append(e);
			del.append("='").append(id).append("'");
			
			if(lstEnt.contains(e) || isAdmin) {
				listsql.add(del);
			}
		}

		HashMap<String, Object> data = selectOne(entity, Optional.of(id), Optional.empty(), Optional.empty(), Optional.empty(), new ArrayList<>(), request);
		//System.out.println(sql.toString());
		
		if(lstEnt.contains(entity) || isAdmin) {
			sqlMapper.execute(sql.toString());
		}
		
		Map<String,Object> obj = sqlMapper.selectOne("SELECT ROW_COUNT()");
		int rows = Integer.valueOf(obj.values().iterator().next().toString());
		
		String strsql = "";
		for (StringBuilder sb: listsql) {
			strsql += sb.append(";\n").toString();
		}

		
		resp.setData(data);
		
		
		//System.out.println(strsql);
		if(rows > 0 && !strsql.equals("")) sqlMapper.execute(strsql);
		resp.setMessage("Data telah dihapus.");
		
		triggerService.onAfterDelete(entity, data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    private HashMap<String, Object> remap(Map<String, Object> rs, String entity, String[] joinent, List<String> hidfld) {
    		HashMap<String, Object> newrs = new HashMap<String, Object>();
		
		for (Map.Entry<String,Object> entry : rs.entrySet()) {
			String k = entry.getKey().replace("_"+entity, "");
			if(entry.getKey().indexOf(entity) >= 0 && !hidfld.contains(entity+"."+k))
				newrs.put(k, entry.getValue());	
		}
		
		
		for(String e: joinent) {
			String[] je =  e.split("\\.");
			
			Map<String, Object> newrsjoin = new HashMap<String, Object>();
			
			for (Map.Entry<String,Object> entry : rs.entrySet()) {
				String k = entry.getKey().replace("_"+je[0], "");
				if(entry.getKey().indexOf(je[0]) >= 0 && !hidfld.contains(je[0]+"."+k))
					newrsjoin.put(k, entry.getValue());	
			}
			
			newrs.put(je[0], newrsjoin);
		}
		
		return newrs;
    }
    
    
    private StringBuilder construct(
    		String entity,
    		Optional<Integer> limit,
    		Optional<Integer> page,
    		Optional<String> filter,
    		Optional<String> select,
    		Optional<String> join
    		) {
    		String tblname = prefix_entity+entity.toLowerCase();
		
		StringBuilder sb = new StringBuilder();
		
//		sb.append("SELECT id_").append(entity);
		sb.append("SELECT 1");
		
		String[] joinent = {};
		if(join.isPresent() && !join.get().equals("")) {
			joinent = join.get().split(",(?=[^\\)]*(\\(|$))");
		}
		
		
		
		if(select.isPresent()) {
			String[] cols = select.get().split(",");
			for(String c: cols) {
				
				sb.append(", ").append(c).append("_").append(entity.toLowerCase());
				//sb.append(" AS ").append(c);	
			}
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}else {
			sb.append(", ").append(tblname).append(".*");
			
			for(String e: joinent) {
				String[] je =  e.split("\\.");
				
				if(je.length > 1) {
					String[] joinsc = je[1].substring(7, je[1].length()-1).split(",");
					
					for(String sc: joinsc) {
						sb.append(", ").append(sc).append("_").append(je[0]);
						//sb.append(" AS ").append(sc);	
					}
				}else {
					sb.append(", ").append(prefix_entity).append(e).append(".*");
				}
			}
					
		}
		
		sb.append(" FROM ").append(tblname);
		
		for(String e: joinent) {
			String[] je =  e.split("\\.");
			sb.append(" LEFT JOIN ").append(prefix_entity).append(je[0]);
			sb.append(" ON ").append("id_").append(je[0]).append("_").append(entity);
			sb.append(" = ").append("id_").append(je[0]);
		}
				
		
		sb.append(" WHERE 1");
		
		
		if(filter.isPresent()) {
			String[] cols = filter.get().split(",(?=[^\\)]*(\\(|$))");
			for(String c: cols) {

				String[] f = c.split("\\.");
				
				sb.append(" AND ").append(f[0]).append("_").append(entity.toLowerCase());
				
				if(f[1].toLowerCase().startsWith("eq")) {
					
					sb.append(" = ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("neq")) {
					
					sb.append(" != ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("like") || f[1].toLowerCase().startsWith("slike")) {
					sb.append(" LIKE '%").append(f[1].substring(5, f[1].length()-1).replaceAll("'", "").replaceFirst(" ", ""));
					sb.append("%'");
				}
				
				if(f[1].toLowerCase().startsWith("more")) {
					
					sb.append(" > ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("less")) {
					
					sb.append(" < ").append(f[1].substring(5, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("between")) {
					
					sb.append(" BETWEEN ").append(f[1].substring(8, f[1].length()-1).replace(",", " AND "));
				}
				
				if(f[1].toLowerCase().startsWith("is")) {
					
					sb.append(" IS ").append(f[1].substring(3, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("not")) {
					
					sb.append(" IS NOT ").append(f[1].substring(4, f[1].length()-1));
				}
				
				if(f[1].toLowerCase().startsWith("in")) {
					
					sb.append(" IN (");
					
					String[] vals = f[1].substring(3, f[1].length()-1).split(",");
					
					StringBuilder strv = new StringBuilder();
					for(String v: vals) {
						strv.append(",").append(v);
					}
					
					sb.append(strv.toString().substring(1));
					
					sb.append(")");
				}
			}
		}
		
		return sb;
    }

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
    
}


