package id.co.lhk.wfh.controllers;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.DetilPekerjaan;
import id.co.lhk.wfh.domain.DetilPekerjaan;
import id.co.lhk.wfh.domain.Kesehatan;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.Pekerjaan;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.KesehatanMapper;
import id.co.lhk.wfh.mapper.PekerjaanMapper;


@RestController
@RequestMapping("/pekerjaan")
public class PekerjaanController extends BaseController {

	@Autowired
	private PekerjaanMapper pekerjaanMapper;

	@Autowired
	private KesehatanMapper kesehatanMapper;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdftime = new SimpleDateFormat("hh:mm");

	@RequestMapping(value = "/check-me", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity checkMe(
			HttpServletRequest request, 
			@RequestParam("id") Optional<String> filterIdLaporan, 
			@RequestParam("tanggal_laporan")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> filterTanggalLaporan
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> map = getLoginUserMap(request);
		if(filterTanggalLaporan.isPresent()) {
			Calendar nowDate = Calendar.getInstance();
			Calendar endDate = Calendar.getInstance();endDate.set(Calendar.HOUR_OF_DAY, 20);endDate.set(Calendar.MINUTE, 00);
			System.out.println(endDate.getTime());
			System.out.println(">>>>");
			System.out.println(nowDate.getTime());
			System.out.println(nowDate.getTime().after(endDate.getTime()));
			//
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Kesehatan.USER_ID + "='"+map.get("user_id").toString()+"'");
			param.setClause(param.getClause()+" AND "+Kesehatan.TANGGAL + "='"+sdf.format(filterTanggalLaporan.get())+"'");
			
			Calendar yest = Calendar.getInstance();
			yest.add(Calendar.DAY_OF_YEAR, -1);
			
			boolean allowed = false;
			
			if(sdf.format(new Date()).equals(sdf.format(filterTanggalLaporan.get())) || sdf.format(yest.getTime()).equals(sdf.format(filterTanggalLaporan.get()))) {
				allowed = true;
			}
			
			
			if(kesehatanMapper.getCount(param) == 0) {
				resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
				resp.setMessage("*Anda belum melaporkan status kesehatan anda.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
			else if(!allowed) {
				resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
				resp.setMessage("*Anda tidak dapat mengisi pekerjaan untuk tanggal lain selain hari kemarin.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
			/*
			else if(nowDate.getTime().after(endDate.getTime())) {
				resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
				resp.setMessage("*Waktu penginputan kinerja dibatasi sampai jam 20.00");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}*/
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
    @RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseWrapperList getList(
    		HttpServletRequest request,
    		@RequestParam("filter_keyword") Optional<String> filter_keyword,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		//if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+ Pegawai.NAMA_PEGAWAI + " LIKE '%"+filter_keyword.get()+"%'");
		if(limit.isPresent()) {
    		param.setLimit(limit.get());
    		if(limit.get() > 2000) param.setLimit(2000);
    	}else {
        	param.setLimit(2000);
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    	}
    	
    	ResponseWrapperList resp = new ResponseWrapperList();
		//param.setClause("1");
		List<Pekerjaan> data = pekerjaanMapper.getList(param);
		resp.setCount(pekerjaanMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);

        return resp;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Pekerjaan data = pekerjaanMapper.getEntity(id);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

    
	@RequestMapping(value = "/me", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity me(HttpServletRequest request) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> map = getLoginUserMap(request);
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Kesehatan.USER_ID + "='"+map.get("user_id").toString()+"'");
		List<Pekerjaan> lst = pekerjaanMapper.getList(param);
		if(lst.size() > 0) resp.setData(lst.get(0));
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		HttpServletRequest request,
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("laporan_id") Optional<String> laporanId,
    		@RequestParam("status_tusi") Optional<String> statusTusi,
    		@RequestParam("nama") Optional<String> nama,
    		@RequestParam("uraian") Optional<String> uraian,
    		@RequestParam("output") Optional<String> output
    		) throws Exception {
    	 	
    		ResponseWrapper resp = new ResponseWrapper();
    		Pekerjaan data = new Pekerjaan(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = pekerjaanMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		if(laporanId.isPresent()) data.setLaporanId(laporanId.get());
    		if(statusTusi.isPresent()) data.setStatusTusi(statusTusi.get());
    		if(nama.isPresent()) data.setNama(nama.get());
    		if(uraian.isPresent()) data.setUraian(uraian.get());
    		if(output.isPresent()) data.setOutput(output.get());
    		
    		boolean pass = true;
    		if(data.getLaporanId()==null || data.getStatusTusi()==null || data.getStatusTusi()==null || data.getNama()==null || data.getUraian()==null || data.getOutput()==null) {
    			pass = false;
    		}else {
    			if(data.getLaporanId().equals("") || data.getStatusTusi().equals("") || data.getStatusTusi().equals("") || data.getNama().equals("") || data.getUraian().equals("") || data.getOutput().equals("")) {
    				pass = false;
    			}
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		Map<String, Object> loginUserMap = getLoginUserMap(request);
    		Kesehatan kesehatan = kesehatanMapper.getEntity(data.getLaporanId());
    		
    		if(id.isPresent()) {
    			pekerjaanMapper.update(data);
    		}else {
    			pekerjaanMapper.insert(data);	
    		}
    		kesehatanMapper.updateTUSI(kesehatan);
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }


    @RequestMapping(value="/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> delete(
			@PathVariable String id
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Pekerjaan data = pekerjaanMapper.getEntity(id);
		resp.setData(data);
		pekerjaanMapper.delete(data);
		Kesehatan kesehatan = kesehatanMapper.getEntity(data.getLaporanId());
		kesehatanMapper.updateTUSI(kesehatan);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    
    @RequestMapping(value = "/list/detil", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList getListDetil(
    		HttpServletRequest request,
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
			@RequestParam("nip_pegawai") Optional<String> nip_pegawai,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page
			) throws Exception{
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		
		if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) param.setClause(param.getClause() + " AND "+DetilPekerjaan.ID_UNIT_KERJA+" = '" + id_unit_kerja.get() + "'");
		if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilPekerjaan.TANGGAL+" BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'");
		else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilPekerjaan.TANGGAL+" >= '" + periode_awal.get() + "'");
		else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilPekerjaan.TANGGAL+" <= '" + periode_akhir.get() + "'");

		
		if((nip_pegawai.isPresent() && !nip_pegawai.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilPekerjaan.NIP_PEGAWAI+" LIKE '%" + nip_pegawai.get() + "%'");
		
		resp.setCount(pekerjaanMapper.getCountDetil(param)); 
		
		param.setOrder(DetilPekerjaan.ID_UNIT_KERJA + "," + DetilPekerjaan.NAMA_PEGAWAI + ", " + DetilPekerjaan.TANGGAL);
		
		if(limit.isPresent()) { param.setLimit(limit.get()); if(limit.get() > 2000) param.setLimit(2000); }
		else { param.setLimit(2000); }
    	
    	int pPage = 1;
    	if(page.isPresent()) { pPage = page.get(); int offset = (pPage-1)*param.getLimit(); param.setOffset(offset); }
    	
		List<DetilPekerjaan> data = pekerjaanMapper.getListDetil(param);
		resp.setData(data);
		resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){ qryString += "&limit="+limit.get(); }
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
		return resp;
	}
}
