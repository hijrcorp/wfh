package id.co.lhk.wfh.controllers;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.lhk.wfh.OrganizationReference;
import id.co.lhk.wfh.domain.Login;
import id.co.lhk.wfh.domain.LoginHistory;
import id.co.lhk.wfh.domain.OauthClient;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.Token;
import id.co.lhk.wfh.helper.BasicAuthRestTemplate;
import id.co.lhk.wfh.mapper.LoginMapper;
import id.co.lhk.wfh.mapper.UserMapper;
import id.co.lhk.wfh.ref.AccountLoginInfo;

@Configuration
@Controller
public class BaseController {
	
	@Autowired
    private UserMapper userMapper;
	
	@Autowired
	protected LoginMapper loginMapper;
	
	@Value("${security.oauth2.client.id}")
	protected String clientId;
	
	@Value("${security.oauth2.client.secret}")
	protected String clientSecret;
	
	@Value("${app.url.sso}")
	protected String ssoEndpointUrl;
	
	@Value("${app.cookie.name}")
	protected String cookieName;
	
	@Value("${domain.cookie.name}")
	protected String domainCookieName;
	
	protected String extractAccountLogin(HttpServletRequest request, AccountLoginInfo type) {
		String[] info = request.getRemoteUser().split("::");
		if(info.length == 2) {
			return info[type.getId()];
		}
		
		return request.getRemoteUser();
	}
	
	protected String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}

		return "";
	}

	protected String getCookieTokenValue(HttpServletRequest request) {
		
		return getCookieTokenValue(request, cookieName);
	}
	
	protected String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
			  String name = cookies[i].getName();
			  String value = cookies[i].getValue();
			  //System.out.println(name + "--" + value);
			  if(name.equals(cookieName)){
				  accessTokenValue= value;
			  }
			}
		}else {
			return extractHeaderToken(request);
			
		}
		return accessTokenValue;
	}
    
    protected boolean isTokenValid(HttpServletRequest request) {
    	return isTokenValid(getCookieTokenValue(request));
    }
    
    protected boolean isTokenValid(String accessTokenValue) {
		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
		ResponseEntity<String> result = null;
		String resp = "";
		boolean pass = true;
		if(accessTokenValue.equals("")) return false;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("token", accessTokenValue);
	
			result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/check_token", map , String.class );
	//		Token token = result.getBody();
	//		System.out.println(token.getAccessToken());
			
			resp = result.getBody();
		} catch (HttpClientErrorException e) {
			resp = e.getResponseBodyAsString();
			pass = false;
		 } catch (Exception e) {
		// TODO: handle exception
			 pass = false;
		 }
		return pass;
	}
	
	protected OauthUserOrganization getUserByToken(HttpServletRequest request) throws Exception {
		String accessTokenValue = getCookieTokenValue(request);
		
		String[] tokenArr = accessTokenValue.split("\\.");
		String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<String, Object>();
		//System.out.println(payload);
		// convert JSON string to Map
		map = mapper.readValue(payload, new TypeReference<Map<String, Object>>(){});
		
		String s = "";
		if(map.get("user_name") != null) {
			s = map.get("user_name").toString();
		}
		
		if(!s.equals("")) {
			String[] userOrg =  s.split("::");
			String username = userOrg[0];
			String orgcode = OrganizationReference.HEDES.toString();
			if(userOrg.length > 1) {
				orgcode = userOrg[1];
			}
			String clause = "(" + OauthUserOrganization.USERNAME + "='"+username+"' or " + OauthUserOrganization.EMAIL + "='"+userOrg[0]+"' )";
			clause += " and " + OauthUserOrganization.ORGANIZATION_CODE + "='"+orgcode+"'";
			List<OauthUserOrganization> lst = userMapper.findListWithOrganization(clause);
			 
			if (lst.size() > 0 ) return lst.get(0);
		}
		
		return null;
	}
	
	public void generateReport(HttpServletResponse response, String filename, FileInputStream inStream) throws Exception {
		String contentType = "application/xlsx";
		if(filename.contains(".pdf")) contentType = "application/pdf";
		response.setContentType(contentType);
		response.setHeader("Content-Disposition", "attachment; filename="+filename);
		
		OutputStream outStream = response.getOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
         
        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        inStream.close();
        outStream.close();
	}
	
	public Map<String, Object> getLoginUserMap(HttpServletRequest request) throws Exception {

		String accessTokenValue = getCookieTokenValue(request);
		String[] tokenArr = accessTokenValue.split("\\.");
		return new ObjectMapper().readValue(new String(Base64.getDecoder().decode(tokenArr[1])), new TypeReference<Map<String, Object>>(){});
		
	}
	
	protected boolean passCookieAuthorization(HttpServletRequest request) throws Exception {
		String accessTokenValue = "";
		Cookie[] cookies = request.getCookies();
		for (int i = 0; i < cookies.length; i++){
		      if (cookies[i].getName().equalsIgnoreCase(cookieName)) {
		    	  accessTokenValue= cookies[i].getValue();
		      }
		     
		   }
		if(!accessTokenValue.equals("")) {
			BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
			ResponseEntity<String> result = null;
			String resp = "";
			boolean pass = true;
			try {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				
				MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
				map.add("token", accessTokenValue);

				result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/check_token", map , String.class );
//				Token token = result.getBody();
//				System.out.println(token.getAccessToken());
				
				resp = result.getBody();
			} catch (HttpClientErrorException e) {
				resp = e.getResponseBodyAsString();
				System.out.println(resp);
				pass = false;
			 } catch (Exception e) {
				 pass = false;
			 }
			
			return pass;
		}
		return false;
	}
	
    
}


