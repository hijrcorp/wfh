package id.co.lhk.wfh.controllers;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.lhk.wfh.OrganizationReference;
import id.co.lhk.wfh.domain.Login;
import id.co.lhk.wfh.domain.LoginHistory;
import id.co.lhk.wfh.domain.OauthClient;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.domain.Token;
import id.co.lhk.wfh.helper.BasicAuthRestTemplate;
import id.co.lhk.wfh.mapper.LoginMapper;
import id.co.lhk.wfh.mapper.UserMapper;

@Configuration
@Controller
public class PageController {

	@Autowired
    private LoginMapper loginMapper;
	@Autowired
    private UserMapper userMapper;
	
	@Value("${security.oauth2.client.id}")
    private String clientId;
	
	@Value("${security.oauth2.client.secret}")
    private String clientSecret;
	
	@Value("${app.state}")
    private String appState;
	
	@Value("${app.url.sso}")
    private String ssoEndpointUrl;
	
	@Value("${app.cookie.name}")
    private String cookieName;
	
	@Value("${domain.cookie.name}")
    private String domainCookieName;
	
	@RequestMapping("/login-logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception{ 
		
		String accessTokenValue = getCookieTokenValue(request);
		if(isTokenValid(accessTokenValue)) {
			
			if (ssoEndpointUrl.contains("localhost") && request.getServerName().equals("localhost")) {
				Login login = loginMapper.findActiveLoginByAccessToken(accessTokenValue);
				
				LoginHistory loginHistory = new LoginHistory(login.getId());
		        loginHistory.setAccessToken(login.getAccessToken());
		        loginHistory.setRefreshToken(login.getRefreshToken());
		        loginHistory.setClientId(login.getClientId());
		        loginHistory.setUserName(login.getUserName());
		        loginHistory.setCreatedTime(login.getCreatedTime());
		        loginHistory.setExpireTime(login.getExpireTime());
		        loginHistory.setTokenObject(login.getTokenObject());
		        loginHistory.setOrganizationId(login.getOrganizationId());
		        loginHistory.setUserId(login.getUserId());
		        loginHistory.setStatus("logout");
		        if(login.getExpireTime().before(new Date())) {
		        		loginHistory.setStatus("expired");
		        }
		        	System.out.println(loginHistory.getId() + " == "+ loginHistory.getStatus());
		        loginMapper.insertLoginHistory(loginHistory); // 1
				loginMapper.deleteActiveLogin(login); // 2
			}
			
			
			Cookie cookie1 = new Cookie(cookieName, null);
			cookie1.setPath("/");
			cookie1.setMaxAge(0);
			if (!request.getServerName().equals("localhost")) {
				cookie1.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
			}
			response.addCookie(cookie1);
		}
		accessTokenValue = getCookieTokenValue(request, domainCookieName);
			
		if(isTokenValid(accessTokenValue)) {
			String scheme = request.getScheme();
			String serverName = request.getServerName();
			int serverPort = request.getServerPort();
			String contextPath = request.getContextPath();  // includes leading forward slash

			String loginPath = scheme + "://" + serverName + ":" + serverPort + contextPath + "/login";
			
			response.sendRedirect(ssoEndpointUrl + "/logout?redirect_uri="+loginPath);	
		}else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
		
		
	}
	
	protected Model addModelTokenInfo(HttpServletRequest request, Locale locale, Model model, String accessTokenValue) throws JsonParseException, JsonMappingException, IOException {
		if(!accessTokenValue.equals("")) {
			String[] tokenArr = accessTokenValue.split("\\.");
			String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = new HashMap<String, Object>();
			System.out.println(payload);
			// convert JSON string to Map
			map = mapper.readValue(payload, new TypeReference<Map<String, Object>>(){});
			
			//System.out.println(map.get("authorities"));
			String myname = map.get("real_name").toString();
			model.addAttribute("realName", myname.replace("null", "").trim());
			model.addAttribute("cookieName", this.cookieName);
			model.addAttribute("position", map.get("position_name").toString());
			model.addAttribute("roleList", (List) map.get("authorities"));
		
		}
 		return model;
	}

	@RequestMapping("/")
    public String loadIndex(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(pass) {
			addModelTokenInfo(request, locale, model,accessTokenValue);
		}
        return "index";
    }
	
	private String getCookieTokenValue(HttpServletRequest request) {
		
		return getCookieTokenValue(request, cookieName);
	}
	private String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		try {
			for(int i = 0; i < cookies.length; i++) {
			  String name = cookies[i].getName();
			  String value = cookies[i].getValue();
			  //System.out.println(name + "--" + value);
			  if(name.equals(cookieName)){
				  accessTokenValue= value;
			  }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return accessTokenValue;
	}
	@RequestMapping(value="/login", method = RequestMethod.GET)
    public String loadLogin(
    		Locale locale,
    		HttpServletRequest request, 
    		HttpServletResponse response,
    		@RequestParam("redirect") Optional<String> redirect,
    		@RequestParam("callback") Optional<String> callback,
    		@RequestParam("client") Optional<String> clientId,
    		@RequestParam("token") Optional<String> requestToken,
    		Model model) throws Exception{
		
		System.out.println(locale.toString());
		
		String accessTokenValue = getCookieTokenValue(request);
		
		boolean pass = isTokenValid(accessTokenValue);

		/*
		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(this.clientId, this.clientSecret);
		ResponseEntity<String> result = null;
		String resp = "";
		boolean pass = true;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("token", accessTokenValue);

			result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/check_token", map , String.class );
//			Token token = result.getBody();
//			System.out.println(token.getAccessToken());
			
			resp = result.getBody();
		} catch (HttpClientErrorException e) {
			resp = e.getResponseBodyAsString();
			pass = false;
		 } catch (Exception e) {
		// TODO: handle exception
		 }
		 */
		System.out.println("login: " + pass);
		
//    		String server = request.getScheme() + "://" +  request.getServerName() + ":" + request.getServerPort();
    		String loginUrl = "login-perform?redirect="+request.getContextPath()+"/page/admin/";
    		String redirectUrl = request.getContextPath()+"/admin/";
    		
    		if(redirect.isPresent()) {
    			redirectUrl = redirect.get();
    			loginUrl = "login-perform?redirect="+redirect.get();
    		}
    		
    		if(callback.isPresent()) {
    			loginUrl = "login-action?callback="+callback.get()+"&client="+clientId.get()+"&token="+requestToken.get();
    			
    			if(pass) {
    				response.sendRedirect(callback.get()+"?access_token="+accessTokenValue);
    			}
    			
    		}else {
    			
    			if(pass) {
    				response.sendRedirect(redirectUrl);
    			}
    		}
    		
    		model.addAttribute("loginPerform", loginUrl);
        return "login";
    }
	
	
	
	@RequestMapping(value="/login-action", method = RequestMethod.POST)
    public String actionLogin(
    		HttpServletRequest request,
    		HttpServletResponse response,
    		@RequestParam("callback") String callback,
    		@RequestParam("token") String requestToken,
    		@RequestParam("client") String client,
    		@RequestParam("email") String email, 
    		@RequestParam("password") String password
    		){
    	
		OauthClient oauthClient = loginMapper.findOauthClientByLoginRequest(client, requestToken, callback);
    		
		boolean isSuccess = false;
    		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(oauthClient.getClientId(), oauthClient.getClientSecret());
    		ResponseEntity<Token> result = null;
    		try {
    			HttpHeaders headers = new HttpHeaders();
    			headers.setContentType(MediaType.APPLICATION_JSON);
    			
    			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
    			map.add("username", email);
    			map.add("password", password);
    			map.add("grant_type", "password");

    			result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/token", map , Token.class );
    			Token token = result.getBody();
    			//System.out.println(token.getAccessToken());
    			
			setCookie(request, response, token);
    			isSuccess = true;
    			response.sendRedirect(callback+"?access_token="+token.getAccessToken());
			
    			
    			
    		} catch (HttpClientErrorException e) {
    			System.out.println(e.getResponseBodyAsString());
    			
		} catch (Exception e) {
			// TODO: handle exception
//			model.addAttribute("error", "true");
//    			model.addAttribute("message", "Invalid email or password");
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    		if(!isSuccess) {
    			try {
    				String loginError = request.getContextPath()+ "/login?error=1&callback="+callback+"&client="+clientId+"&token="+requestToken;
    				response.sendRedirect(loginError);
    			} catch (Exception e2) {
    				// TODO: handle exception
    			}
    		}
    		
    		System.out.println(result);
    		
        return "login";
    }
    
    
	@RequestMapping(value="/login-perform", method = RequestMethod.POST)
    public String performLogin(
    		HttpServletRequest request,
    		HttpServletResponse response,
    		@RequestParam("redirect") String redirect,
    		@RequestParam("username_or_email") String usernameOrEmail, 
    		@RequestParam("password") String password,
    		@RequestParam("organization") Optional<String> organization
    		){

		
    		boolean isSuccess = false;
    		
    		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
    		
    		ResponseEntity<Token> result = null;
    		try {
    			HttpHeaders headers = new HttpHeaders();
    			headers.setContentType(MediaType.APPLICATION_JSON);
    			
    			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
    			String userAuth = usernameOrEmail+"::"+OrganizationReference.WFH.toString();
    			if(organization.isPresent()) {
    				userAuth = usernameOrEmail+"::"+organization.get();
    			}
    			map.add("username", userAuth);
    			map.add("password", password);
    			map.add("grant_type", "password");

    			result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/token", map , Token.class );
    			Token token = result.getBody();
    			//System.out.println(token.getAccessToken());
			
    			setCookie(request, response, token);
    			isSuccess = true;
    			//if(!accessTokenValue.equals("")) {
    				String[] tokenArr = token.getAccessToken().split("\\.");
    				String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
    				ObjectMapper mapper = new ObjectMapper();
    				Map<String, Object> map_new = new HashMap<String, Object>();
    				System.out.println(payload);
    				// convert JSON string to Map
    				map_new = mapper.readValue(payload, new TypeReference<Map<String, Object>>(){});
    			//}
    				
    				//check user complete
    				String clause = OauthUserOrganization.ID + "='"+map_new.get("user_id").toString()+"'";
    				clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
    				List<OauthUserOrganization> lst = userMapper.findListWithOrganization(clause);
    				if(lst.size()>0) {
    					
    					
    					if(map_new.get("position_name").toString().equals("Admin") || map_new.get("position_name").toString().equals("Direktur")) {
    						redirect="page/admin/";
    					}else{
    						if(checkUserComplete(lst.get(0).getId())) {
    							redirect="page/laporan";
    						}else {
    							redirect="page/index";
    						}
    					}
    				}
    				//
    			response.sendRedirect(redirect);

    		} catch (HttpClientErrorException e) {
    			System.out.println(e.getResponseBodyAsString());

		} catch (Exception e) {
			// TODO: handle exception
//			model.addAttribute("error", "true");
//    			model.addAttribute("message", "Invalid email or password");
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    		if(!isSuccess) {
	    		try {
				String loginError = request.getContextPath()+ "/login?error=1&redirect="+redirect;
					response.sendRedirect(loginError);
				} catch (Exception e2) {
					// TODO: handle exception
				}
    		}
    		
    		System.out.println(result);
    		
        return "login";
    }
    
    private void setCookie(HttpServletRequest request,HttpServletResponse response, Token token) {
    		Cookie accessTokenCookie = new Cookie(cookieName, token.getAccessToken());
    		accessTokenCookie.setMaxAge(token.getExpiresIn().intValue()); 
		accessTokenCookie.setPath("/");
		System.out.println("host: " + request.getServerName());
		if(!request.getServerName().equals("localhost")) {
			accessTokenCookie.setDomain("."+request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		
		response.addCookie(accessTokenCookie);
		
//		Cookie refreshTokenCookie = new Cookie("oauth2-refresh-token", token.getRefreshToken());
//		refreshTokenCookie.setMaxAge(60*60*24*365); //Store cookie for 1 year
//		refreshTokenCookie.setPath("/");
//		refreshTokenCookie.setDomain(".hijr.co.id");
//		response.addCookie(refreshTokenCookie);
    }
    
    private String getRedirectURI(HttpServletRequest request, String page)  throws Exception {
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int serverPort = request.getServerPort();
		String contextPath = request.getContextPath();  // includes leading forward slash

		String resultPath = scheme + "://" + serverName + ":" + serverPort + contextPath;
		//System.out.println("Result path: " + resultPath);
		return resultPath + "/login-perform?page="+page;
//		return URLEncoder.encode(resultPath + "/login-perform", "UTF-8");
	}
    
    @RequestMapping(value = "/login-perform", method = RequestMethod.GET)
	public String getLoginPerform(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String code, @RequestParam String page) {

		boolean isSuccess = false;
		String redirect = request.getContextPath() + "/admin/" + page;

		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);

		ResponseEntity<Token> result = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("code", code);
			map.add("redirect_uri", getRedirectURI(request, page));
			map.add("grant_type", "authorization_code");

			result = restTemplate.postForEntity(ssoEndpointUrl + "/oauth/token", map, Token.class);
			Token token = result.getBody();
			// System.out.println(token.getAccessToken());

			setCookie(request, response, token);
			isSuccess = true;
			response.sendRedirect(redirect);

		} catch (HttpClientErrorException e) {
			System.out.println(e.getResponseBodyAsString());

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		if (!isSuccess) {
			try {
				String loginError = request.getContextPath() + "/login?error=1&redirect=" + redirect;
				response.sendRedirect(loginError);
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return "nopage";
	}
    
    private boolean isTokenValid(String accessTokenValue) {
    		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
		ResponseEntity<String> result = null;
		String resp = "";
		boolean pass = true;
		if(accessTokenValue.equals("")) return false;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("token", accessTokenValue);

			result = restTemplate.postForEntity( ssoEndpointUrl + "/oauth/check_token", map , String.class );
//			Token token = result.getBody();
//			System.out.println(token.getAccessToken());
			
			resp = result.getBody();
		} catch (HttpClientErrorException e) {
			resp = e.getResponseBodyAsString();
			pass = false;
		 } catch (Exception e) {
		// TODO: handle exception
			 pass = false;
		 }
		return pass;
    }
    
    @RequestMapping("/page/admin/")
    public String loadMainAdmin(Model model, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
    		String jsp = "index"; // default page, please check
		return loadPageA(model, jsp, request, response);
    }
    
    @RequestMapping("/page/admin")
    public String loadMainAdminNoSlash(Model model, HttpServletRequest request,HttpServletResponse response) throws Exception{
    	String jsp = "index"; // default page, please check
		return loadPageA(model, jsp, request, response);
    }
    
    @RequestMapping("/page/admin/{jsp}")
    public String loadPageA(Model model, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request);
		boolean pass = isTokenValid(accessTokenValue);
		System.out.println("local ==> " + pass);
		
		boolean passSSO = false; 
		if(!pass) { //try SSO login
			accessTokenValue = getCookieTokenValue(request, domainCookieName);
			
			passSSO = isTokenValid(accessTokenValue);
			System.out.println("sso ==> " + passSSO);
		}
		
		if(!pass && !passSSO) {
			response.sendRedirect(request.getContextPath()+"/login");
		}else if(!pass && passSSO) {
			String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + getRedirectURI(request, jsp) + "";
			System.out.println(ssoEndpointUrl + "/oauth/authorize?" + params);
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			System.out.println(">> " + accessTokenValue);
			
			if(!accessTokenValue.equals("")) {
				String[] tokenArr = accessTokenValue.split("\\.");
				String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = new HashMap<String, Object>();
				System.out.println(payload);
				// convert JSON string to Map
				map = mapper.readValue(payload, new TypeReference<Map<String, Object>>(){});
				
				//System.out.println(map.get("authorities"));
				String myname = map.get("real_name").toString();
				model.addAttribute("realName", myname.replace("null", "").trim());
				model.addAttribute("cookieName", this.cookieName);
				model.addAttribute("position", map.get("position_name").toString());
				model.addAttribute("roleList", (List) map.get("authorities"));
			
				//check user position
				pass = false;
				if(map.get("position_name").toString().equals("Admin") ||
						map.get("position_name").toString().equals("Direktur")) {
					pass = true;
				}else {
					if (map.get("position_name").toString().equals("Kepala") && checkUserComplete(map.get("user_id").toString())) {
						pass = true;
					}
				}
				
				System.out.println(">> " + pass);
			}
			if(!pass) {
				response.sendRedirect(request.getContextPath()+"/page/");
			}else {
				return "pages/admin/"+jsp;
			}
		} 
        return "nopage";
    }
    
    
    
    @RequestMapping("/page/")
    public String loadMainUser(Model model, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
    	String jsp = "index"; // default page, please check
		return loadPageUser(model, jsp, request, response);
    }
    @RequestMapping("/page")
    public String loadMainUserWithNoSlash(Model model, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
    		String jsp = "index"; // default page, please check
		return loadPageUser(model, jsp, request, response);
    }
    
    @RequestMapping("/page/{jsp}")
    public String loadPageUser(Model model, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
		String accessTokenValue = getCookieTokenValue(request);
		boolean pass = isTokenValid(accessTokenValue);
		System.out.println("local ==> " + pass);
		
		boolean passSSO = false; 
		if(!pass) { //try SSO login
			accessTokenValue = getCookieTokenValue(request, domainCookieName);
			
			passSSO = isTokenValid(accessTokenValue);
			System.out.println("sso ==> " + passSSO);
		}
		
		if(!pass && !passSSO) {
			response.sendRedirect(request.getContextPath()+"/login");
		}else if(!pass && passSSO) {
			String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + getRedirectURI(request, jsp) + "";
			System.out.println(ssoEndpointUrl + "/oauth/authorize?" + params);
			response.sendRedirect(ssoEndpointUrl + "/oauth/authorize?" + params);
		}else {
			System.out.println(">> " + accessTokenValue);

			String redirect="";
			if(!accessTokenValue.equals("")) {
				String[] tokenArr = accessTokenValue.split("\\.");
				String payload = new String(Base64.getDecoder().decode(tokenArr[1]));
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = new HashMap<String, Object>();
				System.out.println(payload);
				// convert JSON string to Map
				map = mapper.readValue(payload, new TypeReference<Map<String, Object>>(){});
				
				//System.out.println(map.get("authorities"));
				String myname = map.get("real_name").toString();
				model.addAttribute("realName", myname.replace("null", "").trim());
				model.addAttribute("cookieName", this.cookieName);
				model.addAttribute("position", map.get("position_name").toString());
				model.addAttribute("roleList", (List) map.get("authorities"));
			
				//check user complete
				pass=checkUserComplete(map.get("user_id").toString());
				
				model.addAttribute("updateProfil", pass);
				
				if(map.get("position_name").toString().equals("Admin") ||
						map.get("position_name").toString().equals("Direktur") ||
						map.get("position_name").toString().equals("Kepala")) {
					model.addAttribute("adminPage", "yes");
				}
				
				if(map.get("position_name").toString().equals("Admin") || map.get("position_name").toString().equals("Direktur")) {
					redirect="/page/admin/";
					response.sendRedirect(request.getContextPath()+redirect);
				}
			}
			
			if(!pass) {
				return "pages/index";
			}else {
				return "pages/"+jsp;
			}
		} 
        return "nopage";
    }
    private boolean checkUserComplete(String id)  throws Exception {
    		boolean pass=true;
    	//check user complete
		String clause = OauthUserOrganization.ID + "='"+id+"'";
		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
		List<OauthUserOrganizationUnit> lst = userMapper.findListWithOrganizationUnitNew(clause);
		System.out.println("ready for");
		System.out.println(lst.size());
		if(lst.size()>0) {
			OauthUserOrganization o = lst.get(0);
			
			if(o.getBirthDate()==null || o.getMobilePhone()==null || o.getEmail()==null || o.getGender()==null || o.getIdJabatanUserDetil() == null || o.getAlamatRumahUserDetil() == null) {
				pass=false;
			}else if(o.getGender().equals("") || o.getEmail().equals("") || o.getMobilePhone().equals("") || o.getIdJabatanUserDetil().equals("") || o.getAlamatRumahUserDetil().equals("")) {
				pass=false;
			}
		}
		return pass;
	}
}


