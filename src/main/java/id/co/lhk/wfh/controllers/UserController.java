package id.co.lhk.wfh.controllers;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mysql.fabric.xmlrpc.base.Data;

import id.co.lhk.wfh.OrganizationReference;
import id.co.lhk.wfh.core.Clause;
import id.co.lhk.wfh.core.CommonUtil;
import id.co.lhk.wfh.core.Message;
import id.co.lhk.wfh.core.NewMessageResult;
import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.UserDevice;
import id.co.lhk.wfh.domain.OauthRoleOrganization;
import id.co.lhk.wfh.domain.OauthUser;
import id.co.lhk.wfh.domain.OauthUserOperatorOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.domain.Position;
import id.co.lhk.wfh.domain.UnitKerja;
import id.co.lhk.wfh.domain.User;
import id.co.lhk.wfh.domain.UserDetil;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.UserDetilMapper;
import id.co.lhk.wfh.mapper.UserMapper;
import id.co.lhk.wfh.repo.StorageService;
import id.co.lhk.wfh.services.NotificationService;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserDetilMapper userDetilMapper;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Autowired
	protected StorageService storageService;

//    @RequestMapping(value = "/check/{id}", method = RequestMethod.GET,headers="Accept=application/json")
//	public ResponseEntity checkId(
//    		HttpServletRequest request,
//			@PathVariable String id
//			) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		Map<String, Object> loginUserMap = getLoginUserMap(request);
//		System.out.println(loginUserMap.get("authorities"));
//		
//		User user = userMapper.checkByUserId(id);
//		resp.setData(user);
//		if(user.getBirthDate()==null || user.getName().equals("") || user.getGender().equals("") || user.getMobilePhone().equals("") || user.getEmail().equals("")) {
//			resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
//			resp.setMessage("Profile masih belum lengkap, harap dilengkapi terlebih dahulu.");
//			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//		}
//		
//		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//	}
//    
    @RequestMapping(value = "/check/{id}", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity getByUserId(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		String clause = OauthUserOrganization.ID + "='"+id+"'";
		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
		QueryParameter param = new QueryParameter();
		param.setClause(clause);
		List<OauthUserOrganizationUnit> lst = userDetilMapper.getListGabungan(param); // userMapper.findListWithOrganizationUnitNew(clause);
		resp.setData(lst.get(0));
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
    
	@RequestMapping(value = "/me", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity me(HttpServletRequest request) throws Exception {
		Map<String, Object> map = getLoginUserMap(request);
		//Stri userId = Long.parseLong(map.get("user_id").toString());
		return getByUserId(map.get("user_id").toString());
	}
	
	@RequestMapping(value = "/check-username", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<ResponseWrapper> checkUsername(
			HttpServletRequest request, 
			@RequestParam("username") Optional<String> username
			) throws Exception {
		
		ResponseWrapper resp = new ResponseWrapper();
		String clause = OauthUserOrganization.USERNAME + "='"+username.get()+"'";
		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
		List<OauthUserOrganizationUnit> lst = userMapper.findListWithOrganizationUnitNew(clause);
		resp.setData(lst.get(0));
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/unit/{unitKerjaId}", method = RequestMethod.GET)
	public NewMessageResult getListUserUnitKerja(@PathVariable String unitKerjaId, @RequestParam Optional<String> position) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);
		
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String strPos =OauthUserOrganizationUnit.ORGANIZATION_ID+"='"+OrganizationReference.WFH.value()+"' ";
		if (position.isPresent()) {
			String[] arrPos = position.get().trim().split(",");
			if(arrPos.length > 1) {
				String inPos = "";
				for(String s: arrPos) {
					inPos += ",'" + s.trim()+"'";
				}
				strPos += " and " +OauthUserOrganizationUnit.POSITION_ID+" in ("+inPos.substring(1)+")";
			}else {
				strPos += " and " +OauthUserOrganizationUnit.POSITION_ID+"="+position.get();	
			}
			
		}
		if(unitKerjaId.equals("all")) {
			resp.setData(userMapper.findListWithOrganizationUnit(strPos)); 
		}else {
			resp.setData(userMapper.findListWithOrganizationUnit(strPos + " and " + OauthUserOrganizationUnit.UNIT_KERJA_ID+"="+unitKerjaId));
		}
		
		return resp;
	}
	
	@RequestMapping(value = "/unit/{unitKerjaId}/save", method = RequestMethod.POST)
	public NewMessageResult saveUnitKerjaUser(@PathVariable String unitKerjaId, @RequestParam String userId) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);
		
		userMapper.insertUnitKerjaUser(unitKerjaId, userId);
		return resp;
	}
	
	@RequestMapping(value = "/unit/{unitKerjaId}/remove", method = RequestMethod.POST)
	public NewMessageResult deleteUnitKerjaUser(@PathVariable String unitKerjaId, @RequestParam String userId) {
		NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);
		
		userMapper.deleteUnitKerjaUser(unitKerjaId, userId);
		return resp;
	}
	
    @RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message getList(
    		HttpServletRequest request,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("real_name") Optional<String> namaLengkap,
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("name_role") Optional<String> position,
    		@RequestParam("satker") Optional<String> satker
    		)throws Exception {
    	NewMessageResult resp = new NewMessageResult(Message.SUCCESS_CODE);
    	int pLimit = 10;
    	int pPage = 1;
    	if(limit.isPresent()) pLimit = limit.get();
    	if(page.isPresent()) pPage = page.get();
    	int offset = (pPage-1)*pLimit;
    	
		String filter = OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"' ";
		Clause q = new Clause();
		if(namaLengkap.isPresent()) filter +=  q.AND + q.QRY(OauthUserOrganization.FIRST_NAME, "LIKE", namaLengkap.get()) + q.OR + q.QRY(OauthUserOrganization.LAST_NAME, "LIKE", namaLengkap.get());
		if(username.isPresent()) filter += q.AND + q.QRY(OauthUserOrganization.USERNAME, "LIKE", username.get());
		if(position.isPresent()) filter += q.AND + "(" + q.QRY(OauthUserOrganization.POSITION_NAME, "=", position.get() )+ ")";
		
		if(satker.isPresent() && position.isPresent()) {
			filter += q.AND + q.QRY(OauthUserOperatorOrganization.UNIT_AUDITI_NAME, "LIKE", satker.get());
		}
		
		String newParam = filter + " order by " +OauthUserOrganization.FIRST_NAME;
		newParam +=  " LIMIT " + (pLimit) + " OFFSET "+offset;
		if(limit.isPresent()) {
			newParam = filter + " LIMIT " + (pPage*pLimit) + " OFFSET " + offset;
		}
		
//		List<User> data = userMapper.getList(newParam);
//		if(position.isPresent() && position.get().equals("6")) {
//			List<OauthUserOperatorOrganization> data = userMapper.findListWithOrganizationOperator(newParam );
//	    	resp.setData(data);
//	    	resp.setCount(userMapper.getCount(filter));
//	    		resp.setCount(userMapper.findCountListWithOrganizationOperator(filter));
//		}else {
//			List<OauthUserOrganization> data = userMapper.findListWithOrganizationLocal(newParam );
//	    	resp.setData(data);
//	    	resp.setCount(userMapper.getCount(filter));
//	    		resp.setCount(userMapper.findCountListWithOrganizationLocal(filter));	
//		}
		resp.setData(userMapper.findListWithOrganizationUnit(newParam));
		resp.setCount(userMapper.findCountWithOrganizationUnit(filter));
		
		resp.setLimit(pLimit);
		resp.setPageCount(Double.valueOf(Math.ceil((double)resp.getCount()/pLimit)).intValue());
		resp.setActivePage(pPage);
        return resp;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET,headers="Accept=application/json")
	public Message getById(
			@PathVariable String id
			) throws Exception {
		Message resp = new Message(Message.SUCCESS_CODE);
//		OauthUser data = userMapper.findUserById(id);
//		Map<String, Object> data = new HashMap<String, Object>();
		String clause = OauthUserOrganization.ID + "='"+id+"'";
		clause += " and " + OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"'";
		List<OauthUserOrganizationUnit> lst = userMapper.findListWithOrganizationUnitNew(clause);
//		if(lst.size() > 0) {
//			OauthUserOrganization user = lst.get(0);
//			data.put("user", user);
//			data.put("unit_auditi", userMapper.findUnitAuditiUser(user.getId()));
//			data.put("unit_kerja", userMapper.findUnitKerjaUser(user.getId()));
//		}
		resp.setData(lst.get(0));
		return resp;
	}
	
    @RequestMapping(value="/insert", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message insert(
    		HttpServletRequest request,
    		@RequestParam("firstName") String firstName,
    		@RequestParam("lastName") String lastName,
    		@RequestParam("username") String username,
    		@RequestParam("position") String position,
    		@RequestParam("password") String password,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("unitAuditi") Optional<String> unitAuditi,
    		@RequestParam("unitKerja") Optional<String> unitKerja
    		) throws Exception {

//    	System.out.println(unitAuditi.get());
    	
    	Message resp = new Message(Message.SUCCESS_CODE);
		User o = new User(String.valueOf((new Date().getTime()+CommonUtil.randomPlus())));
		o.setFirstName(firstName);
		o.setLastName(lastName);
		o.setUsername(username);
		o.setPassword("" + passwordEncoder.encode(password) + "");
		o.setPosition(position);
		if(email.isPresent()) o.setEmail(email.get());

		if(firstName.isEmpty() || lastName.isEmpty() || username.isEmpty() || position.equals("0") || password.isEmpty()) {
			resp = new Message(Message.WARNING_CODE);
			resp.setMessage("Input masih belum lengkap.");
		}else {
			userMapper.insert(o);
			
			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.WFH.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.WFH.value());
			
			
//			userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
			userMapper.deleteAllUnitKerjaUser(o.getId());
			
//			if(position.equals("3") || position.equals("4")) {
				if(unitKerja.isPresent()) {
					userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
				}
//			}
//			if(position.equals("6")) {
//				if(unitAuditi.isPresent() && !unitAuditi.get().equals("")) {
//			    		System.out.println(unitAuditi.get());
//			    		
//			    		userMapper.insertUnitAuditiUser(unitAuditi.get(), o.getId());
//			    	}
//			}
			
			
			resp.setData(o);
		}
        return resp;
    }
    
    @RequestMapping(value="/{id}/update", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message update(HttpServletRequest request,
			@PathVariable String id,
    		@RequestParam("firstName") Optional<String> firstName,
    		@RequestParam("lastName") Optional<String> lastName,
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("position") Optional<String> position,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("unitAuditi") Optional<String> unitAuditi,
    		@RequestParam("unitKerja") Optional<String> unitKerja
    		) throws Exception {
    	
		Message resp = new Message(Message.SUCCESS_CODE);
		User o = userMapper.findById(id);
		if(firstName.isPresent()) o.setFirstName(firstName.get());
		if(lastName.isPresent()) o.setLastName(lastName.get());
		if(username.isPresent()) o.setUsername(username.get());
		if(position.isPresent()) o.setPosition(position.get());
//		o.setPassword("" + passwordEncoder.encode(o.getPassword()) + "");
		if(email.isPresent()) o.setEmail(email.get());

		userMapper.update(o);
//		deleteAndInsertPositionWithRole(o.getId(),o.getPosition());
		
		userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.WFH.value());
		userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.WFH.value());
		
//		userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
		userMapper.deleteAllUnitKerjaUser(o.getId());
		
		if(unitKerja.isPresent()) {
			userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
		}
		
		
		resp.setData(o);
        return resp;
    }
    

	//new method
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		HttpServletRequest request,
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("first_name") Optional<String> first_name,
    		@RequestParam("last_name") Optional<String> last_name,
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("position") Optional<String> position,
    		@RequestParam("password") Optional<String> password,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("gender") Optional<String> gender,
    		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
    		@RequestParam("mobile_phone") Optional<String> mobilePhone,
    		@RequestParam("unit_auditi") Optional<String> unitAuditi,
    		@RequestParam("unit_kerja") Optional<String> unitKerja,
    		@RequestParam("jabatan") Optional<String> jabatan
//    		@RequestParam("alamat") Optional<String> alamat,
//    		@RequestParam("riwayat_penyakit") Optional<String> riwayatPenyakit
    		) throws Exception {
    	
    	System.out.println("SAVE --------");
    	
    	ResponseWrapper resp = new ResponseWrapper();
		User o = new User(String.valueOf((new Date().getTime()+CommonUtil.randomPlus())));
		if(id.isPresent()) {
			o.setId(String.valueOf(id.get()));
			o = userMapper.findById(id.get());
			o = (User) userMapper.findByIdWithOrganizationUnitNew(id.get());
		}
		if(o == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
//		if(first_name.isPresent()) o.setFirstName(first_name.get());
		if(last_name.isPresent()) o.setLastName(last_name.get());
		if(username.isPresent()) o.setUsername(username.get());
//		if(password.isPresent()) o.setPassword("" + passwordEncoder.encode(password.get()) + "");
		if(position.isPresent()) o.setPosition(position.get());
		if(email.isPresent()) o.setEmail(email.get());
		if(gender.isPresent()) o.setGender(gender.get());
//		if(birthDate.isPresent()) o.setBirthDate(birthDate.get());
		if(mobilePhone.isPresent()) o.setMobilePhone(mobilePhone.get());
		
		
		boolean pass = true;
		if(o.getLastName() == null && o.getUsername() == null && o.getPosition() == null) {
			pass = false;
		}else {
			if(o.getLastName().equals("") && o.getUsername().equals("")  && o.getPosition().equals("0")) {
				pass = false;
			}
		}

		if(!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
		if(id.isPresent()) {
			userMapper.update(o);
//			deleteAndInsertPositionWithRole(o.getId(),o.getPosition());
			
			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.WFH.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.WFH.value());
			
//			userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
			userMapper.deleteAllUnitKerjaUser(o.getId());
			
			if(unitKerja.isPresent() && !unitKerja.get().equals("")) {
				userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
			}
		}else{
			if(password.isPresent()) o.setPassword("" + passwordEncoder.encode(password.get()) + "");
			else{
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("password masih kosong, harap dilengkapi terlebih dahulu.");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
			}
			userMapper.insert(o);
			
			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.WFH.value());
			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.WFH.value());
			
			
			userMapper.deleteAllUnitKerjaUser(o.getId());
			
			if(unitKerja.isPresent()) {
				userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
			}
		}
		
		boolean insert =false;
		UserDetil ud = userDetilMapper.getEntity(o.getId());
		if(ud == null) {
			insert = true;
			ud = new UserDetil(Utils.getLongNumberID());
			ud.setUserId(o.getId());
		}
		if(jabatan.isPresent()) {
			ud.setIdJabatan(jabatan.get());
		}
		if(insert) {
			userDetilMapper.insert(ud);	
		}else {
			userDetilMapper.update(ud);	
		}
		
				
		resp.setData(o);

		return new ResponseEntity<id.co.lhk.wfh.helper.ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
   	public ResponseEntity<ResponseWrapper> changePassword(
   			@RequestParam("oldPassword") Optional<String> oldPassword, 
   			@RequestParam("newPassword") Optional<String> newPassword, 
   			@RequestParam("newPassword2") Optional<String> newPassword2
   	) throws Exception {
    	OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
    	OauthUserOrganization userAuthentication = (OauthUserOrganization)oAuth2Authentication.getUserAuthentication().getPrincipal();
   	
   		ResponseWrapper resp = new ResponseWrapper();
   		OauthUser o = userMapper.findUserById(userAuthentication.getId());
   		if(oldPassword.isPresent() && oldPassword.get().equals("")) {
   			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Password lama tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		}
   		if(newPassword.isPresent() && newPassword.get().equals("")) {
   			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Password baru tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		}
   		if(newPassword.isPresent() && newPassword2.isPresent()) {
   			if(!newPassword.get().equals(newPassword2.get())) {
   	   			resp.setCode(HttpStatus.BAD_REQUEST.value());
   				resp.setMessage("Ketikan ulang Password tidak cocok");
   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   			}else {
   				if (!passwordEncoder.matches(oldPassword.get(), o.getPassword())) {
   					resp.setCode(HttpStatus.BAD_REQUEST.value());
   	   				resp.setMessage("Password lama anda salah");
   	   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		        }else {
   		        	userMapper.updateUserPassword(o.getId(), passwordEncoder.encode(newPassword.get()) );
   	   	   			resp.setData(o);
   		        }
   	   		}
   		}
   		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   	}
    
    @RequestMapping(value="/{id}/updatePass", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> updatePass(
			@PathVariable String id,
			HttpServletRequest request,
		@RequestParam("oldPassword") Optional<String> oldPassword,
    		@RequestParam("password") Optional<String> password,
    		@RequestParam("password2nd") Optional<String> password2nd
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(id.equals("me")) {
			Map<String, Object> map = getLoginUserMap(request);
			id = map.get("user_id").toString();
		}
		
		User o = userMapper.findByIdWithOrganizationUnitNew(id);
		System.out.println(o.getCurrentPassword());
		if(oldPassword.isPresent()) {
			if (!passwordEncoder.matches(oldPassword.get(), o.getPassword())) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
   				resp.setMessage("Password lama anda salah");
   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		     }
		}
		
		if(password.get().isEmpty()) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Kata kunci tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}else if(!password.get().equals(password2nd.get())) {

			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Ketikan ulang kata kunci tidak cocok");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}else {
			o.setPassword("" + passwordEncoder.encode(password.get()) + "");
			userMapper.updatePassword(o);
			resp.setData(o);
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    ///{id}
    @RequestMapping(value="/updateProfile", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> updateProfile(
			//@PathVariable String id,
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("lastName") Optional<String> lastName,
    		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
    		@RequestParam("birthDate")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birth_date,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("mobilePhone") Optional<String> mobilePhone,
    		//other
    		@RequestParam("unitKerja") Optional<String> unitKerja,
    		//other
    		@RequestParam("jabatan") Optional<String> jabatan,
    		@RequestParam("alamat") Optional<String> alamat,
    		@RequestParam("riwayatPenyakit") Optional<String> riwayatPenyakit,
    		// photo
    		@RequestParam("picture") Optional<MultipartFile> picture,
    		//other
    		@RequestParam("oldPassword") Optional<String> oldPassword,
    		@RequestParam("newPassword") Optional<String> newPassword,
    		@RequestParam("newPassword2") Optional<String> newPassword2
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		User o = userMapper.findByIdWithOrganizationUnitNew(id.get());
		if(lastName.isPresent()) o.setLastName(lastName.get());
		if(birthDate.isPresent()) o.setBirthDate(birthDate.get());
		if(birth_date.isPresent()) o.setBirthDate(birth_date.get());
		
		if(picture.isPresent() && !picture.get().isEmpty()) {
			if(picture.get().getContentType().startsWith("image")) {
				o.setPicture(picture.get().getOriginalFilename());
				String ext = FilenameUtils.getExtension(o.getPicture());
    			storageService.store(picture.get(), o.getId() + "."+ext);
			}else {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("Harap unggah file format Gambar (JPG/PNG/GIF).");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
			}
		}

		System.out.println("2");
		if(email.isPresent()) o.setEmail(email.get());
		if(mobilePhone.isPresent()) o.setMobilePhone(mobilePhone.get());
		
		boolean pass = true;
		if(o.getLastName() == null && o.getEmail() == null && o.getMobilePhone() == null) {
			pass = false;
			System.out.println("1");
		}else if(o.getLastName().equals("") && o.getEmail().equals("") && o.getMobilePhone().equals("")) {
			pass = false;
			System.out.println("2");
		}else {
			if(unitKerja.isPresent() && unitKerja.get().equals("")) pass = false;
			if(jabatan.isPresent() && jabatan.get().equals("")) pass = false;
			if(alamat.isPresent() && alamat.get().equals("")) pass = false;
//			if(riwayatPenyakit.isPresent() && riwayatPenyakit.get().equals("")) pass = false;
			System.out.println("3");
		}

		if(!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap semua field dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		//
   		if(oldPassword.isPresent() && oldPassword.get().equals("")) {
   			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Password lama tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		}
   		if(newPassword.isPresent() && newPassword.get().equals("")) {
   			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Password baru tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		}
   		if(newPassword.isPresent() && newPassword2.isPresent()) {
   			if(!newPassword.get().equals(newPassword2.get())) {
   	   			resp.setCode(HttpStatus.BAD_REQUEST.value());
   				resp.setMessage("Ketikan ulang Password tidak cocok");
   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   			}else {
   				if (!passwordEncoder.matches(oldPassword.get(), o.getPassword())) {
   					resp.setCode(HttpStatus.BAD_REQUEST.value());
   	   				resp.setMessage("Password lama anda salah");
   	   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		        }else {
   		        	userMapper.updateUserPassword(o.getId(), passwordEncoder.encode(newPassword.get()) );
   	   	   			resp.setData(o);
   		        }
   	   		}
   		}
		//
		userMapper.update(o);
		
		userMapper.deleteAllUnitKerjaUser(o.getId());
		userMapper.insertUnitKerjaUser(unitKerja.get(), o.getId());
		
		UserDetil ud = userDetilMapper.getEntity(o.getId());
		if(alamat.isPresent()) ud.setAlamatRumah(alamat.get());
		if(riwayatPenyakit.isPresent()) ud.setRiwayatPenyakit(riwayatPenyakit.get());
		if(jabatan.isPresent()) ud.setIdJabatan(jabatan.get());
		
		userMapper.deleteAllUserDetil(o.getId());
		userMapper.insertUserDetil(String.valueOf((new Date().getTime()+CommonUtil.randomPlus())), o.getId(), ud.getAlamatRumah(), ud.getRiwayatPenyakit(), ud.getIdJabatan());
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    @RequestMapping(value="/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message delete(@PathVariable String id) throws Exception {
		Message resp = new Message(Message.SUCCESS_CODE);
		OauthUser o = userMapper.findUserById(id);
		userMapper.deleteUserPosition(Long.valueOf(id), OrganizationReference.WFH.value());
		System.out.println(userMapper.isNipPegawaiExist(o.getUsername()));
		if(userMapper.isNipPegawaiExist(o.getUsername()) == false) {
			userMapper.deleteAllUnitAuditiUser(o.getId());
			userMapper.deleteAllUnitKerjaUser(o.getId());
//			userMapper.deleteUserRole(Long.valueOf(id));
			userMapper.deleteUser(Long.valueOf(id));
		}
		resp.setData(o);
        return resp;
    }
    
    // Responden
//    @RequestMapping(value="/generate-responden", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public Message generateResponden(
//    		HttpServletRequest request,
//    		@RequestParam("id_surat_tugas") String idSuratTugas,
//    		@RequestParam("id_surat_tugas_tim") String idSuratTugasTim,
//    		@RequestParam("jumlah") Integer jumlah
//    		) throws Exception {
//
////    	System.out.println(unitAuditi.get());
//    	
//    	Message resp = new Message(Message.SUCCESS_CODE);
//		if(idSuratTugas.equals("") || idSuratTugasTim.equals("")) {
//			resp = new Message(Message.WARNING_CODE);
//			resp.setMessage("Input masih belum lengkap.");
//		}
//		
//		List<User> listUser = new ArrayList<User>();
//		
//		for(int i = 0 ; i < jumlah ; i++) {
//
//			User o = new User(String.valueOf((new Date().getTime()+CommonUtil.randomPlus())));
//			o.setUsername(o.getId());
//			o.setEmail(Utils.generatePassword());
//			o.setPassword("" + passwordEncoder.encode(o.getEmail()) + "");
//			o.setPosition("100");
//
//			userMapper.insert(o);
//			SuratTugasUserResponden suratTugasUserResponden = new SuratTugasUserResponden(String.valueOf((new Date().getTime()+CommonUtil.randomPlus())));
//			suratTugasUserResponden.setSuratTugasId(idSuratTugas);
//			suratTugasUserResponden.setUserId(o.getId());
//			suratTugasUserResponden.setTimId(idSuratTugasTim);
//			suratTugasUserResponden.setStatus("CREATED");
//			suratTugasUserRespondenMapper.insert(suratTugasUserResponden);
//			
//			userMapper.deleteUserPosition(Long.valueOf(o.getId()), OrganizationReference.LAPGAS.value());
//			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.LAPGAS.value());
//			userMapper.insertUserPosition(Long.valueOf(o.getId()), Long.valueOf(o.getPosition()), OrganizationReference.ALHP.value());
//			
//			
////				userMapper.deleteUnitAuditiUser(unitAuditi.get(), o.getId());
//			userMapper.deleteAllUnitKerjaUser(o.getId());
//			
//			listUser.add(o);
//		
//			
//		}
//		resp.setData(listUser);
//        return resp;
//    }

	
	@RequestMapping(value = "/position/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper listPosition() {	
		ResponseWrapper resp = new ResponseWrapper();

		List<OauthRoleOrganization> data = userMapper.findRolesByOrganization(OrganizationReference.WFH.value());
		resp.setData(data);
		
		return resp;
	}

	
	
	
    @RequestMapping(value="/list-load-more", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseWrapperList  getListLoadMore(
    		HttpServletRequest request,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> keyword,
    		@RequestParam("real_name") Optional<String> namaLengkap,
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("name_role") Optional<String> position,
    		@RequestParam("satker") Optional<String> satker
    		//@RequestParam("position-exclude") Optional<String> positionExclude
    		)throws Exception {

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND "+ OauthUserOrganization.ORGANIZATION_ID + " = '"+OrganizationReference.WFH.value()+"'");
		if(keyword.isPresent()) param.setClause(param.getClause() + " AND ( "+OauthUserOrganization.FIRST_NAME+" LIKE '%"+keyword.get()+"%' OR "+OauthUserOrganization.LAST_NAME+" LIKE '%"+keyword.get()+"%' OR "+OauthUserOrganization.USERNAME+" LIKE '%"+keyword.get()+"%' OR "+OauthUserOrganization.POSITION_NAME+" LIKE '%"+keyword.get()+"%' OR "+OauthUserOrganization.EMAIL+" LIKE '%"+keyword.get()+"%')");
		if(namaLengkap.isPresent()) param.setClause(param.getClause() + " AND "+ OauthUserOrganization.FIRST_NAME + " LIKE '%"+namaLengkap.get()+"%'");
		if(username.isPresent()) param.setClause(param.getClause() + " AND "+ OauthUserOrganization.USERNAME + " LIKE '%"+username.get()+"%'");
		if(position.isPresent()) param.setClause(param.getClause() + " AND "+ OauthUserOrganization.POSITION_NAME + " = '"+position.get()+"'");
		
		if(satker.isPresent() && position.isPresent()) {
			if(position.isPresent()) param.setClause(param.getClause() + " AND "+ OauthUserOperatorOrganization.UNIT_AUDITI_NAME + " LIKE '%"+position.get()+"%'");
		}
		
		String[] positionExclude = request.getParameterValues("position-exclude");
    	if(positionExclude != null) {
    		String strFilter = ""; for(String s: positionExclude) { strFilter += ","+s+""; }
    		param.setClause(param.getClause() + " AND " + OauthUserOrganization.POSITION_ID + " NOT IN ("+strFilter.substring(1)+")");
    	}
    	System.out.println("user param: " + param.getClause());
		
		if(limit.isPresent()) { param.setLimit(limit.get()); if(limit.get() > 2000) param.setLimit(2000); } 
		else { param.setLimit(2000); }
	    	
    	int pPage = 1;
    	if(page.isPresent()) { pPage = page.get(); int offset = (pPage-1)*param.getLimit(); param.setOffset(offset); }
    	
    	String filter = param.getClause() + " group by "+OauthUserOrganization.ID+" order by " +OauthUserOrganization.FIRST_NAME;
    	String newParam = filter;
		if(limit.isPresent()) newParam += " LIMIT " + (pPage*param.getLimit()) + " OFFSET " + param.getOffset();
		else newParam +=  " LIMIT " + (param.getLimit()) + " OFFSET "+param.getOffset(); 
		List<OauthUserOrganizationUnit> data = userMapper.findListWithOrganizationUnitNew(newParam);
		System.out.println("param: " + newParam);
		System.out.println("filter: " + filter);

    	ResponseWrapperList resp = new ResponseWrapperList();
		resp.setData(data);
		resp.setCount(userMapper.findCountWithOrganizationUnit(filter));
		resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){ qryString += "&limit="+limit.get(); }
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);

        return resp;
    }
   
    
    @RequestMapping(value="/insert-pegawai", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message insertPegawai(
    		HttpServletRequest request,
    		@RequestParam("nip") String nip,
    		@RequestParam("position") String position
    		) throws Exception {
    	
		Message resp = new Message(Message.SUCCESS_CODE);

		String filter1 = OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.ALHP.value()+"' ";
		String filter2 = OauthUserOrganization.ORGANIZATION_ID + "='"+OrganizationReference.WFH.value()+"' ";
		Clause clause = new Clause();
		filter1 += " AND " + clause.QRY(OauthUserOrganization.USERNAME, "=", nip);
		filter2 += " AND " + clause.QRY(OauthUserOrganization.USERNAME, "=", nip);
		System.out.println("[insert-pegawai] filter: " + filter1);
		if(userMapper.isNipPegawaiExist(nip) == false) {
			resp = new Message(Message.ERROR_CODE);
			resp.setMessage("Pegawai dengan NIP '"+nip+"' tidak ada");
		}
		else if(userMapper.findListWithOrganizationUnit(filter1).size() == 0) {
			resp = new Message(Message.ERROR_CODE);
			resp.setMessage("User dengan NIP '"+nip+"' belum terdaftar");
		}
		else if(userMapper.findListWithOrganizationUnit(filter2).size() > 0) {
			resp = new Message(Message.ERROR_CODE);
			resp.setMessage("User dengan NIP '"+nip+"' sudah di import");
		}
		else {
			userMapper.deleteUserPositionByNIP(nip, OrganizationReference.WFH.value());
			userMapper.insertUserPositionByNIP(nip, Long.valueOf(position), OrganizationReference.WFH.value());
		}
		
        return resp;
    }
    
    @RequestMapping(value="/update/device", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public Message saveDevice(
    		HttpServletRequest request,
    		@RequestParam("device_id") String deviceId,
    		@RequestParam("platform_type") String platformType,
    		@RequestParam("apns_token") String apnsToken,
    		@RequestParam("fcm_token") String fcmToken
    		) throws Exception {
    	
		Message resp = new Message(Message.SUCCESS_CODE);

		Map<String, Object> map = getLoginUserMap(request);

		UserDevice device = new UserDevice();
		device.setDeviceId(deviceId);
		device.setPlatformType(platformType);
		device.setApnsToken(apnsToken);
		device.setFcmToken(fcmToken);
		device.setLastUpdated(new Date());
		device.setUserId(map.get("user_id").toString());
		
		userMapper.updateUserDevice(device);
		
		resp.setData(device);
		
        return resp;
    }
    
    
}
