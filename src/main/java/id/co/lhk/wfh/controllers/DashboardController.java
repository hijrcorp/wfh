package id.co.lhk.wfh.controllers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.StyleExcel;
import id.co.lhk.wfh.domain.Dashboard1;
import id.co.lhk.wfh.domain.Dashboard2;
import id.co.lhk.wfh.domain.Laporan1;
import id.co.lhk.wfh.domain.Laporan2;
import id.co.lhk.wfh.domain.Laporan3;
import id.co.lhk.wfh.domain.Laporan4;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.DashboardMapper;
import id.co.lhk.wfh.mapper.LaporanMapper;


@RestController
@RequestMapping("/dashboard")
public class DashboardController extends BaseController {

	@Autowired
	private DashboardMapper dashboardMapper;
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	
	@RequestMapping(value = "/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList get1(@RequestParam("id") Integer id, @RequestParam("tanggal") String tanggal) throws Exception{
		return new ResponseWrapperList(id==1?dashboardMapper.getDashboard1(tanggal):dashboardMapper.getDashboard2(tanggal)); 
	}
    
    @RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Integer id, 
			@RequestParam("tanggal") Optional<String> tanggalLaporan) throws Exception {
    	
		if(!passCookieAuthorization(request)) throw new Exception("User not authorized");
		
		final List<Dashboard1> listDashboard1 = dashboardMapper.getDashboard1(tanggalLaporan.get());
		
		final List<Dashboard2> listDashboard2 = dashboardMapper.getDashboard2(tanggalLaporan.get());

		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapHeader = new HashMap<>();
		mapHeader.put("header", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true);
		Map<String, Boolean> mapBodyCenter = new HashMap<>();
		mapBodyCenter.put("wrap", true);
		mapBodyCenter.put("center", true);
		Map<String, Boolean> mapBodyCenterYes = new HashMap<>();
		mapBodyCenterYes.put("wrap", true);
		mapBodyCenterYes.put("center", true);
		mapBodyCenterYes.put("green", true);
		Map<String, Boolean> mapBodyCenterNo = new HashMap<>();
		mapBodyCenterNo.put("wrap", true);
		mapBodyCenterNo.put("center", true);
		mapBodyCenterNo.put("red", true);
		Map<String, Boolean> mapBodyCenterMaybe = new HashMap<>();
		mapBodyCenterMaybe.put("wrap", true);
		mapBodyCenterMaybe.put("center", true);
		mapBodyCenterMaybe.put("orange", true);
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		Map<String, Boolean> mapFilter = new HashMap<>();
		mapFilter.put("filter", true);
		mapFilter.put("no-border", true);
		
		
		int panjangKolom = 0;
		String filename = "";
		String titlename = "";
		String sheetname = null;
		List<String> listHeader = new ArrayList<String>();
		List<String> listHeader2 = new ArrayList<String>();
		List<String> listHeader3 = new ArrayList<String>();
		if(id == 1) { 
			panjangKolom = 8; 
			titlename = "MONITORING PELAPORAN";
			filename = "MONITORING-PELAPORAN";
			sheetname = "Monitoring Pelaporan";
			String[] headerArr = {"No.:2:1","Unit Kerja:2:1","Jumlah Pegawai:2:1","Kinerja:1:3","Kesehatan:1:3"};
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			listHeader2.add("Melapor:1:1:3");
			listHeader2.add("Tidak:1:1:4");
			listHeader2.add("%:1:1:5");
			listHeader2.add("Melapor:1:1:6");
			listHeader2.add("Tidak:1:1:7");
			listHeader2.add("%:1:1:8");
		}
		else if(id == 2) { 
			panjangKolom = 17; 
			titlename = "MONITORING HASIL PELAPORAN";
			filename = "MONITORING-HASIL-PELAPORAN";
			sheetname = "monitoring-hasil-pelaporan";
			String[] headerArr = {"No.:3:1","Unit Kerja:3:1","Jumlah Pegawai:3:1","Kinerja:1:4","Kesehatan:1:11"};
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			listHeader2.add("Melapor:1:2:3");
			listHeader2.add("Tusi:2:1:5");
			listHeader2.add("Tambahan:2:1:6");
			listHeader2.add("Melapor:1:2:7");
			listHeader2.add("Sehat:1:2:9");
			listHeader2.add("Sakit:1:2:11");
			listHeader2.add("Rincian Sakit:1:5:13");

			listHeader3.add("Jumlah:1:1:3");
			listHeader3.add("%:1:1:4");
			//skip 2
			listHeader3.add("Jumlah:1:1:7");
			listHeader3.add("%:1:1:8");
			listHeader3.add("Jumlah:1:1:9");
			listHeader3.add("%:1:1:10");
			listHeader3.add("Jumlah:1:1:11");
			listHeader3.add("%:1:1:12");
			listHeader3.add("DM:1:1:13");
			listHeader3.add("BT:1:1:14");
			listHeader3.add("PL:1:1:15");
			listHeader3.add("ST:1:1:16");
			listHeader3.add("SN:1:1:17");
		}
		else throw new Exception("Format Laporan "+id+" tidak tersedia!");
		
		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename="+filename+".xlsx"); //name file
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(sheetname==null?filename:sheetname);
		StyleExcel style = new StyleExcel();

		XSSFRow aRow;
	    
		int idxRow = 0;
		int idx = 0;
		int iB = 0,iS = 0;
		idxRow++;
		XSSFRow title = sheet.createRow(idxRow);
		title.createCell(0).setCellValue(titlename);
		title.getCell(0).setCellStyle(style.styleBody(workbook, mapTitle));
		CellRangeAddress range = new CellRangeAddress(idxRow, idxRow, 0, panjangKolom);
		sheet.addMergedRegion(range);
		aRow = sheet.createRow(++idxRow);
		aRow.createCell(0).setCellValue("Tanggal: " + changeToDDMMYYYY(tanggalLaporan.get()));
		aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapFilter));
		range = new CellRangeAddress(idxRow, idxRow, 0, 3);
		sheet.addMergedRegion(range);
		idxRow++;
		idxRow++;
		
		XSSFRow header = sheet.createRow(idxRow++);
		int maxBaris = 1;
		for(String namaHeader : listHeader) {
			if(namaHeader.split(":").length > 1) {
				String nama = namaHeader.split(":")[0]; 
				int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
				int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
				try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
				maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
				header.createCell(idx).setCellValue(nama);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
					range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
					RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
					RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
					sheet.addMergedRegion(range);
				}
				idx += jumlahKolom;
			}
			else {
				header.createCell(idx).setCellValue(namaHeader);
				header.getCell(idx).setCellStyle(style.styleHeader(workbook));
				idx++;
			}
		}
		if(listHeader2.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader2) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		if(id==2)
		if(listHeader3.isEmpty()) for(int i = 1 ; i < maxBaris ; i++) { idxRow++; }
		else {
			header = sheet.createRow(idxRow++);
			for(int i = 0 ; i < idx ; i++) { header.createCell(i); header.getCell(i).setCellStyle(style.styleBody(workbook, mapHeader)); }
			for(String namaHeader : listHeader3) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0]; 
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					try { idx = Integer.parseInt(namaHeader.split(":")[3]); } catch(Exception e) {}
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleBody(workbook, mapHeader));
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}
				else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
		}
		
		int i = 1;
		if(id==1) {
			for(Dashboard1 o : listDashboard1) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getNamaUnitKerja());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
				aRow.createCell(iB++).setCellValue(o.getJumlahPegawai());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporPekerjaan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTidakMelaporPekerjaan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporPekerjaan()+"%");
	
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporKesehatan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahTidakMelaporKesehatan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporKesehatan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			}
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			aRow.createCell(iB++).setCellValue("");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue("Jumlah");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard1.stream().map(x -> x.getJumlahPegawai()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard1.stream().map(x -> x.getJumlahMelaporPekerjaan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard1.stream().map(x -> x.getJumlahTidakMelaporPekerjaan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard1.stream().map(x -> x.getJumlahPersenMelaporPekerjaan()).reduce(0.0, Double::sum) / listDashboard1.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
	
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard1.stream().map(x -> x.getJumlahMelaporKesehatan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard1.stream().map(x -> x.getJumlahTidakMelaporKesehatan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard1.stream().map(x -> x.getJumlahPersenMelaporKesehatan()).reduce(0.0, Double::sum) / listDashboard1.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			
			sheet.setColumnWidth(0, (5  * 256) + 200); // ID
			sheet.setColumnWidth(1, (27 * 256) + 200); // Tanggal Laporan
			sheet.setColumnWidth(2, (15 * 256) + 200); // Jumlah
			sheet.setColumnWidth(3, (15 * 256) + 200); // Ya
			sheet.setColumnWidth(4, (15 * 256) + 200); // Tidak
			sheet.setColumnWidth(5, (15 * 256) + 200); // %
			sheet.setColumnWidth(6, (15 * 256) + 200); // Jumlah
			sheet.setColumnWidth(7, (15 * 256) + 200); // %
			sheet.setColumnWidth(8, (15 * 256) + 200); // Jumlah
			sheet.setColumnWidth(9, (15 * 256) + 200); // %
		}
		else if(id==2) {
			for(Dashboard2 o : listDashboard2) {
				iB = 0; iS = 0;
				aRow = sheet.createRow(idxRow++);
				aRow.createCell(iB++).setCellValue(i++);
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getNamaUnitKerja());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
				aRow.createCell(iB++).setCellValue(o.getJumlahPegawai());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporPekerjaan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporPekerjaan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

				aRow.createCell(iB++).setCellValue(o.getJumlahTusiLaporan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

				aRow.createCell(iB++).setCellValue(o.getJumlahTambahanLaporan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahMelaporKesehatan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue(o.getJumlahPersenMelaporKesehatan()+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahSehat());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue((o.getJumlahPersenSehat()!=null?o.getJumlahPersenSehat():0)+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahSakit());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				aRow.createCell(iB++).setCellValue((o.getJumlahPersenSakit()!=null?o.getJumlahPersenSakit():0)+"%");
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahDemam());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahBatuk());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahPilek());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahTenggorokan());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
				aRow.createCell(iB++).setCellValue(o.getJumlahSesak());
				aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
				
			}
			iB = 0; iS = 0;
			aRow = sheet.createRow(idxRow++);
			aRow.createCell(iB++).setCellValue("");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue("Jumlah");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBody));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahPegawai()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahMelaporPekerjaan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard2.stream().map(x -> x.getJumlahPersenMelaporPekerjaan()).reduce(0.0, Double::sum) / listDashboard2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahTusiLaporan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahTambahanLaporan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
	
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahMelaporKesehatan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard2.stream().map(x -> x.getJumlahPersenMelaporKesehatan()).reduce(0.0, Double::sum) / listDashboard2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
	
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahSehat()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard2.stream().map(x -> (x.getJumlahPersenSehat()!=null?x.getJumlahPersenSehat():0)).reduce(0.0, Double::sum) / listDashboard2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
	
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahSakit()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(df2.format((double)listDashboard2.stream().map(x -> (x.getJumlahPersenSakit()!=null?x.getJumlahPersenSakit():0)).reduce(0.0, Double::sum) / listDashboard2.size()) + "%");
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));

			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahDemam()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahBatuk()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahPilek()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahTenggorokan()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
			aRow.createCell(iB++).setCellValue(Math.round((double)listDashboard2.stream().map(x -> x.getJumlahSesak()).reduce(0, Integer::sum)));
			aRow.getCell(iS++).setCellStyle(style.styleBody(workbook, mapBodyCenter));
//			
			sheet.setColumnWidth(0, (5  * 256) + 200); // ID
			sheet.setColumnWidth(1, (27 * 256) + 200); // Tanggal Laporan
//			sheet.setColumnWidth(2, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(3, (15 * 256) + 200); // Ya
//			sheet.setColumnWidth(4, (15 * 256) + 200); // Tidak
//			sheet.setColumnWidth(5, (15 * 256) + 200); // %
//			sheet.setColumnWidth(6, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(7, (15 * 256) + 200); // %
//			sheet.setColumnWidth(8, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(9, (15 * 256) + 200); // %
//			sheet.setColumnWidth(10, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(11, (15 * 256) + 200); // %
//			sheet.setColumnWidth(12, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(13, (15 * 256) + 200); // %
//			sheet.setColumnWidth(14, (15 * 256) + 200); // Jumlah
//			sheet.setColumnWidth(15, (15 * 256) + 200); // %
//			sheet.setColumnWidth(16, (15 * 256) + 200); // %
		}
		workbook.write(response.getOutputStream());
	}
    
    private String changeToDDMMYYYY(String date) {
    	try {
	    	String arr[] = date.split("-");
	    	return arr[2] + "/" + arr[1] + "/" + arr[0];
    	} catch(Exception e) {
    		return date;
    	}
    }
}
