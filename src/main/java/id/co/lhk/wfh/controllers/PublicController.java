package id.co.lhk.wfh.controllers;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.CommonUtil;
import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.OauthUser;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.domain.User;
import id.co.lhk.wfh.domain.UserDetil;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.LoginMapper;
import id.co.lhk.wfh.mapper.UserDetilMapper;
import id.co.lhk.wfh.mapper.UserMapper;
import id.co.lhk.wfh.services.NotificationService;

@RestController
@RequestMapping("/public")
public class PublicController {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserDetilMapper userDetilMapper;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Value("${app.organization.id}")
	private String organizationId;
	
	@Value("${app.organization.code}")
	private String organizationCode;
    
	 @RequestMapping(value="/daftar", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> daftar(
    		@RequestParam("username") Optional<String> username,
    		@RequestParam("lastName") Optional<String> lastName,
    		@RequestParam("gender") Optional<String> gender,
    		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("mobilePhone") Optional<String> mobilePhone,
    		//other
    		@RequestParam("unitKerja") Optional<String> unitKerja,
    		//other
    		@RequestParam("jabatan") Optional<String> jabatan,
    		@RequestParam("alamat") Optional<String> alamat,
    		@RequestParam("riwayatPenyakit") Optional<String> riwayatPenyakit,
    		//other
    		@RequestParam("newPassword") Optional<String> newPassword,
    		@RequestParam("newPassword2") Optional<String> newPassword2
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(username.isPresent()) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + OauthUserOrganizationUnit.USERNAME + "='"+username.get()+"'");
			List<OauthUserOrganizationUnit> lstUser = userDetilMapper.getListGabungan(param);
			if(lstUser.size() > 0) {
				resp.setCode(HttpStatus.BAD_REQUEST.value());
				resp.setMessage("NIP Anda sudah terdaftar");
				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
			}
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Semua input belum lengkap diisi.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
		User o = new User(Utils.getLongNumberID());
		if(username.isPresent()) o.setUsername(username.get());
		o.setFirstName("");
		if(lastName.isPresent()) o.setLastName(lastName.get());
		if(birthDate.isPresent()) o.setBirthDate(birthDate.get());
		if(gender.isPresent()) o.setGender(gender.get());
		System.out.println("2");
		if(email.isPresent()) o.setEmail(email.get());
		if(mobilePhone.isPresent()) o.setMobilePhone(mobilePhone.get());
		
		boolean pass = true;
		if(o.getLastName() == null && o.getEmail() == null && o.getMobilePhone() == null) {
			pass = false;
			System.out.println("1");
		}else if(o.getLastName().equals("") && o.getEmail().equals("") && o.getMobilePhone().equals("")) {
			pass = false;
			System.out.println("2");
		}else {
			if(unitKerja.isPresent() && unitKerja.get().equals("")) pass = false;
			if(jabatan.isPresent() && jabatan.get().equals("")) pass = false;
			if(alamat.isPresent() && alamat.get().equals("")) pass = false;
//				if(riwayatPenyakit.isPresent() && riwayatPenyakit.get().equals("")) pass = false;
			System.out.println("3");
		}

		if(!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Semua input belum lengkap diisi.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		//
   		if(newPassword.isPresent() && newPassword.get().equals("")) {
   			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Password baru tidak boleh kosong");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   		}
   		if(newPassword.isPresent() && newPassword2.isPresent()) {
   			if(!newPassword.get().equals(newPassword2.get())) {
   	   			resp.setCode(HttpStatus.BAD_REQUEST.value());
   				resp.setMessage("Ketikan ulang Password tidak cocok");
   				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   			}else {
//   		        	userMapper.updateUserPassword(o.getId(), passwordEncoder.encode(newPassword.get()) );
//   	   	   		resp.setData(o);
   	   	   		o.setPassword(passwordEncoder.encode(newPassword.get()));
   	   		}
   		}
		//
		userMapper.insert(o);
		
		userMapper.deleteAllUnitAuditiUser(o.getId());
		userMapper.insertUnitAuditiUser(unitKerja.get(), o.getId());
		
		userMapper.deleteUserPosition(Long.valueOf(o.getId()), organizationId);
		userMapper.insertUserPosition(Long.valueOf(o.getId()), 2, organizationId);
		
		UserDetil userDetil = new UserDetil(Utils.getLongNumberID());
		userDetil.setUserId(o.getId());
		userDetil.setAlamatRumah(alamat.get());
		userDetil.setRiwayatPenyakit(riwayatPenyakit.get());
		userDetil.setKodeJabatanSimpeg(jabatan.get());
		
		userDetilMapper.insert(userDetil);
		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	 
	 @Autowired
	NotificationService notificationService;
    
    @RequestMapping(value="/forgot/password", method = RequestMethod.POST, produces = "application/json")
   	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> forgotPassword(
       		HttpServletRequest request,
       		@RequestParam("username") String username
       		) throws Exception {
       	
    		ResponseWrapper resp = new ResponseWrapper();

   		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + OauthUserOrganizationUnit.USERNAME + "='"+username+"'");
		List<OauthUserOrganizationUnit> lstUser = userDetilMapper.getListGabungan(param);
		if(lstUser.size() == 0) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("NIP Anda belum terdaftar");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
		OauthUserOrganizationUnit user = lstUser.get(0);
		
		LinkedMultiValueMap<String, Object> payload = new LinkedMultiValueMap<String, Object>();
	    	payload.add("notification_app", organizationCode);
	    	payload.add("notification_title", "Reset Sandi");
	    	payload.add("notification_body_text", "Anda telah mengirim permintaan reset sandi aplikasi Simawas Mobile, kami akan segera menghubungi anda. <br/> Terimakasih.");
	    	payload.add("notification_settings", "ITJEN");
	    	payload.add("notification_user_added", organizationId);
	    //payload.add("email_attachments", new FileSystemResource(file));
	    payload.add("email_recipients", "EMAIL_SINGLE_TO:"+user.getEmail());
	    	notificationService.callSendEmailService(payload);
   		
   		
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
       }
}
