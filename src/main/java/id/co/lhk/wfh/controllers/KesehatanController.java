package id.co.lhk.wfh.controllers;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.DetilKesehatan;
import id.co.lhk.wfh.domain.Kesehatan;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.Pekerjaan;
import id.co.lhk.wfh.helper.ResponseWrapper;
import id.co.lhk.wfh.helper.ResponseWrapperList;
import id.co.lhk.wfh.helper.Utils;
import id.co.lhk.wfh.mapper.KesehatanMapper;
import id.co.lhk.wfh.mapper.PekerjaanMapper;


@RestController
@RequestMapping("/kesehatan")
public class KesehatanController extends BaseController {

	@Autowired
	private KesehatanMapper kesehatanMapper;

	@Autowired
	private PekerjaanMapper pekerjaanMapper;
	
    @RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseWrapperList getList(
    		HttpServletRequest request,
    		@RequestParam("filter_keyword") Optional<String> filter_keyword,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		//if(filter_keyword.isPresent()) param.setClause(param.getClause() + " AND "+ Pegawai.NAMA_PEGAWAI + " LIKE '%"+filter_keyword.get()+"%'");
		if(limit.isPresent()) {
    		param.setLimit(limit.get());
    		if(limit.get() > 2000) param.setLimit(2000);
    	}else {
        	param.setLimit(2000);
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    	}
    	
    	ResponseWrapperList resp = new ResponseWrapperList();
		//param.setClause("1");
		List<Kesehatan> data = kesehatanMapper.getList(param);
		resp.setCount(kesehatanMapper.getCount(param));
		resp.setData(data);
		resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);

        return resp;
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(@PathVariable String id) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Kesehatan data = kesehatanMapper.getEntity(id);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

    
	@RequestMapping(value = "/me", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<ResponseWrapper> me(HttpServletRequest request) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> map = getLoginUserMap(request);
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Kesehatan.USER_ID + "='"+map.get("user_id").toString()+"'");
		List<Kesehatan> lst = kesehatanMapper.getList(param);
		if(lst.size() > 0) resp.setData(lst.get(0));
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/me/latest", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<ResponseWrapper> meLatest(HttpServletRequest request) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> map = getLoginUserMap(request);
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Kesehatan.USER_ID + "='"+map.get("user_id").toString()+"'");
		
		Date latestDate = kesehatanMapper.getMaxDate(param);
		
		if(latestDate != null) {
			param.setClause(param.getClause()+" AND "+Kesehatan.TANGGAL + "='"+new SimpleDateFormat("yyyy-MM-dd").format(latestDate)+"'");
			List<Kesehatan> lst = kesehatanMapper.getList(param);
			if(lst.size() > 0) resp.setData(lst.get(0));
		}
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}

	@RequestMapping(value = "/check-laporan", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity checkLaporan(
			HttpServletRequest request, 
			@RequestParam("id") Optional<String> filterIdLaporan, 
			@RequestParam("tanggal_laporan")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> filterTanggalLaporan
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Map<String, Object> map = getLoginUserMap(request);
		QueryParameter param = new QueryParameter();
		if(filterTanggalLaporan.isPresent() && filterTanggalLaporan.get()==null) {
			resp.setCode(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
			resp.setMessage("Tanggal laporan masih kosong.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		param.setClause(param.getClause()+" AND "+Kesehatan.USER_ID + "='"+map.get("user_id").toString()+"'");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		param.setClause(param.getClause()+" AND "+Kesehatan.TANGGAL + "='"+sdf.format(filterTanggalLaporan.get())+"'");
		
		List<Kesehatan> lst = kesehatanMapper.getList(param);
		for(Kesehatan l : lst) {
			QueryParameter paramPekerjaan = new QueryParameter();
			paramPekerjaan.setClause(paramPekerjaan.getClause()+" AND "+Pekerjaan.LAPORAN_ID+" ='"+l.getId()+"'");
			l.setDetilPekerjaan(pekerjaanMapper.getList(paramPekerjaan));
		}
		resp.setData(lst);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		HttpServletRequest request,
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("tanggal_laporan") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalLaporan,
    		@RequestParam("status_sehat_laporan") Optional<String> statusSehatLaporan,
    		@RequestParam("keterangan_sakit_laporan") Optional<String> keteranganSakitLaporan,
    		@RequestParam("kondisi_sakit_batuk_laporan") Optional<String> kondisiSakitBatuk,
    		@RequestParam("kondisi_sakit_demam_laporan") Optional<String> kondisiSakitDemam,
    		@RequestParam("kondisi_sakit_pilek_laporan") Optional<String> kondisiSakitPilek,
    		@RequestParam("kondisi_sakit_tenggorokan_laporan") Optional<String> kondisiSakitTenggorokan,
    		@RequestParam("kondisi_sakit_sesak_laporan") Optional<String> kondisiSakitSesak,
    		@RequestParam("lama_gejala_sakit_laporan") Optional<Integer> lamaGejalaSakit
    		) throws Exception {
    		
    		Map<String, Object> loginUserMap = getLoginUserMap(request);
    		ResponseWrapper resp = new ResponseWrapper();
    		Kesehatan data = new Kesehatan(Utils.getLongNumberID());
    		
    		boolean update = false;
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = kesehatanMapper.getEntity(id.get());
    			
    			update = true;
    		}else {
    			Date selectedDate = new Date();
    			if(tanggalLaporan.isPresent()) {
    				selectedDate = tanggalLaporan.get();
    			}
    			QueryParameter param = new QueryParameter();
    			param.setClause(param.getClause() + " AND " + Kesehatan.TANGGAL + "='"+new SimpleDateFormat("yyyy-MM-dd").format(selectedDate)+"'");
    			param.setClause(param.getClause() + " AND " + Kesehatan.USER_ID + "='"+loginUserMap.get("user_id").toString()+"'");
    			List<Kesehatan> lstKes = kesehatanMapper.getList(param);
    			if(lstKes.size()>0) {
    				update = true;
    				
    				data= lstKes.get(0);
    			}else {
    				data.setTanggal(selectedDate);
    			}
    			
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		if(tanggalLaporan.isPresent()) data.setTanggal(tanggalLaporan.get());
    		if(statusSehatLaporan.isPresent()) data.setStatusSehat(statusSehatLaporan.get());
    		if(keteranganSakitLaporan.isPresent()) data.setKeteranganSakit(keteranganSakitLaporan.get());
    		if(lamaGejalaSakit.isPresent()) data.setLamaGejalaSakit(lamaGejalaSakit.get());
    		
    		if(kondisiSakitBatuk.isPresent()) {
    			data.setKondisiSakitBatuk(kondisiSakitBatuk.get());
    		}else {
    			 data.setKondisiSakitBatuk("NO");
    		}
    		if(kondisiSakitDemam.isPresent()) {
    			data.setKondisiSakitDemam(kondisiSakitDemam.get());
    		}else {
    			data.setKondisiSakitDemam("NO");
    		}
    		if(kondisiSakitPilek.isPresent()) {
    			data.setKondisiSakitPilek(kondisiSakitPilek.get());
    		}else {
    			data.setKondisiSakitPilek("NO");
    		}
    		if(kondisiSakitTenggorokan.isPresent()) {
    			data.setKondisiSakitTenggorokan(kondisiSakitTenggorokan.get());
    		}else {
    			data.setKondisiSakitTenggorokan("NO");
    		}
    		if(kondisiSakitSesak.isPresent()) {
    			data.setKondisiSakitSesak(kondisiSakitSesak.get());
    		}else {
    			data.setKondisiSakitSesak("NO");
    		}
    		
    		boolean pass = true;
    		if(data.getStatusSehat()==null) {
    			pass = false;
    		}else if(data.getStatusSehat().equals("")) {
    				pass = false;
    		}else {
    			if(data.getStatusSehat().equals("NO")) {
    				
    				
    	    			if(data.getKondisiSakitBatuk()==null && data.getKondisiSakitDemam()==null && data.getKondisiSakitPilek()==null && data.getKondisiSakitTenggorokan()==null && data.getKondisiSakitSesak()==null) {
    	    				pass = false;
    	    				System.out.println("1");
    	    			}else {
    	    				if(data.getKondisiSakitBatuk().equals("") && data.getKondisiSakitDemam().equals("") && data.getKondisiSakitPilek().equals("") && data.getKondisiSakitTenggorokan().equals("") && data.getKondisiSakitSesak().equals("")) {
    	    					pass = false;
    	    					System.out.println("2");
    	    				}
    	    				if(data.getKondisiSakitBatuk().equals("NO") && data.getKondisiSakitDemam().equals("NO") && data.getKondisiSakitPilek().equals("NO") && data.getKondisiSakitTenggorokan().equals("NO") && data.getKondisiSakitSesak().equals("NO")) {
    	    					pass = false;
    	    					System.out.println("3");
    	    				}
    	    			}
        		}else {
        			data.setKondisiSakit(null);
    				data.setKondisiSakitDemam("NO");
    				data.setKondisiSakitBatuk("NO");
    				data.setKondisiSakitPilek("NO");
    				data.setKondisiSakitTenggorokan("NO");
    				data.setKondisiSakitSesak("NO");
    				data.setKeteranganSakit(null);
    				data.setLamaGejalaSakit(null);
        		}
    		}
    		
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		data.setUserId(loginUserMap.get("user_id").toString());
    		if(update) {
    			data.setModifiedTime(new Date());
    			data.setModifiedUser(loginUserMap.get("user_id").toString());
    			kesehatanMapper.update(data);
        		kesehatanMapper.updateTUSI(data);
    		}else {
    			data.setAddedTime(new Date());
    			data.setAddedUser(loginUserMap.get("user_id").toString());
    			kesehatanMapper.insert(data);	
    		}

    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }


    @RequestMapping(value="/{id}/delete", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> delete(
			@PathVariable String id
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Kesehatan data = kesehatanMapper.getEntity(id);
		resp.setData(data);
		kesehatanMapper.delete(data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
    @RequestMapping(value = "/list/detil", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapperList getListDetil(
    		HttpServletRequest request,
			@RequestParam("id_unit_kerja") Optional<Integer> id_unit_kerja, 
			@RequestParam("periode_awal") Optional<String> periode_awal, 
			@RequestParam("periode_akhir") Optional<String> periode_akhir,
			@RequestParam("status_kesehatan") Optional<String> status_kesehatan, 
			@RequestParam("nip_pegawai") Optional<String> nip_pegawai,
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("page") Optional<Integer> page
			) throws Exception{
		ResponseWrapperList resp = new ResponseWrapperList();
		QueryParameter param = new QueryParameter();
		
		if(id_unit_kerja.isPresent() && !id_unit_kerja.get().equals(null)) param.setClause(param.getClause() + " AND "+DetilKesehatan.ID_UNIT_KERJA+" = '" + id_unit_kerja.get() + "'");
		if((periode_awal.isPresent() && !periode_awal.get().equals("")) && (periode_akhir.isPresent() && !periode_akhir.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilKesehatan.TANGGAL+" BETWEEN '" + periode_awal.get() + "' AND '" + periode_akhir.get() + "'");
		else if((periode_awal.isPresent() && !periode_awal.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilKesehatan.TANGGAL+" >= '" + periode_awal.get() + "'");
		else if((periode_akhir.isPresent() && !periode_akhir.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilKesehatan.TANGGAL+" <= '" + periode_akhir.get() + "'");

		
		if(status_kesehatan.isPresent() && !status_kesehatan.get().equals("ALL")) param.setClause(param.getClause() + " AND "+DetilKesehatan.STATUS_SEHAT+" = '" + status_kesehatan.get() + "'");
		if((nip_pegawai.isPresent() && !nip_pegawai.get().equals(""))) param.setClause(param.getClause() + " AND "+DetilKesehatan.NIP_PEGAWAI+" LIKE '%" + nip_pegawai.get() + "%'");
		
		
		resp.setCount(kesehatanMapper.getCountDetil(param)); 
		
		param.setOrder(DetilKesehatan.ID_UNIT_KERJA + "," + DetilKesehatan.NAMA_PEGAWAI + ", " + DetilKesehatan.TANGGAL);
		
		if(limit.isPresent()) { param.setLimit(limit.get()); if(limit.get() > 2000) param.setLimit(2000); }
		else { param.setLimit(2000); }
    	
    	int pPage = 1;
    	if(page.isPresent()) { pPage = page.get(); int offset = (pPage-1)*param.getLimit(); param.setOffset(offset); }
    	
		List<DetilKesehatan> data = kesehatanMapper.getListDetil(param);
		resp.setData(data);
		resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){ qryString += "&limit="+limit.get(); }
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
		return resp;
	}
    
}
