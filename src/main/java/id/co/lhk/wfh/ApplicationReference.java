package id.co.lhk.wfh;

public enum ApplicationReference {
	WFH("WFH")
    ;

    private final String text;

    /**
     * @param text
     */
    private ApplicationReference(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
