package id.co.lhk.wfh.repo;

import java.util.Map;

public interface MessagingService {

	public void send(String destination, Map<String,Object> payload)  throws Exception;

}
