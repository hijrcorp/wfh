package id.co.lhk.wfh.repo;

import java.io.File;

public interface SocialMediaService {

	public void post(String title, String content, File photo);
}
