package id.co.lhk.wfh.helper;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class Utils {

	public static String capitailizeFirstWord(String str) {
    	//str = str.toLowerCase();
        StringBuffer s = new StringBuffer();
        char ch = ' '; 
        for (int i = 0; i < str.length(); i++) { 
            if (ch == ' ' && str.charAt(i) != ' ') 
                s.append(Character.toUpperCase(str.charAt(i))); 
            else
                s.append(str.charAt(i)); 
            ch = str.charAt(i); 
        }
        return s.toString().trim(); 
    } 
	
	/* ----- BEGIN ENCRYPT PDF & EXCEL ----- */

    // PDF
    // ini untuk encryptPDF
    public static void encryptPDF(File src, String user) throws IOException, DocumentException {
        encryptPDF(src.getAbsolutePath(), user, user);
    }
    public static void encryptPDF(File src, String user, String owner) throws IOException, DocumentException {
        encryptPDF(src.getAbsolutePath(), user, owner);
    }
    
    public static void encryptPDF(File src, File dest, String user) throws IOException, DocumentException {
        encryptPDF(src.getAbsolutePath(), dest.getAbsolutePath(), user, user);
    }
    
    public static void encryptPDF(File src, File dest, String user, String owner) throws IOException, DocumentException {
        encryptPDF(src.getAbsolutePath(), dest.getAbsolutePath(), user, owner);
    }
    
    public static void encryptPDF(String dest, String user) throws IOException, DocumentException {
    	encryptPDF(dest, user, user);
    }
    
    public static File encryptPDF(InputStream inputStream, String filename, String user) throws IOException, DocumentException {
    	return encryptPDF(inputStream, filename, user, user);
    }
    
    public static File encryptPDF(InputStream inputStream, String filename, String user, String owner) throws IOException, DocumentException {
    	File file = new File(System.getProperty("java.io.tmpdir"), filename);
        FileUtils.copyInputStreamToFile(inputStream, file);
        return encryptPDF(file.getAbsolutePath(), user, owner);
    }
    
    public static File encryptPDF(String dest, String user, String owner) throws IOException, DocumentException {
    	File fileSrc = File.createTempFile(dest, ".tmp");
        FileUtils.copyInputStreamToFile(new FileInputStream(dest), fileSrc);
        return encryptPDF(fileSrc.getAbsolutePath(), new File(dest).getAbsolutePath(), user, owner);
    }
    
    public static File encryptPDF(String src, String dest, String user, String owner) throws IOException, DocumentException {
    	byte[] USER = user.getBytes();
    	byte[] OWNER = owner.getBytes();
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        stamper.setEncryption(USER, OWNER, PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128 | PdfWriter.DO_NOT_ENCRYPT_METADATA);
        stamper.close();
        reader.close();
        FileUtils.deleteQuietly(FileUtils.getFile(src));
        return new File(dest);
    }
    
    // EXCEL
    // bisa mengencripsi dengan apache poi versi ~ 3.9
	public static void encryptHSSF(HSSFWorkbook workbook, String password) {
        Biff8EncryptionKey.setCurrentUserPassword(password);
        workbook.writeProtectWorkbook(Biff8EncryptionKey.getCurrentUserPassword(), "");
	}

    // untuk menggunakan encryptHSSF dan encryptXSFF harus menggunakan apache poi versi >= 4.x
    public static void encryptHSSF(File file, String password) throws IOException, EncryptedDocumentException, InvalidFormatException {
    	encryptHSSF(file.getAbsolutePath(), password);
    }

    public static void encryptHSSF(String path, String password) throws IOException, EncryptedDocumentException, InvalidFormatException {
        try (HSSFWorkbook hwb = (HSSFWorkbook)WorkbookFactory.create(new File(path))) {
            Biff8EncryptionKey.setCurrentUserPassword(password);
            hwb.write();
        }
    }
    
	public static File encryptXSSF(InputStream inputStream, String filename, String password) throws IOException, GeneralSecurityException {
		//String filenameArray[]  = filename.split("\\.");
		//File file = File.createTempFile(filenameArray[0], filenameArray[1]);
		File file = new File(System.getProperty("java.io.tmpdir"), filename);
        FileUtils.copyInputStreamToFile(inputStream, file);
		return encryptXSSF(file.getAbsolutePath(), password);
	}
    
	public static File encryptXSSF(File file, String password) throws IOException, GeneralSecurityException {
		return encryptXSSF(file.getAbsolutePath(), password);
	}
	
	public static File encryptXSSF(String path, String password) throws IOException, GeneralSecurityException {
        try (POIFSFileSystem fs = new POIFSFileSystem()) {
            EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile);
            Encryptor enc = info.getEncryptor();
            enc.confirmPassword(password);

            try (OutputStream os = enc.getDataStream(fs)) {
                try (InputStream is = new FileInputStream(path)) {
                    IOUtils.copy(is, os);
                }
            }

            try (FileOutputStream fos = new FileOutputStream(path)) {
                fs.writeFilesystem(fos);
            }
        }
        return new File(path);
	}
	
	/* ----- END ENCRYPT PDF & EXCEL ----- */
	
	/* ----- BEGIN GENERATE PASSWORD ----- */
	
	// function to generate a random string of length n 
    private static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789";
                                    //+ "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 
	
	public static String generatePassword() {
		return getAlphaNumericString(10);
	}
	
	public static void main(String []args) {
		System.out.println("math: " + generatePassword());
	}
	
	/* ----- END GENERATE PASSWORD ----- */
	
	/* ----- BEGIN EXCEL UTILS ----- */
	
	public static InputStream workbookToInputStream(Workbook workbook) throws IOException {
		File file = File.createTempFile("file", ".tmp");
        FileOutputStream out = new FileOutputStream(file);
        workbook.write(out);
        out.close();
        return new FileInputStream(file);
	}
	
	/* ----- END EXCEL UTILS ----- */

	public static int randomPlus() {
		int min = 100;
		int max = 999;

		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	public static String getLongNumberID() {
		return (new Date().getTime() + "" + randomPlus());
	}
	
	public static String formatSqlDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	
	public static String formatIndonesianReportDate(Date date) {
		return formatIndonesianReportDate(date, "-");
	}
	
	public static String formatIndonesianReportDate(Date date, String separator) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd"+separator+"MM"+separator+"yyyy");
		return sdf.format(date);
	}
	

	
	public static Dimension getImageDimension(File imgFile) throws IOException {
	  int pos = imgFile.getName().lastIndexOf(".");
	  if (pos == -1)
	    throw new IOException("No extension for file: " + imgFile.getAbsolutePath());
	  String suffix = imgFile.getName().substring(pos + 1);
	  Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
	  while(iter.hasNext()) {
	    ImageReader reader = iter.next();
	    try {
	      ImageInputStream stream = new FileImageInputStream(imgFile);
	      reader.setInput(stream);
	      int width = reader.getWidth(reader.getMinIndex());
	      int height = reader.getHeight(reader.getMinIndex());
	      return new Dimension(width, height);
	    } catch (IOException e) {
	      System.out.println("Error reading: " + imgFile.getAbsolutePath());
	    } finally {
	      reader.dispose();
	    }
	  }

	  throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
	}
}
