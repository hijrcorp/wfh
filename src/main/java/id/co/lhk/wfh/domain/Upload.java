package id.co.lhk.wfh.domain;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Upload {

	/********************************** - Begin Generate - ************************************/

	public static final String ID_UPLOAD = "id_upload";
	public static final String NAMA_UPLOAD = "nama_upload";
	public static final String REFERENSI_ID_UPLOAD = "referensi_id_upload";
	public static final String JENIS_REFERENSI_UPLOAD = "jenis_referensi_upload";
	public static final String NAMA_FILE_UPLOAD = "nama_file_upload";
	public static final String KETERANGAN_FILE_UPLOAD = "keterangan_file_upload";
	public static final String TANGGAL_UPLOAD = "tanggal_upload";
	public static final String USER_ADDED_UPLOAD = "user_added_upload";
	public static final String DATE_ADDED_UPLOAD = "date_added_upload";
	
	private String idUpload;
	private String namaUpload;
	private String referensiIdUpload;
	private String jenisReferensiUpload;
	private String namaFileUpload;
	private String keteranganFileUpload;
	private Date tanggalUpload;
	private String userAddedUpload;
	private Date dateAddedUpload;
	
	
	public Upload() {
	
	}
	public Upload(String id) {
		this.idUpload = id;
	}
	
	@JsonProperty(ID_UPLOAD)
	public String getIdUpload() {
		return idUpload;
	}
	
	public void setIdUpload(String idUpload) {
		this.idUpload = idUpload;
	}
	
	@JsonProperty(NAMA_UPLOAD)
	public String getNamaUpload() {
		return namaUpload;
	}
	
	public void setNamaUpload(String namaUpload) {
		this.namaUpload = namaUpload;
	}
	
	@JsonProperty(REFERENSI_ID_UPLOAD)
	public String getReferensiIdUpload() {
		return referensiIdUpload;
	}
	
	public void setReferensiIdUpload(String referensiIdUpload) {
		this.referensiIdUpload = referensiIdUpload;
	}
	
	@JsonProperty(JENIS_REFERENSI_UPLOAD)
	public String getJenisReferensiUpload() {
		return jenisReferensiUpload;
	}
	
	public void setJenisReferensiUpload(String jenisReferensiUpload) {
		this.jenisReferensiUpload = jenisReferensiUpload;
	}
	
	@JsonProperty(NAMA_FILE_UPLOAD)
	public String getNamaFileUpload() {
		return namaFileUpload;
	}
	
	public void setNamaFileUpload(String namaFileUpload) {
		this.namaFileUpload = namaFileUpload;
	}
	
	@JsonProperty(KETERANGAN_FILE_UPLOAD)
	public String getKeteranganFileUpload() {
		return keteranganFileUpload;
	}
	
	public void setKeteranganFileUpload(String keteranganFileUpload) {
		this.keteranganFileUpload = keteranganFileUpload;
	}
	
	@JsonProperty(TANGGAL_UPLOAD)
	public Date getTanggalUpload() {
		return tanggalUpload;
	}
	
	public void setTanggalUpload(Date tanggalUpload) {
		this.tanggalUpload = tanggalUpload;
	}
	
	@JsonProperty(USER_ADDED_UPLOAD)
	public String getUserAddedUpload() {
		return userAddedUpload;
	}
	
	public void setUserAddedUpload(String userAddedUpload) {
		this.userAddedUpload = userAddedUpload;
	}
	
	@JsonProperty(DATE_ADDED_UPLOAD)
	public Date getDateAddedUpload() {
		return dateAddedUpload;
	}
	
	public void setDateAddedUpload(Date dateAddedUpload) {
		this.dateAddedUpload = dateAddedUpload;
	}
	
	
	/********************************** - End Generate - ************************************/

}
