package id.co.lhk.wfh.domain;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pegawai {

	/********************************** - Begin Generate - ************************************/

	public static final String ID_PEGAWAI = "id_pegawai";
	public static final String NIP_PEGAWAI = "nip_pegawai";
	public static final String NAMA_PEGAWAI = "nama_pegawai";
	public static final String JABATAN = "jabatan";
	public static final String GOLONGAN = "golongan";
	public static final String EMAIL_PEGAWAI = "email_pegawai";
	public static final String ID_UNIT_KERJA_PEGAWAI = "id_unit_kerja_pegawai";
	public static final String NAMA_UNIT_KERJA_PEGAWAI = "nama_unit_kerja_pegawai";
	
	private String idPegawai;
	private String nipPegawai;
	private String namaPegawai;
	private String jabatan;
	private String golongan;
	private String emailPegawai;
	private String idUnitKerjaPegawai;
	private String namaUnitKerjaPegawai;
	
	private UnitKerja unitKerja;
	
	public Pegawai() {
	
	}
	public Pegawai(String id) {
		this.idPegawai = id;
	}
	
	@JsonProperty(ID_PEGAWAI)
	public String getIdPegawai() {
		return idPegawai;
	}
	
	public void setIdPegawai(String idPegawai) {
		this.idPegawai = idPegawai;
	}
	
	@JsonProperty(NIP_PEGAWAI)
	public String getNipPegawai() {
		return nipPegawai;
	}
	
	public void setNipPegawai(String nipPegawai) {
		this.nipPegawai = nipPegawai;
	}
	
	@JsonProperty(NAMA_PEGAWAI)
	public String getNamaPegawai() {
		return namaPegawai;
	}
	
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	
	@JsonProperty(JABATAN)
	public String getJabatan() {
		return jabatan;
	}
	
	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	
	@JsonProperty(GOLONGAN)
	public String getGolongan() {
		return golongan;
	}
	
	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}
	
	@JsonProperty(EMAIL_PEGAWAI)
	public String getEmailPegawai() {
		return emailPegawai;
	}
	
	public void setEmailPegawai(String emailPegawai) {
		this.emailPegawai = emailPegawai;
	}
	
	@JsonProperty(ID_UNIT_KERJA_PEGAWAI)
	public String getIdUnitKerjaPegawai() {
		return idUnitKerjaPegawai;
	}
	
	public void setIdUnitKerjaPegawai(String idUnitKerjaPegawai) {
		this.idUnitKerjaPegawai = idUnitKerjaPegawai;
	}
	
	@JsonProperty(NAMA_UNIT_KERJA_PEGAWAI)
	public String getNamaUnitKerjaPegawai() {
		return namaUnitKerjaPegawai;
	}
	
	public void setNamaUnitKerjaPegawai(String namaUnitKerjaPegawai) {
		this.namaUnitKerjaPegawai = namaUnitKerjaPegawai;
	}
	
	
	public void setUnitKerja(UnitKerja unitKerja) {
		this.unitKerja = unitKerja;
	}
	
	public UnitKerja getUnitKerja() {
		return unitKerja;
	}
	
	/********************************** - End Generate - ************************************/

}
