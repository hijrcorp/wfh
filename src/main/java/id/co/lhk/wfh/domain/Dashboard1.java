package id.co.lhk.wfh.domain;

public class Dashboard1 {
	
	private Integer idUnitKerja;
	private String namaUnitKerja;
	private Integer jumlahPegawai;
	private Integer jumlahMelaporPekerjaan;
	private Integer jumlahTidakMelaporPekerjaan;
	private Double jumlahPersenMelaporPekerjaan;
	private Integer jumlahMelaporKesehatan;
	private Integer jumlahTidakMelaporKesehatan;
	private Double jumlahPersenMelaporKesehatan;
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}
	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}
	public Integer getJumlahPegawai() {
		return jumlahPegawai;
	}
	public void setJumlahPegawai(Integer jumlahPegawai) {
		this.jumlahPegawai = jumlahPegawai;
	}
	public Integer getJumlahMelaporPekerjaan() {
		return jumlahMelaporPekerjaan;
	}
	public void setJumlahMelaporPekerjaan(Integer jumlahMelaporPekerjaan) {
		this.jumlahMelaporPekerjaan = jumlahMelaporPekerjaan;
	}
	public Integer getJumlahTidakMelaporPekerjaan() {
		return jumlahTidakMelaporPekerjaan;
	}
	public void setJumlahTidakMelaporPekerjaan(Integer jumlahTidakMelaporPekerjaan) {
		this.jumlahTidakMelaporPekerjaan = jumlahTidakMelaporPekerjaan;
	}
	public Double getJumlahPersenMelaporPekerjaan() {
		return jumlahPersenMelaporPekerjaan;
	}
	public void setJumlahPersenMelaporPekerjaan(Double jumlahPersenMelaporPekerjaan) {
		this.jumlahPersenMelaporPekerjaan = jumlahPersenMelaporPekerjaan;
	}
	public Integer getJumlahMelaporKesehatan() {
		return jumlahMelaporKesehatan;
	}
	public void setJumlahMelaporKesehatan(Integer jumlahMelaporKesehatan) {
		this.jumlahMelaporKesehatan = jumlahMelaporKesehatan;
	}
	public Integer getJumlahTidakMelaporKesehatan() {
		return jumlahTidakMelaporKesehatan;
	}
	public void setJumlahTidakMelaporKesehatan(Integer jumlahTidakMelaporKesehatan) {
		this.jumlahTidakMelaporKesehatan = jumlahTidakMelaporKesehatan;
	}
	public Double getJumlahPersenMelaporKesehatan() {
		return jumlahPersenMelaporKesehatan;
	}
	public void setJumlahPersenMelaporKesehatan(Double jumlahPersenMelaporKesehatan) {
		this.jumlahPersenMelaporKesehatan = jumlahPersenMelaporKesehatan;
	}
	
	
	
}
