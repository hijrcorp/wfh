package id.co.lhk.wfh.domain;

import java.util.Date;

public class Laporan4 {
	
	private Integer idUnitKerja;
	private String namaUnitKerja;
	private Date tanggalLaporan;
	private String nipPegawai;
	private String namaPegawai;
	private String emailPegawai;
	private String statusPelaporan;
	private String statusTusiLaporan;
	private String statusTambahanLaporan;
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}
	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}
	public Date getTanggalLaporan() {
		return tanggalLaporan;
	}
	public void setTanggalLaporan(Date tanggalLaporan) {
		this.tanggalLaporan = tanggalLaporan;
	}
	public String getNipPegawai() {
		return nipPegawai;
	}
	public void setNipPegawai(String nipPegawai) {
		this.nipPegawai = nipPegawai;
	}
	public String getNamaPegawai() {
		return namaPegawai;
	}
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	public String getStatusPelaporan() {
		return statusPelaporan;
	}
	public void setStatusPelaporan(String statusPelaporan) {
		this.statusPelaporan = statusPelaporan;
	}
	public String getStatusTusiLaporan() {
		return statusTusiLaporan;
	}
	public void setStatusTusiLaporan(String statusTusiLaporan) {
		this.statusTusiLaporan = statusTusiLaporan;
	}
	public String getStatusTambahanLaporan() {
		return statusTambahanLaporan;
	}
	public void setStatusTambahanLaporan(String statusTambahanLaporan) {
		this.statusTambahanLaporan = statusTambahanLaporan;
	}
	public String getEmailPegawai() {
		return emailPegawai;
	}
	public void setEmailPegawai(String emailPegawai) {
		this.emailPegawai = emailPegawai;
	}
	
	
}
