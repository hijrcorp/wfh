package id.co.lhk.wfh.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class Kesehatan {
	
	public static final String ID = "id_laporan";
	public static final String TANGGAL = "tanggal_laporan";
	public static final String USER_ID = "user_id_laporan";
	public static final String JUMLAH_TUSI = "jumlah_tusi_laporan";
	public static final String JUMLAH_TAMBAHAN = "jumlah_tambahan_laporan";
	public static final String STATUS_SEHAT = "status_sehat_laporan";
	public static final String KONDISI_SAKIT = "kondisi_sakit_laporan";
	public static final String KETERANGAN_SAKIT = "keterangan_sakit_laporan";
	public static final String ADDED_TIME = "added_time_laporan";
	public static final String ADDED_USER = "added_user_laporan";
	public static final String KONDISI_SAKIT_BATUK = "kondisi_sakit_batuk_laporan";
	public static final String KONDISI_SAKIT_DEMAM = "kondisi_sakit_demam_laporan";
	public static final String KONDISI_SAKIT_PILEK = "kondisi_sakit_pilek_laporan";
	public static final String KONDISI_SAKIT_TENGGOROKAN = "kondisi_sakit_tenggorokan_laporan";
	public static final String KONDISI_SAKIT_SESAK = "kondisi_sakit_sesak_laporan";
	public static final String MODIFIED_TIME = "modified_time_laporan";
	public static final String MODIFIED_USER = "modified_user_laporan";
	public static final String LAMA_GEJALA_SAKIT = "lama_gejala_sakit_laporan";

	private String id;
	private Date tanggal;
	private String userId;
	private Integer jumlahTusi;
	private Integer jumlahTambahan;
	private String statusSehat;
	private String kondisiSakit;
	private String keteranganSakit;
	private Date addedTime;
	private String addedUser;
	private String kondisiSakitBatuk;
	private String kondisiSakitDemam;
	private String kondisiSakitPilek;
	private String kondisiSakitTenggorokan;
	private String kondisiSakitSesak;
	private Date modifiedTime;
	private String modifiedUser;
	private Integer lamaGejalaSakit;

	public Kesehatan() {

	}

	public Kesehatan(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("tanggal")
	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	@JsonProperty("user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonProperty("jumlah_tusi")
	public Integer getJumlahTusi() {
		return jumlahTusi;
	}

	public void setJumlahTusi(Integer jumlahTusi) {
		this.jumlahTusi = jumlahTusi;
	}

	@JsonProperty("jumlah_tambahan")
	public Integer getJumlahTambahan() {
		return jumlahTambahan;
	}

	public void setJumlahTambahan(Integer jumlahTambahan) {
		this.jumlahTambahan = jumlahTambahan;
	}

	@JsonProperty("status_sehat")
	public String getStatusSehat() {
		return statusSehat;
	}

	public void setStatusSehat(String statusSehat) {
		this.statusSehat = statusSehat;
	}

	@JsonProperty("kondisi_sakit")
	public String getKondisiSakit() {
		return kondisiSakit;
	}

	public void setKondisiSakit(String kondisiSakit) {
		this.kondisiSakit = kondisiSakit;
	}

	@JsonProperty("keterangan_sakit")
	public String getKeteranganSakit() {
		return keteranganSakit;
	}

	public void setKeteranganSakit(String keteranganSakit) {
		this.keteranganSakit = keteranganSakit;
	}

	@JsonProperty("added_time")
	public Date getAddedTime() {
		return addedTime;
	}

	public void setAddedTime(Date addedTime) {
		this.addedTime = addedTime;
	}

	@JsonProperty("added_user")
	public String getAddedUser() {
		return addedUser;
	}

	public void setAddedUser(String addedUser) {
		this.addedUser = addedUser;
	}

	@JsonProperty("kondisi_sakit_batuk")
	public String getKondisiSakitBatuk() {
		return kondisiSakitBatuk;
	}

	public void setKondisiSakitBatuk(String kondisiSakitBatuk) {
		this.kondisiSakitBatuk = kondisiSakitBatuk;
	}

	@JsonProperty("kondisi_sakit_demam")
	public String getKondisiSakitDemam() {
		return kondisiSakitDemam;
	}

	public void setKondisiSakitDemam(String kondisiSakitDemam) {
		this.kondisiSakitDemam = kondisiSakitDemam;
	}

	@JsonProperty("kondisi_sakit_pilek")
	public String getKondisiSakitPilek() {
		return kondisiSakitPilek;
	}

	public void setKondisiSakitPilek(String kondisiSakitPilek) {
		this.kondisiSakitPilek = kondisiSakitPilek;
	}

	@JsonProperty("kondisi_sakit_tenggorokan")
	public String getKondisiSakitTenggorokan() {
		return kondisiSakitTenggorokan;
	}

	public void setKondisiSakitTenggorokan(String kondisiSakitTenggorokan) {
		this.kondisiSakitTenggorokan = kondisiSakitTenggorokan;
	}

	@JsonProperty("kondisi_sakit_sesak")
	public String getKondisiSakitSesak() {
		return kondisiSakitSesak;
	}

	public void setKondisiSakitSesak(String kondisiSakitSesak) {
		this.kondisiSakitSesak = kondisiSakitSesak;
	}

	@JsonProperty("modified_time")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@JsonProperty("modified_user")
	public String getModifiedUser() {
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser) {
		this.modifiedUser = modifiedUser;
	}

	@JsonProperty("lama_gejala_sakit")
	public Integer getLamaGejalaSakit() {
		return lamaGejalaSakit;
	}

	public void setLamaGejalaSakit(Integer lamaGejalaSakit) {
		this.lamaGejalaSakit = lamaGejalaSakit;
	}


	private List<Pekerjaan> detilPekerjaan = new ArrayList<Pekerjaan>();

	public List<Pekerjaan> getDetilPekerjaan() {
		return detilPekerjaan;
	}

	public void setDetilPekerjaan(List<Pekerjaan> detilPekerjaan) {
		this.detilPekerjaan = detilPekerjaan;
	}
	

	// ========================================
	
	public boolean isAllowed() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar yest = Calendar.getInstance();
		yest.add(Calendar.DAY_OF_YEAR, -1);
		boolean allowed = false;
		
		if(sdf.format(new Date()).equals(sdf.format(getTanggal())) || sdf.format(yest.getTime()).equals(sdf.format(getTanggal()))) {
			allowed = true;
		}
		
		return allowed;
	}
	
}
