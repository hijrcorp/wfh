package id.co.lhk.wfh.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OauthUserOrganization extends OauthUser {
	
	public static final String POSITION_ID="position_id";
	public static final String POSITION_NAME="position_name";
	public static final String ORGANIZATION_ID="organization_id";
	public static final String ORGANIZATION_CODE="organization_code";
	public static final String ORGANIZATION_NAME="organization_name";
	public static final String ALAMAT_RUMAH="alamat_rumah_user_detil";
	
	private String positionId;
	private String positionName;
	private String organizationId;
	private String organizationCode;
	private String organizationName;
	private String unitKerjaId;
	private String unitKerjaName;
	private String alamatRumahUserDetil;
	private String riwayatPenyakitUserDetil;
	private String idJabatanUserDetil;
	
	public OauthUserOrganization() {
		
	}
	
	public OauthUserOrganization(String id) {
		super(id);
	}
	
//	@JsonProperty(POSITION_ID)
	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

//	@JsonProperty(POSITION_NAME)
	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

//	@JsonProperty(ORGANIZATION_ID)
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

//	@JsonProperty(ORGANIZATION_NAME)
	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

//	@JsonProperty(ORGANIZATION_CODE)
	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getUnitKerjaId() {
		return unitKerjaId;
	}

	public void setUnitKerjaId(String unitKerjaId) {
		this.unitKerjaId = unitKerjaId;
	}

	public String getUnitKerjaName() {
		return unitKerjaName;
	}

	public void setUnitKerjaName(String unitKerjaName) {
		this.unitKerjaName = unitKerjaName;
	}
	
	//@JsonProperty(ALAMAT_RUMAH)
//	public String getAlamatRumah() {
//		return alamatRumah;
//	}
//
//	public void setAlamatRumah(String alamatRumah) {
//		this.alamatRumah = alamatRumah;
//	}

	public String getAlamatRumahUserDetil() {
		return alamatRumahUserDetil;
	}

	public String getRiwayatPenyakitUserDetil() {
		return riwayatPenyakitUserDetil;
	}

	public void setRiwayatPenyakitUserDetil(String riwayatPenyakitUserDetil) {
		this.riwayatPenyakitUserDetil = riwayatPenyakitUserDetil;
	}

	public String getIdJabatanUserDetil() {
		return idJabatanUserDetil;
	}

	public void setIdJabatanUserDetil(String idJabatanUserDetil) {
		this.idJabatanUserDetil = idJabatanUserDetil;
	}

	public void setAlamatRumahUserDetil(String alamatRumahUserDetil) {
		this.alamatRumahUserDetil = alamatRumahUserDetil;
	}


	
}
