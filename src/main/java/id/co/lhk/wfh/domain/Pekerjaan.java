package id.co.lhk.wfh.domain;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Pekerjaan {
	
	public static final String ID = "id_pekerjaan";
	public static final String LAPORAN_ID = "laporan_id_pekerjaan";
	public static final String STATUS_TUSI = "status_tusi_pekerjaan";
	public static final String NAMA = "nama_pekerjaan";
	public static final String URAIAN = "uraian_pekerjaan";
	public static final String OUTPUT = "output_pekerjaan";

	private String id;
	private String laporanId;
	private String statusTusi;
	private String nama;
	private String uraian;
	private String output;

	public Pekerjaan() {

	}

	public Pekerjaan(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("laporan_id")
	public String getLaporanId() {
		return laporanId;
	}

	public void setLaporanId(String laporanId) {
		this.laporanId = laporanId;
	}

	@JsonProperty("status_tusi")
	public String getStatusTusi() {
		return statusTusi;
	}

	public void setStatusTusi(String statusTusi) {
		this.statusTusi = statusTusi;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("uraian")
	public String getUraian() {
		return uraian;
	}

	public void setUraian(String uraian) {
		this.uraian = uraian;
	}

	@JsonProperty("output")
	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}
	
}
