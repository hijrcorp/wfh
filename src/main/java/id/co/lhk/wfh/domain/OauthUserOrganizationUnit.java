package id.co.lhk.wfh.domain;

public class OauthUserOrganizationUnit extends OauthUserOrganization {
	
	public static final String UNIT_KERJA_ID="unit_kerja_id";
	public static final String UNIT_KERJA_NAME="unit_kerja_name";
	public static final String ID_JABATAN="id_jabatan";
	public static final String NAMA_JABATAN="nama_jabatan";
	
	private String unitKerjaId;
	private String unitKerjaName;
	private String idJabatan;
	private String namaJabatan;
	
	public OauthUserOrganizationUnit() {
		
	}
	
	public OauthUserOrganizationUnit(String id) {
		super(id);
	}

	public String getUnitKerjaId() {
		return unitKerjaId;
	}

	public void setUnitKerjaId(String unitKerjaId) {
		this.unitKerjaId = unitKerjaId;
	}

	public String getUnitKerjaName() {
		return unitKerjaName;
	}

	public void setUnitKerjaName(String unitKerjaName) {
		this.unitKerjaName = unitKerjaName;
	}

	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}
	
	
}
