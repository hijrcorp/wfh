package id.co.lhk.wfh.domain;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnitAuditi {

	/********************************** - Begin Generate - ************************************/

	public static final String ID_UNIT_AUDITI = "id_unit_auditi";
	public static final String NAMA_UNIT_AUDITI = "nama_unit_auditi";
	public static final String ID_PROPINSI_UNIT_AUDITI = "id_propinsi_unit_auditi";
	public static final String ID_ESELON1_UNIT_AUDITI = "id_eselon1_unit_auditi";
	
	private String idUnitAuditi;
	private String namaUnitAuditi;
	private String idPropinsiUnitAuditi;
	private String idEselon1UnitAuditi;
	
	private Propinsi propinsi;
	private Eselon1 eselon1;
	
	public UnitAuditi() {
	
	}
	public UnitAuditi(String id) {
		this.idUnitAuditi = id;
	}
	
	@JsonProperty(ID_UNIT_AUDITI)
	public String getIdUnitAuditi() {
		return idUnitAuditi;
	}
	
	public void setIdUnitAuditi(String idUnitAuditi) {
		this.idUnitAuditi = idUnitAuditi;
	}
	
	@JsonProperty(NAMA_UNIT_AUDITI)
	public String getNamaUnitAuditi() {
		return namaUnitAuditi;
	}
	
	public void setNamaUnitAuditi(String namaUnitAuditi) {
		this.namaUnitAuditi = namaUnitAuditi;
	}
	
	@JsonProperty(ID_PROPINSI_UNIT_AUDITI)
	public String getIdPropinsiUnitAuditi() {
		return idPropinsiUnitAuditi;
	}
	
	public void setIdPropinsiUnitAuditi(String idPropinsiUnitAuditi) {
		this.idPropinsiUnitAuditi = idPropinsiUnitAuditi;
	}
	
	@JsonProperty(ID_ESELON1_UNIT_AUDITI)
	public String getIdEselon1UnitAuditi() {
		return idEselon1UnitAuditi;
	}
	
	public void setIdEselon1UnitAuditi(String idEselon1UnitAuditi) {
		this.idEselon1UnitAuditi = idEselon1UnitAuditi;
	}
	
	
	public void setPropinsi(Propinsi propinsi) {
		this.propinsi = propinsi;
	}
	
	public Propinsi getPropinsi() {
		return propinsi;
	}
	
	public void setEselon1(Eselon1 eselon1) {
		this.eselon1 = eselon1;
	}
	
	public Eselon1 getEselon1() {
		return eselon1;
	}
	
	/********************************** - End Generate - ************************************/

	private String namaPropinsi;
	private String namaEselon1;

	@JsonProperty("nama_propinsi")
	public String getNamaPropinsi() {
		return namaPropinsi;
	}
	public void setNamaPropinsi(String namaPropinsi) {
		this.namaPropinsi = namaPropinsi;
	}
	
	@JsonProperty("nama_eselon1")
	public String getNamaEselon1() {
		return namaEselon1;
	}
	public void setNamaEselon1(String namaEselon1) {
		this.namaEselon1 = namaEselon1;
	}
	
	
}
