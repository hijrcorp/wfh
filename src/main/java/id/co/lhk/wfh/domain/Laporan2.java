package id.co.lhk.wfh.domain;

import java.util.Date;

public class Laporan2 {
	
	private Integer idUnitKerja;
	private String namaUnitKerja;
	private Date tanggalLaporan;
	private Integer jumlahPegawai;
	private Integer jumlahMelaporKesehatan;
	private Integer jumlahTidakMelaporKesehatan;
	private Double jumlahPersenMelaporKesehatan;
	private Integer jumlahSehat;
	private Double jumlahPersenSehat;
	private Integer jumlahSakit;
	private Double jumlahPersenSakit;
	private Integer jumlahDemam;
	private Double jumlahPersenDemam;
	private Integer jumlahBatuk;
	private Double jumlahPersenBatuk;
	private Integer jumlahPilek;
	private Double jumlahPersenPilek;
	private Integer jumlahTenggorokan;
	private Double jumlahPersenTenggorokan;
	private Integer jumlahSesak;
	private Double jumlahPersenSesak;
	
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}
	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}
	public Integer getJumlahPegawai() {
		return jumlahPegawai;
	}
	public void setJumlahPegawai(Integer jumlahPegawai) {
		this.jumlahPegawai = jumlahPegawai;
	}
	public Integer getJumlahMelaporKesehatan() {
		return jumlahMelaporKesehatan;
	}
	public void setJumlahMelaporKesehatan(Integer jumlahMelaporKesehatan) {
		this.jumlahMelaporKesehatan = jumlahMelaporKesehatan;
	}
	public Double getJumlahPersenMelaporKesehatan() {
		return jumlahPersenMelaporKesehatan;
	}
	public void setJumlahPersenMelaporKesehatan(Double jumlahPersenMelaporKesehatan) {
		this.jumlahPersenMelaporKesehatan = jumlahPersenMelaporKesehatan;
	}
	public Integer getJumlahSehat() {
		return jumlahSehat;
	}
	public void setJumlahSehat(Integer jumlahSehat) {
		this.jumlahSehat = jumlahSehat;
	}
	public Double getJumlahPersenSehat() {
		return jumlahPersenSehat;
	}
	public void setJumlahPersenSehat(Double jumlahPersenSehat) {
		this.jumlahPersenSehat = jumlahPersenSehat;
	}
	public Integer getJumlahSakit() {
		return jumlahSakit;
	}
	public void setJumlahSakit(Integer jumlahSakit) {
		this.jumlahSakit = jumlahSakit;
	}
	public Double getJumlahPersenSakit() {
		return jumlahPersenSakit;
	}
	public void setJumlahPersenSakit(Double jumlahPersenSakit) {
		this.jumlahPersenSakit = jumlahPersenSakit;
	}
	public Integer getJumlahDemam() {
		return jumlahDemam;
	}
	public void setJumlahDemam(Integer jumlahDemam) {
		this.jumlahDemam = jumlahDemam;
	}
	public Integer getJumlahBatuk() {
		return jumlahBatuk;
	}
	public void setJumlahBatuk(Integer jumlahBatuk) {
		this.jumlahBatuk = jumlahBatuk;
	}
	public Integer getJumlahPilek() {
		return jumlahPilek;
	}
	public void setJumlahPilek(Integer jumlahPilek) {
		this.jumlahPilek = jumlahPilek;
	}
	public Integer getJumlahTenggorokan() {
		return jumlahTenggorokan;
	}
	public void setJumlahTenggorokan(Integer jumlahTenggorokan) {
		this.jumlahTenggorokan = jumlahTenggorokan;
	}
	public Integer getJumlahSesak() {
		return jumlahSesak;
	}
	public void setJumlahSesak(Integer jumlahSesak) {
		this.jumlahSesak = jumlahSesak;
	}
	public Integer getJumlahTidakMelaporKesehatan() {
		return jumlahTidakMelaporKesehatan;
	}
	public void setJumlahTidakMelaporKesehatan(Integer jumlahTidakMelaporKesehatan) {
		this.jumlahTidakMelaporKesehatan = jumlahTidakMelaporKesehatan;
	}
	public Date getTanggalLaporan() {
		return tanggalLaporan;
	}
	public void setTanggalLaporan(Date tanggalLaporan) {
		this.tanggalLaporan = tanggalLaporan;
	}
	public Double getJumlahPersenDemam() {
		return jumlahPersenDemam;
	}
	public void setJumlahPersenDemam(Double jumlahPersenDemam) {
		this.jumlahPersenDemam = jumlahPersenDemam;
	}
	public Double getJumlahPersenBatuk() {
		return jumlahPersenBatuk;
	}
	public void setJumlahPersenBatuk(Double jumlahPersenBatuk) {
		this.jumlahPersenBatuk = jumlahPersenBatuk;
	}
	public Double getJumlahPersenPilek() {
		return jumlahPersenPilek;
	}
	public void setJumlahPersenPilek(Double jumlahPersenPilek) {
		this.jumlahPersenPilek = jumlahPersenPilek;
	}
	public Double getJumlahPersenTenggorokan() {
		return jumlahPersenTenggorokan;
	}
	public void setJumlahPersenTenggorokan(Double jumlahPersenTenggorokan) {
		this.jumlahPersenTenggorokan = jumlahPersenTenggorokan;
	}
	public Double getJumlahPersenSesak() {
		return jumlahPersenSesak;
	}
	public void setJumlahPersenSesak(Double jumlahPersenSesak) {
		this.jumlahPersenSesak = jumlahPersenSesak;
	}
	
}
