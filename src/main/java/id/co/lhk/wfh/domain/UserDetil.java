package id.co.lhk.wfh.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDetil {
	
	public static final String ID = "id_user_detil";
	public static final String USER_ID = "user_id_user_detil";
	public static final String ALAMAT_RUMAH = "alamat_rumah_user_detil";
	public static final String RIWAYAT_PENYAKIT = "riwayat_penyakit_user_detil";
	public static final String ID_JABATAN = "id_jabatan_user_detil";
	public static final String KODE_JABATAN_SIMPEG = "kode_jabatan_simpeg_user_detil";

	private String id;
	private String userId;
	private String alamatRumah;
	private String riwayatPenyakit;
	private String idJabatan;
	private String kodeJabatanSimpeg;

	public UserDetil() {

	}

	public UserDetil(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonProperty("alamat_rumah")
	public String getAlamatRumah() {
		return alamatRumah;
	}

	public void setAlamatRumah(String alamatRumah) {
		this.alamatRumah = alamatRumah;
	}

	@JsonProperty("riwayat_penyakit")
	public String getRiwayatPenyakit() {
		return riwayatPenyakit;
	}

	public void setRiwayatPenyakit(String riwayatPenyakit) {
		this.riwayatPenyakit = riwayatPenyakit;
	}

	@JsonProperty("id_jabatan")
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	@JsonProperty("kode_jabatan_simpeg")
	public String getKodeJabatanSimpeg() {
		return kodeJabatanSimpeg;
	}

	public void setKodeJabatanSimpeg(String kodeJabatanSimpeg) {
		this.kodeJabatanSimpeg = kodeJabatanSimpeg;
	}



	/**********************************************************************/




}
