package id.co.lhk.wfh.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DetilKesehatan {
	public static final String ID = "id_detil_kesehatan";
	public static final String NIP_PEGAWAI = "nip_pegawai_detil_kesehatan";
	public static final String NAMA_PEGAWAI = "nama_pegawai_detil_kesehatan";
	public static final String ID_UNIT_KERJA = "id_unit_kerja_detil_kesehatan";
	public static final String NAMA_UNIT_KERJA = "nama_unit_kerja_detil_kesehatan";
	public static final String ID_JABATAN = "id_jabatan_detil_kesehatan";
	public static final String NAMA_JABATAN = "nama_jabatan_detil_kesehatan";
	public static final String EMAIL = "email_detil_kesehatan";
	public static final String HANDPHONE = "handphone_detil_kesehatan";
	public static final String ALAMAT_RUMAH = "alamat_rumah_detil_kesehatan";
	public static final String RIWAYAT_PENYAKIT = "riwayat_penyakit_detil_kesehatan";
	public static final String TANGGAL = "tanggal_detil_kesehatan";
	public static final String STATUS_SEHAT = "status_sehat_detil_kesehatan";
	public static final String KONDISI_SAKIT_DEMAM = "kondisi_sakit_demam_detil_kesehatan";
	public static final String KONDISI_SAKIT_BATUK = "kondisi_sakit_batuk_detil_kesehatan";
	public static final String KONDISI_SAKIT_PILEK = "kondisi_sakit_pilek_detil_kesehatan";
	public static final String KONDISI_SAKIT_TENGGOROKAN = "kondisi_sakit_tenggorokan_detil_kesehatan";
	public static final String KONDISI_SAKIT_SESAK = "kondisi_sakit_sesak_detil_kesehatan";
	public static final String LAMA_GEJALA_SAKIT = "lama_gejala_sakit_detil_kesehatan";
	public static final String KETERANGAN_SAKIT = "keterangan_sakit_detil_kesehatan";

	private String id;
	private String nipPegawai;
	private String namaPegawai;
	private String idUnitKerja;
	private String namaUnitKerja;
	private String idJabatan;
	private String namaJabatan;
	private String email;
	private String handphone;
	private String alamatRumah;
	private String riwayatPenyakit;
	private Date tanggal;
	private String statusSehat;
	private String kondisiSakitDemam;
	private String kondisiSakitBatuk;
	private String kondisiSakitPilek;
	private String kondisiSakitTenggorokan;
	private String kondisiSakitSesak;
	private Integer lamaGejalaSakit;
	private String keteranganSakit;

	public DetilKesehatan() {

	}

	public DetilKesehatan(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("nip_pegawai")
	public String getNipPegawai() {
		return nipPegawai;
	}

	public void setNipPegawai(String nipPegawai) {
		this.nipPegawai = nipPegawai;
	}

	@JsonProperty("nama_pegawai")
	public String getNamaPegawai() {
		return namaPegawai;
	}

	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}

	@JsonProperty("id_unit_kerja")
	public String getIdUnitKerja() {
		return idUnitKerja;
	}

	public void setIdUnitKerja(String idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}

	@JsonProperty("nama_unit_kerja")
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}

	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}

	@JsonProperty("id_jabatan")
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	@JsonProperty("nama_jabatan")
	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("handphone")
	public String getHandphone() {
		return handphone;
	}

	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}

	@JsonProperty("alamat_rumah")
	public String getAlamatRumah() {
		return alamatRumah;
	}

	public void setAlamatRumah(String alamatRumah) {
		this.alamatRumah = alamatRumah;
	}

	@JsonProperty("riwayat_penyakit")
	public String getRiwayatPenyakit() {
		return riwayatPenyakit;
	}

	public void setRiwayatPenyakit(String riwayatPenyakit) {
		this.riwayatPenyakit = riwayatPenyakit;
	}

	@JsonProperty("tanggal")
	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	@JsonProperty("status_sehat")
	public String getStatusSehat() {
		return statusSehat;
	}

	public void setStatusSehat(String statusSehat) {
		this.statusSehat = statusSehat;
	}

	@JsonProperty("kondisi_sakit_demam")
	public String getKondisiSakitDemam() {
		return kondisiSakitDemam;
	}

	public void setKondisiSakitDemam(String kondisiSakitDemam) {
		this.kondisiSakitDemam = kondisiSakitDemam;
	}

	@JsonProperty("kondisi_sakit_batuk")
	public String getKondisiSakitBatuk() {
		return kondisiSakitBatuk;
	}

	public void setKondisiSakitBatuk(String kondisiSakitBatuk) {
		this.kondisiSakitBatuk = kondisiSakitBatuk;
	}

	@JsonProperty("kondisi_sakit_pilek")
	public String getKondisiSakitPilek() {
		return kondisiSakitPilek;
	}

	public void setKondisiSakitPilek(String kondisiSakitPilek) {
		this.kondisiSakitPilek = kondisiSakitPilek;
	}

	@JsonProperty("kondisi_sakit_tenggorokan")
	public String getKondisiSakitTenggorokan() {
		return kondisiSakitTenggorokan;
	}

	public void setKondisiSakitTenggorokan(String kondisiSakitTenggorokan) {
		this.kondisiSakitTenggorokan = kondisiSakitTenggorokan;
	}

	@JsonProperty("kondisi_sakit_sesak")
	public String getKondisiSakitSesak() {
		return kondisiSakitSesak;
	}

	public void setKondisiSakitSesak(String kondisiSakitSesak) {
		this.kondisiSakitSesak = kondisiSakitSesak;
	}

	@JsonProperty("lama_gejala_sakit")
	public Integer getLamaGejalaSakit() {
		return lamaGejalaSakit;
	}

	public void setLamaGejalaSakit(Integer lamaGejalaSakit) {
		this.lamaGejalaSakit = lamaGejalaSakit;
	}

	@JsonProperty("keterangan_sakit")
	public String getKeteranganSakit() {
		return keteranganSakit;
	}

	public void setKeteranganSakit(String keteranganSakit) {
		this.keteranganSakit = keteranganSakit;
	}



	/**********************************************************************/

}
