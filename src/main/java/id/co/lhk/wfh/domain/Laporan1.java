package id.co.lhk.wfh.domain;

import java.util.Date;

public class Laporan1 {
	
	private Integer idUnitKerja;
	private Date tanggalLaporan;
	private Integer jumlahPegawai;
	private Integer jumlahMelaporPekerjaan;
	private Integer jumlahTidakMelaporPekerjaan;
	private Double jumlahPersenMelaporPekerjaan;
	private Integer jumlahTusiLaporan;
	private Double jumlahPersenTusiLaporan;
	private Integer jumlahTambahanLaporan;
	private Double jumlahPersenTambahanLaporan;
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public Integer getJumlahPegawai() {
		return jumlahPegawai;
	}
	public void setJumlahPegawai(Integer jumlahPegawai) {
		this.jumlahPegawai = jumlahPegawai;
	}
	public Integer getJumlahMelaporPekerjaan() {
		return jumlahMelaporPekerjaan;
	}
	public void setJumlahMelaporPekerjaan(Integer jumlahMelaporPekerjaan) {
		this.jumlahMelaporPekerjaan = jumlahMelaporPekerjaan;
	}
	public Integer getJumlahTidakMelaporPekerjaan() {
		return jumlahTidakMelaporPekerjaan;
	}
	public void setJumlahTidakMelaporPekerjaan(Integer jumlahTidakMelaporPekerjaan) {
		this.jumlahTidakMelaporPekerjaan = jumlahTidakMelaporPekerjaan;
	}
	public Double getJumlahPersenMelaporPekerjaan() {
		return jumlahPersenMelaporPekerjaan;
	}
	public void setJumlahPersenMelaporPekerjaan(Double jumlahPersenMelaporPekerjaan) {
		this.jumlahPersenMelaporPekerjaan = jumlahPersenMelaporPekerjaan;
	}
	public Date getTanggalLaporan() {
		return tanggalLaporan;
	}
	public void setTanggalLaporan(Date tanggalLaporan) {
		this.tanggalLaporan = tanggalLaporan;
	}
	public Integer getJumlahTusiLaporan() {
		return jumlahTusiLaporan;
	}
	public void setJumlahTusiLaporan(Integer jumlahTusiLaporan) {
		this.jumlahTusiLaporan = jumlahTusiLaporan;
	}
	public Double getJumlahPersenTusiLaporan() {
		return jumlahPersenTusiLaporan;
	}
	public void setJumlahPersenTusiLaporan(Double jumlahPersenTusiLaporan) {
		this.jumlahPersenTusiLaporan = jumlahPersenTusiLaporan;
	}
	public Integer getJumlahTambahanLaporan() {
		return jumlahTambahanLaporan;
	}
	public void setJumlahTambahanLaporan(Integer jumlahTambahanLaporan) {
		this.jumlahTambahanLaporan = jumlahTambahanLaporan;
	}
	public Double getJumlahPersenTambahanLaporan() {
		return jumlahPersenTambahanLaporan;
	}
	public void setJumlahPersenTambahanLaporan(Double jumlahPersenTambahanLaporan) {
		this.jumlahPersenTambahanLaporan = jumlahPersenTambahanLaporan;
	}
	
	
}
