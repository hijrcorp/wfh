package id.co.lhk.wfh.domain;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Eselon1 {

	/********************************** - Begin Generate - ************************************/

	public static final String ID_ESELON1 = "id_eselon1";
	public static final String KODE_ESELON1 = "kode_eselon1";
	public static final String NAMA_ESELON1 = "nama_eselon1";
	public static final String KODE_SIMPEG_ESELON1 = "kode_simpeg_eselon1";
	
	private String idEselon1;
	private String kodeEselon1;
	private String namaEselon1;
	private String kodeSimpegEselon1;
	
	
	public Eselon1() {
	
	}
	public Eselon1(String id) {
		this.idEselon1 = id;
	}
	
	@JsonProperty(ID_ESELON1)
	public String getIdEselon1() {
		return idEselon1;
	}
	
	public void setIdEselon1(String idEselon1) {
		this.idEselon1 = idEselon1;
	}
	
	@JsonProperty(KODE_ESELON1)
	public String getKodeEselon1() {
		return kodeEselon1;
	}
	
	public void setKodeEselon1(String kodeEselon1) {
		this.kodeEselon1 = kodeEselon1;
	}
	
	@JsonProperty(NAMA_ESELON1)
	public String getNamaEselon1() {
		return namaEselon1;
	}
	
	public void setNamaEselon1(String namaEselon1) {
		this.namaEselon1 = namaEselon1;
	}
	
	@JsonProperty(KODE_SIMPEG_ESELON1)
	public String getKodeSimpegEselon1() {
		return kodeSimpegEselon1;
	}
	
	public void setKodeSimpegEselon1(String kodeSimpegEselon1) {
		this.kodeSimpegEselon1 = kodeSimpegEselon1;
	}
	
	
	/********************************** - End Generate - ************************************/

}
