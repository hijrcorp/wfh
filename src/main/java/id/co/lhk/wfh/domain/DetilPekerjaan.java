package id.co.lhk.wfh.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DetilPekerjaan {
	public static final String ID = "id_detil_pekerjaan";
	public static final String NIP_PEGAWAI = "nip_pegawai_detil_pekerjaan";
	public static final String NAMA_PEGAWAI = "nama_pegawai_detil_pekerjaan";
	public static final String ID_UNIT_KERJA = "id_unit_kerja_detil_pekerjaan";
	public static final String NAMA_UNIT_KERJA = "nama_unit_kerja_detil_pekerjaan";
	public static final String ID_JABATAN = "id_jabatan_detil_pekerjaan";
	public static final String NAMA_JABATAN = "nama_jabatan_detil_pekerjaan";
	public static final String EMAIL = "email_detil_pekerjaan";
	public static final String HANDPHONE = "handphone_detil_pekerjaan";
	public static final String ALAMAT_RUMAH = "alamat_rumah_detil_pekerjaan";
	public static final String TANGGAL = "tanggal_detil_pekerjaan";
	public static final String STATUS_TUSI = "status_tusi_detil_pekerjaan";
	public static final String NAMA = "nama_detil_pekerjaan";
	public static final String URAIAN = "uraian_detil_pekerjaan";
	public static final String OUTPUT = "output_detil_pekerjaan";

	private String id;
	private String nipPegawai;
	private String namaPegawai;
	private String idUnitKerja;
	private String namaUnitKerja;
	private String idJabatan;
	private String namaJabatan;
	private String email;
	private String handphone;
	private String alamatRumah;
	private Date tanggal;
	private String statusTusi;
	private String nama;
	private String uraian;
	private String output;

	public DetilPekerjaan() {

	}


	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty("nip_pegawai")
	public String getNipPegawai() {
		return nipPegawai;
	}

	public void setNipPegawai(String nipPegawai) {
		this.nipPegawai = nipPegawai;
	}

	@JsonProperty("nama_pegawai")
	public String getNamaPegawai() {
		return namaPegawai;
	}

	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}

	@JsonProperty("id_unit_kerja")
	public String getIdUnitKerja() {
		return idUnitKerja;
	}

	public void setIdUnitKerja(String idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}

	@JsonProperty("nama_unit_kerja")
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}

	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}

	@JsonProperty("id_jabatan")
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	@JsonProperty("nama_jabatan")
	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("handphone")
	public String getHandphone() {
		return handphone;
	}

	public void setHandphone(String handphone) {
		this.handphone = handphone;
	}

	@JsonProperty("alamat_rumah")
	public String getAlamatRumah() {
		return alamatRumah;
	}

	public void setAlamatRumah(String alamatRumah) {
		this.alamatRumah = alamatRumah;
	}

	@JsonProperty("tanggal")
	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	@JsonProperty("status_tusi")
	public String getStatusTusi() {
		return statusTusi;
	}

	public void setStatusTusi(String statusTusi) {
		this.statusTusi = statusTusi;
	}

	@JsonProperty("nama")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@JsonProperty("uraian")
	public String getUraian() {
		return uraian;
	}

	public void setUraian(String uraian) {
		this.uraian = uraian;
	}

	@JsonProperty("output")
	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}


	/**********************************************************************/

}
