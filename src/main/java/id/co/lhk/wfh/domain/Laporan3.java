package id.co.lhk.wfh.domain;

import java.util.Date;

public class Laporan3 {
	
	private Integer rownum;
	
	private Integer idUnitKerja;
	private String namaUnitKerja;
	private Date tanggalLaporan;
	private String nipPegawai;
	private String namaPegawai;
	private String emailPegawai;
	private String statusPelaporan;
	private String statusSehatLaporan;
	private String kondisiSakitDemamLaporan;
	private String kondisiSakitBatukLaporan;
	private String kondisiSakitPilekLaporan;
	private String kondisiSakitTenggorokanLaporan;
	private String kondisiSakitSesakLaporan;
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}
	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}
	public Date getTanggalLaporan() {
		return tanggalLaporan;
	}
	public void setTanggalLaporan(Date tanggalLaporan) {
		this.tanggalLaporan = tanggalLaporan;
	}
	public String getNipPegawai() {
		return nipPegawai;
	}
	public void setNipPegawai(String nipPegawai) {
		this.nipPegawai = nipPegawai;
	}
	public String getNamaPegawai() {
		return namaPegawai;
	}
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	public String getStatusPelaporan() {
		return statusPelaporan;
	}
	public void setStatusPelaporan(String statusPelaporan) {
		this.statusPelaporan = statusPelaporan;
	}
	public String getStatusSehatLaporan() {
		return statusSehatLaporan;
	}
	public void setStatusSehatLaporan(String statusSehatLaporan) {
		this.statusSehatLaporan = statusSehatLaporan;
	}
	public String getKondisiSakitDemamLaporan() {
		return kondisiSakitDemamLaporan;
	}
	public void setKondisiSakitDemamLaporan(String kondisiSakitDemamLaporan) {
		this.kondisiSakitDemamLaporan = kondisiSakitDemamLaporan;
	}
	public String getKondisiSakitBatukLaporan() {
		return kondisiSakitBatukLaporan;
	}
	public void setKondisiSakitBatukLaporan(String kondisiSakitBatukLaporan) {
		this.kondisiSakitBatukLaporan = kondisiSakitBatukLaporan;
	}
	public String getKondisiSakitPilekLaporan() {
		return kondisiSakitPilekLaporan;
	}
	public void setKondisiSakitPilekLaporan(String kondisiSakitPilekLaporan) {
		this.kondisiSakitPilekLaporan = kondisiSakitPilekLaporan;
	}
	public String getKondisiSakitTenggorokanLaporan() {
		return kondisiSakitTenggorokanLaporan;
	}
	public void setKondisiSakitTenggorokanLaporan(String kondisiSakitTenggorokanLaporan) {
		this.kondisiSakitTenggorokanLaporan = kondisiSakitTenggorokanLaporan;
	}
	public String getKondisiSakitSesakLaporan() {
		return kondisiSakitSesakLaporan;
	}
	public void setKondisiSakitSesakLaporan(String kondisiSakitSesakLaporan) {
		this.kondisiSakitSesakLaporan = kondisiSakitSesakLaporan;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public String getEmailPegawai() {
		return emailPegawai;
	}
	public void setEmailPegawai(String emailPegawai) {
		this.emailPegawai = emailPegawai;
	}
	
}
