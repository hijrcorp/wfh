package id.co.lhk.wfh.domain;

public class Dashboard2 {
	
	private Integer idUnitKerja;
	private String namaUnitKerja;
	private Integer jumlahPegawai;
	private Integer jumlahMelaporPekerjaan;
	private Integer jumlahTusiLaporan;
	private Integer jumlahTambahanLaporan;
	private Double jumlahPersenMelaporPekerjaan;
	private Integer jumlahMelaporKesehatan;
	private Double jumlahPersenMelaporKesehatan;
	private Integer jumlahSehat;
	private Double jumlahPersenSehat;
	private Integer jumlahSakit;
	private Double jumlahPersenSakit;
	private Integer jumlahDemam;
	private Integer jumlahBatuk;
	private Integer jumlahPilek;
	private Integer jumlahTenggorokan;
	private Integer jumlahSesak;
	
	
	public Integer getIdUnitKerja() {
		return idUnitKerja;
	}
	public void setIdUnitKerja(Integer idUnitKerja) {
		this.idUnitKerja = idUnitKerja;
	}
	public String getNamaUnitKerja() {
		return namaUnitKerja;
	}
	public void setNamaUnitKerja(String namaUnitKerja) {
		this.namaUnitKerja = namaUnitKerja;
	}
	public Integer getJumlahPegawai() {
		return jumlahPegawai;
	}
	public void setJumlahPegawai(Integer jumlahPegawai) {
		this.jumlahPegawai = jumlahPegawai;
	}
	public Integer getJumlahMelaporPekerjaan() {
		return jumlahMelaporPekerjaan;
	}
	public void setJumlahMelaporPekerjaan(Integer jumlahMelaporPekerjaan) {
		this.jumlahMelaporPekerjaan = jumlahMelaporPekerjaan;
	}
	public Integer getJumlahTusiLaporan() {
		return jumlahTusiLaporan;
	}
	public void setJumlahTusiLaporan(Integer jumlahTusiLaporan) {
		this.jumlahTusiLaporan = jumlahTusiLaporan;
	}
	public Integer getJumlahTambahanLaporan() {
		return jumlahTambahanLaporan;
	}
	public void setJumlahTambahanLaporan(Integer jumlahTambahanLaporan) {
		this.jumlahTambahanLaporan = jumlahTambahanLaporan;
	}
	public Double getJumlahPersenMelaporPekerjaan() {
		return jumlahPersenMelaporPekerjaan;
	}
	public void setJumlahPersenMelaporPekerjaan(Double jumlahPersenMelaporPekerjaan) {
		this.jumlahPersenMelaporPekerjaan = jumlahPersenMelaporPekerjaan;
	}
	public Integer getJumlahMelaporKesehatan() {
		return jumlahMelaporKesehatan;
	}
	public void setJumlahMelaporKesehatan(Integer jumlahMelaporKesehatan) {
		this.jumlahMelaporKesehatan = jumlahMelaporKesehatan;
	}
	public Double getJumlahPersenMelaporKesehatan() {
		return jumlahPersenMelaporKesehatan;
	}
	public void setJumlahPersenMelaporKesehatan(Double jumlahPersenMelaporKesehatan) {
		this.jumlahPersenMelaporKesehatan = jumlahPersenMelaporKesehatan;
	}
	public Integer getJumlahSehat() {
		return jumlahSehat;
	}
	public void setJumlahSehat(Integer jumlahSehat) {
		this.jumlahSehat = jumlahSehat;
	}
	public Double getJumlahPersenSehat() {
		return jumlahPersenSehat;
	}
	public void setJumlahPersenSehat(Double jumlahPersenSehat) {
		this.jumlahPersenSehat = jumlahPersenSehat;
	}
	public Integer getJumlahSakit() {
		return jumlahSakit;
	}
	public void setJumlahSakit(Integer jumlahSakit) {
		this.jumlahSakit = jumlahSakit;
	}
	public Double getJumlahPersenSakit() {
		return jumlahPersenSakit;
	}
	public void setJumlahPersenSakit(Double jumlahPersenSakit) {
		this.jumlahPersenSakit = jumlahPersenSakit;
	}
	public Integer getJumlahDemam() {
		return jumlahDemam;
	}
	public void setJumlahDemam(Integer jumlahDemam) {
		this.jumlahDemam = jumlahDemam;
	}
	public Integer getJumlahBatuk() {
		return jumlahBatuk;
	}
	public void setJumlahBatuk(Integer jumlahBatuk) {
		this.jumlahBatuk = jumlahBatuk;
	}
	public Integer getJumlahPilek() {
		return jumlahPilek;
	}
	public void setJumlahPilek(Integer jumlahPilek) {
		this.jumlahPilek = jumlahPilek;
	}
	public Integer getJumlahTenggorokan() {
		return jumlahTenggorokan;
	}
	public void setJumlahTenggorokan(Integer jumlahTenggorokan) {
		this.jumlahTenggorokan = jumlahTenggorokan;
	}
	public Integer getJumlahSesak() {
		return jumlahSesak;
	}
	public void setJumlahSesak(Integer jumlahSesak) {
		this.jumlahSesak = jumlahSesak;
	}
	
	
	
}
