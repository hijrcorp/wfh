package id.co.lhk.wfh.domain;

public class Jabatan {
	private String idJabatan;
	private String namaJabatan;
	private String groupJabatan;
	
	public Jabatan() {
		
	}

	public Jabatan(String id) {
		this.idJabatan = id;
	}
	public Jabatan(String id, String namaJabatan,String groupJabatan) {
		super();
		this.idJabatan = id;
		this.namaJabatan = namaJabatan;
		this.groupJabatan = groupJabatan;
	}

	public String getId() {
		return idJabatan;
	}
	public void setId(String id) {
		this.idJabatan = id;
	}
	public String getIdJabatan() {
		return idJabatan;
	}

	public void setIdJabatan(String idJabatan) {
		this.idJabatan = idJabatan;
	}

	public String getNamaJabatan() {
		return namaJabatan;
	}

	public void setNamaJabatan(String namaJabatan) {
		this.namaJabatan = namaJabatan;
	}

	public String getGroupJabatan() {
		return groupJabatan;
	}

	public void setGroupJabatan(String groupJabatan) {
		this.groupJabatan = groupJabatan;
	}
	
}
