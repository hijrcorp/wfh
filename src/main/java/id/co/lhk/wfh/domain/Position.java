package id.co.lhk.wfh.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import id.co.lhk.wfh.core.Metadata;

public class Position extends Metadata implements Serializable {
	
	private String name;
	private String description;
	
	
	public Position() {
			
	}
	public Position(int id) {
		super(id);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}


