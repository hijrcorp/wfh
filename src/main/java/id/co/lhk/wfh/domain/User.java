package id.co.lhk.wfh.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.lhk.wfh.core.JsonDateTimeSerializer;
import id.co.lhk.wfh.core.Metadata;

public class User  {
	private String id;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private String currentPassword;
	private String description;
	private String token;
	private Date expired;
	private String role;
	private String position;
	private String namePosition;
	private String userId;
	private String email;
	private String picture;
	
	public User() {
		
	}
	public User(String id) {
		this.id = id;
	}
	
	public String getName() {
		return firstName + " " + lastName;
	}

	public String getNamePosition() {
		return namePosition;
	}
	
	public User(String id, String firstName, String lastName, String username, String password, String currentPassword,
			String description, String token, Date expired, String role, String position, String email) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.currentPassword = currentPassword;
		this.description = description;
		this.token = token;
		this.expired = expired;
		this.role = role;
		this.position = position;
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public String getUserId() {
		return userId;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getExpired() {
		return expired;
	}
	public void setExpired(Date expired) {
		this.expired = expired;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	private String tem_password;

	public String getTem_password() {
		return tem_password;
	}
	public void setTem_password(String tem_password) {
		this.tem_password = tem_password;
	}	
	
	//new model attr
	private String gender;
	private Date birthDate;
	private String mobilePhone;

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
}

