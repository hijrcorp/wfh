package id.co.lhk.wfh;

public enum OrganizationReference {
	ALHP("ALHP", "1508629035347"),
	PTL("PTL", "1508628920428"),
	SURVEY("SURVEY", "1508628921431"),
	LAPGAS("LAPGAS", "1508628920430"),
	HEDES("HEDES", "1508628921432"),
	WFH("WFH", "2008628921432"),
	ITJEN("ITJEN","1508629134827")
    ;

    private final String text;
    private final String value;

    /**
     * @param text
     */
    private OrganizationReference(final String text, final String value) {
        this.text = text;
        this.value = value;
    }
    
    public String value() {
        return value;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
