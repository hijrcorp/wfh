package id.co.lhk.wfh.core;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import id.co.lhk.wfh.domain.User;

public interface BaseService {

	public void setUserLogin(User user);
	
	public void loadUserLogin(OAuth2Authentication authentication);
	
}
