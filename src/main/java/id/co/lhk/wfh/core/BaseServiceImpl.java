package id.co.lhk.wfh.core;

import javax.annotation.PostConstruct;

import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import id.co.lhk.wfh.domain.User;

public abstract class BaseServiceImpl  {
	
	protected User userLogin;
	protected OAuth2Authentication userAuthentication;
	
	
	public void setUserLogin(User user){
		this.userLogin = user;
	}
	
	
	public void loadUserLogin(OAuth2Authentication authentication) {
		System.out.println("loaded..");

    		  userAuthentication = authentication;
	}
	
	public OAuth2Authentication getAuthentication() {
		return userAuthentication;
	}

}
