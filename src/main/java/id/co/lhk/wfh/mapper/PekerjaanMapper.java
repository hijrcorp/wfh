package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.DetilKesehatan;
import id.co.lhk.wfh.domain.DetilPekerjaan;
import id.co.lhk.wfh.domain.Pekerjaan;

@Mapper
public interface PekerjaanMapper {
	
	@Insert("INSERT INTO wfh_pekerjaan (id_pekerjaan, laporan_id_pekerjaan, status_tusi_pekerjaan, nama_pekerjaan, uraian_pekerjaan, output_pekerjaan) VALUES (#{id:VARCHAR}, #{laporanId:VARCHAR}, #{statusTusi:VARCHAR}, #{nama:VARCHAR}, #{uraian:VARCHAR}, #{output:VARCHAR})")
	void insert(Pekerjaan pekerjaan);

	@Update("UPDATE wfh_pekerjaan SET id_pekerjaan=#{id:VARCHAR}, laporan_id_pekerjaan=#{laporanId:VARCHAR}, status_tusi_pekerjaan=#{statusTusi:VARCHAR}, nama_pekerjaan=#{nama:VARCHAR}, uraian_pekerjaan=#{uraian:VARCHAR}, output_pekerjaan=#{output:VARCHAR} WHERE id_pekerjaan=#{id}")
	void update(Pekerjaan pekerjaan);

	@Delete("DELETE FROM wfh_pekerjaan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM wfh_pekerjaan WHERE id_pekerjaan=#{id}")
	void delete(Pekerjaan pekerjaan);

	List<Pekerjaan> getList(QueryParameter param);

	Pekerjaan getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	List<DetilPekerjaan> getListDetil(QueryParameter param);
	
	long getCountDetil(QueryParameter param);
}
