package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.Jabatan;
import id.co.lhk.wfh.domain.Propinsi;
import id.co.lhk.wfh.domain.UnitKerja;

@Mapper
public interface ReferenceMapper {
//	
//	@Select("select `value` from tbl_config where `key`=concat(#{key},(select `value` from tbl_config where `key`='DEV_CONFIG_SUFFIX'))")
//	String getConfigValue(@Param("key") String key);
//
//	@Select("select * from tbl_kelompok_upt")
//	List<KelompokUPT> getListKelompokUPT();
//
//	@Select("select * from tbl_program")
//	List<Program> getListProgram();
//
//	@Select("select * from tbl_tugas_fungsi")
//	List<TugasFungsi> getListTugasFungsi();
//
	@Select("select * from wfh_jabatan")
	List<Jabatan> getListJabatan();
//	
////	@Select("SELECT tugas_fungsi_id, uraian_tugas_fungsi, unit_auditi_id, kode_unit_auditi, nama_unit_auditi, nama_kelompok_upt FROM tbl_unit_auditi a INNER JOIN tbl_kelompok_upt b ON a.kelompok_upt_id=b.kelompok_upt_id INNER JOIN tbl_tugas_fungsi c ON a.kelompok_upt_id=c.kelompok_upt_id \r\n" + 
////			"WHERE unit_auditi_id=#{id}\r\n" + 
////			"ORDER BY tugas_fungsi_id, uraian_tugas_fungsi")
////	UnitAuditi findBySatker(@Param("id") String id);
//	
//	@Select("SELECT tugas_fungsi_id, uraian_tugas_fungsi, unit_auditi_id, kode_unit_auditi, nama_unit_auditi, nama_kelompok_upt FROM tbl_unit_auditi a INNER JOIN tbl_kelompok_upt b ON a.kelompok_upt_id=b.kelompok_upt_id INNER JOIN tbl_tugas_fungsi c ON a.kelompok_upt_id=c.kelompok_upt_id \r\n" + 
//			"WHERE unit_auditi_id=#{id}\r\n" + 
//			"ORDER BY tugas_fungsi_id, uraian_tugas_fungsi")
//	List<UnitAuditia> findBySatker(@Param("id") String id);

//	@Select("select * from hedes_kodefikasi WHERE ${clause} ")
//	List<Kodefikasi> getListKodefikasi(QueryParameter param);

	@Select("select * from wfh_unit_kerja")
	List<UnitKerja> getListUnitKerja();
}
