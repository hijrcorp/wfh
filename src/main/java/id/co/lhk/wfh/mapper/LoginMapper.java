package id.co.lhk.wfh.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.lhk.wfh.domain.Login;
import id.co.lhk.wfh.domain.LoginHistory;
import id.co.lhk.wfh.domain.LoginRequest;
import id.co.lhk.wfh.domain.OauthClient;
import id.co.lhk.wfh.domain.OauthUser;



@Mapper
public interface LoginMapper {
	
	@Insert("insert into tbl_login (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, now(), #{expireTime}, #{tokenObject})")
	void insert(Login login);

	@Select("select * from tbl_login where access_token=#{accessToken} and expire_time > now()")
	Login findActiveLoginByAccessToken(@Param("accessToken") String accessToken);
	
	@Select("select * from tbl_login where refresh_token=#{refreshToken}")
	Login findLoginByRefreshToken(@Param("refreshToken") String refreshToken);
	
	@Select("select * from tbl_login where client_id=#{clientId} and user_name=#{userName} and expire_time > now()")
	Login findActiveLoginByClientAndUser(@Param("clientId") String clientId, @Param("userName") String userName);
	
	@Delete("delete from tbl_login where id=#{id}")
	void deleteActiveLogin(Login login);
	
	@Insert("insert into tbl_login_history (id, access_token, client_id, user_name, refresh_token, created_time, expire_time, token_object, status, user_id, organization_id) values (#{id}, #{accessToken}, #{clientId}, #{userName}, #{refreshToken}, #{createdTime}, #{expireTime}, #{tokenObject}, #{status}, #{userId}, #{organizationId})")
	void insertLoginHistory(LoginHistory loginHistory);
	
	@Select("select * from oauth_client_details where client_id=#{clientId} and client_secret=#{clientSecret}")
	OauthClient findOauthClient(@Param("clientId") String clientId, @Param("clientSecret") String clientSecret);
	
	@Select("select * from oauth_client_details where client_id=#{clientId}")
	OauthClient findOauthClientById(@Param("clientId") String clientId);
	
	@Insert("insert into tbl_login_request (id, client_id, request_token, callback, request_time) values (#{id}, #{clientId}, #{requestToken}, #{callback}, now())")
	void insertLoginRequest(LoginRequest loginRequest);
	
	@Select("select a.* from oauth_client_details a inner join tbl_login_request b where a.client_id=b.client_id and a.client_id=#{clientId} and request_token=#{requestToken} and callback=#{callback}")
	OauthClient findOauthClientByLoginRequest(@Param("clientId") String clientId, @Param("requestToken") String requestToken, @Param("callback") String callback);
	
	@Insert("insert into tbl_mobile_token (fcm_token) values (#{fcmToken})")
	void insertMobileToken(@Param("fcmToken") String fcmToken);
	
	@Update("insert into tbl_mobile_token (user_id, fcm_token) values (#{userId}, #{fcmToken}) on duplicate key update user_id=#{userId}")
	void updateMobileUserToken(@Param("userId") String userId, @Param("fcmToken") String fcmToken);
	
	@Select("select fcm_token from tbl_mobile_token where user_id=#{userId}")
	String findMobilToken(@Param("userId") String userId);
}
