package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.domain.UserDetil;

@Mapper
public interface UserDetilMapper {
	
	@Insert("INSERT INTO tbl_user_detil (id_user_detil, user_id_user_detil, alamat_rumah_user_detil, riwayat_penyakit_user_detil, id_jabatan_user_detil, kode_jabatan_simpeg_user_detil) VALUES (#{id:VARCHAR}, #{userId:VARCHAR}, #{alamatRumah:VARCHAR}, #{riwayatPenyakit:VARCHAR}, #{idJabatan:VARCHAR}, #{kodeJabatanSimpeg:VARCHAR})")
	void insert(UserDetil userDetil);

	@Update("UPDATE tbl_user_detil SET id_user_detil=#{id:VARCHAR}, user_id_user_detil=#{userId:VARCHAR}, alamat_rumah_user_detil=#{alamatRumah:VARCHAR}, riwayat_penyakit_user_detil=#{riwayatPenyakit:VARCHAR}, id_jabatan_user_detil=#{idJabatan:VARCHAR}, kode_jabatan_simpeg_user_detil=#{kodeJabatanSimpeg:VARCHAR} WHERE id_user_detil=#{id}")
	void update(UserDetil userDetil);

	@Delete("DELETE FROM tbl_user_detil WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_user_detil WHERE id_user_detil=#{id}")
	void delete(UserDetil userDetil);

	List<UserDetil> getList(QueryParameter param);

	UserDetil getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	List<OauthUserOrganizationUnit> getListGabungan(QueryParameter param);
}
