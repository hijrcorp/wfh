package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import id.co.lhk.wfh.domain.Laporan1;
import id.co.lhk.wfh.domain.Laporan2;
import id.co.lhk.wfh.domain.Laporan3;
import id.co.lhk.wfh.domain.Laporan4;
import id.co.lhk.wfh.domain.UnitKerja;

@Mapper
public interface LaporanMapper {
	
	@Select("select * from tbl_unit_kerja where unit_kerja_id=#{id}")
	UnitKerja getUnitKerja(@Param("id") Integer id);

	long getCountLaporan1(@Param("tanggal_clause") String tanggalClause, @Param("id_unit_auditi_clause") String idUnitAuditiClause);
	List<Laporan1> getListLaporan1(@Param("tanggal_clause") String tanggalClause, @Param("id_unit_auditi_clause") String idUnitAuditiClause, @Param("limit") Integer limit, @Param("offset") Integer offset);

	long getCountLaporan2(@Param("tanggal_clause") String tanggalClause, @Param("id_unit_auditi_clause") String idUnitAuditiClause);
	List<Laporan2> getListLaporan2(@Param("tanggal_clause") String tanggalClause, @Param("id_unit_auditi_clause") String idUnitAuditiClause, @Param("limit") Integer limit, @Param("offset") Integer offset);

	long getCountLaporan3(@Param("tanggal_clause") String tanggalClause, @Param("status_pelaporan") String statusPelaporan);
	List<Laporan3> getListLaporan3(@Param("tanggal_clause") String tanggalClause, @Param("status_pelaporan") String statusPelaporan, @Param("limit") Integer limit, @Param("offset") Integer offset);

	long getCountLaporan4(@Param("tanggal_clause") String tanggalClause, @Param("status_pelaporan") String statusPelaporan);
	List<Laporan4> getListLaporan4(@Param("tanggal_clause") String tanggalClause, @Param("status_pelaporan") String statusPelaporan, @Param("limit") Integer limit, @Param("offset") Integer offset);
	
}
