package id.co.lhk.wfh.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.lhk.wfh.core.QueryParameter;
import id.co.lhk.wfh.domain.DetilKesehatan;
import id.co.lhk.wfh.domain.Kesehatan;

@Mapper
public interface KesehatanMapper {
	
	@Insert("INSERT INTO wfh_laporan (id_laporan, tanggal_laporan, user_id_laporan, jumlah_tusi_laporan, jumlah_tambahan_laporan, status_sehat_laporan, kondisi_sakit_laporan, keterangan_sakit_laporan, added_time_laporan, added_user_laporan, kondisi_sakit_batuk_laporan, kondisi_sakit_demam_laporan, kondisi_sakit_pilek_laporan, kondisi_sakit_tenggorokan_laporan, kondisi_sakit_sesak_laporan, modified_time_laporan, modified_user_laporan, lama_gejala_sakit_laporan) VALUES (#{id:VARCHAR}, #{tanggal:DATE}, #{userId:VARCHAR}, #{jumlahTusi:NUMERIC}, #{jumlahTambahan:NUMERIC}, #{statusSehat:VARCHAR}, #{kondisiSakit:VARCHAR}, #{keteranganSakit:VARCHAR}, #{addedTime:TIMESTAMP}, #{addedUser:VARCHAR}, #{kondisiSakitBatuk:VARCHAR}, #{kondisiSakitDemam:VARCHAR}, #{kondisiSakitPilek:VARCHAR}, #{kondisiSakitTenggorokan:VARCHAR}, #{kondisiSakitSesak:VARCHAR}, #{modifiedTime:TIMESTAMP}, #{modifiedUser:VARCHAR}, #{lamaGejalaSakit:NUMERIC})")
	void insert(Kesehatan laporan);

	@Update("UPDATE wfh_laporan SET id_laporan=#{id:VARCHAR}, tanggal_laporan=#{tanggal:DATE}, user_id_laporan=#{userId:VARCHAR}, jumlah_tusi_laporan=#{jumlahTusi:NUMERIC}, jumlah_tambahan_laporan=#{jumlahTambahan:NUMERIC}, status_sehat_laporan=#{statusSehat:VARCHAR}, kondisi_sakit_laporan=#{kondisiSakit:VARCHAR}, keterangan_sakit_laporan=#{keteranganSakit:VARCHAR}, added_time_laporan=#{addedTime:TIMESTAMP}, added_user_laporan=#{addedUser:VARCHAR}, kondisi_sakit_batuk_laporan=#{kondisiSakitBatuk:VARCHAR}, kondisi_sakit_demam_laporan=#{kondisiSakitDemam:VARCHAR}, kondisi_sakit_pilek_laporan=#{kondisiSakitPilek:VARCHAR}, kondisi_sakit_tenggorokan_laporan=#{kondisiSakitTenggorokan:VARCHAR}, kondisi_sakit_sesak_laporan=#{kondisiSakitSesak:VARCHAR}, modified_time_laporan=#{modifiedTime:TIMESTAMP}, modified_user_laporan=#{modifiedUser:VARCHAR}, lama_gejala_sakit_laporan=#{lamaGejalaSakit:NUMERIC} WHERE id_laporan=#{id}")
	void update(Kesehatan laporan);

	@Delete("DELETE FROM wfh_laporan WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM wfh_laporan WHERE id_laporan=#{id}")
	void delete(Kesehatan laporan);

	List<Kesehatan> getList(QueryParameter param);

	Kesehatan getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	
	@Update("update wfh_laporan set jumlah_tusi_laporan=(select count(*) from wfh_pekerjaan where status_tusi_pekerjaan = 'YES' and laporan_id_pekerjaan=#{id:VARCHAR}), jumlah_tambahan_laporan=(select count(*) from wfh_pekerjaan where status_tusi_pekerjaan = 'NO' and laporan_id_pekerjaan=#{id:VARCHAR}) where id_laporan=#{id:VARCHAR}")
	void updateTUSI(Kesehatan kesehatan);
	
	List<DetilKesehatan> getListDetil(QueryParameter param);
	
	long getCountDetil(QueryParameter param);
	
	Date getMaxDate(QueryParameter param);
}
