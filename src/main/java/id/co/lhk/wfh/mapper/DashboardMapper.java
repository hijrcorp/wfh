package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import id.co.lhk.wfh.domain.Dashboard1;
import id.co.lhk.wfh.domain.Dashboard2;

@Mapper
public interface DashboardMapper {
	
	List<Dashboard1> getDashboard1(@Param("tanggal") String tanggal);
	List<Dashboard2> getDashboard2(@Param("tanggal") String tanggal);
	
}
