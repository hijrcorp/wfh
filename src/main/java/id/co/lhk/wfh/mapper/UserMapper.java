package id.co.lhk.wfh.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.lhk.wfh.domain.UserDevice;
import id.co.lhk.wfh.domain.OauthRoleOrganization;
import id.co.lhk.wfh.domain.OauthUser;
import id.co.lhk.wfh.domain.OauthUserOperatorOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganization;
import id.co.lhk.wfh.domain.OauthUserOrganizationUnit;
import id.co.lhk.wfh.domain.User;
@Mapper
public interface UserMapper {
	
	public static final String QRY_MASTER_USER = "select a.*, c.position_id, name_position position_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id";
	public static final String QRY_MASTER_LIST_USER = "select a.* from (select a.*, c.position_id, name_position, concat(concat(first_name,' '),last_name) real_name, organization_id from (select id, id user_id, username, first_name, last_name, email description, \"\" token, now() expired from tbl_oauth_user) a inner join tbl_oauth_user_position b on a.user_id=b.user_id inner join tbl_position c on b.position_id=c.position_id ) a inner join (select * from tbl_oauth_user_position where organization_id='2008628921432') b on a.user_id!=b.user_id";
	
	public static final String QRY_USER_ORGANIZATION = "select a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id inner join tbl_organization d on b.organization_id=d.id";
	public static final String QRY_USER_ORGANIZATION_LOCAL = "select a.* from (select a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id inner join tbl_organization d on b.organization_id=d.id) a inner join (select * from tbl_oauth_user_position where organization_id='2008628921432') b on a.id!=b.user_id";
	public static final String QRY_USER_ORGANIZATION_UNIT = "select a.*, c.unit_kerja_id, nama_unit_kerja unit_kerja_name from ("+QRY_USER_ORGANIZATION_LOCAL+") a left join tbl_unit_kerja_user b on a.id=b.user_id left join tbl_unit_kerja c on b.unit_kerja_id=c.unit_kerja_id";
	public static final String QRY_USER_ORGANIZATION_OPERATOR = "select a.*, b.unit_auditi_id, c.nama_unit_auditi unit_auditi_name from ("+QRY_USER_ORGANIZATION_LOCAL+") a left join tbl_unit_auditi_user b on a.id=b.user_id inner join tbl_unit_auditi c on b.unit_auditi_id=c.unit_auditi_id";
	public static final String QRY_USER_ORGANIZATION_UNIT_NEW = "SELECT a.*, b.unit_kerja_id, nama_unit_kerja unit_kerja_name, c.*, e.* FROM (SELECT a.*, c.position_id, name_position position_name, d.id organization_id, d.code organization_code, d.name organization_name FROM tbl_oauth_user a INNER JOIN tbl_oauth_user_position b ON a.id=b.user_id INNER JOIN tbl_position c ON b.position_id=c.position_id INNER JOIN tbl_organization d ON b.organization_id=d.id WHERE b.organization_id = '2008628921432') a LEFT JOIN( SELECT c.*,b.user_id FROM tbl_unit_kerja_user b LEFT JOIN tbl_unit_kerja c ON b.unit_kerja_id=c.unit_kerja_id ) b ON b.user_id =a.id LEFT JOIN tbl_user_detil c ON c.user_id_user_detil = a.id  left join tbl_jabatan e on id_jabatan=id_jabatan_user_detil";

	@Select("select * from tbl_oauth_user where id=#{userId}")
	User checkByUserId(@Param("userId") String userId);
	
	@Select("select unit_kerja_id from tbl_unit_kerja_user where user_id=#{userId}")
	List<String> findUnitKerjaByUserId(@Param("userId") String userId);
	
	@Select("select unit_auditi_id from tbl_unit_auditi_user where user_id=#{userId}")
	List<String> findUnitAuditiByUserId(@Param("userId") String userId);
	
	@Select("select lhp_id from tbl_lhp_analis where analis_id=#{userId}")
	List<String> findLhpIdByUserId(@Param("userId") String userId);
	
	@Select("select unit_kerja_id from tbl_unit_kerja_user where user_id=#{userId}")
	List<String> findUnitKerjaUser(@Param("userId") String userId);
	
	@Select("select unit_auditi_id from tbl_unit_auditi_user where user_id=#{userId}")
	List<String> findUnitAuditiUser(@Param("userId") String userId);
	
	//unit kerja user
	@Insert("insert into tbl_unit_kerja_user values (#{unitKerjaId}, #{userId})")
	void insertUnitKerjaUser(@Param("unitKerjaId") String unitKerjaId, @Param("userId") String userId);
	
	@Delete("delete from tbl_unit_kerja_user where unit_kerja_id=#{unitKerjaId} and user_id=#{userId}")
	void deleteUnitKerjaUser(@Param("unitKerjaId") String unitKerjaId, @Param("userId") String userId);
	
	@Delete("delete from tbl_unit_kerja_user where user_id=#{userId}")
	void deleteAllUnitKerjaUser(@Param("userId") String userId);
	//end here
	
	//user detil 
	@Insert("insert into tbl_user_detil (id_user_detil, user_id_user_detil, alamat_rumah_user_detil, riwayat_penyakit_user_detil, id_jabatan_user_detil) values (#{idUserDetil},#{userIdUserDetil}, #{alamatRumahUserDetil}, #{riwayatPenyakitUserDetil}, #{idJabatanUserDetil})")
	void insertUserDetil(@Param("idUserDetil") String idUserDetil, @Param("userIdUserDetil") String userIdUserDetil, @Param("alamatRumahUserDetil") String alamatRumahUserDetil, @Param("riwayatPenyakitUserDetil") String riwayatPenyakitUserDetil,@Param("idJabatanUserDetil") String idJabatanUserDetil);
	
	//@Delete("delete from tbl_unit_kerja_user where unit_kerja_id=#{unitKerjaId} and user_id=#{userId}")
	//void deleteUnitKerjaUser(@Param("unitKerjaId") String unitKerjaId, @Param("userId") String userId);
	
	@Delete("delete from tbl_user_detil where user_id_user_detil=#{userIdUserDetil}")
	void deleteAllUserDetil(@Param("userIdUserDetil") String userIdUserDetil);
	//end here
	@Insert("insert into tbl_unit_auditi_user (unit_auditi_id, user_id) values (#{unitAuditiId}, #{userId})")
	void insertUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);
	
	@Delete("delete from tbl_unit_auditi_user where unit_auditi_id=#{unitAuditiId} and user_id=#{userId}")
	void deleteUnitAuditiUser(@Param("unitAuditiId") String unitAuditiId, @Param("userId") String userId);
	
	@Delete("delete from tbl_unit_auditi_user where user_id=#{userId}")
	void deleteAllUnitAuditiUser(@Param("userId") String userId);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION_OPERATOR+") as a where ${clause}")
	List<OauthUserOperatorOrganization> findListWithOrganizationOperator(@Param("clause") String clause);
	
	@Select("select count(*) from ("+QRY_USER_ORGANIZATION_OPERATOR+") t where ${clause}")
	long findCountListWithOrganizationOperator(@Param("clause") String clause);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION_LOCAL+") as a where ${clause}")
	List<OauthUserOrganization> findListWithOrganizationLocal(@Param("clause") String clause);
	
	@Select("select count(*) from ("+QRY_USER_ORGANIZATION_LOCAL+") t where ${clause}")
	long findCountListWithOrganizationLocal(@Param("clause") String clause);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION_UNIT+") as a where ${clause}")
	List<OauthUserOrganizationUnit> findListWithOrganizationUnit(@Param("clause") String clause);

	@Select("select * from ("+QRY_USER_ORGANIZATION_UNIT_NEW+") as a where ${clause}")
	List<OauthUserOrganizationUnit> findListWithOrganizationUnitNew(@Param("clause") String clause);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION_UNIT_NEW+") as a where id=#{id} GROUP BY id")
	User findByIdWithOrganizationUnitNew(@Param("id") String id);
	
	@Select("select count(*) from (select * from ("+QRY_USER_ORGANIZATION_UNIT+") as a where ${clause}) as a")
	long findCountWithOrganizationUnit(@Param("clause") String clause);
	
	@Select("select * from ("+QRY_USER_ORGANIZATION+") as a where ${clause}")
	List<OauthUserOrganization> findListWithOrganization(@Param("clause") String clause);
	
	@Select("select count(*) from ("+QRY_USER_ORGANIZATION+") t where ${clause}")
	long findCountListWithOrganization(@Param("clause") String clause);
	
	@Select("select c.name from tbl_oauth_user_position a inner join tbl_oauth_position_role b on a.position_id=b.position_id inner join tbl_oauth_role c on role_id=c.id where user_id=#{userId} and organization_id=#{organizationId}")
	List<String> findRolesByUserOrganization(@Param("userId") String userId, @Param("organizationId") String organizationId);
	//new
	@Select("select a.*, b.code code_organization, b.name name_organization, c.name_position from tbl_organization_position a left join tbl_organization b on id = a.organization_id left join tbl_position c on c.position_id = a.position_id where organization_id=#{organizationId}")
	List<OauthRoleOrganization> findRolesByOrganization(@Param("organizationId") String organizationId);
	
	//
	@Select("select * from ("+QRY_MASTER_USER+") as a where id=#{loginId} or username=#{loginId} or email=#{loginId} or mobile_phone=#{loginId}")
	OauthUser findUserByLoginId(@Param("loginId") String loginId);

	@Select("select a.*, b.* from tbl_oauth_user a "
			+ "inner join tbl_oauth_user_position b on b.user_id = a.id "
			+ "where id=#{id} GROUP BY id")
	OauthUser findUserById(@Param("id") String id);
	//basic
	@Select("select count(*) from("+QRY_MASTER_LIST_USER+") t where ${filter}")
	Integer getCount(@Param("filter") String filter);
	
	@Select("select * from ("+QRY_MASTER_LIST_USER+") t where user_id=#{id} GROUP BY user_id") // group by user_id karna ada duplicate
	User findById(@Param("id") String id);
	
	@Select("select * from ("+QRY_MASTER_LIST_USER+") t where ${filter}")
	List<User> getList(@Param("filter") String filter);
	
	@Insert("insert into tbl_oauth_user\r\n" + 
			"		(id,first_name, last_name, mobile_phone,username,password,email, gender, enabled, account_non_expired, credentials_non_expired, account_non_locked) \r\n" + 
			"		values \r\n" + 
			"		(#{id:NUMERIC},#{firstName:VARCHAR},#{lastName:VARCHAR},#{mobilePhone:VARCHAR},#{username:VARCHAR},#{password:VARCHAR},#{email:VARCHAR},#{gender:VARCHAR},1,1,1,1\r\n" + 
			"		)")
	void insert(User user);
	
	@Update("update tbl_oauth_user set \r\n" + 
			"		first_name=#{firstName:VARCHAR},\r\n" + 
			"		last_name=#{lastName:VARCHAR}, \r\n" + 
			"		username=#{username:VARCHAR},\r\n" + 
			"		birth_date=#{birthDate:TIMESTAMP},\r\n" + 
			"		email=#{email:VARCHAR},\r\n" + 
			"		mobile_phone=#{mobilePhone:VARCHAR},\r\n" + 
			"		gender=#{gender:VARCHAR},\r\n" + 
			"		picture=#{picture:VARCHAR}\r\n" + 
			"	where id=#{id}")
	void update(User user);

	@Update("update tbl_oauth_user set password=#{password:VARCHAR} where id=#{id}")
	void updatePassword(User user);
	//end here
	@Select("select b.name from tbl_oauth_user_role a inner join tbl_oauth_role b on a.role_id=b.id where a.user_id=#{userId}")
	List<String> findRolesByUserId(@Param("userId") String userId);
	
	@Insert("insert into tbl_oauth_user_position (user_id, position_id, organization_id) values (#{userId}, #{positionId}, #{organizationId})")
	void insertUserPosition(@Param("userId") long userId, @Param("positionId") long positionId, @Param("organizationId") String organizationId);
	
	@Insert("insert into tbl_oauth_user_role select #{userId} user_id, role_id from tbl_oauth_position_role a inner join tbl_position b on a.position_id=b.position_id where a.position_id=#{positionId}")
	void insertUserRole(@Param("userId") long userId, @Param("positionId") long positionId);
	
	@Update("update tbl_oauth_user_position set position_id =#{positionId} where user_id=#{userId}")
	void updateUserPosition(@Param("userId") long userId, @Param("positionId") long positionId);
	
	@Delete("delete from tbl_oauth_user_position where user_id=#{userId} and organization_id=#{organizationId}")
	void deleteUserPosition(@Param("userId") long userId, @Param("organizationId") String organizationId);
	
	@Delete("delete from tbl_oauth_user_role where user_id=#{userId}")
	void deleteUserRole(@Param("userId") long userId);
	
	@Delete("delete from tbl_oauth_user where id=#{userId}")
	void deleteUser(@Param("userId") long userId);
	
	@Update("update tbl_oauth_user set password =#{newPassword} where id=#{userId}")
	void updateUserPassword(@Param("userId") String userId, @Param("newPassword") String newPassword);

	@Select("select * from (select a.*, unit_kerja_id, d.position_id, d.name_position position_name from tbl_oauth_user a inner join tbl_unit_kerja_user b on a.id=b.user_id inner join tbl_oauth_user_position c on a.id=c.user_id inner join tbl_position d on c.position_id=d.position_id) t where ${clause}")
	List<OauthUser> findUserUnitKerja(@Param("clause") String clause);
	
	@Select("select * from (select a.*, c.position_id, c.name_position position_name from tbl_oauth_user a inner join tbl_oauth_user_position b on a.id=b.user_id inner join tbl_position c on b.position_id=c.position_id) t where ${clause}")
	List<OauthUser> findUserPosition(@Param("clause") String clause);
	
	@Delete("delete from tbl_oauth_user_position where user_id=(select id from tbl_oauth_user where username =#{nip}) and organization_id=#{organizationId}")
	void deleteUserPositionByNIP(@Param("nip") String nip, @Param("organizationId") String organizationId);
	
	@Insert("insert into tbl_oauth_user_position select id user_id, #{positionId} position_id, #{organizationId} organization_id  from tbl_oauth_user where username =#{nip}")
	void insertUserPositionByNIP(@Param("nip") String nip, @Param("positionId") long positionId, @Param("organizationId") String organizationId);
	
	// Cek keadaan is
	@Select("select if(count(*)>0,1,0) from tbl_pegawai where nip_pegawai=#{nip}")
	boolean isNipPegawaiExist(@Param("nip") String nip);
	
	@Insert("insert into tbl_oauth_user_device (device_id, user_id, platform_type, apns_token, fcm_token, last_updated) values (#{deviceId}, #{userId}, #{platformType}, #{apnsToken}, #{fcmToken}, NOW()) ON DUPLICATE KEY UPDATE user_id=#{userId}, apns_token=#{apnsToken}, fcm_token=#{fcmToken}")
	void updateUserDevice(UserDevice mobileDevice);
}
