package id.co.lhk.wfh.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {

	@Value("${security.oauth2.resource.id}")
    private String resourceId;
    
	@Value("${security.oauth2.client.id}")
    private String clientId;
	
	@Value("${security.oauth2.client.secret}")
    private String clientSecret;
	
	@Value("${app.url.sso}")
    private String ssoEndpointUrl;
	
	@Value("${security.oauth2.token.key}")
    private String tokenKey;


    // To allow the rResourceServerConfigurerAdapter to understand the token,
    // it must share the same characteristics with AuthorizationServerConfigurerAdapter.
    // So, we must wire it up the beans in the ResourceServerSecurityConfigurer.
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
                .resourceId(resourceId)
                .tokenServices(remoteTokenServices())
                ;
    }
    
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
    	 http.cors().and()
//    	 	.anonymous().disable()
//         .csrf().disable()
         .authorizeRequests()
         .antMatchers("/", "/favicon.ico", "/images/**", "/css/**", "/dist/**","/img/**","/fonts/**","/js/**","/login**", "/error**", "/rest/uploadFile/**", "/rest/export/**", "/export/**", "/rest/export-to-mail/**", "/page**", "/page/*", "/page/**", "/files/**", "/public/**", "/lhp/**", "/simpeg/*", "/ref/*", "/ref/**", "/simpeg/ref/*", "/ticket/save", "/ticket/track", "/informasi/list", "/informasi/update/*", "/**/download").permitAll()
         .anyRequest().authenticated()
         ;
    }
    
    @Autowired
	public UserDetailsService userDetailsService;

    @Bean
    public UserAuthenticationConverter userAuthenticationConverter () {
        DefaultUserAuthenticationConverter duac 
            = new DefaultUserAuthenticationConverter();
        duac.setUserDetailsService(userDetailsService);
        return duac;
    }

    @Bean
    public AccessTokenConverter accessTokenConverter() {
        DefaultAccessTokenConverter datc 
            = new DefaultAccessTokenConverter();
        datc.setUserTokenConverter(userAuthenticationConverter());
        return datc;
    }

    @Bean
    public JwtAccessTokenConverter JwtTokenConverter() {
    		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(tokenKey);
        converter.setAccessTokenConverter(accessTokenConverter());
        return converter;
        
    }
    
    

    @Bean
    public ResourceServerTokenServices remoteTokenServices() {
    	RemoteTokenServices tokenService =  new RemoteTokenServices();
    		tokenService.setClientId(clientId);
	   tokenService.setClientSecret(clientSecret);
	   tokenService.setAccessTokenConverter(JwtTokenConverter());
	   tokenService.setCheckTokenEndpointUrl(ssoEndpointUrl + "/oauth/check_token");
        return tokenService;
    }

}
