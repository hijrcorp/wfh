package id.co.lhk.wfh.ref;

public enum AccountLoginInfo {
	ACCOUNT(0),SOURCE(1);

	private int id;

	private AccountLoginInfo(final int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}

}
