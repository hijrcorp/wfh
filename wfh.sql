# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: itjenlhkdb
# Generation Time: 2020-03-29 02:46:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_user_detil
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user_detil`;

CREATE TABLE `tbl_user_detil` (
  `id_user_detil` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id_user_detil` bigint(20) DEFAULT NULL,
  `id_jabatan_user_detil` int(11) DEFAULT NULL,
  `alamat_rumah_user_detil` text,
  `riwayat_penyakit_user_detil` text,
  PRIMARY KEY (`id_user_detil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_user_detil` WRITE;
/*!40000 ALTER TABLE `tbl_user_detil` DISABLE KEYS */;

INSERT INTO `tbl_user_detil` (`id_user_detil`, `user_id_user_detil`, `id_jabatan_user_detil`, `alamat_rumah_user_detil`, `riwayat_penyakit_user_detil`)
VALUES
	(1,1000012,4,NULL,NULL);

/*!40000 ALTER TABLE `tbl_user_detil` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wfh_jabatan
# ------------------------------------------------------------

DROP VIEW IF EXISTS `wfh_jabatan`;

CREATE TABLE `wfh_jabatan` (
   `id_jabatan` INT(11) UNSIGNED NOT NULL,
   `nama_jabatan` VARCHAR(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM;



# Dump of table wfh_laporan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wfh_laporan`;

CREATE TABLE `wfh_laporan` (
  `id_laporan` varchar(50) NOT NULL DEFAULT '',
  `tanggal_laporan` date DEFAULT NULL,
  `user_id_laporan` varchar(50) DEFAULT NULL,
  `jumlah_tusi_laporan` int(11) DEFAULT NULL,
  `jumlah_tambahan_laporan` int(11) DEFAULT NULL,
  `status_sehat_laporan` varchar(10) DEFAULT NULL,
  `kondisi_sakit_laporan` varchar(50) DEFAULT NULL,
  `keterangan_sakit_laporan` varchar(500) DEFAULT NULL,
  `added_time_laporan` datetime DEFAULT NULL,
  `added_user_laporan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_laporan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wfh_laporan` WRITE;
/*!40000 ALTER TABLE `wfh_laporan` DISABLE KEYS */;

INSERT INTO `wfh_laporan` (`id_laporan`, `tanggal_laporan`, `user_id_laporan`, `jumlah_tusi_laporan`, `jumlah_tambahan_laporan`, `status_sehat_laporan`, `kondisi_sakit_laporan`, `keterangan_sakit_laporan`, `added_time_laporan`, `added_user_laporan`)
VALUES
	('001','2020-03-26','1000005',1,1,'YES',NULL,NULL,'2020-03-28 15:50:52','198507092010121003'),
	('002','2020-03-26','1000012',1,0,'NO','BATUK','berdahak','2020-03-28 16:34:43','198703172010121007'),
	('003','2020-03-26','1000017',0,0,'NO','DEMAM','suhu 37.5','2020-03-28 16:40:20','197410301999032001'),
	('004','2020-03-26','1000011',1,0,'YES',NULL,NULL,'2020-03-28 16:43:08','198701102009121006'),
	('005','2020-03-27','1000017',1,0,'YES',NULL,NULL,'2020-03-28 16:40:20','197410301999032001'),
	('006','2020-03-27','1000011',1,1,'YES',NULL,NULL,'2020-03-28 16:43:08','198701102009121006');

/*!40000 ALTER TABLE `wfh_laporan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wfh_pekerjaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wfh_pekerjaan`;

CREATE TABLE `wfh_pekerjaan` (
  `id_pekerjaan` varchar(50) NOT NULL DEFAULT '',
  `laporan_id_pekerjaan` varchar(50) DEFAULT NULL,
  `status_tusi_pekerjaan` varchar(10) DEFAULT NULL,
  `nama_pekerjaan` varchar(200) DEFAULT NULL,
  `uraian_pekerjaan` text,
  `output_pekerjaan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_pekerjaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wfh_pekerjaan` WRITE;
/*!40000 ALTER TABLE `wfh_pekerjaan` DISABLE KEYS */;

INSERT INTO `wfh_pekerjaan` (`id_pekerjaan`, `laporan_id_pekerjaan`, `status_tusi_pekerjaan`, `nama_pekerjaan`, `uraian_pekerjaan`, `output_pekerjaan`)
VALUES
	('001','001','YES','ngoding','aplikasi WFH','CRUD pekerjaan'),
	('002','001','NO','import data','user pegawai','200 item data'),
	('003','002','YES','ngolah data','menganalisa data itjen','laporan hasil analisa'),
	('004','004','YES','meriksa laporan','mengoreksi hasil laporan','5 laporan terkoreksi'),
	('005','005','YES','input data','spip online','data terinput'),
	('006','006','YES','meriksa laporan','mengoreksi hasil laporan','3 laporan terkoreksi'),
	('007','006','NO','input data','alhp','3 lhp');

/*!40000 ALTER TABLE `wfh_pekerjaan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wfh_unit_kerja
# ------------------------------------------------------------

DROP VIEW IF EXISTS `wfh_unit_kerja`;

CREATE TABLE `wfh_unit_kerja` (
   `id_unit_kerja` INT(11) NOT NULL,
   `nama_unit_kerja` VARCHAR(200) NULL DEFAULT NULL
) ENGINE=MyISAM;





# Replace placeholder table for wfh_jabatan with correct view syntax
# ------------------------------------------------------------

DROP TABLE `wfh_jabatan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wfh_jabatan`
AS SELECT
   `tbl_jabatan`.`id_jabatan` AS `id_jabatan`,
   `tbl_jabatan`.`nama_jabatan` AS `nama_jabatan`
FROM `tbl_jabatan` where (`tbl_jabatan`.`group_jabatan` = 'ITJEN');


# Replace placeholder table for wfh_unit_kerja with correct view syntax
# ------------------------------------------------------------

DROP TABLE `wfh_unit_kerja`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wfh_unit_kerja`
AS SELECT
   `tbl_unit_kerja`.`unit_kerja_id` AS `id_unit_kerja`,
   `tbl_unit_kerja`.`nama_unit_kerja` AS `nama_unit_kerja`
FROM `tbl_unit_kerja`;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
